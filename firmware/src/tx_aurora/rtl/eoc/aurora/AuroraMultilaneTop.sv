
`ifndef AURORA_MULTILANE_TOP__SV   // include guard
`define AURORA_MULTILANE_TOP__SV

`timescale 1ns / 1ps

`include "rtl/eoc/aurora/AuroraDefines.sv"
`include "rtl/eoc/aurora/AuroraGearbox66to20.sv"
`include "rtl/eoc/aurora/AuroraMultilaneDataFrameFSM.sv"
`include "rtl/eoc/aurora/AuroraMultilaneNewDataFSM.sv"
`include "rtl/eoc/aurora/AuroraMultilanePriorityMux.sv"
`include "rtl/eoc/aurora/AuroraMultilaneUserKFSM.sv"
`include "rtl/eoc/aurora/AuroraPeriodicFSM.sv"
`include "rtl/eoc/aurora/AuroraPrbsGenerator.sv"
`include "rtl/eoc/aurora/AuroraScrambler.sv"

module AuroraMultilaneTop
#(
    parameter IW_WIDTH = 4,
    parameter CCW_WIDTH = 4,
    parameter CCS_WIDTH = 4,
    parameter CBW_WIDTH = 20,
    parameter CBS_WIDTH = 4
) (
    input wire [3:0] ActiveLanes,
    input wire EnablePRBS,

    input wire [7:0][31:0] DataEOC,
    input wire [7:0] DataMask,
    input wire DataEOC_empty,
    input wire DataEOC_EOF,
    output logic DataEOC_read,

    input wire [3:0][63:0] Monitor,
    input wire Monitor_empty,
    output logic Monitor_read,

    output logic [3:0][19:0] ToSerializer20,

    input wire SerializerLock,

    input wire [IW_WIDTH-1:0] InitWait,
    input wire [CCW_WIDTH-1:0] CCWait,
    input wire [CBW_WIDTH-1:0] CBWait,
    input wire [CBS_WIDTH-1:0] CBSend,
    input wire [CCS_WIDTH-1:0] CCSend,

    input wire Clk,
    input wire Rst_b

    ) ;

    logic [19:0]             prbs20;
    logic [3:0][19:0]        auroraToSerializer20;

    enum                     {INIT_LANE, SEND_IDLE, SEND_CB, LANE_READY} lane_fsm_state;//synopsys keep_signal_name "lane_fsm_state"

    logic                    lane_ready;


    logic [`NPRIORITIES-1:0] block_to_send;
    logic [`NPRIORITIES-1:0] block_sent;
    logic [`NPRIORITIES-1:0] block_sending;

    logic [3:0][63:0]        data_word;
    logic [3:0][63:0]        data_to_send;
    logic [3:0][3:0]         data_bytes;
    logic [3:0]              data_fill;
    logic [3:0]              data_busy;

    logic [3:0][63:0]        monitor_userk;

    logic                    init_cb_send;
    logic                    periodic_cb_send;

    // FSMs send signals ORs
    logic [4:0] periodic_cb_send_cooldown;
    assign block_to_send[`CHANNEL_BONDING] = init_cb_send | (periodic_cb_send & !periodic_cb_send_cooldown);
    
        // Defining these signasl outside to make it visible for  SVAs (A.R)
        logic [IW_WIDTH-1:0] init_counter;
        logic [2:0]      init_block_counter;
    
    //synopsys sync_set_reset "Rst_b"
        
    // lane initialization FSM
    always_ff @(posedge Clk) begin

        if (Rst_b == 1'b0) begin
            init_counter <= '0;
            init_block_counter <= '0;
            lane_fsm_state <= INIT_LANE;
            periodic_cb_send_cooldown <= 5'b00000;
            init_cb_send <= 1'b0;
            block_to_send[`IDLE] <= 1'b0;
            block_to_send[`NOT_READY] <= 1'b0;
            block_to_send[`NATIVE_FLOW_CONTROL] <= 1'b0;
            block_to_send[`USER_FLOW_CONTROL] <= 1'b0;
        end else begin
            block_to_send[`NOT_READY] <= 1'b0;
            block_to_send[`NATIVE_FLOW_CONTROL] <= 1'b0;
            block_to_send[`USER_FLOW_CONTROL] <= 1'b0;
            case(lane_fsm_state)
            INIT_LANE : begin
                if (init_block_counter == '0) begin
                    init_cb_send <= 1'b1;
                    if (block_sent[`CHANNEL_BONDING] && SerializerLock) begin
                        if (init_counter < InitWait) begin
//                            init_counter <= init_counter + {{(IW_WIDTH-1){1'b0}},1'b1};
                            init_counter <= IW_WIDTH'(init_counter + 1);
                           init_block_counter <= init_block_counter + 3'b001;
                        end else begin
                            init_cb_send <= 1'b0;
                            lane_fsm_state <= LANE_READY;
                        end
                    end
                end else begin
                    init_cb_send <= 1'b0;
                    block_to_send[`IDLE] <= 1'b1;
                    if (block_sent[`IDLE] && SerializerLock) begin
                        init_block_counter <= init_block_counter + 3'b001;
                    end
                end
            end
            LANE_READY : begin
//        init_cb_send <= 1'b0;
                block_to_send[`IDLE] <= 1'b0;
                if (periodic_cb_send | periodic_cb_send_cooldown)
                  periodic_cb_send_cooldown <= periodic_cb_send_cooldown + 5'b00001;
                    if (~SerializerLock) begin
                        lane_fsm_state <= INIT_LANE;
                    end
                end
            endcase
        end // else: !if(Rst)
    end // always_ff @ (posedge Clk or posedge Rst)

    assign lane_ready = (lane_fsm_state == LANE_READY);

    // Clock Compensation FSM
    AuroraPeriodicFSM #(CCW_WIDTH, CCS_WIDTH) cc_fsm(.LaneReady(lane_ready),
                .SendBlock(block_to_send[`CLOCK_COMPENSATION]),
                .BlockSent(block_sent[`CLOCK_COMPENSATION]),
                .WaitToSend(CCWait),
                .BlocksToSend(CCSend),
                .Clk(Clk),
                .Rst_b(Rst_b));

    // Channel Bonding FSM
    AuroraPeriodicFSM #(CBW_WIDTH, CBS_WIDTH) cb_fsm(.LaneReady(lane_ready),
                .SendBlock(periodic_cb_send),
                .BlockSent(block_sent[`CHANNEL_BONDING]),
                .WaitToSend(CBWait),
                .BlocksToSend(CBSend),
                .Clk(Clk),
                .Rst_b(Rst_b));

    // data FSM

    AuroraMultilaneNewDataFSM eoc_fsm (.LaneReady(lane_ready),
            .FIFO_Empty(DataEOC_empty),
            .FIFO_Data(DataEOC),
            .FIFO_DataMask(DataMask),
            .EndOfFrame(DataEOC_EOF),
            .ActiveLanes(ActiveLanes),
            .BlockSent(block_sent[`USER_DATA]),
            .Clk(Clk),
            .Rst_b(Rst_b),
            .FIFO_Read(DataEOC_read),
            .DataToSend(data_to_send),
            .SendBlock(block_to_send[`USER_DATA]),
            .BytesToSend(data_bytes));

    AuroraMultilaneUserKFSM userk_fsm(.LaneReady(lane_ready),
            .FIFO_Empty(Monitor_empty),
            .FIFO_UserK(Monitor),
            .BlockSent(block_sent[`USER_KBLOCKS]),
            .Clk(Clk),
            .Rst_b(Rst_b),
            .FIFO_Read(Monitor_read),
            .UserKToSend(monitor_userk),
            .SendBlock(block_to_send[`USER_KBLOCKS]));


    logic [3:0][65:0] aurora_block;
    logic [3:0][65:0] aurora_scrambled;

    logic [3:0][`NPRIORITIES-1:0] local_block_sent;

    genvar prio;
    generate
        for (prio = 0; prio < `NPRIORITIES; prio = prio+1) begin
            assign block_sent[prio] = local_block_sent[0][prio] |
                    (
                    local_block_sent[1][prio] |
                    local_block_sent[2][prio] |
                    local_block_sent[3][prio] );
        end
    endgenerate         

    genvar i;
    generate
        for (i = 0; i < 4; i=i+1) begin: multi_lanes
            logic   aurora_ack, aurora_ack20;


            AuroraMultilanePriorityMux priority_mux (.Data(data_to_send[i]),
                    .DataBytes(data_bytes[i]),
                    .UserK(monitor_userk[i]),
                    .ToSend(block_to_send),
                    .AuroraAck(aurora_ack),
                    .Clk(Clk),
                    .Rst_b(Rst_b),
                    .Sent(local_block_sent[i]),
                    //.Sending(block_sending),
                    .AuroraBlock(aurora_block[i]));

            AuroraScrambler scrambler (.DataIn(aurora_block[i][63:0]),
                    .SyncBits(aurora_block[i][65:64]),
                    .Ena(aurora_ack),
                    .Clk(Clk),
                    .Rst_b(Rst_b),
                    .DataOut(aurora_scrambled[i]));



            AuroraGearbox66to20 gearbox20 (.Rst_b(Rst_b),
                    .Clk(Clk),
                    .Data66(aurora_scrambled[i]),
                    .Data20(auroraToSerializer20[i]),
                    .DataNext(aurora_ack20));

            assign aurora_ack = aurora_ack20;

            assign ToSerializer20[i] = EnablePRBS ? prbs20 : auroraToSerializer20[i];
        end: multi_lanes
    endgenerate

   logic 	    reset_prbs;
   assign reset_prbs = Rst_b & ~EnablePRBS;
   
    //PRBS15Generate20 prbsgen20 (.PRBS_Out(prbs20), .Clk(Clk), .Rst(Rst));
    AuroraPrbsGenerator #(.WIDTH(20), .TAP1(15), .TAP2(14) ) prbsgen20 (.PRBS_Out(prbs20), .Clk(Clk), .Rst_b(reset_prbs));

//---------------------------------//Assertions and properties to test//---------------------------------//---------------------------------
    //synopsys translate_off
    default clocking @(posedge Clk); endclocking

    property S_DEFAULT  ;
    disable iff (Rst_b)     (Rst_b == 1'b0)                                    |-> ##1 (lane_fsm_state == INIT_LANE) ;  
    endproperty 
    
    property S_DLYREADY  ;
    disable iff (Rst_b)     (lane_fsm_state == INIT_LANE) ##0 (init_counter < InitWait) |-> (lane_fsm_state == INIT_LANE) ;  
    endproperty 
     
    property S_LANE_READY ;
    disable iff (Rst_b == 1'b0) (SerializerLock == 1'b1)  ##0 (lane_fsm_state == LANE_READY)  |-> (lane_fsm_state == LANE_READY) ;  
    endproperty  
  
    property S_LOST_SERLOCK ;
    disable iff (Rst_b == 1'b0) $fell(SerializerLock)     ##0 (lane_fsm_state == LANE_READY)  |->  ##1 (lane_fsm_state == INIT_LANE) ;  
    endproperty  

  
//---------------------------------//---------------------------------//---------------------------------//---------------------------------
 `ifdef SVA_EN
   generate      
    begin : SVA_APeriodc_FSM     
     ERR_S_DEFAULT_STATE    : assert property (S_DEFAULT); // Reset msut get the state machine back to default "INIT_LANE" in here      
     ERR_S_DELAY_READY      : assert property (S_DLYREADY); // Wait till init counter getts over delay time     
     ERR_S_LANE_READY       : assert property (S_LANE_READY); // If seriliser lock is locked stay in ready ##0 
     ERR_S_SERIAL_LOCK      : assert property (S_LOST_SERLOCK); // If seriliser lock is lost move to default  
//---------------------------------//---------------------------------//---------------------------------//---------------------------------
    end
   endgenerate
 `endif
//synopsys translate_on
   
endmodule : AuroraMultilaneTop

`endif   // AURORA_MULTILANE_TOP

