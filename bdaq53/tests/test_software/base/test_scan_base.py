#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import copy
import logging
import mock
import os
import unittest
import shutil
import time

import pytest
import yaml
import tables as tb
import numpy as np

import bdaq53
from bdaq53.system import scan_base
from bdaq53.tests import utils
from bdaq53.tests import bdaq_mock
from bdaq53.analysis.analysis import Analysis

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config_file = os.path.abspath(os.path.join(bdaq53_path, "testbench.yaml"))
data_folder = os.path.abspath(os.path.join(bdaq53_path, "..", "data", "fixtures"))

scan_configuration = {"start_column": 0, "stop_column": 400, "start_row": 0, "stop_row": 192, "VCAL_MED": 500, "VCAL_HIGH": 1300}


def create_test_scan(parallel=False):
    """ Must be in method to allow delayed import of ScanBase """
    from bdaq53.system.scan_base import ScanBase

    class TestScan(ScanBase):
        scan_id = "test_scan"

        is_parallel_scan = parallel

        if is_parallel_scan:
            scan_id += "_parallel"

        def _configure(self, **_):
            pass

        def _scan(self, **_):
            for i in range(3):
                with self.readout(scan_param_id=i, timeout=0.1, par_1=i, par_2=i ** 2):
                    pass

        def _analyze(self):
            # Create empty analyzed data file for configuration loading checks
            with Analysis(raw_data_file=self.output_filename + ".h5") as a:
                a.analyze_data()

    return TestScan


class TestScanBase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestScanBase, cls).setUpClass()

        # Catch digital scan output to check for errors reported
        scan_logger = logging.getLogger("DigitalScan")
        cls.scan_log_handler = utils.MockLoggingHandler(level="DEBUG")
        scan_logger.addHandler(cls.scan_log_handler)
        cls.scan_log_messages = cls.scan_log_handler.messages
        analysis_logger = logging.getLogger("Analysis")
        cls.analysis_log_handler = utils.MockLoggingHandler(level="DEBUG")
        analysis_logger.addHandler(cls.analysis_log_handler)
        cls.analysis_log_messages = cls.analysis_log_handler.messages
        plotting_logger = logging.getLogger("Plotting")
        cls.plotting_log_handler = utils.MockLoggingHandler(level="DEBUG")
        plotting_logger.addHandler(cls.plotting_log_handler)
        cls.plotting_log_messages = cls.plotting_log_handler.messages

        # Use hardware mocks to be able to test without hardware
        cls.bhm = bdaq_mock.BdaqMock(n_chips=4)
        # Speed up testing time drastically, by not calling mask shifting
        cls.bhm.patch_function("bdaq53.chips.rd53a.RD53AMaskObject.update")
        cls.bhm.patch_function("bdaq53.chips.ITkPixV1.ITkPixV1MaskObject.update")
        cls.bhm.start()

        cls.TestScan = create_test_scan(parallel=False)
        cls.TestScanParallel = create_test_scan(parallel=True)

        # Load standard bench config to change in test cases
        with open(bench_config_file) as f:
            cls.bench_config = yaml.full_load(f)

    @classmethod
    def tearDownClass(cls):
        cls.bhm.stop()

    @classmethod
    def tearDown(cls):
        # Reset messages after each test
        cls.scan_log_handler.reset()
        cls.analysis_log_handler.reset()
        cls.plotting_log_handler.reset()
        cls.bhm.reset()  # clear mock call storage to save RAM
        cls.bhm.patch_function("bdaq53.chips.rd53a.RD53AMaskObject.update")
        cls.bhm.patch_function("bdaq53.chips.ITkPixV1.ITkPixV1MaskObject.update")
        shutil.rmtree("output_data", ignore_errors=True)  # always delete output from previous scan
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024

    def check_scan_success(self):
        """ Check the log output if scan was successfull """
        if self.scan_log_messages["error"] or self.analysis_log_messages["error"] or self.plotting_log_messages["error"]:
            return False
        if "Scan finished" not in self.scan_log_messages["success"] or "All done!" not in self.scan_log_messages["success"]:
            return False
        if "Analyzing data..." not in self.analysis_log_messages["info"]:
            return False
        return True

    def test_scan_single_chip(self):
        from bdaq53.scans import scan_digital

        with scan_digital.DigitalScan(bench_config=self.bench_config) as scan:
            scan.start()
        self.assertTrue(self.check_scan_success())

    def test_scan_par_storing(self):
        """ Check that scan config has the scan parameter stored """

        def check_scan_pars(output_filename):
            # Check for info in raw data file config out
            with tb.open_file(output_filename + ".h5", "r") as in_file:
                self.assertTrue(np.array_equal(in_file.root.configuration_out.scan.scan_params[:]["scan_param_id"], np.array([0, 1, 2])))
                self.assertTrue(np.array_equal(in_file.root.configuration_out.scan.scan_params[:]["par_1"], np.array([0, 1, 2])))
                self.assertTrue(np.array_equal(in_file.root.configuration_out.scan.scan_params[:]["par_2"], np.array([0, 1, 4])))

        # Normal scan
        with self.TestScan(bench_config=self.bench_config) as scan:
            scan.start()
            check_scan_pars(scan.output_filename)

        # Parallel scan
        with self.TestScanParallel(bench_config=self.bench_config) as scan:
            scan.start()
            check_scan_pars(scan.output_filename)

        # Multiple chips
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config["modules"]["module_0"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_0"]["chip_0"])
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"
        bench_config["modules"]["module_0"]["chip_1"]["chip_id"] = 1
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx1"
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5501"

        with self.TestScan(bench_config=bench_config) as scan:
            scan.start()
            for _ in scan.iterate_chips():
                check_scan_pars(scan.output_filename)

    def test_scan_missing_configure(self):
        from bdaq53.scans import scan_digital

        with scan_digital.DigitalScan(bench_config=self.bench_config) as scan:
            with self.assertRaises(RuntimeError):
                scan.scan()

    def test_scan_quad_module(self):
        """ Test digital scan on one quad chip module """
        from bdaq53.scans import scan_digital

        # Add second chip to module
        bench_config = copy.deepcopy(self.bench_config)
        bench_config["modules"]["module_0"]["module_type"] = "common_quad"
        for i in range(1, 4):
            bench_config["modules"]["module_0"]["chip_%d" % i] = copy.deepcopy(bench_config["modules"]["module_0"]["chip_0"])
            bench_config["modules"]["module_0"]["chip_%d" % i]["chip_sn"] = "0x000%d" % (i + 1)
            bench_config["modules"]["module_0"]["chip_%d" % i]["chip_id"] = i
            bench_config["modules"]["module_0"]["chip_%d" % i]["receiver"] = "rx%d" % i
            bench_config["modules"]["module_0"]["chip_%d" % i]["send_data"] = "tcp://127.0.0.1:550%d" % i

        with scan_digital.DigitalScan(bench_config=bench_config) as scan:
            scan.start()
            self.assertEqual(scan.get_n_modules(), 1)
            self.assertTrue(len(scan.chips), 4)
        self.assertTrue(self.check_scan_success())

    def test_scan_four_modules(self):
        """ Test digital scan on four single chip modules """
        from bdaq53.scans import scan_digital

        # Add second module
        bench_config = copy.deepcopy(self.bench_config)
        for i in range(1, 4):
            bench_config["modules"]["module_%d" % i] = copy.deepcopy(bench_config["modules"]["module_0"])
            bench_config["modules"]["module_%d" % i]["chip_0"]["chip_sn"] = "0x000%d" % (i + 1)
            bench_config["modules"]["module_%d" % i]["chip_0"]["receiver"] = "rx%d" % i
            bench_config["modules"]["module_%d" % i]["chip_0"]["send_data"] = "tcp://127.0.0.1:550%d" % i

        with scan_digital.DigitalScan(bench_config=bench_config) as scan:
            scan.start()
            self.assertEqual(scan.get_n_modules(), 4)
            self.assertTrue(len(scan.chips), 4)
        self.assertTrue(self.check_scan_success())

    def test_scan_one_dc_modules(self):
        """ Test digital scan on a double chip modules """
        from bdaq53.scans import scan_digital

        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config["modules"]["module_0"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_0"]["chip_0"])
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"
        bench_config["modules"]["module_0"]["chip_1"]["chip_id"] = 1
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx1"
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5501"

        with scan_digital.DigitalScan(bench_config=bench_config) as scan:
            scan.start()
            self.assertEqual(scan.get_n_modules(), 1)
            self.assertTrue(len(scan.chips), 2)
        self.assertTrue(self.check_scan_success())

    def test_scan_two_dc_modules(self):
        """ Test digital scan on two double chip modules """
        from bdaq53.scans import scan_digital

        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config["modules"]["module_0"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_0"]["chip_0"])
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"
        bench_config["modules"]["module_0"]["chip_1"]["chip_id"] = 1
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx1"
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5501"
        # Add additional module
        bench_config["modules"]["module_1"] = copy.deepcopy(bench_config["modules"]["module_0"])
        bench_config["modules"]["module_1"]["chip_0"]["chip_sn"] = "0x0003"
        bench_config["modules"]["module_1"]["chip_1"]["chip_id"] = 2
        bench_config["modules"]["module_1"]["chip_0"]["receiver"] = "rx2"
        bench_config["modules"]["module_1"]["chip_0"]["send_data"] = "tcp://127.0.0.1:5502"
        # Add additional chip to module 1
        bench_config["modules"]["module_1"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_1"]["chip_0"])
        bench_config["modules"]["module_1"]["chip_1"]["chip_sn"] = "0x0004"
        bench_config["modules"]["module_1"]["chip_1"]["chip_id"] = 3
        bench_config["modules"]["module_1"]["chip_1"]["receiver"] = "rx3"
        bench_config["modules"]["module_1"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5503"

        with scan_digital.DigitalScan(bench_config=bench_config) as scan:
            scan.start()
            self.assertEqual(scan.get_n_modules(), 2)
            self.assertTrue(len(scan.chips), 4)
        self.assertTrue(self.check_scan_success())

    def test_scan_two_modules_sc_dc(self):
        """ Test digital scan on two modules, one single and one double chip """
        from bdaq53.scans import scan_digital

        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional module
        bench_config["modules"]["module_1"] = copy.deepcopy(bench_config["modules"]["module_0"])
        bench_config["modules"]["module_1"]["chip_0"]["chip_sn"] = "0x0002"
        bench_config["modules"]["module_1"]["chip_0"]["chip_id"] = 1
        bench_config["modules"]["module_1"]["chip_0"]["receiver"] = "rx1"
        bench_config["modules"]["module_1"]["chip_0"]["send_data"] = "tcp://127.0.0.1:5501"
        # Add additional chip to module 1
        bench_config["modules"]["module_1"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_1"]["chip_0"])
        bench_config["modules"]["module_1"]["chip_1"]["chip_sn"] = "0x0003"
        bench_config["modules"]["module_1"]["chip_1"]["chip_id"] = 2
        bench_config["modules"]["module_1"]["chip_1"]["receiver"] = "rx2"
        bench_config["modules"]["module_1"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5502"

        with scan_digital.DigitalScan(bench_config=bench_config) as scan:
            scan.start()
            self.assertEqual(scan.get_n_modules(), 2)
            self.assertTrue(len(scan.chips), 3)
        self.assertTrue(self.check_scan_success())

    @unittest.skip("Not implemented yet")
    def test_scan_wrong_module_cfg(self):
        """ Test exceptions for invalid module configuration """
        from bdaq53.scans import scan_digital

        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config["modules"]["module_0"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_0"]["chip_0"])
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"  # same serial number as first chip will raise KeyError
        bench_config["modules"]["module_0"]["chip_1"]["chip_id"] = 1
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx1"
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5501"

        # Same chip sn (should raise in future)
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0001"  # same serial number as first chip will raise KeyError
        with self.assertRaises(KeyError):
            with scan_digital.DigitalScan(bench_config=bench_config):
                pass
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"  # reset

        # Same send data socket  (should raise in future)
        bench_config["modules"]["module_0"]["chip_1"]["chip_id"] = 1
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx1"
        # ValueError: Two or more chips have the same data sending address/port!
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5500"
        with self.assertRaises(ValueError):
            with scan_digital.DigitalScan(bench_config=bench_config):
                pass
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5501"  # reset

        # Same receiver (should raise in future)
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx0"
        with scan_digital.DigitalScan(bench_config=bench_config):
            pass

    def test_chip_registers_loading(self):
        """ Check that register settings from previous scans are loaded """
        from bdaq53.scans import scan_digital

        # Define specific chip config in the scan config
        scan_config = copy.deepcopy(scan_digital.scan_configuration)
        scan_config["chip"] = {"registers": {"VCAL_HIGH": 1111}}  # std. cfg. is 500
        # Do a digital scan
        with self.TestScan(bench_config=self.bench_config, scan_config=scan_config) as scan:
            self.assertTrue(scan.chip.registers["VCAL_HIGH"]["value"] == scan_config["chip"]["registers"]["VCAL_HIGH"])
            scan.start()
            output_filename = scan.output_filename + ".h5"

        # Check that changed chip config is stored in config output
        with tb.open_file(output_filename, "r") as in_file:
            chip_registers_in = scan_base.fill_dict_from_conf_table(in_file.root.configuration_in.chip.registers)
            chip_registers_out = scan_base.fill_dict_from_conf_table(in_file.root.configuration_out.chip.registers)
            self.assertTrue(chip_registers_in["VCAL_HIGH"] == scan_config["chip"]["registers"]["VCAL_HIGH"])
            self.assertTrue(chip_registers_out["VCAL_HIGH"] == scan_config["chip"]["registers"]["VCAL_HIGH"])

        # Check that a preceeding scan loads the previous scan cfg
        with self.TestScan(bench_config=self.bench_config) as scan:
            self.assertTrue(scan.chip.registers["VCAL_HIGH"]["value"] == scan_config["chip"]["registers"]["VCAL_HIGH"])
            scan.start()
            output_filename = scan.output_filename + ".h5"

        # Check that changed chip config is stored in config output
        with tb.open_file(output_filename, "r") as in_file:
            chip_registers_in = scan_base.fill_dict_from_conf_table(in_file.root.configuration_in.chip.registers)
            chip_registers_out = scan_base.fill_dict_from_conf_table(in_file.root.configuration_out.chip.registers)
            self.assertTrue(chip_registers_in["VCAL_HIGH"] == scan_config["chip"]["registers"]["VCAL_HIGH"])
            self.assertTrue(chip_registers_out["VCAL_HIGH"] == scan_config["chip"]["registers"]["VCAL_HIGH"])

    def test_config_file_loading(self):
        """ Check that only available h5 files from previous runs are used to load the configuration

            Issue #421
        """

        # Catch log output of scan
        scan_logger = logging.getLogger("TestScan")
        scan_log_handler = utils.MockLoggingHandler(level="DEBUG")
        scan_logger.addHandler(scan_log_handler)
        scan_log_messages = scan_log_handler.messages

        # Do a scan, no previous scan exists --> use std. chip config from yaml
        with self.TestScan(bench_config=self.bench_config) as scan:
            scan.start()
            config_h5 = scan.output_filename + "_interpreted.h5"
        self.assertTrue(any("No explicit configuration supplied for chip" in msg for msg in scan_log_messages["warning"]))

        # Check that previous interpreted file is used
        scan_log_handler.reset()
        with self.TestScan(bench_config=self.bench_config) as scan:
            scan.start()
            output_filename = scan.output_filename
        self.assertFalse(scan_log_messages["warning"])  # no warning since previous config is used
        self.assertTrue(any("Loading chip configuration for chip" in msg for msg in scan_log_messages["info"]))  # previous h5 file is used
        self.assertTrue(any(config_h5 in msg for msg in scan_log_messages["info"]))  # interpreted file from previous scan is used

        # Check that previous raw data file is used, if interpreted file handle is not available
        scan_log_handler.reset()
        # Open in append mode to create exception when it is opene in read only mode
        h5_file = tb.open_file(output_filename + "_interpreted.h5", "r+")
        config_h5 = output_filename + ".h5"
        with self.TestScan(bench_config=self.bench_config) as scan:
            scan.start()
        self.assertFalse(scan_log_messages["warning"])  # no warning since previous config is used
        self.assertTrue(any("Loading chip configuration for chip" in msg for msg in scan_log_messages["info"]))  # previous h5 file is used
        self.assertTrue(any("Use chip configuration at configuration_out node" in msg for msg in scan_log_messages["info"]))  # previous h5 file is used
        self.assertTrue(any(config_h5 in msg for msg in scan_log_messages["info"]))  # interpreted file from previous scan is used
        h5_file.close()

        # Change folder and check that new config is created
        scan_log_handler.reset()
        bench_config = copy.deepcopy(self.bench_config)
        bench_config["modules"]["SCC99"] = bench_config["modules"].pop("module_0")
        # Do a scan, no previous scan exists --> use std. chip config from yaml
        with self.TestScan(bench_config=bench_config) as scan:
            scan.start()
            output_filename = scan.output_filename
        self.assertTrue(any("No explicit configuration supplied for chip" in msg for msg in scan_log_messages["warning"]))

        # Delete configuration_out node and check that configuration_in is used, issue #465
        scan_log_handler.reset()
        with tb.open_file(output_filename + ".h5", "r+") as in_file:
            in_file.remove_node("/configuration_out", recursive=True)
        utils.try_remove(output_filename + "_interpreted.h5")
        time.sleep(0.1)

        with self.TestScan(bench_config=bench_config) as scan:
            scan.start()
        self.assertFalse(scan_log_messages["warning"])  # no warning since previous config is used
        self.assertTrue(any("Loading chip configuration for chip" in msg for msg in scan_log_messages["info"]))  # previous h5 file is used
        self.assertTrue(any("Use chip configuration at configuration_in node" in msg for msg in scan_log_messages["info"]))  # previous h5 file is used

    def test_chip_config_from_testbench(self):
        """ Check that chip settings are overwritten by test bench definitions """

        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config["modules"]["module_0"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_0"]["chip_0"])
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"  # same serial number as first chip will raise KeyError
        bench_config["modules"]["module_0"]["chip_1"]["chip_id"] = 1
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx1"
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5501"
        # Custom overwrites
        bench_config["modules"]["module_0"]["chip_0"]["registers"] = {"VCAL_HIGH": 1000}  # std. cfg. is 500
        bench_config["modules"]["module_0"]["chip_1"]["registers"] = {"VCAL_HIGH": 1001}  # std. cfg. is 500
        bench_config["modules"]["module_0"]["chip_0"]["trim"] = {"VREF_A_TRIM": 0}
        bench_config["modules"]["module_0"]["chip_1"]["trim"] = {"VREF_A_TRIM": 1}
        bench_config["modules"]["module_0"]["chip_0"]["calibration"] = {"ADC_a": 0, "e_conversion:": {"slope": 0}}
        bench_config["modules"]["module_0"]["chip_1"]["calibration"] = {"ADC_a": 1, "e_conversion:": {"slope": 1}}
        bench_config["modules"]["module_0"]["chip_0"]["disable_pixel"] = ["0,0", "10, 20"]  # std. cfg. is 500
        bench_config["modules"]["module_0"]["chip_1"]["disable_pixel"] = ["1, 1", "20, 21", "7:13:5, 100:200"]  # std. cfg. is 500
        with self.TestScan(bench_config=bench_config) as scan:
            for i, _ in enumerate(scan.iterate_chips()):
                self.assertTrue(scan.chip.registers["VCAL_HIGH"]["value"] == 1000 + i)
                self.assertTrue(scan.chip.configuration["trim"]["VREF_A_TRIM"] == i)
                self.assertTrue(scan.chip.configuration["calibration"]["ADC_a"] == i)
                self.assertTrue(scan.chip.configuration["calibration"]["e_conversion:"]["slope"] == i)
                m = np.ones_like(scan.chip.masks.disable_mask)
                if i == 0:
                    m[0, 0] = 0
                    m[10, 20] = 0
                elif i == 1:
                    m[1, 1] = 0
                    m[20, 21] = 0
                    m[7:13:5, 100:200] = 0
                self.assertTrue(np.all(scan.chip.masks.disable_mask == m))

    def test_chip_masks_loading(self):
        """ Check that masks settings are stored but not loaded from previous scans except its it the tdac mask or
        th virtual use_pixel/disable pixel mask """
        from bdaq53.scans import scan_digital

        # New scan that changes TDAC mask
        class TestScan(scan_digital.DigitalScan):
            def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
                super(TestScan, self)._configure(start_column, stop_column, start_row, stop_row)
                self.chip.masks["tdac"] = np.zeros_like(self.chip.masks["tdac"])
                self.chip.masks.disable_mask = np.zeros_like(self.chip.masks.disable_mask)
                self.chip.masks.disable_mask[0, 0] = 1

            def _analyze(self):
                super(TestScan, self)._analyze()
                self.chip.masks["enable"] = np.zeros_like(self.chip.masks["enable"])
                self.chip.masks["enable"][0, 0] = 1
                self.chip.masks["hitbus"][0, 0] = 1
                self.chip.masks["tdac"][:] = 10

        # Do a digital scan
        with TestScan(bench_config=self.bench_config) as scan:
            scan.configure()
            self.assertTrue(np.all(scan.chip.masks["tdac"] == 0))  # check that new TDAC mask from config step is set
            self.assertTrue(np.all(scan.chip.masks["enable"] == 1))
            self.assertTrue(np.all(scan.chip.masks["hitbus"] == 0))
            self.assertTrue(np.all(scan.chip.masks.disable_mask[1:, 1:] == 0))
            self.assertTrue(scan.chip.masks.disable_mask[0, 0] == 1)
            scan.scan()
            scan.analyze()
            self.assertTrue(np.count_nonzero(scan.chip.masks["enable"]) == 1)
            self.assertTrue(np.count_nonzero(scan.chip.masks["hitbus"]) == 1)
            self.assertTrue(np.count_nonzero(scan.chip.masks.disable_mask) == 1)
            self.assertTrue(np.all(scan.chip.masks["tdac"][:] == 10))
            filename = scan.output_filename

        # Check that changed chip config from config/scan step is stored in config output of raw data file
        with tb.open_file(filename + ".h5", "r") as in_file:
            # Settings from configure / scan step
            chip_TDAC_mask_in = in_file.root.configuration_in.chip.masks.tdac[:]
            chip_TDAC_mask_out = in_file.root.configuration_out.chip.masks.tdac[:]
            self.assertFalse(np.all(chip_TDAC_mask_in == 0))
            self.assertTrue(np.count_nonzero(chip_TDAC_mask_in == 7))  # std. setting for some pixels TDAC = 7
            self.assertFalse(np.all(chip_TDAC_mask_out == 10))
            self.assertTrue(np.all(in_file.root.configuration_out.chip.use_pixel[1:, 1:] == 0))
            self.assertTrue(in_file.root.configuration_out.chip.use_pixel[0, 0] == 1)

            # Settings changed after scan step should not be stored in config output of raw data file
            self.assertFalse(np.count_nonzero(in_file.root.configuration_out.chip.masks.enable[:]) == 1)
            self.assertFalse(np.count_nonzero(in_file.root.configuration_out.chip.masks.hitbus[:]) == 1)

        # Check that changed chip config from analysis step is stored in config output of interpreted data file
        with tb.open_file(filename + "_interpreted.h5", "r") as in_file:
            chip_TDAC_mask_in = in_file.root.configuration_in.chip.masks.tdac[:]
            chip_TDAC_mask_out = in_file.root.configuration_out.chip.masks.tdac[:]
            self.assertFalse(np.all(chip_TDAC_mask_in == 10))
            self.assertTrue(np.all(chip_TDAC_mask_out == 10))
            self.assertTrue(np.count_nonzero(in_file.root.configuration_in.chip.use_pixel[:]) == 1)
            self.assertTrue(np.all(in_file.root.configuration_in.chip.use_pixel[1:, 1:] == 0))
            self.assertTrue(in_file.root.configuration_in.chip.use_pixel[0, 0] == 1)
            self.assertTrue(np.count_nonzero(in_file.root.configuration_out.chip.use_pixel[:]) == 1)
            self.assertTrue(np.all(in_file.root.configuration_out.chip.use_pixel[1:, 1:] == 0))
            self.assertTrue(in_file.root.configuration_out.chip.use_pixel[0, 0] == 1)
            # Settings changed after scan step are stored in config output interpreted file
            self.assertTrue(np.count_nonzero(in_file.root.configuration_out.chip.masks.enable[:]) == 1)
            self.assertTrue(np.count_nonzero(in_file.root.configuration_out.chip.masks.hitbus[:]) == 1)

        # Test mask loading from previous scan
        with TestScan(bench_config=self.bench_config) as scan:
            self.assertTrue(np.all(scan.chip.masks["tdac"] == 10))  # check that new TDAC mask from previous cfg is used
            self.assertTrue(np.count_nonzero(scan.chip.masks["enable"]) == 0)  # check that enable mask std. cfg is used
            self.assertTrue(np.count_nonzero(scan.chip.masks["hitbus"]) == 0)  # check that enable mask std. cfg is used
            # check that previous cfg is used
            self.assertTrue(np.all(scan.chip.masks.disable_mask[1:, 1:] == 0))
            self.assertTrue(scan.chip.masks.disable_mask[0, 0] == 1)
            self.assertTrue(np.count_nonzero(scan.chip.masks.disable_mask) == 1)
            scan.configure()
            self.assertTrue(np.all(scan.chip.masks["tdac"] == 0))  # mask reset to TDAC = 0 during configure
            self.assertTrue(np.count_nonzero(scan.chip.masks["hitbus"]) == 0)
            self.assertTrue(np.count_nonzero(scan.chip.masks["enable"]) == 1)  # from and with disable mask during configure
            self.assertTrue(np.count_nonzero(scan.chip.masks.disable_mask) == 1)

        with TestScan(bench_config=self.bench_config) as scan:
            scan.configure()

        # Test TDAC mask reset (= use std. masks) from testbench config
        bench_config = copy.deepcopy(self.bench_config)
        bench_config["modules"]["module_0"]["chip_0"]["masks"] = None  # use std. mask config
        with TestScan(bench_config=bench_config) as scan:
            self.assertTrue(np.all(scan.chip.masks["tdac"][:128] == 0))  # check that std TDAC mask cfg is used
            self.assertTrue(np.all(scan.chip.masks["tdac"][128:264] == 7))  # check that std TDAC mask cfg is used
            self.assertTrue(np.all(scan.chip.masks["tdac"][264:] == 0))  # check that std TDAC mask cfg is used
            self.assertTrue(np.count_nonzero(scan.chip.masks["enable"]) == 0)  # check that enable mask std. cfg is used
            self.assertTrue(np.count_nonzero(scan.chip.masks["hitbus"]) == 0)  # check that enable mask std. cfg is used
            self.assertTrue(np.count_nonzero(scan.chip.masks.disable_mask) == 1)  # check that previous cfg is used
            scan.configure()

        # Test TDAC mask reset (= use std. masks) from scan config
        scan_config = copy.deepcopy(scan_configuration)
        scan_config["chip"] = {"masks": None}
        with TestScan(scan_config=scan_config) as scan:
            self.assertTrue(np.all(scan.chip.masks["tdac"][:128] == 0))  # check that std TDAC mask cfg is used
            self.assertTrue(np.all(scan.chip.masks["tdac"][128:264] == 7))  # check that std TDAC mask cfg is used
            self.assertTrue(np.all(scan.chip.masks["tdac"][264:] == 0))  # check that std TDAC mask cfg is used
            self.assertTrue(np.count_nonzero(scan.chip.masks["enable"]) == 0)  # check that enable mask std. cfg is used
            self.assertTrue(np.count_nonzero(scan.chip.masks["hitbus"]) == 0)  # check that enable mask std. cfg is used
            self.assertTrue(np.count_nonzero(scan.chip.masks.disable_mask) == 1)  # check that previous cfg is used
            scan.configure()

    def test_bench_cfg_from_scan_config(self):
        """ Test overwrites from scan config for the bench config """

        scan_config = {"bench": {"TLU": {"TRIGGER_MODE": 3, "TRIGGER_SELECT": 0}}}  # Trigger configuration  # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)  # HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
        with self.TestScan(bench_config=self.bench_config, scan_config=scan_config) as scan:
            output_filename = scan.output_filename + ".h5"

        # Check that changed chip config is stored in config output
        with tb.open_file(output_filename, "r") as in_file:
            tlu_cfg = scan_base.fill_dict_from_conf_table(in_file.root.configuration_in.bench.TLU)
            self.assertTrue(scan_config["bench"]["TLU"].items() <= tlu_cfg.items())

    def test_scan_conf_per_chip(self):
        """ Check that scan conf per chip is loaded correctly """
        from bdaq53.scans import scan_digital

        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config["modules"]["module_0"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_0"]["chip_0"])
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"
        bench_config["modules"]["module_0"]["chip_1"]["chip_id"] = 1
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx1"
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5501"
        # Add additional module
        bench_config["modules"]["module_1"] = copy.deepcopy(bench_config["modules"]["module_0"])
        bench_config["modules"]["module_1"]["chip_0"]["chip_sn"] = "0x0003"
        bench_config["modules"]["module_1"]["chip_0"]["receiver"] = "rx2"
        bench_config["modules"]["module_1"]["chip_0"]["send_data"] = "tcp://127.0.0.1:5502"
        # Add additional chip to module 1
        bench_config["modules"]["module_1"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_1"]["chip_0"])
        bench_config["modules"]["module_1"]["chip_1"]["chip_sn"] = "0x0004"
        bench_config["modules"]["module_1"]["chip_1"]["receiver"] = "rx3"
        bench_config["modules"]["module_1"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5503"

        scan_config_per_chip = {}
        scan_config_per_chip["module_0"] = {"chip_0": {"chip": {"registers": {"Vthreshold_LIN": 0}}}, "chip_1": {"chip": {"registers": {"Vthreshold_LIN": 1}}}}
        scan_config_per_chip["module_1"] = {"chip_0": {"chip": {"registers": {"Vthreshold_LIN": 2}}}, "chip_1": {"chip": {"registers": {"Vthreshold_LIN": 3}}}}

        with scan_digital.DigitalScan(bench_config=bench_config, scan_config_per_chip=scan_config_per_chip) as scan:
            for i, _ in enumerate(scan.iterate_chips()):
                self.assertTrue(scan.chip.registers["Vthreshold_LIN"].get() == i)  # check that new TDAC mask from previous cfg is used

    @unittest.skip("Fixture Missing")
    def test_double_chip_analysis(self):
        """ Check that analysis step with data from two chips works """
        from bdaq53.scans import scan_digital

        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config["modules"]["module_0"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_0"]["chip_0"])
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"
        bench_config["modules"]["module_0"]["chip_1"]["chip_id"] = 1
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx1"
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5501"

        scan = scan_digital.DigitalScan(bench_config=bench_config)
        scan.init()
        for i, _ in enumerate(scan.iterate_chips()):
            scan.output_filename = os.path.join(data_folder, "digital_scan_chip_%d" % i)
        scan.analyze()
        scan.close()

    def test_parallel_scan(self):
        """ Check that parallel scans work """

        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config["modules"]["module_0"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_0"]["chip_0"])
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"
        bench_config["modules"]["module_0"]["chip_1"]["chip_id"] = 1
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx1"
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5501"

        with self.TestScanParallel(bench_config=bench_config, scan_config=scan_configuration) as scan:
            scan.start()

    def test_itkpixv1(self):
        """ Check that a digital scan with ITkPixV1 works """
        from bdaq53.scans import scan_digital

        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        bench_config["modules"]["module_0"]["chip_0"]["chip_type"] = "itkpixv1"
        bench_config["modules"]["module_0"]["chip_0"]["use_ptot"] = False
        with scan_digital.DigitalScan(bench_config=bench_config) as scan:
            scan.start()
        self.assertTrue(self.check_scan_success())

    def test_no_database_setting(self):
        bench_config = copy.deepcopy(self.bench_config)

        db_mock = mock.MagicMock()
        self.bhm.patch_function("bdaq53.manage_databases.check_chip_in_database", db_mock)

        from bdaq53.scans import scan_digital

        with scan_digital.DigitalScan(bench_config=bench_config):
            pass
        db_mock.assert_not_called()

        bench_config["general"]["use_database"] = True  # testbench setting to use database
        with scan_digital.DigitalScan(bench_config=bench_config):
            pass
        db_mock.assert_called()

    def test_return_values(self):
        """ Test that return values in scan steps: configure, scan, analyze are set """
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config["modules"]["module_0"]["chip_1"] = copy.deepcopy(bench_config["modules"]["module_0"]["chip_0"])
        bench_config["modules"]["module_0"]["chip_1"]["chip_sn"] = "0x0002"
        bench_config["modules"]["module_0"]["chip_1"]["chip_id"] = 1
        bench_config["modules"]["module_0"]["chip_1"]["receiver"] = "rx1"
        bench_config["modules"]["module_0"]["chip_1"]["send_data"] = "tcp://127.0.0.1:5501"

        # Create test scan that returns values for all scan steps
        from bdaq53.system.scan_base import ScanBase

        class TestReturnValuesScan(ScanBase):
            scan_id = "test_return_values_scan"

            def _configure(self, **_):
                return self.chip_settings["chip_type"], self.chip_settings["chip_sn"]

            def _scan(self, **_):
                return self.chip_settings["chip_type"], self.chip_settings["chip_sn"]

            def _analyze(self):
                return self.chip_settings["chip_type"], self.chip_settings["chip_sn"]

        with TestReturnValuesScan(bench_config=bench_config) as scan:
            values = scan.configure()
            self.assertListEqual([("rd53a", "0x0001"), ("rd53a", "0x0002")], values)
            values = scan.scan()
            self.assertListEqual([("rd53a", "0x0001"), ("rd53a", "0x0002")], values)
            values = scan.analyze()
            self.assertListEqual([("rd53a", "0x0001"), ("rd53a", "0x0002")], values)

    def test_file_naming(self):
        """ Check that not the same file name is used again """
        from bdaq53.scans import scan_digital

        with mock.patch("time.strftime", mock.MagicMock(return_value="20201127_163842")):
            scan = scan_digital.DigitalScan(bench_config=self.bench_config, scan_config=scan_digital.scan_configuration)
            scan.init()
            fname = scan.output_filename
            scan.close()
            scan = scan_digital.DigitalScan(bench_config=self.bench_config, scan_config=scan_digital.scan_configuration)
            scan.init()
            fname_2 = scan.output_filename
            scan.close()

        self.assertTrue(fname + "_2" == fname_2)


if __name__ == "__main__":
    pytest.main(["-s", __file__])

