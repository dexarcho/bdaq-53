#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import yaml
import os
import time
import math

import numpy as np
from numba import njit

from bdaq53.chips.chip_base import ChipBase, RegisterObject, MaskObject
from bdaq53.analysis import analysis_utils
from bdaq53.analysis import rd53b_analysis as anb
from bdaq53.system import logger as logger

FLAVOR_COLS = {'ITKPixV1': range(0, 400)}


def get_flavor(col):
    for fe, cols in FLAVOR_COLS.items():
        if col in cols:
            return fe


def get_tdac_range(fe):
    return -15, 15, 31, 1


@njit(cache=True, fastmath=True)
def encode_rd53b(address, data):
    ''' Encodes address + data information into the custom RD53b 5bit values -> 8bit symbols protocoll '''
    # Lookup table, 5-bit values to 8-bit symbols, index = value
    symbols = [0b01101010, 0b01101100, 0b01110001, 0b01110010, 0b01110100, 0b10001011, 0b10001101, 0b10001110, 0b10010011, 0b10010101,  # value 1 - 10
               0b10010110, 0b10011001, 0b10011010, 0b10011100, 0b10100011, 0b10100101, 0b10100110, 0b10101001, 0b01011001, 0b10101100,  # value 11 - 20
               0b10110001, 0b10110010, 0b10110100, 0b11000011, 0b11000101, 0b11000110, 0b11001001, 0b11001010, 0b11001100, 0b11010001,   # value 21- 30
               0b11010010, 0b11010100]    # value 31 - 32

    word_1 = address >> 5  # Write mode + first 4 bits of address
    word_2 = address & 0x1f  # Last 5 bits of address
    word_3 = data >> 11  # First 5 bits of data
    word_4 = (data >> 6) & 0x1f  # Middle 5 bits of data
    word_5 = (data >> 1) & 0x1f  # Middle 5 bits of data
    word_6 = (data & 0x1) << 4  # Last bit of data

    return [symbols[word_1], symbols[word_2], symbols[word_3], symbols[word_4], symbols[word_5], symbols[word_6]]


class ITkPixV1Calibration(object):
    # Natural constants
    e = 1.6021766208e-19
    kb = 1.38064852e-23

    def __init__(self, calibration=None):
        if calibration is None:
            calibration = {}
        # Chip specific calibration constants
        try:
            self.e_conversion = {}
            self.e_conversion['slope'] = calibration['e_conversion'].get('slope', 4.652)
            self.e_conversion['slope_error'] = calibration['e_conversion'].get('slope_error', 0.4)
            self.e_conversion['offset'] = calibration['e_conversion'].get('offset', 0)
            self.e_conversion['offset_error'] = calibration['e_conversion'].get('offset_error', 60)
        except KeyError:
            self.e_conversion = {'slope': 4.652, 'slope_error': 0.4, 'offset': 0, 'offset_error': 60}

        self.ADC_a = calibration.get('ADC_a', 0.2)      # Conversion factors ADC counts - voltage: slope
        self.ADC_b = calibration.get('ADC_b', 15.5)     # Conversion factors ADC counts - voltage: offset

        self.t_sensors_calibrated = False
        Nf_global = calibration.get('Nf', 1.24)         # Chip specific conversion factors for on-chip T sensors: voltage - temperature
        if 'Nf_0' in calibration.keys():
            self.t_sensors_calibrated = True
        self.Nf_0 = calibration.get('Nf_0', Nf_global)  # Generally, there is one Nf per sensor. For a default fallback in case of
        self.Nf_1 = calibration.get('Nf_1', Nf_global)  # uncalibrated chips, all fall back to a global value.
        self.Nf_2 = calibration.get('Nf_2', Nf_global)
        self.Nf_3 = calibration.get('Nf_3', Nf_global)

    def return_all_values(self):
        values = {}
        values['e_conversion_slope'] = self.e_conversion['slope']
        values['e_conversion_slope_error'] = self.e_conversion['slope_error']
        values['e_conversion_offset'] = self.e_conversion['offset']
        values['e_conversion_offset_error'] = self.e_conversion['offset_error']
        values['ADC_a'] = self.ADC_a
        values['ADC_b'] = self.ADC_b
        values['Nf_0'] = self.Nf_0
        values['Nf_1'] = self.Nf_1
        values['Nf_2'] = self.Nf_2
        values['Nf_3'] = self.Nf_3

        return values

    def _dV_to_T(self, dV, sensor):
        Nf = eval('self.Nf_{0}'.format(sensor))
        return self.e / (Nf * self.kb * np.log(15)) * dV - 273.15

    def get_V_from_ADC(self, ADC, use_offset=True):
        if use_offset:
            return round(((self.ADC_a * ADC + self.ADC_b) / 1000), 8)
        else:
            return round(((self.ADC_a * ADC) / 1000), 8)

    def get_temperature_from_ADC(self, dADC, sensor):
        # ToDo: Calibrate diode temperature sensors
        return round((self._dV_to_T(self.get_V_from_ADC(dADC, use_offset=False), sensor=sensor)), 3)

    def _get_temperature_from_ADC_resistive_sensors(self, dADC, sensor):
        # ToDo: Calibrate resisitve temperature sensors
        return dADC


class ITkPixV1MaskObject(MaskObject):
    def remap_to_global(self, col, row):
        ''' Returns global address from column and row '''
        core_col = int(col / 8)
        core_row = int(row / 8)
        region = 2 * (row % 8) + int((col % 8) / 4)
        pixel_pair = int((col % 4) / 2)
        pixel = col % 2

        return (core_col, core_row, region, pixel_pair, pixel)

    def remap_to_local(self, core_col, core_row, region, pixel_pair, pixel):
        ''' Returns column and row from global address'''
        col = core_col * 8 + (region % 2) * 4 + pixel_pair * 2 + pixel
        row = core_row * 8 + int(region / 2)

        return (col, row)

    def remap_to_registers(self, core_col, core_row, region, pixel_pair):
        region_bin = '{0:04b}'.format(region)
        region_col = '{0:#06b}{1:s}{2:01b}'.format(core_col, region_bin[3], pixel_pair)
        region_row = '{0:#06b}{1:s}'.format(core_row, region_bin[:3])

        return (region_col, region_row)

    def get_pixel_data(self, column, row):
        ''' Return register value for one given pixel '''
        enable = str(int(self['enable'][column, row]))
        injection_enable = str(int(self['injection'][column, row]))
        hitbus_enable = str(int(self['hitbus'][column, row]))

        if self['tdac'][column, row] >= 0:
            tdac = str(bin(self['tdac'][column, row]))[2:].zfill(5)
        else:
            tdac = '1' + str(bin(abs(self['tdac'][column, row])))[2:].zfill(4)

        return tdac + hitbus_enable + injection_enable + enable

    def get_double_pixel_data(self, core_col, core_row, region, pixel_pair):
        ''' Return register value for given double pixel '''
        return int('0b' + self.get_pixel_data(*self.remap_to_local(core_col, core_row, region, pixel_pair, 1)) +
                   self.get_pixel_data(*self.remap_to_local(core_col, core_row, region, pixel_pair, 0)), 2)

    def reset_all(self):
        super(ITkPixV1MaskObject, self).reset_all()

    def update(self, force=False, broadcast=False):
        ''' Write the actual pixel register configuration

            Only write changes for speed up
        '''
        if self.chip.bdaq.board_version == 'SIMULATION':
            return []
        indata = self.chip.write_sync(write=False)
        self.chip.registers['PIX_DEFAULT_CONFIG'].set(0x9ce2)  # Write Magic numbers to disable constant reset
        self.chip.registers['PIX_DEFAULT_CONFIG_B'].set(0x631d)  # Write Magic numbers to disable constant reset
        indata += self.chip.registers['PIX_DEFAULT_CONFIG'].get_write_command(0x9ce2)  # Write Magic numbers to disable constant reset
        indata += self.chip.registers['PIX_DEFAULT_CONFIG_B'].get_write_command(0x631d)  # Write Magic numbers to disable constant reset
        self._find_changes()
        trig_del = self.chip.registers['TriggerConfig'].get()
        cal_conf = self.chip.registers['CalibrationConfig'].get()
        indata += self.chip.registers['TriggerConfig'].get_write_command(511)
        indata += self.chip.registers['CalibrationConfig'].get_write_command(0b10000000)
        if broadcast:
            write_range = (0, 8, 2)
            broadcast_value = 4
        else:
            write_range = (0, self.dimensions[0], 2)
            broadcast_value = 0
        if force:
            write_mask = np.ones(self.dimensions, bool)
        else:   # Find out which pixels need to be updated
            write_mask = self.to_write
        to_write = np.column_stack((np.where(write_mask)))

        data = []
        if len(to_write) < 10000:   # Write only double pixels which need to be updated
            self.chip.registers['PIX_MODE'].set(2)
            indata += self.chip.registers['PIX_MODE'].get_write_command(2)    # Disable broadcast, auto-col, auto_row
            indata += self.chip.write_sync(write=False) * 10
            last_coords = (-1, -1, -1, -1, -1, -1)
            written = []
            for (col, row) in to_write:
                (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, row)
                (region_col, region_row) = self.remap_to_registers(core_col, core_row, region, pixel_pair)

                # Speedup block
                if (core_col, core_row, region, pixel_pair) in written:  # This pixel pair has already been updated
                    continue
                if col != last_coords[0]:  # Downshifting patterns always changes two pixels below each other
                    indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
                if row != last_coords[1]:  # Sideshifting patterns always changes two pixels next to each other
                    indata += self.chip.registers['REGION_ROW'].get_write_command(int(region_row, 2))
                last_coords = (col, row, core_col, core_row, region, pixel_pair)
                written.append((core_col, core_row, region, pixel_pair))

                # Write pixel pair value
                indata += self.chip.registers['PIX_PORTAL'].get_write_command(self.get_double_pixel_data(core_col, core_row, region, pixel_pair))
                indata += self.chip.write_sync(write=False)
                if len(indata) > 4000:    # Write command to chip before it gets too long
                    if force and self.chip.reset_tot_latches:
                        indata += self.chip.write_sync(write=False)
                        indata += self.chip.write_cal(cal_edge_mode=1, cal_edge_width=2, cal_edge_dly=0, write=False) * 8  # Injection
                        indata += self.chip.write_sync(write=False)
                    self.chip.write_command(indata)
                    data.append(indata)
                    indata = self.chip.write_sync(write=False)
            indata += self.chip.registers['REGION_COL'].get_write_command(255)
            indata += self.chip.registers['REGION_ROW'].get_write_command(511)
            self.chip.write_command(indata)
            data.append(indata)
        else:
            self.chip.registers['PIX_MODE'].set(3 + broadcast_value)
            indata += self.chip.registers['PIX_MODE'].get_write_command(3 + broadcast_value)    # Disable broadcast, enable auto-row
            indata += self.chip.write_sync(write=False)
            for col in range(write_range[0], write_range[1], write_range[2]):  # Loop over every second column, since two neighboring pixels are always written together
                # Set column address to next column pair
                (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, 0)
                (region_col, _) = self.remap_to_registers(core_col, core_row, region, pixel_pair)
                indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
                indata += self.chip.write_sync(write=False)
                # Set row to 0
                indata += self.chip.registers['REGION_ROW'].get_write_command(0)
                indata += self.chip.write_sync(write=False)
                for row in range(self.dimensions[1]):  # Loop over every row, write double pixels
                    (_, core_row, region, _, _) = self.remap_to_global(col, row)
                    indata += self.chip.registers['PIX_PORTAL'].get_write_command(self.get_double_pixel_data(core_col, core_row, region, pixel_pair))
                    indata += self.chip.write_sync(write=False)
                    if len(indata) > 4000:  # Write command to chip before it gets too long
                        if force and self.chip.reset_tot_latches:
                            indata += self.chip.write_sync(write=False)
                            indata += self.chip.write_cal(cal_edge_mode=1, cal_edge_width=2, cal_edge_dly=0, write=False) * 8  # Injection
                            indata += self.chip.write_sync(write=False)
                        self.chip.write_command(indata)
                        data.append(indata)
                        indata = self.chip.write_sync(write=False)
            indata += self.chip.registers['REGION_COL'].get_write_command(255)
            indata += self.chip.registers['REGION_ROW'].get_write_command(511)
            self.chip.write_command(indata)
            data.append(indata)
        # Set this mask as last mask to be able to find changes in next update()
        for name, mask in self.items():
            self.was[name][:] = mask[:]
        self.chip.registers['TriggerConfig'].write(trig_del)
        self.chip.registers['CalibrationConfig'].write(cal_conf)
        return data

    def _update_bit_mode(self, force=False, write_TDAC=False):
        indata = self.chip.write_sync(write=False)
        if force:
            self.chip.registers['PIX_DEFAULT_CONFIG'].set(0x9ce2)  # Write Magic numbers to disable constant reset
            self.chip.registers['PIX_DEFAULT_CONFIG_B'].set(0x631d)  # Write Magic numbers to disable constant reset
            indata += self.chip.registers['PIX_DEFAULT_CONFIG'].get_write_command(0x9ce2)  # Write Magic numbers to disable constant reset
            indata += self.chip.registers['PIX_DEFAULT_CONFIG_B'].get_write_command(0x631d)  # Write Magic numbers to disable constant reset
        data = []
        pix_data = []
        for col in range(0, self.dimensions[0], 2):  # Loop over every second column, since two neighboring pixels are always written together
            indata += self.chip.write_sync(write=False)
            self.chip.registers['PIX_MODE'].set((int(write_TDAC) << 1) + 1)
            indata += self.chip.registers['PIX_MODE'].get_write_command((int(write_TDAC) << 1) + 1)  # Enable broadcast, enable auto-row
            indata += self.chip.registers['REGION_ROW'].get_write_command(0)
            (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, 0)
            (region_col, _) = self.remap_to_registers(core_col, core_row, region, pixel_pair)
            indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
            indata += self.chip.write_sync(write=False)
            for row in range(self.dimensions[1]):  # Loop over every row, write double pixels
                (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, row)
                full_pix_conf = self.get_double_pixel_data(core_col, core_row, region, pixel_pair)
                if not write_TDAC:
                    pix_bit = ((full_pix_conf & 0x700) >> 3) + (full_pix_conf & 0x7)
                else:
                    pix_bit = (((full_pix_conf >> 11) & 0x1f) << 5) + ((full_pix_conf >> 3) & 0x1f)
                pix_data += [pix_bit]
                if len(indata) + len(self.chip._write_pixels_fast(pix_data)) > 4000:  # Write command to chip before it gets too long
                    indata += self.chip._write_pixels_fast(pix_data)
                    self.chip.write_command(indata)
                    data.append(indata)
                    indata = self.chip.write_sync(write=False)
                    indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
                    pix_data = []

            indata += self.chip._write_pixels_fast(pix_data)
            pix_data = []
            self.chip.write_command(indata)
            data.append(indata)
            indata = self.chip.write_sync(write=False)
        data.append(indata)

        # Set this mask as last mask to be able to find changes in next update()
        for name, mask in self.items():
            self.was[name][:] = mask[:]
        return data

    def _update_broadcast(self, force=False, write_TDAC=False):
        indata = self.chip.write_sync(write=False)
        if force:
            self.chip.registers['PIX_DEFAULT_CONFIG'].set(0x9ce2)  # Write Magic numbers to disable constant reset
            self.chip.registers['PIX_DEFAULT_CONFIG_B'].set(0x631d)  # Write Magic numbers to disable constant reset
            indata += self.chip.registers['PIX_DEFAULT_CONFIG'].get_write_command(0x9ce2)  # Write Magic numbers to disable constant reset
            indata += self.chip.registers['PIX_DEFAULT_CONFIG_B'].get_write_command(0x631d)  # Write Magic numbers to disable constant reset
        data = []
        pix_data = []
        for col in range(0, 8, 2):  # Loop over every second column, since two neighboring pixels are always written together
            indata += self.chip.write_sync(write=False)
            self.chip.registers['PIX_MODE'].set((int(write_TDAC) << 1) + 5)
            indata += self.chip.registers['PIX_MODE'].get_write_command((int(write_TDAC) << 1) + 5)  # Enable broadcast, enable auto-row
            indata += self.chip.registers['REGION_ROW'].get_write_command(0)
            (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, 0)
            (region_col, _) = self.remap_to_registers(core_col, core_row, region, pixel_pair)
            indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
            indata += self.chip.write_sync(write=False)
            for row in range(self.dimensions[1]):  # Loop over every row, write double pixels
                (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, row)
                full_pix_conf = self.get_double_pixel_data(core_col, core_row, region, pixel_pair)
                if not write_TDAC:
                    pix_bit = ((full_pix_conf & 0x700) >> 3) + (full_pix_conf & 0x7)
                else:
                    pix_bit = (((full_pix_conf >> 11) & 0x1f) << 5) + ((full_pix_conf >> 3) & 0x1f)
                pix_data += [pix_bit]
                if len(indata) + len(
                        self.chip._write_pixels_fast(pix_data)) > 4000:  # Write command to chip before it gets too long
                    indata += self.chip._write_pixels_fast(pix_data)
                    self.chip.write_command(indata)
                    data.append(indata)
                    indata = self.chip.write_sync(write=False)
                    indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
                    pix_data = []

            indata += self.chip._write_pixels_fast(pix_data)
            pix_data = []
            self.chip.write_command(indata)
            data.append(indata)
            indata = self.chip.write_sync(write=False)
        data.append(indata)

        # Set this mask as last mask to be able to find changes in next update()
        for name, mask in self.items():
            self.was[name][:] = mask[:]
        return data

    def apply_good_pixel_mask_diff(self):
        pass

    def load_logo_mask(self, masks=['injection']):
        from pylab import imread
        img = np.invert(imread(os.path.join(os.path.dirname(__file__), 'RD53B_logo.bmp'))).astype('bool')
        img_mask = np.transpose(img[:, :, 0])
        img_mask[:, 0:8] = True
        img_mask[:, img_mask.shape[1] - 8:img_mask.shape[1]] = True
        for name in masks:
            self[name] = img_mask
        return img_mask


class ITkPixV1(ChipBase):
    '''
    Main class for ITkPixV1 chip
    '''

    cmd_data_map = {
        0: 0b01101010,
        1: 0b01101100,
        2: 0b01110001,
        3: 0b01110010,
        4: 0b01110100,
        5: 0b10001011,
        6: 0b10001101,
        7: 0b10001110,
        8: 0b10010011,
        9: 0b10010101,
        10: 0b10010110,
        11: 0b10011001,
        12: 0b10011010,
        13: 0b10011100,
        14: 0b10100011,
        15: 0b10100101,
        16: 0b10100110,
        17: 0b10101001,
        18: 0b01011001,
        19: 0b10101100,
        20: 0b10110001,
        21: 0b10110010,
        22: 0b10110100,
        23: 0b11000011,
        24: 0b11000101,
        25: 0b11000110,
        26: 0b11001001,
        27: 0b11001010,
        28: 0b11001100,
        29: 0b11010001,
        30: 0b11010010,
        31: 0b11010100
    }

    trigger_map = {
        0: 0b00101011,
        1: 0b00101011,
        2: 0b00101101,
        3: 0b00101110,
        4: 0b00110011,
        5: 0b00110101,
        6: 0b00110110,
        7: 0b00111001,
        8: 0b00111010,
        9: 0b00111100,
        10: 0b01001011,
        11: 0b01001101,
        12: 0b01001110,
        13: 0b01010011,
        14: 0b01010101,
        15: 0b01010110
    }
    CMD_SYNC = [0b10000001, 0b01111110]  # 0x(817E)
    CMD_PLL_LOCK = [0b01010101, 0b01010101]
    # CMD_TRIGGER = trigger_map[x]
    CMD_READ_TRIGGER = 0b01101001
    CMD_CLEAR = 0b01011010
    CMD_GLOBAL_PULSE = 0b01011100
    CMD_CAL = 0b01100011
    CMD_REGISTER = 0b01100110
    CMD_RDREG = 0b01100101
    CMD_NULL = 0b01101001

    core_regs = [{'columns': range(0, 128),
                  'core_columns': range(0, 16),
                  'EnCoreCol': 'EnCoreCol_0',
                  'EnCoreColumnCalibration': 'EnCoreColumnCalibration_0',
                  'EnCoreColumnReset': 'EnCoreColumnReset_0',
                  'PrecisionToTEnable': 'PrecisionToTEnable_0',
                  'HITOR_MASK': 'HITOR_MASK_0'},

                 {'columns': range(128, 256),
                  'core_columns': range(16, 32),
                  'EnCoreCol': 'EnCoreCol_1',
                  'EnCoreColumnCalibration': 'EnCoreColumnCalibration_1',
                  'EnCoreColumnReset': 'EnCoreColumnReset_1',
                  'PrecisionToTEnable': 'PrecisionToTEnable_1',
                  'HITOR_MASK': 'HITOR_MASK_1'},

                 {'columns': range(256, 384),
                  'core_columns': range(32, 48),
                  'EnCoreCol': 'EnCoreCol_2',
                  'EnCoreColumnCalibration': 'EnCoreColumnCalibration_2',
                  'EnCoreColumnReset': 'EnCoreColumnReset_2',
                  'PrecisionToTEnable': 'PrecisionToTEnable_2',
                  'HITOR_MASK': 'HITOR_MASK_2'},

                 {'columns': range(384, 432),
                  'core_columns': range(48, 54),
                  'EnCoreCol': 'EnCoreCol_3',
                  'EnCoreColumnCalibration': 'EnCoreColumnCalibration_3',
                  'EnCoreColumnReset': 'EnCoreColumnReset_3',
                  'PrecisionToTEnable': 'PrecisionToTEnable_3',
                  'HITOR_MASK': 'HITOR_MASK_3'}
                 ]

    default_dac_values = {'DAC_PREAMP_L': 50,
                          'DAC_PREAMP_R': 51,
                          'DAC_PREAMP_TL': 52,
                          'DAC_PREAMP_TR': 53,
                          'DAC_PREAMP_T': 54,
                          'DAC_PREAMP_M': 55,
                          'DAC_PRECOMP': 56,
                          'DAC_COMP': 57,
                          'DAC_VFF': 100,
                          'DAC_TH1_L': 100,
                          'DAC_TH1_R': 100,
                          'DAC_TH1_M': 100,
                          'DAC_TH2': 0,
                          'DAC_LCC': 100,
                          'LEACKAGE_FEEDBACK': 0}

    voltage_mux = {'VREF_ADC': 0,
                   'I_MUX': 1,
                   'NTC_PAD': 2,
                   'VREF_VDAC': 3,
                   'VDDA_HALF_CAP': 4,
                   'TEMPSENS_T': 5,
                   'TEMPSENS_B': 6,
                   'VCAL_HI': 7,
                   'VCAL_HIGH': 7,
                   'VCAL_MED': 8,
                   'DAC_TH2': 9,
                   'DAC_TH1_M': 10,
                   'DAC_TH1_L': 11,
                   'DAC_TH1_R': 12,
                   'RADSENS_A': 13,
                   'TEMPSENS_A': 14,
                   'RADSENS_D': 15,
                   'TEMPSENS_D': 16,
                   'RADSENS_C': 17,
                   'TEMPSENS_C': 18,
                   'GNDA19': 19,
                   'GNDA20': 20,
                   'GNDA21': 21,
                   'GNDA22': 22,
                   'GNDA23': 23,
                   'GNDA24': 24,
                   'GNDA25': 25,
                   'GNDA26': 26,
                   'GNDA27': 27,
                   'GNDA28': 28,
                   'GNDA29': 29,
                   'GNDA30': 30,
                   'VREF_CORE': 31,
                   'VREF_PRE': 32,
                   'VINA_HALF': 33,
                   'VDDA_HALF': 34,
                   'VREFA': 35,
                   'VOFS_HALF': 36,
                   'VIND_HALF': 37,
                   'VDDD_HALF': 38,
                   'VREFD': 39}

    current_mux = {'IREF': 0,
                   'CDR_VCO_MAIN': 1,
                   'CDR_VCO_BUF': 2,
                   'CDR_CP': 3,
                   'CDR_FD': 4,
                   'CDR_BUF': 5,
                   'CML_TAP2': 6,
                   'CML_TAP1': 7,
                   'CML_MAIN': 8,
                   'NTC_PAD': 9,
                   'CAP': 10,
                   'CAP_PARASIT': 11,
                   'PREAMP_MATRIX_MAIN': 12,
                   'DAC_PRECOMP': 13,
                   'DAC_COMP': 14,
                   'DAC_TH2': 15,
                   'DAC_TH1_M': 16,
                   'DAC_LCC': 17,
                   'DAC_FB': 18,
                   'DAC_PREAMP_L': 19,
                   'DAC_TH1_L': 20,
                   'DAC_PREAMP_R': 21,
                   'DAC_PREAMP_TL': 22,
                   'DAC_TH1_R': 23,
                   'DAC_PREAMP_T': 24,
                   'DAC_PREAMP_TR': 25,
                   'IINA': 28,
                   'I_SHUNT_A': 29,
                   'IIND': 30,
                   'I_SHUNT_D': 31}

    flavor_cols = FLAVOR_COLS

    def __init__(self, bdaq, chip_sn='0x0000', chip_id=15, receiver='rx0', config=None):
        self.log = logger.setup_derived_logger('ITkPixV1 - ' + chip_sn)
        self.bdaq = bdaq
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if chip_id not in range(16):
            raise ValueError("Invalid Chip ID %r, use integer 0-15" % chip_id)

        self.chip_type = 'ITKPixV1'
        self.chip_sn = chip_sn
        self.chip_id = chip_id
        self.receiver = receiver

        if config is None or len(config) == 0:
            self.log.warning("No explicit configuration supplied. Using 'ITkPixV1_default.cfg.yaml'!")
            with open(os.path.join(os.path.dirname(__file__), 'ITkPixV1_default.cfg.yaml'), 'r') as f:
                self.configuration = yaml.full_load(f)
        elif isinstance(config, dict):
            self.configuration = config
        elif isinstance(config, str) and os.path.isfile(config):
            with open(config) as f:
                self.configuration = yaml.full_load(f)
        else:
            raise TypeError('Supplied config has unknown format!')

        self.registers = RegisterObject(self, 'ITkPixV1_registers.yaml')

        masks = {'enable': {'default': False},
                 'injection': {'default': False},
                 'hitbus': {'default': False},
                 'tdac': {'default': 0},
                 'lin_gain_sel': {'default': True},
                 'injection_delay': {'default': 351}}
        self.masks = ITkPixV1MaskObject(self, masks, (400, 384))

        # Load disabled pixels from chip config
        if 'disable' in self.configuration.keys():
            for pix in self.configuration['disable']:
                self.masks.disable_mask[pix[0], pix[1]] = False

        self.calibration = ITkPixV1Calibration(self.configuration['calibration'])

        self.ptot_enabled = False  # Flag to indicate if PTOT mode is enabled
        self.reset_tot_latches = self.configuration.get('reset_tot_latches', True)

        self.compress_hmap = self.configuration.get('compress_hmap', False)

    def init(self, write_chip_reset=True, *args):
        if self.bdaq.board_version != 'SIMULATION':
            self.write_trimbits()  # Write trimbits for better communication
            self.init_communication(write_chip_reset=write_chip_reset)
            self.reset()
            # if self.bdaq.enable_NTC:
            #     self.get_temperature_NTC()
            # else:
            #     self.get_temperature_sensors()
        else:
            self.init_communication(repetitions=30)

    def get_sn(self):
        return self.chip_sn

    def write_trimbits(self, verify=False):
        '''
        Write trimbits for all reference voltages to the chip, if they are supplied in the chip config.
        '''

        if 'trim' not in self.configuration:
            return

        vref_a_trim = self.configuration['trim'].get('VREF_A_TRIM', 15)
        vref_d_trim = self.configuration['trim'].get('VREF_D_TRIM', 15)
        self.registers['VOLTAGE_TRIM'].write(int('00' + format(vref_a_trim, '04b') + format(vref_d_trim, '04b'), 2), verify=verify)

        mon_adc_trim = self.configuration['trim'].get('MON_ADC_TRIM', 0)
        self.registers['MON_ADC'].write(int('001' + format(mon_adc_trim, '06b'), 2), verify=verify)

    def setup_aurora(self, tx_lanes=4, CB_Wait=255, CB_Send=1, only_cb=False, bypass_mode=False, receiver='rx0', lane_test=False, **_):
        if bypass_mode:
            self.log.info("Switching chip to BYPASS MODE.")
            self.registers['CdrConf'].set(7)
        elif self.bdaq['system']['AURORA_RX_640M'] == 1:
            self.log.info("Chip is running at 640Mb/s")
            self.registers['CdrConf'].set(1)
        else:
            self.log.info("Chip is running at 1.28Gb/s")
        if only_cb is False:
            self.log.debug("Aurora transmitter settings: Lanes=%u, CB_Wait=%u, CB_Send=%u", tx_lanes, CB_Wait, CB_Send)
            if tx_lanes == 4:
                self.log.info("4 Aurora lanes active")
                # Sets 4-lane-mode
                self.registers['AuroraConfig'].set(0b0111101100111)
                # Enable 4 CML outputs
                self.registers['CML_CONFIG'].set(0b00001111)
                self.registers['DataMergingMux'].set(0b1110010000011011)
            elif tx_lanes == 3:
                self.log.info("3 Aurora lanes active")
                # Sets 3-lane-mode
                self.registers['AuroraConfig'].set(0b0011101100111)
                # Enable 2 CML outputs
                self.registers['CML_CONFIG'].set(0b00000111)
                self.registers['DataMergingMux'].set(0b1110010000011011)
            elif tx_lanes == 2:
                self.log.info("2 Aurora lanes active")
                # Sets 2-lane-mode
                self.registers['AuroraConfig'].set(0b0001101100111)
                # Enable 2 CML outputs
                self.registers['CML_CONFIG'].set(0b00000011)
                self.registers['DataMergingMux'].set(0b1110010000011011)
            elif tx_lanes == 1:
                self.log.info("1 Aurora lane active")
                # Sets 1-lane-mode
                if receiver == 'rx3' and lane_test:
                    self.log.info("Configuring aurora lane 3 on rx3")
                    self.registers['AuroraConfig'].set(0b0000101100111)
                    self.registers['CML_CONFIG'].set(0b00000001)
                    self.registers['DataMergingMux'].set(0b1110010011100100)
                elif receiver == 'rx2' and lane_test:
                    self.log.info("Configuring aurora lane 2 on rx2")
                    self.registers['AuroraConfig'].set(0b0000101100111)
                    self.registers['CML_CONFIG'].set(0b00000010)
                    self.registers['DataMergingMux'].set(0b1110010000000000)
                elif receiver == 'rx1' and lane_test:
                    self.log.info("Configuring aurora lane 1 on rx1")
                    self.registers['AuroraConfig'].set(0b0000101100111)
                    self.registers['CML_CONFIG'].set(0b00000100)
                    self.registers['DataMergingMux'].set(0b1110010000000000)
                else:
                    self.log.info("Configuring aurora lane 0 for general single lane receiver")
                    self.registers['AuroraConfig'].set(0b0000101100111)
                    self.registers['CML_CONFIG'].set(0b00001000)
                    self.registers['DataMergingMux'].set(0b1110010000011011)
            else:
                self.log.error("Aurora lane configuration (1,2,3,4) must be specified")
        else:
            self.log.debug("Aurora settings: CB_Wait=%u, CB_Send=%u", CB_Wait, CB_Send)

        # Set CB frame distance and number
        self.registers['AURORA_CB_CONFIG0'].set(((CB_Wait << 4) | CB_Send & 0xf) & 0xffff)
        self.registers['AURORA_CB_CONFIG1'].set((CB_Wait >> 12) & 0xff)  # Set CB frame distance and number
        self.registers.write_all()

        # Reset Aurora and serializers
        self.send_global_pulse(bitnames=['reset_serializer'], pulse_width=0xff)
        self.write_command(self.write_sync(write=True) * 16)
        self.send_global_pulse(bitnames=['reset_aurora'], pulse_width=0xff)

    def enable_data_merging(self, data_en=0b0001):
        self.send_global_pulse(bitnames=['reset_data_merging'], pulse_width=0xff)

        DataMergingInputPolarityInvert = 0b0000
        EnChipId = 0b0
        EnGatingDataMergeClk1280 = 0b0  # 0: clock enable
        SelDataMergeClk = 0b0  # 0: 640 MHz, 1: 1280 MHz
        EnDataMerge = data_en
        MergeChBonding = 0b0
        self.DataMergingRegister = (0x000
                                    | DataMergingInputPolarityInvert << 8
                                    | EnChipId << 7
                                    | EnGatingDataMergeClk1280 << 6
                                    | SelDataMergeClk << 5
                                    | EnDataMerge << 1
                                    | MergeChBonding)
        self.registers['DataMerging'].write(self.DataMergingRegister)

    def disable_data_merging(self):
        self.registers['DataMerging'].write(self.registers['DataMerging']['default'])

    def init_communication(self, repetitions=1000, write_chip_reset=True):
        self.log.info('Initializing communication...')
        # Configure cmd encoder. TODO: why is this not needed?
        # self.bdaq['cmd'].reset()
        self.bdaq.disable_auto_sync()
        self.bdaq.set_chip_type_ITkPixV1()
        if write_chip_reset:
            self._write_reset(write=True, repetitions=repetitions)
        self.write_sync_01(write=True, repetitions=repetitions)
        self.write_sync(write=True, repetitions=repetitions * 2)
        self.bdaq.enable_auto_sync()
        self.write_ecr()

        # Prepare chip
        self.use_programmed_configuration()  # Switch to programmed configuration in order to programm registers other than the default
        self.write_trimbits()  # Make sure that trimbits are written
        self.registers['DataMerging'].write(0b000001000000)  # Switch chip to 1.28 GHz and disable chip id. FIXME: Raw data analysis needs to know that!
        self.CoreColEncoderConfRegister = (0b0
                                           | (not self.compress_hmap) << 9
                                           | 0b000000000)
        self.registers['CoreColEncoderConf'].write(self.CoreColEncoderConfRegister)  # do not compress hmap
        self.registers['RingOscConfig'].write(0x7fff)
        self.registers['RingOscConfig'].write(0x5eff)

        # Wait for PLL lock
        self.bdaq.wait_for_pll_lock()

        # Init data path
        self.setup_aurora(tx_lanes=self.bdaq.rx_lanes[self.receiver], bypass_mode=self.bdaq.configuration['hardware']['bypass_mode'],
                          receiver=self.receiver, lane_test=self.configuration.get('lane_test', False))

        # Workaround for locking problems.
        for _ in range(30):
            self.bdaq.set_chip_type_ITkPixV1()
            self.write_command(self.write_sync(write=False) * 32)
            self.write_ecr()
            try:
                self.bdaq.wait_for_aurora_sync(sync_receivers=self.receiver)
            except RuntimeError:
                pass
            else:
                break

            time.sleep(0.01)
        else:
            self.bdaq.wait_for_aurora_sync()
        self.log.success('Communication established')

    def write_global_pulse(self, width, write=True):
        # 0101_1100    ChipId<3:0>,0    Width<3:0>,0
        indata = self.write_sync()
        if self.registers['GlobalPulseWidth'].get() != width:
            indata += self.registers['GlobalPulseWidth'].get_write_command(width)
            indata += self.write_sync() * 10
        indata += [self.CMD_GLOBAL_PULSE]
        indata += [self.cmd_data_map[self.chip_id]]
        if write:
            self.write_command(indata)

        return indata

    def write_cal(self, cal_edge_mode=0, cal_edge_width=10, cal_edge_dly=2, cal_aux_value=0, cal_aux_dly=0, write=True):
        '''
            Command to send a digital or analog injection to the chip.
            Digital or analog injection is selected globally via the INJECTION_SELECT register.

            For digital injection, only CAL_edge signal is relevant:
                - CAL_edge_mode switches between step (0) and pulse (1) mode
                - CAL_edge_dly is counted in bunch crossings. It sets the delay before the rising edge of the signal
                - CAL_edge_width is the duration of the pulse (only in pulse mode) and is counted in cycles of the 160MHz clock
            For analog injection, the CAL_aux signal is used as well:
                - CAL_aux_value is the value of the CAL_aux signal
                - CAL_aux_dly is counted in cycles of the 160MHz clock and sets the delay before the edge of the signal

            {ChipId[3:0],CalEdgeMode, CalEdgeDelay[2:0],CalEdgeWidth[5:4]}{CalEdgeWidth[3:0],CalAuxMode, CalAuxDly[4:0]}
        '''
        indata = [self.CMD_CAL]
        indata += [self.cmd_data_map[self.chip_id]]
        indata += [self.cmd_data_map[((cal_edge_mode << 4) + ((cal_edge_dly & 0b11110) >> 1))]]  # mode & e-delay
        indata += [self.cmd_data_map[((cal_edge_dly & 0b00001) << 4) + ((cal_edge_width & 0b11110000) >> 4)]]  # Eduration
        indata += [self.cmd_data_map[(((cal_edge_width & 0b00001111) << 1) + cal_aux_value)]]  # cal_aux
        indata += [self.cmd_data_map[(cal_aux_dly)]]  # A-delay

        if write:
            self.write_command(indata)

        return indata

    def _read_register(self, address, write=True):
        indata = [self.CMD_RDREG]
        indata += [self.cmd_data_map[self.chip_id]]
        indata += [self.cmd_data_map[address >> 5]]  # first 4 bits of address
        indata += [self.cmd_data_map[address & 0x1f]]  # last 5 bits of address

        if write:
            self.write_command(indata)
        return indata

    def _get_register_value(self, address, timeout=1000, tries=10):
        self.enable_monitor_data()
        for _ in range(tries):
            self._read_register(address)
            self.write_command(self.write_sync(write=False) * 10)
            self._read_register(address)
            self.write_command(self.write_sync(write=False) * 10)
            for _ in range(timeout):
                if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                    data = self.bdaq['FIFO'].get_data()
                    userk_data = analysis_utils.process_userk(anb.interpret_userk_data(data))
                    if len(userk_data) == 0:
                        continue
                    index_i = np.where(userk_data['Address'] == address)[0]
                    if len(index_i) == 0:
                        if address == 0:
                            index_i = np.where(userk_data['Address'] == self.registers['REGION_ROW'].get())[0]
                        else:
                            continue
                    return userk_data['Data'][index_i][0]
                self.write_command(self.write_sync(write=False) * 10)
            else:
                self.log.warning('Timeout while waiting for register response.')
        else:
            raise RuntimeError('Timeout while waiting for register response.')

    def _write_register(self, address, data, write=True):
        '''
            Sends write command to register with data

            Parameters:
            ----------
                address : int
                    Address of the register to be written to

            Returns:
            ----------
                indata : binarray
                    Boolean representation of register write command.
        '''
        indata = [self.CMD_REGISTER, self.cmd_data_map[self.chip_id]] + encode_rd53b(address, data)

        if write:
            self.write_command(indata)

        return indata

    def _write_pixels_fast(self, data, write=False):
        '''
            Sends write command to register with data

            Parameters:
            ----------
                address : int
                    Address of the register to be written to

            Returns:
            ----------
                indata : binarray
                    Boolean representation of register write command.
        '''
        sync_counter = 0
        indata = [self.CMD_REGISTER]  # Write Command
        indata += [self.cmd_data_map[self.chip_id]]
        indata += [self.cmd_data_map[(0 >> 5) + 16]]  # Write mode + first 4 bits of address
        indata += [self.cmd_data_map[0 & 0x1f]]  # Last 5 bits of address
        for data_words in data:
            indata += [self.cmd_data_map[data_words >> 5]]  # First 5 bits of data
            indata += [self.cmd_data_map[(data_words) & 0x1f]]  # Last 5 bits of data
            if sync_counter > 32:
                indata += self.write_sync(write=False)
                sync_counter = 0
            sync_counter += 1
        if write:
            self.write_command(indata)
        return indata

    def write_null(self, write=True):
        indata = [self.CMD_NULL] * 2  # [0b01101001]
        if write:
            self.write_command(indata)
        return indata

    def write_ecr(self, write=True):
        indata = [self.CMD_CLEAR]
        indata += [self.cmd_data_map[self.chip_id]]
        if write:
            self.write_command(indata)
        return indata

    def write_bcr(self, write=True):
        self.log("Write BCR not yet implemented")
        return 0

    def write_sync(self, write=True, repetitions=1):
        indat = [0b10000001, 0b01111110]
        if write:
            self.write_command(indat, repetitions=repetitions)
        return indat

    def write_sync_01(self, write=True, repetitions=1):
        indata = [0b10101010]
        indata += [0b10101010]
        if write:
            self.write_command(indata, repetitions=repetitions)
        return indata

    def _write_reset(self, write=True, repetitions=300):
        indata = [0xff] * 10
        indata += [0x00] * 10
        if write:
            self.write_command(indata, repetitions=repetitions)
        return indata

    def send_trigger(self, trigger, tag=0, write=True):
        # Trigger is always followed by 5 Data bits
        indata = [self.trigger_map[trigger]]
        indata += [self.cmd_data_map[tag]]
        if write:
            self.write_command(indata)
        return indata

    def _send_trigger_read(self, trigger_tag, write=True):
        # Trigger is always followed by 5 Data bits
        indata = [self.CMD_READ_TRIGGER]
        indata += [self.cmd_data_map[trigger_tag]]
        if write:
            self.write_command(indata)
        return indata

    def send_trigger_tag(self, trigger, trigger_tag, write=True):
        if trigger == 0:
            self.log.error("Illegal trigger number")
            return
        else:
            indata = [self.trigger_map[trigger]]
            indata += [self.cmd_data_map[trigger_tag]]
            if write:
                self.write_command(indata)
            return indata

    def write_command(self, data, repetitions=1, wait_for_done=True, wait_for_ready=False):
        '''
            Write data to the command encoder.

            Parameters:
            ----------
                data : list
                    Up to [get_cmd_size()] bytes
                repetitions : integer
                    Sets repetitions of the current request. 1...2^16-1. Default value = 1.
                wait_for_done : boolean
                    Wait for completion after sending the command. Not advisable in case of repetition mode.
                wait_for_ready : boolean
                    Wait for completion of preceding commands before sending the command.
        '''
        if isinstance(data[0], list):
            for indata in data:
                self.write_command(indata, repetitions, wait_for_done, wait_for_ready)
            return

        assert (0 < repetitions < 65536), "Repetition value must be 0<n<2^16"
        if repetitions > 1:
            self.log.debug("Repeating command %i times." % (repetitions))

        if wait_for_ready:
            while (not self.bdaq['cmd'].is_done()):
                pass

        self.bdaq['cmd'].set_data(data)
        self.bdaq['cmd'].set_size(len(data))
        self.bdaq['cmd'].set_repetitions(repetitions)
        self.bdaq['cmd'].start()
        # self.command_counter += (len(data) * repetitions)
        # self.log.info("Sent Commands: %i ", self.command_counter)
        if wait_for_done:
            while (not self.bdaq['cmd'].is_done()):
                pass

    def send_global_pulse(self, bitnames, pulse_width, global_pulse_route_file=None, write=True):
        '''
            Used to set a single or multiple bits of GLOBAL_PULSE_ROUTE register by name, using names defined in rd53a_global_pulse_route.yaml

            Parameters:
            ----------
                bitnames : str or list of str
                    Name(s) of the bit(s) to be set to 1
        '''

        if not global_pulse_route_file:
            global_pulse_route_file = os.path.join(self.proj_dir, 'chips' + os.sep + 'ITkPixV1_global_pulse_route.yaml')

        with open(global_pulse_route_file, 'r') as infile:
            global_pulse_route_map = yaml.full_load(infile)

        if type(bitnames) == str:
            bitnames = [bitnames]

        listofnames = [bit['name'] for bit in global_pulse_route_map]

        for bitname in bitnames:
            if bitname not in listofnames:
                self.log.warning('Could not find %s in possible values for register GLOBAL_PULSE_ROUTE' % bitname)

        val = ''
        for i in range(16):
            if global_pulse_route_map[i]['name'] in bitnames:
                val += '1'
            else:
                val += '0'

        val = '0b' + val[::-1]
        indata = []
        if self.registers['GlobalPulseConf'].get() != int(val, 2):
            indata += self.registers['GlobalPulseConf'].get_write_command(int(val, 2))
        indata += self.write_global_pulse(width=pulse_width)
        if write:
            self.write_command(indata)

        return indata

    def reset(self):
        if self.bdaq.board_version == 'SIMULATION':
            return

        #  Set all registers which are allowed to reset (reset flag == 1) to default.
        self.registers.reset_all()
        self.send_global_pulse(bitnames='reset_monitor_data', pulse_width=0xff)  # Reset Monitor Data
        for reg, val in self.configuration['registers'].items():
            if self.registers[reg]['reset'] == 1:
                self.registers[reg].write(val)

        # Set all masks to default
        self.masks.reset_all()
        self.masks.update(force=True)

        self.enable_core_col_clock(range(54))  # Enable clock on full chip
        self.enable_macro_col_cal(range(54))  # Enable analog calibration on full chip
        self._enable_core_col_reset(range(54))  # Reset core columns
        self.write_ecr(write=True)  # Reset of end-of-column logic
        self.set_low_power_mode()  # Reduce (digital) power consumption

    def set_low_power_mode(self):
        ''' Reduce (digital) power consumption
        '''
        self._reset_tot(write=True)  # Reset all ToT memories to 0.

    def _reset_tot(self, write=False):
        ''' Mitigate ToT-Latch issue in RD53B.
            Set ToT values in all ToT memories to 0 via digital injection.
            Prevents high (digital) current consumption.
        '''

        indata = self.write_sync(write=False)
        if self.reset_tot_latches:
            trig_del = self.registers['TriggerConfig'].get()
            cal_conf = self.registers['CalibrationConfig'].get()
            indata += self.registers['TriggerConfig'].get_write_command(511)
            indata += self.registers['CalibrationConfig'].get_write_command(0b10000000)
            indata += self.write_cal(cal_edge_mode=1, cal_edge_width=2, cal_edge_dly=0, write=False) * 30  # Injection
            indata += self.write_sync(write=False)
            indata += self.registers['TriggerConfig'].get_write_command(trig_del)
            indata += self.registers['CalibrationConfig'].get_write_command(cal_conf)
        if write:
            self.write_command(indata, repetitions=1)
        return indata

    def use_programmed_configuration(self):
        ''' Give corret keys (magic numbers) to registers that control the global configuration
            such that the programmed configuration (instead of hard-wired configuration) is used.
        '''
        self.registers['GCR_DEFAULT_CONFIG'].write(0xac75)
        self.registers['GCR_DEFAULT_CONFIG_B'].write(0x538a)

    def generate_trigger_command(self, trigger_pattern):
        pattern = str(bin(trigger_pattern))[2:]
        cmd = []
        for tag, i in enumerate(range(math.ceil(len(pattern) / 4))):
            pat = pattern[i * 4:i * 4 + 4]
            cmd += self.send_trigger_tag(int(pat, 2), trigger_tag=tag, write=False)
        return cmd

    def setup_digital_injection(self, fine_delay=0):
        indata = self.write_sync(write=False) * 10
        self.registers['CalibrationConfig'].set(int('0b10' + format(fine_delay, '06b'), 2))  # Enable digital injection with zero delay
        indata += self.registers['CalibrationConfig'].get_write_command(int('0b10' + format(fine_delay, '06b'), 2))  # Enable digital injection with zero delay
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=4, cal_edge_dly=2, write=False)  # Injection
        indata += self.write_sync(write=False) * 10
        self.write_command(indata)

    def inject_digital(self, cal_edge_width=4, cal_edge_dly=2, latency=124, wait_cycles=200, trigger_pattern=0xffffffff, repetitions=1):
        '''
            Injects a digital pulse in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger, accepts values [117:124]
                repetitions : int
                    Number of times, the injection command is repeated, i.e. number of injections
        '''
        indata = self.write_sync(write=False) * 10
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=cal_edge_width, cal_edge_dly=cal_edge_dly, write=False)  # Injection
        indata += self.write_sync(write=False) * latency  # Wait for latency
        indata += self.generate_trigger_command(trigger_pattern)
        if self.bdaq.board_version != 'SIMULATION':
            indata += self._reset_tot()
            indata += self.write_sync(write=False) * wait_cycles  # Wait for data
        self.write_command(indata, repetitions=repetitions)
        return indata

    def setup_analog_injection(self, vcal_high, vcal_med, fine_delay=8, injection_mode=0, write=True):
        indata = self.write_sync(write=False) * 10

        self.registers['CalibrationConfig'].set(int('0b0' + format(injection_mode, '01b') + format(fine_delay, '06b'), 2))  # Enable analog injection in uniform mode with configured fine delay
        self.registers['VCAL_HIGH'].set(vcal_high)  # Set VCAL_HIGH
        self.registers['VCAL_MED'].set(vcal_med)  # Set VCAL_MED
        indata += self.registers['CalibrationConfig'].get_write_command(int('0b0' + format(injection_mode, '01b') + format(fine_delay, '06b'), 2))  # Enable analog injection in uniform mode with configured fine delay
        indata += self.registers['VCAL_HIGH'].get_write_command(vcal_high)  # Set VCAL_HIGH
        indata += self.registers['VCAL_MED'].get_write_command(vcal_med)  # Set VCAL_MED

        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_aux_value=0, cal_edge_dly=0, write=False)  # CalEdge -> 0
        indata += self.write_sync(write=False) * 10
        indata += self._reset_tot()
        if write:
            self.write_command(indata)
        return indata

    def revive(self):
        self.write_ecr()
        self.write_ecr()
        for _ in range(10):
            time.sleep(0.01)  # Needed to prevent non empty FIFO, since chip send data after ECR
            if self.bdaq['FIFO'].get_FIFO_SIZE() == 0:
                break

    def inject_analog_single(self, repetitions=1, latency=121, wait_cycles=300, send_ecr=False, trigger_pattern=0xffffffff, send_trigger=True, write=True):
        '''
            Injects a single analog pulse in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger
        '''

        indata = self.write_sync(write=False)
        indata += self.write_cal(cal_edge_mode=0, cal_edge_width=1, cal_edge_dly=0, cal_aux_value=0, write=False)  # CalEdge -> 1 (inject)
        indata += self.write_sync(write=False) * latency  # Wait for latency
        if send_trigger:
            indata += self.generate_trigger_command(trigger_pattern)
        indata += self.write_sync(write=False)
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, cal_aux_value=0, write=False)  # CalEdge -> 0
        if self.bdaq.board_version != 'SIMULATION':
            indata += self._reset_tot()
            indata += self.write_sync(write=False) * wait_cycles  # Wait for data
        if write:
            self.write_command(indata, repetitions=repetitions)
        return indata

    def inject_analog_double(self, repetitions=1, latency=124, wait_cycles=400, trigger_pattern=0xffffffff, send_trigger=True, write=True):
        '''
            Injects a two consecutive analog pulses in a short time in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger
        '''

        indata = self.write_sync(write=False)
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=30, cal_edge_dly=16, cal_aux_value=1, cal_aux_dly=30, write=False)  # CalEdge -> 1 (inject) 25-->19
        indata += self.write_sync(write=False) * latency  # Wait for latency
        if send_trigger:
            indata += self.generate_trigger_command(trigger_pattern)
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, cal_aux_value=0, write=False)  # CalEdge -> 0
        if self.bdaq.board_version != 'SIMULATION':
            indata += self._reset_tot()
            indata += self.write_sync(write=False) * wait_cycles  # Wait for data

        if write:
            self.write_command(indata, repetitions=repetitions)
        return indata

    def toggle_output_select(self, repetitions=1, latency=122, wait_cycles=400, fine_delay=8, send_ecr=False):
        '''
            Toggles between digital and analog injection. If the comparator is stuck high
            this creates a transient on hit_t and thus a hit.

            Note
            ----
            Chip implementation:
            assign hit_t = EnDigHit ? (CalEdge & cal_en) : hit ;
            assign HitOut = hit_t & hit_en;

            See also Figure 29 in RD53 manual.

            Parameters:
            ----------
                latency : int
                    Number of write_sync commands between injection and trigger, accepts values [117:124]
        '''

        # Enable digital injection = falling edge for stuck high pixels
        indata = self.registers['CalibrationConfig'].get_write_command(0x80 + fine_delay)
        indata += self.write_sync(write=False) * 10
        # Enable analog injection, this creates a rising edge if comparator output is stuck high
        indata += self.registers['CalibrationConfig'].get_write_command(fine_delay)
        indata += self.write_sync(write=False) * latency  # Wait for latency
        indata += self.generate_trigger_command(0xffffffff)
        indata += self.write_sync(write=False) * wait_cycles
        self.write_command(indata, repetitions=repetitions)

    def enable_core_col_clock(self, core_cols=None, write=True):
        '''
            Enable clocking of given core columns. After POR everything is disabled.

            ----------
            Parameters:
                core_cols : list of int
                    A list of core columns to enable. Default = None disables everything
        '''

        indata = self._enable_ccolwise_registers("EnCoreCol", core_cols=core_cols, write=False)
        indata += self.write_sync() * 2
        indata += self._enable_core_col_reset(core_cols=core_cols, write=False)
        if write:
            self.write_command(indata)
        return indata

    def enable_macro_col_cal(self, macro_cols=None, write=True):
        '''
            Enable analog calibration of given macro (double-) columns. After POR everything is enabled.

            ----------
            Parameters:
                macro_cols : list of int
                    A list of macro columns to enable. Default = None disables everything
        '''

        indata = self._enable_ccolwise_registers("EnCoreColumnCalibration", core_cols=macro_cols, write=write)
        return indata

    def _enable_core_col_reset(self, core_cols=None, write=True):
        '''
            Enable reset of given core columns.

            ----------
            Parameters:
                core_cols : list of int
                    A list of core columns to enable. Default = None disables everything
        '''

        indata = self._enable_ccolwise_registers("EnCoreColumnReset", core_cols=core_cols, write=write)
        return indata

    def _enable_col_precision_tot(self, core_cols=None, write=True):
        '''
            Enable precision tot of given core columns.

            ----------
            Parameters:
                core_cols : list of int
                    A list of core columns to enable. Default = None disables everything
        '''

        indata = self._enable_ccolwise_registers("PrecisionToTEnable", core_cols=core_cols, write=write)
        return indata

    def _enable_hitor_mask(self, core_cols=None, write=True):
        '''
            Enable Hitor lanes of given core columns.

            ----------
            Parameters:
                core_cols : list of int
                    A list of core columns to enable. Default = None disables everything
        '''

        indata = self._enable_ccolwise_registers("HITOR_MASK", core_cols=core_cols, write=write)
        return indata

    def _enable_ccolwise_registers(self, register, core_cols=None, write=True):
        '''
            Enable Registers that have to be enabled per ccol.
            Valid registers are [EnCoreCol, EnCoreColumnCalibration, EnCoreColumnReset, PrecisionToTEnable]

            ----------
            Parameters:
                core_cols : list of int
                    A list of core columns to enable. Default = None disables everything
        '''

        if register not in ["EnCoreCol", "EnCoreColumnCalibration", "EnCoreColumnReset", "PrecisionToTEnable", "HITOR_MASK"]:
            self.log.warning("Columwise register does not exist. Choose one of the followig: [EnCoreCol, EnCoreColumnCalibration, EnCoreColumnReset, PrecisionToTEnable, HITOR_MASK]")

        # Disable everything
        indata = self.write_sync(write=False)
        indata += self.registers[register + '_0'].get_write_command(0x0000)
        indata += self.registers[register + '_1'].get_write_command(0x0000)
        indata += self.registers[register + '_2'].get_write_command(0x0000)
        indata += self.registers[register + '_3'].get_write_command(0b000000)
        if core_cols:
            indata += self.write_sync(write=False)
            for r in self.core_regs:
                bits = []
                for i, core_col in enumerate(r['core_columns']):
                    if core_col in core_cols:
                        bits.append(i)

                bits = list(set(bits))  # Remove duplicates

                data = ''
                for i in range(len(r['core_columns'])):
                    data += '1' if i in bits else '0'
                indata += self.registers[r[register]].get_write_command(int(data[::-1], 2))

        if write:
            self.write_command(indata)

        return indata

    def enable_monitor_data(self):
        self.registers['AutoRead0'].write(511)
        self.registers['AutoRead1'].write(511)
        self.registers['AutoRead2'].write(511)
        self.registers['AutoRead3'].write(511)
        self.registers['AutoRead4'].write(511)
        self.registers['AutoRead5'].write(511)
        self.registers['AutoRead6'].write(511)
        self.bdaq.set_monitor_filter(receivers=self.receiver, mode='filter')
        self.registers['ServiceDataConf'].write(0x1ff)
        self.log.debug('Monitor data enabled')

    def disable_monitor_data(self):
        self.registers['ServiceDataConf'].write(int('0b000110010', 2))
        self.bdaq.set_monitor_filter(receivers=self.receiver, mode='block')
        self.log.debug('Monitor data disabled')

    def get_ADC_value(self, name, measure):
        """
        Sends multiplexer settings and reads ADC measurement
        Output is in ADC LSB.

        Parameters:
        ----------
            name: str
                The name of the ADC register to be measured
            measure: str
                Either 'current', 'i', 'voltage' or 'v'; addresses the corresponding multiplexer

        Returns:
        ----------
            ADC value
        """
        # Helper variables
        _measure, _current, _voltage = measure.lower(), ('current', 'i'), ('voltage', 'v')

        if _measure not in _current + _voltage:
            raise ValueError("*measure* must be either 'voltage' ('v') or 'current' ('i')")

        try:
            if _measure in _current:
                bitstring = int('1' + format(self.current_mux[name], '06b') + format(1, '06b'), 2)
            else:
                bitstring = int('1' + format(32, '06b') + format(self.voltage_mux[name], '06b'), 2)
        except KeyError:
            raise ValueError("Invalid ADC register name {} for {} measurement".format(name, _measure))

        # Perform measurement and collect result
        val_adc = self._set_mux_read_adc(mux_val=bitstring)

        # ADC to physical value conversion
        val_real = None

        # FIXME: calibration for current measurement is missing
        if _measure in _voltage:
            val_real = self.calibration.get_V_from_ADC(val_adc)

        return val_adc, val_real

    def _set_mux_read_adc(self, mux_val):
        """

        Parameters
        ----------
        mux_val: int
            Value to write to multiplexer

        Returns
        -------
        ADC value LSB
        """
        for n in range(2):
            # Reset ADC
            self.send_global_pulse(bitnames='reset_adc', pulse_width=0xff)

            # Select MUX
            self.registers['MonitorConfig'].write(mux_val)

            # Start ADC conversion
            self.send_global_pulse(bitnames='adc_start_conversion', pulse_width=0xff)  # Start ADC

        # Read and return ADC result
        return self.registers['MonitoringDataADC'].read()

    def get_chip_status(self):
        """
            Returns a map of all important chip parameters.
            Can only be called before or after a scan!
        """

        self.log.info('Recording chip status...')
        try:

            # Get voltages and currents
            voltages = {v_mon: self.get_ADC_value(v_mon, measure='v') for v_mon in self.voltage_mux if v_mon != 'VREF_CORE'}
            currents = {c_mon: self.get_ADC_value(c_mon, measure='i') for c_mon in self.current_mux}

        except RuntimeError as e:
            self.log.error('There was an error while receiving the chip status: %s' % e)
            voltages, currents = {}, {}

        return voltages, currents

    def get_ring_oscillators(self, pulse_width=30):
        '''
            Returns data of ring oscillators:
            {
                RING_OSC_i: {
                    raw_data: Raw register data, consisting of a cycle counter and the actual counter,
                    counter: Value of the actual counter (last 12 bits of register value),
                    frequency: Counter value devided by pulse length. In Hz.
                }
            }
        '''

        oscillators = {}
        bank = 'A'
        for i in range(8):
            osc_data = {}
            raw_data = self._get_single_ring_oscillator(bank=bank, number=i, pulse_width=pulse_width)
            counter = int('{0:016b}'.format(raw_data)[-12:], 2)
            cycles = int('{0:016b}'.format(raw_data)[-16:-12], 2)
            frequency = counter / (cycles * 2 * pulse_width) * 40e6

            osc_data['raw_data'] = raw_data
            osc_data['counter'] = counter
            osc_data['cycles'] = cycles
            osc_data['frequency'] = frequency

            oscillators['RING_OSC_' + str(bank) + '_' + str(i)] = osc_data
        bank = 'B'
        for i in range(34):
            osc_data = {}
            raw_data = self._get_single_ring_oscillator(bank=bank, number=i, pulse_width=pulse_width)
            counter = int('{0:016b}'.format(raw_data)[-12:], 2)
            cycles = int('{0:016b}'.format(raw_data)[-16:-12], 2)
            frequency = counter / (cycles * 2 * pulse_width) * 40e6

            osc_data['raw_data'] = raw_data
            osc_data['counter'] = counter / cycles
            osc_data['cycles'] = cycles
            osc_data['frequency'] = frequency

            oscillators['RING_OSC_' + str(i)] = osc_data

        return oscillators

    def _get_single_ring_oscillator(self, bank='A', number=0, pulse_width=30):
        self.write_command(self.write_sync(write=False) * 300)
        self.registers['RingOscConfig'].write(0x7fff)
        self.registers['RingOscConfig'].write(0x3eff)
        if bank == 'A':
            bitstring = int(format(number, '03b') + format(0, '06b'), 2)
        elif bank == 'B':
            bitstring = int(format(0, '03b') + format(number, '06b'), 2)
        else:
            self.log.error('Bank %s doe not exist.' % bank)
        self.registers['RingOscRoute'].write(bitstring)

        self.send_global_pulse('start_ringosc_' + (bank.lower()), pulse_width=pulse_width)
        self.write_command(self.write_sync(write=False) * 40)
        raw_data = self.registers['RING_OSC_' + str(bank) + '_OUT'].read()
        return raw_data

    def get_temperature_sensors(self, samples=1, log=True):
        sensor_array = ['TEMPSENS_A', 'TEMPSENS_D', 'TEMPSENS_C']  # 'TEMPSENS_T', 'TEMPSENS_B',
        temp = {}
        try:
            for sensor in sensor_array:
                temp[sensor] = {}
                if sensor in ['TEMPSENS_A', 'TEMPSENS_D', 'TEMPSENS_C']:
                    temp[sensor]['temp'], temp[sensor]['dADC'] = self._get_diode_temperature_sensor(sensor, samples)
                    self.log.debug('Sensor: {0}\nTemperature: {1}\nADC: {2}'.format(sensor, temp[sensor]['temp'], temp[sensor]['dADC']))
                # elif sensor in ['TEMPSENS_T', 'TEMPSENS_B']:
                #     temp[sensor]['temp'], temp[sensor]['dADC'] = self._get_resistive_temperature_sensor(sensor, samples)
                    # self.log.debug('Sensor: {0}\nTemperature: {1}\nADC: {2}'.format(sensor, temp[sensor]['temp'], temp[sensor]['dADC']))
            mean_temp = round(np.mean([val['temp'] for val in temp.values()]), 0)
            t_error = 3 if self.calibration.t_sensors_calibrated else 6
            if log:
                self.log.info('Mean chip temperature is ({0} +- {1})°C'.format(int(mean_temp), t_error))

        except RuntimeError as e:
            temp = {0: -99, 1: -99, 2: -99, 3: -99}
            mean_temp = -99
            self.log.error('There was an error reading temperature sensors: %s' % e)
        return mean_temp, temp

    def _get_resistive_temperature_sensor(self, sensor, samples, log=True):
        lower = []
        sensor_id_dict = {'TEMPSENS_T': 0, 'TEMPSENS_B': 1}
        before_value = self.registers['MON_ADC'].get()
        self.get_ADC_value(sensor, measure='v')[0]
        for reps in range(samples):
            if sensor == 'TEMPSENS_T':
                self.registers['MON_ADC'].write(0x80 + (before_value & 0x3f))
            elif sensor == 'TEMPSENS_B':
                self.registers['MON_ADC'].write(0x100 + (before_value & 0x3f))
            self.write_sync()
            lower.append(self.get_ADC_value(sensor, measure='v')[0])
        self.registers['MON_ADC'].write(0x40 + (before_value & 0x3f))
        mean_adc = int(np.mean(lower))
        mean_temp = self.calibration._get_temperature_from_ADC_resistive_sensors(mean_adc, sensor=sensor_id_dict[sensor])
        if log:
            self.log.info('Mean chip temperature is ({0} +- 6)°C'.format(int(mean_temp)))
        return mean_temp, mean_adc

    def _get_diode_temperature_sensor(self, sensor, log=True):
        lower, upper = [], []
        sensor_id_dict = {'TEMPSENS_A': 0, 'TEMPSENS_D': 1, 'TEMPSENS_C': 2}
        self.get_ADC_value(sensor, measure='v')[0]  # Read adc once for proper reset
        for diode_current in range(0, 16, 1):
            if sensor == 'TEMPSENS_A':
                bitstring_SLDO = int('0' + format(0, '04b') + '01' + format(diode_current, '04b') + '0', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_D':
                bitstring_SLDO = int('1' + format(diode_current, '04b') + '00' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_C':
                bitstring_SLDO = int('0' + format(0, '04b') + '00' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('1' + format(diode_current, '04b') + '0', 2)
            self.registers['MON_SENS_SLDO'].write(bitstring_SLDO)
            self.registers['MON_SENS_ACB'].write(bitstring_ACB)
            self.write_sync()
            lower.append(self.get_ADC_value(sensor, measure='v')[0])

            if sensor == 'TEMPSENS_A':
                bitstring_SLDO = int('0' + format(0, '04b') + '01' + format(diode_current, '04b') + '1', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_D':
                bitstring_SLDO = int('1' + format(diode_current, '04b') + '10' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('0' + format(0, '04b') + '0', 2)
            elif sensor == 'TEMPSENS_C':
                bitstring_SLDO = int('0' + format(0, '04b') + '00' + format(0, '04b') + '0', 2)
                bitstring_ACB = int('1' + format(diode_current, '04b') + '1', 2)
            self.registers['MON_SENS_SLDO'].write(bitstring_SLDO)
            self.registers['MON_SENS_ACB'].write(bitstring_ACB)
            self.write_sync()
            upper.append(self.get_ADC_value(sensor, measure='v')[0])
        mean_adc = int(np.mean(upper) - np.mean(lower))
        mean_temp = self.calibration.get_temperature_from_ADC(mean_adc, sensor=sensor_id_dict[sensor])
        if log:
            self.log.info('Mean {0} temperature is ({1} +- 6)°C'.format(sensor, int(mean_temp)))
        return mean_temp, mean_adc

    def get_temperature_NTC(self, log=True):
        T = self.bdaq.get_temperature_NTC(self.receiver)
        if log:
            self.log.info('NTC temperature is {0:1.2f}°C'.format(T))
        return T

    def get_temperature(self, log=True):
        if self.bdaq.enable_NTC:
            return self.get_temperature_NTC(log=log)
        else:
            return self.get_temperature_sensors(log=log)[0]

    def get_flavor(self, col):
        return get_flavor(col)

    def get_tdac_range(self, fe):
        return get_tdac_range(fe)

    def enable_ptot(self):
        ''' Enable and configure PTOT mode.
        '''
        ptot_del = 110
        self.log.info('Enabling PTOT mode')
        self._enable_col_precision_tot(range(0, 54))
        # Set enable bits for PToT, PToA and configure PToT latency
        self.registers['ToTConfig'].write(int(format(12, '04b') + format(ptot_del, '09b'), 2))
        self.registers['TriggerConfig'].write(511)
        self.ptot_enabled = True

    def disable_ptot(self):
        ''' Disable PTOT mode and reset trigger and ToT config.
        '''
        self.log.info('Disabling PTOT mode')
        self._enable_col_precision_tot(None)
        self.registers['ToTConfig'].write(int(format(0, '04b') + format(500, '09b'), 2))
        self.registers['TriggerConfig'].reset()
        self.ptot_enabled = False

    def write_trigger_latency(self, latency):
        self.registers['TriggerConfig'].write(latency)

    def get_trigger_latency(self):
        return self.registers['TriggerConfig'].get()


if __name__ == '__main__':
    ITkPixV1_chip = ITkPixV1()
    ITkPixV1_chip.init()
