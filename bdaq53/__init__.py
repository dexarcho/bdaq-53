import sys

__version__ = '1.9.0'

if sys.version_info[0] < 3:
    raise Exception("This version of BDAQ53 requires Python 3! See https://gitlab.cern.ch/silab/bdaq53/wikis/User-guide/Installation")
