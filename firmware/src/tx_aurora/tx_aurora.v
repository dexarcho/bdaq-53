/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps

module tx_aurora
#(
) (
    input wire BUS_CLK,
    input wire AURORA_SER_CLK,

    input wire [79:0] TX_CONTROL,
    input wire RX_CHANNEL_UP,
    output wire [3:0] DATA_MERGING_OUTPUT,
    output wire [3:0] DATA_MERGING_ENABLE,
    output wire AURORA_SER_CLK_OUT
);

// Aurora transmitter
localparam ActiveLanes = 4'b0001;
localparam enable_prbs = 1'b0;
localparam send_alt_output = 1'b0;
localparam aurora_alt_output = 20'h0;
localparam InitWait = 11'h20;
localparam CBWait = 20'hff;
localparam CBSend = 4'd1;
localparam CCWait = 6'd25;
localparam CCSend = 2'd3;
reg [3:0][19:0] tx_data;
wire [3:0][63:0] DataEOC;
wire [7:0] DataMask;
wire DataEOC_empty, DataEOC_read;
wire [3:0][63:0] MonitorDataEOC;
wire MonitorDataEOC_empty, MonitorDataEOC_read;

wire clkfbout;

assign DataMask = 8'h03;
wire INC_ENABLE = TX_CONTROL[78];
wire INC_SET = TX_CONTROL[79];
wire INC_ENABLE_sync;
wire dm_reset;

pulse_gen_rising i_pulse_gen_rising_data_merging_phase(.clk_in(AURORA_SER_CLK), .in(INC_ENABLE), .out(INC_ENABLE_sync));

  MMCME2_ADV
   #(.BANDWIDTH            ("OPTIMIZED"),
     .CLKOUT4_CASCADE      ("FALSE"),
     .COMPENSATION         ("ZHOLD"),
     .STARTUP_WAIT         ("FALSE"),
     .DIVCLK_DIVIDE        (1),
     .CLKFBOUT_MULT_F      (2),
     .CLKFBOUT_PHASE       (0.000),
     .CLKFBOUT_USE_FINE_PS ("FALSE"),
     .CLKOUT0_DIVIDE_F     (2),
     .CLKOUT0_PHASE        (0.000),
     .CLKOUT0_DUTY_CYCLE   (0.500),
     .CLKOUT0_USE_FINE_PS  ("TRUE"),
     .CLKIN1_PERIOD        (3.125),
     .CLKOUT1_DIVIDE       (1),
     .CLKOUT1_PHASE        (0.000),
     .CLKOUT1_DUTY_CYCLE   (0.500),
     .CLKOUT1_USE_FINE_PS  ("FALSE"),
     .CLKOUT2_DIVIDE       (1),
     .CLKOUT2_PHASE        (0.000),
     .CLKOUT2_DUTY_CYCLE   (0.500),
     .CLKOUT2_USE_FINE_PS  ("FALSE"),
     .CLKOUT3_DIVIDE       (1),
     .CLKOUT3_PHASE        (0.000),
     .CLKOUT3_DUTY_CYCLE   (0.500),
     .CLKOUT3_USE_FINE_PS  ("FALSE"),
     .REF_JITTER1          (0.010))
   mmcm_adv_inst_tx
     // Output clocks
    (.CLKFBOUT            (clkfbout),
     .CLKOUT0             (AURORA_SER_CLK_OUT),
     .CLKOUT1             (),
     .CLKOUT2             (),
     .CLKOUT3             (),
      // Input clock control
     .CLKFBIN             (clkfbout),
     .CLKIN1              (AURORA_SER_CLK),
     .CLKIN2              (1'b0),
      // Tied to always select the primary input clock
     .CLKINSEL            (1'b1),
     // Ports for dynamic reconfiguration
     .DADDR               (7'h0),
     .DCLK                (1'b0),
     .DEN                 (1'b0),
     .DI                  (16'h0),
     .DO                  (),
     .DRDY                (),
     .DWE                 (1'b0),
     // Ports for dynamic phase shift
     .PSCLK               (AURORA_SER_CLK),
     .PSEN                (INC_ENABLE_sync),
     .PSINCDEC            (INC_SET),
     .PSDONE              (),
     // Other control and status signals
     .LOCKED              (locked_i),
     .CLKINSTOPPED        (),
     .CLKFBSTOPPED        (),
     .PWRDWN              (1'b0),
     .RST                 (dm_reset));




// Slow word clock for the TX core (320/20=16 MHz)
clock_divider #(
    .DIVISOR(20)
) i_clock_divisor_aur (
    .CLK(AURORA_SER_CLK_OUT),
    .RESET(1'b0),
    .CE(),
    .CLOCK(AURORA_TX_CLK)
);

// ------------------------
// RD53B Aurora transmitter
// ------------------------
AuroraMultilaneTop #( .IW_WIDTH(11), .CCW_WIDTH(16), .CCS_WIDTH(2) , .CBW_WIDTH(20), .CBS_WIDTH(4))
  AuroraMultilaneTop (
    .Clk             (                     AURORA_TX_CLK ),
    .Rst_b           (        (|RX_CHANNEL_UP) | dm_reset), // Synchronized reset for Aurora (Active Low)

    .ActiveLanes     (                  ActiveLanes[3:0] ), // Lanes active in the Aurora link
    .EnablePRBS      (                       enable_prbs ), // Pseudo-Random Bit Stream

    .DataEOC         (                           DataEOC ), // wire [3:0][63:0] data_lane_align
    .DataMask        (                          DataMask ),
    .DataEOC_empty   (                     DataEOC_empty ),
    .DataEOC_EOF     (                     DataEOC_empty ),
    .DataEOC_read    (                      DataEOC_read ),

    .Monitor         (                    MonitorDataEOC ), // Data coming from ServiceData block (has higher priority than normal data)
    .Monitor_empty   (              MonitorDataEOC_empty ),
    .Monitor_read    (               MonitorDataEOC_read ),

    .ToSerializer20  (                           tx_data ), // wire [3:0][19:0] tx_data1G
    .SerializerLock  (                              1'b1 ), // There is no Lock signal coming from the PLL

    .InitWait        (                    InitWait[10:0] ), // 
    .CBWait          (                      CBWait[19:0] ), //  Aurora Channel Bonding configuration bits
    .CBSend          (                       CBSend[3:0] ), //  Aurora Channel Bonding configuration bits
    .CCWait          (              {CCWait[5:0], 10'b0} ),
    .CCSend          (                       CCSend[1:0] )
    );

wire [63:0] dm_tx_data;
wire dm_start, dm_start_pulse, dm_start_pulse_sync, dm_invert, dm_type;
wire [2:0] state;

localparam MONITOR = 1'b0;
localparam HITDATA = 1'b1;

assign DATA_MERGING_ENABLE = TX_CONTROL[75:72];  // controls the top level IOBUFDS
assign state = TX_CONTROL[71:69];
assign dm_invert = TX_CONTROL[67];
assign dm_reset = TX_CONTROL[66];
assign dm_type = TX_CONTROL[65];
assign dm_start = TX_CONTROL[64];
assign dm_tx_data = TX_CONTROL[63:0];

assign MonitorDataEOC = (dm_type == 1'b0) ? {dm_tx_data, dm_tx_data, dm_tx_data, dm_tx_data} : 0;

assign DataEOC = (dm_type == 1'b1) ? {dm_tx_data, dm_tx_data, dm_tx_data, dm_tx_data} : 0;

pulse_gen_rising i_pulse_gen_rising_data_merging(.clk_in(BUS_CLK), .in(dm_start), .out(dm_start_pulse));

flag_domain_crossing i_rst_flag_domain_crossing_data_merging (
    .CLK_A(BUS_CLK),
    .CLK_B(AURORA_TX_CLK),
    .FLAG_IN_CLK_A(dm_start_pulse),
    .FLAG_OUT_CLK_B(dm_start_pulse_sync)
);

// Single word monitor data handshake
reg dm_handshake = 0;
always @ (posedge AURORA_TX_CLK) begin
    case (dm_handshake)
        0: begin
            if (dm_start_pulse_sync)  // start waiting for fifo
                dm_handshake <= 1;
        end
        1: begin
            if (dm_type == MONITOR ? MonitorDataEOC_read : DataEOC_read)  // return when fifo is ready
                dm_handshake <= 0;
        end
    endcase
end

assign MonitorDataEOC_empty = (dm_type == MONITOR) ? ~dm_handshake : 1;
assign DataEOC_empty = (dm_type == HITDATA) ? ~dm_handshake : 1;


// Shift register for Aurora serialization
reg [3:0][19:0] tx_data_reg;
reg AURORA_TX_CLK_prev;
always @ (posedge AURORA_SER_CLK_OUT) begin
    if (AURORA_TX_CLK & !AURORA_TX_CLK_prev)
        tx_data_reg <= {tx_data[3], tx_data[2], tx_data[1], tx_data[0]};
    else
        tx_data_reg[0] <= tx_data_reg[0] >> 1;
        tx_data_reg[1] <= tx_data_reg[1] >> 1;
        tx_data_reg[2] <= tx_data_reg[2] >> 1;
        tx_data_reg[3] <= tx_data_reg[3] >> 1;
    AURORA_TX_CLK_prev <= AURORA_TX_CLK;
end
assign DATA_MERGING_OUTPUT = dm_invert ? ~{tx_data_reg[0][0], tx_data_reg[0][0], tx_data_reg[0][0], tx_data_reg[0][0]}  // For now, duplicate the output to all 4 outputs
                                       : {tx_data_reg[0][0], tx_data_reg[0][0], tx_data_reg[0][0], tx_data_reg[0][0]};  // Invert the signal if needed

endmodule