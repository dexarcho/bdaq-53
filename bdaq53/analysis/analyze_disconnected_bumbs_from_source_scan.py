#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script analyses interpreted source scan files and generates a bump connectivity map for modules.
    It devides the pixel matrix of each chip into blocks and calculates a minimal occupancy for each block.
    If a pixel exccedes the minimal occupancy of its block but has a low mean ToT it gets marked as having a poor connection.
'''

import numpy as np
import tables as tb
import os
import scipy
import scipy.stats
import scipy.optimize
import warnings

from bdaq53.system import logger
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import plotting
from bdaq53.analysis import combine_module_files
from bdaq53.modules.module_type import ModuleType

log = logger.setup_derived_logger("SrcScanBumpConAna")


def analyze_block(occupancy, allowed_diviation_of_itensity_down=0.4, allowed_diviation_of_itensity_up=4.0, alpha_max_noise_occ=.01, alpha_false_neg=.001, definit_noisy_factor=2, **params):
    '''
        Is called for each block. Estimates the expected occupancy
        and calculates the maximal / minimal ocupancy of disconnected bumps / noisy pixels
        using the specified significance levels of a Poisson distribution for the occupancy of connected bumps.
        Deviations of the estimated expected occupancy are allowed.
        Returns disconnected bumps, pixels with high occupancy, and definite noisy pixels
    '''

    if len(occupancy) == 0:
        return occupancy, occupancy, occupancy

    n_pix = occupancy.size

    occ = np.bincount(occupancy)

    try:
        # search first local minimum
        min_occ = next(i for i in range(1, len(occ)) if occ[i - 1] < occ[i])

        # use most frequent occ as an estimator
        mu = np.bincount(occupancy[occupancy > min_occ]).argmax()
    except Exception:
        mu = 1

    # calculate significance level
    alpha1 = 1 - (1 - alpha_max_noise_occ)**(1 / n_pix)

    def helper(n, mu, alpha):
        try:
            y = au.pixel_hits_prob_root_more_n(n, mu, alpha)
        except Exception:
            y = 1 - alpha
        return y

    # calculate max occ to identify noisy pixel
    mu_max = mu * allowed_diviation_of_itensity_up
    try:
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            max_true_hits = int(scipy.optimize.root_scalar(f=helper, args=(mu_max, alpha1),
                                                           method='secant', x0=mu_max, x1=2 * mu_max).root)
    except Exception:
        max_true_hits = mu_max * 1.5

    noisy_pixels = occupancy > max_true_hits
    very_noisy_pixels = occupancy > max_true_hits * definit_noisy_factor

    # calculate significance level
    alpha2 = 1 - (1 - alpha_false_neg)**(1 / n_pix)

    def helper(n, mu, alpha):
        try:
            y = au.pixel_hits_prob_root_less_n(n, mu, alpha)
        except Exception:
            y = 1 - alpha
        return y

    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        # calculate min occ to identify connected pixel
        mu_min = mu * allowed_diviation_of_itensity_down
        min_true_hits = int(scipy.optimize.root_scalar(f=helper, args=(mu_min, alpha2),
                                                       method='bisect', bracket=[0, mu_min]).root)
    if min_true_hits == 0:
        min_true_hits = 1

    dead_pixels = occupancy < min_true_hits

    return dead_pixels, noisy_pixels, very_noisy_pixels


def get_connectivity_map(occupancy, enabled, mean_tot, block_width=64, block_height=64, masks=None, min_tot_r=None, **params):
    '''
        Returns the connectivity map of a pixel matrix with given occupancy, enable mask and mean ToT
        Devides matrix into blocks with suppplied size, and runs analysis separatly.
        A list of masks defines additional regions that are analysed seperatly.
        Low mean ToT is calculated by supplying a list min_tot_r of tuples (mask, min_tot_ratio)
        while min_tot_ratio defines the minimal ratio of mean ToT of a region given by the mask.
        A pixel with a low mean ToT and expected occupancy is marked as poorly connected
        A pixel with a low mean ToT and high occupancy is marked as noisy
    '''

    disconnected_pixels = np.zeros_like(enabled)
    noisy_pixels = np.zeros_like(enabled)
    very_noisy_pixels = np.zeros_like(enabled)
    color = np.zeros_like(occupancy)

    # skip analysis in provided masks blocks
    enabled_pixels = enabled.copy()
    for mask in masks:
        enabled_pixels[mask] = False

    # perform analysis on blocks
    c_blocks = int(np.floor(occupancy.shape[0] / block_width)) + 1
    r_blocks = int(np.floor(occupancy.shape[1] / block_height)) + 1
    num_blocks = c_blocks * r_blocks

    for i in range(c_blocks):
        for j in range(r_blocks):
            r = (slice(i * block_width, (i + 1) * block_width), slice(j * block_height, (j + 1) * block_height))
            en = np.zeros_like(enabled_pixels)
            en[r] = enabled_pixels[r]

            disconnected_pixels[en], noisy_pixels[en], very_noisy_pixels[en] = analyze_block(occupancy[en], **params)
            color[en] = i * r_blocks + j

    # perform analysis seperatly on provided masks
    for i, mask in enumerate(masks):
        en = np.zeros_like(enabled)
        en[mask] = enabled[mask]

        disconnected_pixels[en], noisy_pixels[en], very_noisy_pixels[en] = analyze_block(occupancy[en], **params)
        color[en] = i + num_blocks

    if min_tot_r is not None:
        # pixes with low mean tot probably have a bad connection
        poorly_connected = np.zeros_like(enabled)
        for r, min_r in min_tot_r:
            min_tot = min_r * np.mean(mean_tot[r][(np.logical_and(np.logical_not(disconnected_pixels), enabled))[r]])
            poorly_connected[r] = np.logical_and(np.logical_and(np.logical_not(disconnected_pixels), enabled), mean_tot < min_tot)[r]

        # pixels with high occ are noisy if it also has low mean tot
        noisy_pixels[np.logical_and(noisy_pixels, np.logical_not(poorly_connected))] = False
        poorly_connected[noisy_pixels] = False
        noisy_pixels[very_noisy_pixels] = True
    else:
        poorly_connected = False

    enabled_pixels[noisy_pixels] = False

    bump_bonds_connection_map = np.zeros_like(occupancy, dtype="f")
    bump_bonds_connection_map[np.logical_not(disconnected_pixels)] = 1
    bump_bonds_connection_map[noisy_pixels] = 2
    bump_bonds_connection_map[poorly_connected] = .5
    bump_bonds_connection_map[np.logical_not(enabled)] = -1

    return bump_bonds_connection_map


def get_connectivity_map_simple(occupancy, enabled, masks=None):
    '''
        returns the connectivity map of a pixel matrix with given occupancy.
        threshold for disconnected pixel is 5% of mean Occ.
    '''

    disconnected_pixels = np.zeros_like(enabled)
    noisy_pixels = np.zeros_like(enabled)

    # skip analyse in on provided masks blocks
    enabled_pixels = enabled.copy()
    for mask in masks:
        enabled_pixels[mask] = False

    mean_occ = np.mean(occupancy)
    print(mean_occ)
    disconnected_pixels = occupancy < 0.05 * mean_occ
    noisy_pixels = occupancy > 4 * mean_occ

    enabled_pixels[noisy_pixels] = False

    bump_bonds_connection_map = np.zeros_like(occupancy, dtype="f")
    bump_bonds_connection_map[np.logical_not(disconnected_pixels)] = 1
    bump_bonds_connection_map[noisy_pixels] = 2
    bump_bonds_connection_map[np.logical_not(enabled)] = -1

    return bump_bonds_connection_map


def analyze_chip(source_scan_file, analog_scan_file=None, customize_connectivity_map=None, min_tot_ratio=None, module_type_name=None, create_plots=False, analyze_id='normal', **params):
    '''
        Analyses source scan of an chip and generates sepertate .h5 and .pdf files of results.
        An analog scan file may be supplied, such that pixels with a bad analog performance are not classified as disconnected.
        Detects regions with a low occupancy or corners with a high occupancy according to module type definition.
        Minimal mean ToT is calculated (for each RD53A flavor separatly).
        Generated connectivity maps may be modified before plotting using customize_connectivity_map.
        Function is called if supplied and may be used to handle edge cases
        where e.g. the resulting maps are clearly wrong due to failures of a module front end.
    '''

    # mark pixels that are not working in analog scan
    if analog_scan_file is not None:
        with tb.open_file(analog_scan_file + '_interpreted.h5', 'r') as in_file:
            occupancy_analog = np.sum(in_file.root.HistOcc[:], axis=2)
        working = occupancy_analog > .5 * np.mean(occupancy_analog)
    else:
        working = True

    with tb.open_file(source_scan_file + '_interpreted.h5', 'r') as in_file:
        occupancy = np.array([[b[0] for b in a] for a in in_file.root.HistOcc[:]])
        enabled_pixels = in_file.root.configuration_in.chip.use_pixel[:]
        tot = au.get_mean_from_histogram(in_file.root.HistTot[:].sum(axis=(2)), np.arange(16), axis=2)
        tot[tot != tot] = 0
        module_settings = au.ConfigDict(in_file.root.configuration_in.chip.module[:])
        chip_settings = au.ConfigDict(in_file.root.configuration_in.chip.settings[:])
        run_config = au.ConfigDict(in_file.root.configuration_in.scan.run_config[:])
        chip_id = chip_settings['chip_id']

    if module_type_name is None:
        module_type_name = module_settings['module_type']
    try:
        module_type = ModuleType(module_type_name, chip_settings["chip_type"])
    except ValueError:
        module_type = None

    if chip_settings["chip_type"].lower in ["rd53a"]:
        if min_tot_ratio is None:
            min_tot_r = ((slice(0, 128), .5), (slice(128, 264), .5), (slice(264, 400), .3))
        else:
            min_tot_r = ((slice(0, 128), min_tot_ratio[0]), (slice(128, 264), min_tot_ratio[1]), (slice(264, 400), min_tot_ratio[2]))
    else:
        if min_tot_ratio is None:
            min_tot_r = ((slice(None), .5),)
        else:
            min_tot_r = ((slice(None), min_tot_ratio),)

    masks = []

    if module_type:
        # Analyse corners seperatly if they have bigger pixels (higher occupancy)
        chip_corners = module_type.get_big_corners(chip_id)
        n = module_type.get_max_corner_size()
        corners = [
            ('left', lambda i, j: (slice(-j, -i if i > 0 else None), slice(None)), "Detected larger pixels in first %i column(s)."),
            ('right', lambda i, j: (slice(i, j), slice(None)), "Detected larger pixels in last %i column(s)."),
            ('top', lambda i, j: (slice(None), slice(-j, -i if i > 0 else None)), "Detected larger pixels in first %i row(s)."),
            ('bottom', lambda i, j: (slice(None), slice(i, j)), "Detected larger pixels in last %i row(s).")
        ]
        for corner, f, log_string in corners:
            if corner in chip_corners:
                for i in range(n, 0, -1):
                    # look for sudden jumps in occupancy
                    if np.mean(occupancy[f(i - 1, i)]) > 1.5 * np.mean(occupancy[f(i, i + 1)]):
                        masks += [f(0, i)]
                        log.info(log_string % (i))

        # Add regions to mask list spezified in module definition which have low occupancys
        for low_occ in module_type.get_low_occupancy(chip_id):
            surround = np.zeros_like(occupancy, dtype="b")
            surround[low_occ[0].start - 10: low_occ[0].stop + 10, low_occ[1].start - 10: low_occ[1].stop + 10] = True
            surround[low_occ] = False
            if np.mean(occupancy[low_occ]) * 2 < np.mean(occupancy[surround]):
                masks += [low_occ]
                log.info("Detected low occupancy at [%i: %i, %i: %i]." % (low_occ[0].start, low_occ[0].stop, low_occ[1].start, low_occ[1].stop))

    if analyze_id == 'simple':
        connectivity_map = get_connectivity_map_simple(occupancy, enabled_pixels, masks=masks)
    else:
        connectivity_map = get_connectivity_map(occupancy, enabled_pixels, tot, masks=masks, min_tot_r=min_tot_r, **params)
    connectivity_map[np.logical_and(np.logical_not(working), enabled_pixels)] = -12

    # customize_connectivity_map may be used to modifie the results
    if customize_connectivity_map is not None:
        customize_connectivity_map(connectivity_map=connectivity_map, occupancy=occupancy, enabled=enabled_pixels, tot=tot, **params)

    conn_state_count = dict(zip(*np.unique(connectivity_map, return_counts=True)))
    num_connected_bumps = conn_state_count.get(1, 0)
    num_poorly_connected_bumps = conn_state_count.get(0.5, 0)
    num_noisy_pixel = conn_state_count.get(2, 0)
    num_disconnected_bumps = conn_state_count.get(0, 0)
    num_non_enabled = conn_state_count.get(-1, 0)
    num_unknown_state_bumps = conn_state_count.get(-12, 0)

    stats = {
        "connected_bumps": num_connected_bumps,
        "poorly_connected_bumps": num_poorly_connected_bumps,
        "disconnected_bumps": num_disconnected_bumps,
        "unknown_state_bumps": num_unknown_state_bumps + num_noisy_pixel,
        "non_enabled_pixels": num_non_enabled
    }
    log.info("Results of chip %s : %s." % (chip_settings["chip_sn"], str(stats)))

    if run_config["scan_id"] == "bump_connection_source_scan":
        results_filename = source_scan_file + "_interpreted"
    else:
        results_filename = source_scan_file + "_bump_connectivity"

        # Copy signifficant tables from interpretted source file
        with tb.open_file(results_filename + ".h5", 'w') as in_file:
            with tb.open_file(source_scan_file + '_interpreted.h5', 'r') as raw_file:
                raw_file.root.HistEventStatus.copy(in_file.root, 'HistEventStatus')
                raw_file.root.HistOcc.copy(in_file.root, 'HistOcc')
                raw_file.root.HistTot.copy(in_file.root, 'HistTot')
                raw_file.root.HistRelBCID.copy(in_file.root, 'HistRelBCID')
                raw_file.root.HistBCIDError.copy(in_file.root, 'HistBCIDError')
                raw_file.root.HistTrigID.copy(in_file.root, 'HistTrigID')
                raw_file.root.configuration_out._f_copy(in_file.root, 'configuration_in', recursive=True)
            in_file.root.configuration_in.scan.run_config.cols.value[0] = "bump_connection_analysis_of_source_scan"

    with tb.open_file(results_filename + ".h5", 'r+') as in_file:
        in_file.root.configuration_in.chip.use_pixel[:] = enabled_pixels
        in_file.create_carray(in_file.root, name='BumpConnectivityMap', title='Bump Connectivity', obj=connectivity_map,
                              filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
        in_file.create_table(in_file.root, name='BumpConnectivityStatistics', title='Bump Connections',
                             obj=np.array([(key, stats[key]) for key in stats.keys()], dtype=[('attribute', 'S64'), ('value', 'i4')]))

    if create_plots:
        with plotting.Plotting(analyzed_data_file=results_filename + ".h5") as p:
            p.create_standard_plots()


def plot_module(data_files, module_type_name=None, **_):
    with combine_module_files.CombineModuleFiles(data_files, module_type=module_type_name) as cmf:
        cmf.combine_standard_tables()
    with plotting.Plotting(analyzed_data_file=cmf.target_file) as p:
        p.create_standard_plots()


def _min_occ_thr(connectivity_map, occupancy, enabled, min_occ_thr=1, **_):
    '''
        May be used as customize_connectivity_map for modules that have big regions with disconnected bumps
        The algorithm of the above script has problemms with such regions
        In this case this funktions can override the results with a connectivity map
        that is given by a supplied gloabal minimal occupancy
    '''
    connectivity_map[occupancy >= min_occ_thr] = 1
    connectivity_map[occupancy < min_occ_thr] = 0
    connectivity_map[np.logical_not(enabled)] = -12


if __name__ == '__main__':

    # The folder in which the individual folders for each module lie
    prefix = "/path/to/modules/folder/"

    # Analysis is performed on each module in this list.
    # Two exempplary use cases are given.
    # Parameters can be customized using the params dictionary (options are listed below).
    module_dtype = np.dtype([("module", "U10"), ('chips', 'O'), ('date_source', 'U16'), ('date_analog', 'U16'), ('params', 'O')])
    modules = np.array([
        ("SCC0", ["0x0000"], "20210101_000000", None, {"customize_connectivity_map": _min_occ_thr, "min_occ_thr": 2}),
        ("DCM0", ["0x0000", "0x0001"], "20210101_000000", "20200101_000001", {"allowed_diviation_of_itensity_up": 2})
    ], dtype=module_dtype)

    ''' Possible parameters in..
        analyze_chip:
            min_tot_ratio                       : float/(float, float, float): minimal ratio of the mean tot a normal pixel should have (1 value per FE type if RD53A)
            module_type_name                    : str: overwrites module type (definition can be found in bdaq53/modules/module_types.yaml)
            customize_connectivity_map          : f(connectivity_map, occupancy, enabled, ...): gets called after generation of connectivity_map to make modifications to it
        get_connectivity_map:
            block_width                         : int: spezifies width of indiviual blocks which get analysed seperatly
            block_height                        : int: spezifies height of indiviual blocks which get analysed seperatly
        analyze_block:
            allowed_diviation_of_itensity_down  : float (>1): apart from statistical flactuations, a factor on how much the occupancy is allowed diviate up from mpv
            allowed_diviation_of_itensity_up    : float (0< <1):  apart from statistical flactuations, a factor on how much the occupancy is allowed diviate down from mpv
            alpha_false_neg                     : float: significance value / max probabilaty of identifying any normal pixel in the block as being disconnected
            alpha_max_noise_occ                 : float: significance value / max probabilaty of identifying any normal pixel in the block as having a high occupancy
            definit_noisy_factor                : float (>1): pixel above max_occupancy are noisy if they also have a low meanTot
                                                              pixel above definit_noisy_factor * max_occupancy always get marked as being noisy
    '''

    for module in modules:
        module_folder = os.path.join(prefix, module["module"])
        interpreted_data_filenames = [None] * len(module["chips"])
        for chip_i, chip in enumerate(module["chips"]):
            source_file = os.path.join(module_folder, chip, module["date_source"] + "_source_scan")
            interpreted_data_filenames[chip_i] = source_file + '_bump_connectivity.h5'
            analog_file = None if module["date_analog"] == "None" else os.path.join(module_folder, chip, module["date_analog"] + "_analog_scan")
            analyze_chip(source_file, analog_file, create_plots=True, **module["params"])
        if len(module["chips"]) > 1:
            plot_module(interpreted_data_filenames, **module["params"])
