''' Script to check that the fifo readout works
'''

import copy
import time
import logging
import os
import shutil
import unittest
import yaml

import numpy as np
import tables as tb

import bdaq53
from bdaq53.system import scan_base  # noqa: F401
from bdaq53.chips import rd53a  # noqa: F401
from bdaq53.tests import bdaq_mock, utils

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))


def create_test_scan(parallel=False, fill_buffer=False):
    ''' Must be in method to allow delayed import of ScanBase after mocking'''
    from bdaq53.system.scan_base import ScanBase

    class TestScan(ScanBase):
        scan_id = 'test_scan'

        is_parallel_scan = parallel

        def _configure(self, **_):
            if self.is_parallel_scan:  # workaround bug #371
                self.data.n_trigger = 0

        def _scan(self, **_):
            with self.readout(fill_buffer=fill_buffer):
                time.sleep(2)

    return TestScan


class TestFifoReadout(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        ''' Do the minimum necessary to run bdaq53 without hardware '''
        # Use hardware mocks to be able to test without hardware
        cls.bhm = bdaq_mock.BdaqMock(n_chips=2)
        cls.bhm.start()

        cls.TestScanSerial = create_test_scan(parallel=False)
        cls.TestScanParallel = create_test_scan(parallel=True)

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)
            cls.bench_config['general']['use_database'] = False  # deactivate failing feature

    @classmethod
    def tearDownClass(cls):
        cls.bhm.stop()

    @classmethod
    def setUp(cls):
        # Reset overwrites to std. overwrites after each test
        cls.bhm.reset()

    @classmethod
    def tearDown(cls):
        shutil.rmtree('output_data', ignore_errors=True)  # always delete output from previous scan
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024

    def check_data(self, file_names):
        ''' Check of raw data and meta date makes sense '''

        for file_name in file_names:
            with tb.open_file(file_name, 'r') as raw_data_h5:
                meta_data = raw_data_h5.root.meta_data[:]
                self.assertTrue(np.all(np.diff(meta_data['index_start']) == 1))
                self.assertTrue(np.all(np.diff(meta_data['index_stop']) == 1))
                self.assertTrue(np.all(np.diff(meta_data['timestamp_start']) > 0))
                self.assertTrue(np.all(np.diff(meta_data['timestamp_stop']) > 0))

                raw_data = raw_data_h5.root.raw_data[:]
                self.assertTrue(np.all(np.diff(raw_data) == 1))

    def test_scan_single_chip(self):
        with self.TestScanSerial(bench_config=self.bench_config) as scan:
            scan.start()

        file_names = []
        for _ in scan.iterate_chips():
            file_names.append(scan.output_filename + '.h5')
        self.check_data(file_names)

    def test_scan_one_dc_modules_serial(self):
        ''' Test raw data aquisition with a double chip modules for serial scans'''
        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config['modules']['module_0']['chip_1'] = copy.deepcopy(bench_config['modules']['module_0']['chip_0'])
        bench_config['modules']['module_0']['chip_1']['chip_sn'] = '0x0002'
        bench_config['modules']['module_0']['chip_1']['chip_id'] = 1
        bench_config['modules']['module_0']['chip_1']['receiver'] = "rx1"
        bench_config['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()

        file_names = []
        for _ in scan.iterate_chips():
            file_names.append(scan.output_filename + '.h5')
        self.check_data(file_names)

    def test_scan_one_dc_modules_parallel(self):
        ''' Test raw data aquisition with a double chip modules for serial scans'''
        # Change std. bench config
        bench_config = copy.deepcopy(self.bench_config)
        # Add additional chip to module 0
        bench_config['modules']['module_0']['chip_1'] = copy.deepcopy(bench_config['modules']['module_0']['chip_0'])
        bench_config['modules']['module_0']['chip_1']['chip_sn'] = '0x0002'
        bench_config['modules']['module_0']['chip_1']['chip_id'] = 1
        bench_config['modules']['module_0']['chip_1']['receiver'] = "rx1"
        bench_config['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

        with self.TestScanParallel(bench_config=bench_config) as scan:
            scan.start()

        file_names = []
        for _ in scan.iterate_chips():
            file_names.append(scan.output_filename + '.h5')
        self.check_data(file_names)

    def test_rx_sync_error_handling(self):
        ''' Test RX fifo error handling'''

        # Catch log output of scan
        scan_logger = logging.getLogger('TestScan')
        scan_log_handler = utils.MockLoggingHandler(level='DEBUG')
        scan_logger.addHandler(scan_log_handler)
        scan_log_messages = scan_log_handler.messages

        bench_config = copy.deepcopy(self.bench_config)

        # No RX issue
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()
        self.assertFalse(scan_log_messages['error'])  # no error

        # Make all RX have no sync
        self.bhm.patch_function('bdaq53.system.fifo_readout.FifoReadout.get_rx_sync_status', lambda *_: [False, False, False, False])

        # Second RX has no sync, abort on error
        scan_log_handler.reset()
        bench_config['general']['abort_on_rx_error'] = True
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()
        self.assertTrue('Aborting run...' in scan_log_messages['error'])

        # Second RX has no sync, continue on error
        scan_log_handler.reset()
        bench_config['general']['abort_on_rx_error'] = False
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()

        self.assertFalse('Aborting run...' in scan_log_messages['error'])
        self.assertTrue('Aurora sync lost' in scan_log_messages['error'])

    def test_rx_soft_error_handling(self):
        ''' Test RX soft error handling'''

        # Catch log output of scan
        scan_logger = logging.getLogger('TestScan')
        scan_log_handler = utils.MockLoggingHandler(level='DEBUG')
        scan_logger.addHandler(scan_log_handler)
        scan_log_messages = scan_log_handler.messages

        bench_config = copy.deepcopy(self.bench_config)

        # No RX issue
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()
        self.assertFalse(scan_log_messages['error'])  # no error

        # Make 2nd RX has soft errors
        self.bhm.patch_function('bdaq53.system.fifo_readout.FifoReadout.get_rx_soft_error_count', lambda *_: [0, 91, 0, 0])

        # Second RX has soft errors, abort on soft errors should not happen, they should be logged
        scan_log_handler.reset()
        bench_config['general']['abort_on_rx_error'] = True
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()
        self.assertFalse('Aborting run...' in scan_log_messages['error'])
        self.assertTrue(str(scan_log_messages['error']).count('Aurora soft errors detected') == 1)
        self.assertTrue(str(scan_log_messages['error']).count('[0, 91, 0, 0]') == 1)

        # Second RX has soft errors, they should be logged
        scan_log_handler.reset()
        bench_config['general']['abort_on_rx_error'] = False
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()

        self.assertFalse('Aborting run...' in scan_log_messages['error'])
        self.assertTrue(str(scan_log_messages['error']).count('Aurora soft errors detected') == 1)
        self.assertTrue(str(scan_log_messages['error']).count('[0, 91, 0, 0]') == 1)

    def test_rx_hard_error_handling(self):
        ''' Test RX hard error handling'''

        # Catch log output of scan
        scan_logger = logging.getLogger('TestScan')
        scan_log_handler = utils.MockLoggingHandler(level='DEBUG')
        scan_logger.addHandler(scan_log_handler)
        scan_log_messages = scan_log_handler.messages

        bench_config = copy.deepcopy(self.bench_config)

        # No RX issue
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()
        self.assertFalse(scan_log_messages['error'])  # no error

        # Make 2nd RX has soft errors
        self.bhm.patch_function('bdaq53.system.fifo_readout.FifoReadout.get_rx_hard_error_count', lambda *_: [37, 0, 0, 0])

        # Second RX has soft errors, abort on soft errors should not happen, they should be logged
        scan_log_handler.reset()
        bench_config['general']['abort_on_rx_error'] = True
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()
        self.assertTrue('Aborting run...' in scan_log_messages['error'])
        self.assertTrue(any('Aurora hard errors detected' in msg for msg in scan_log_messages['error']))
        self.assertTrue(any('[37, 0, 0, 0]' in msg for msg in scan_log_messages['error']))

        # Second RX has soft errors, they should be logged
        scan_log_handler.reset()
        bench_config['general']['abort_on_rx_error'] = False
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()

        self.assertFalse('Aborting run...' in scan_log_messages['error'])
        self.assertTrue(any('Aurora hard errors detected' in msg for msg in scan_log_messages['error']))
        self.assertTrue(any('[37, 0, 0, 0]' in msg for msg in scan_log_messages['error']))

    def test_rx_discard_error_handling(self):
        ''' Test RX fifo discard error handling'''

        # Catch log output of scan
        scan_logger = logging.getLogger('TestScan')
        scan_log_handler = utils.MockLoggingHandler(level='DEBUG')
        scan_logger.addHandler(scan_log_handler)
        scan_log_messages = scan_log_handler.messages

        bench_config = copy.deepcopy(self.bench_config)

        # No RX issue
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()
        self.assertFalse(scan_log_messages['error'])  # no error

        # Make 2nd RX have discard error
        self.bhm.patch_function('bdaq53.system.fifo_readout.FifoReadout.get_rx_fifo_discard_count', lambda *_: [0, 1, 0, 0])

        # Second RX has soft errors, abort on soft errors should not happen, they should be logged
        scan_log_handler.reset()
        bench_config['general']['abort_on_rx_error'] = True
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()
        self.assertTrue('Aborting run...' in scan_log_messages['error'])
        self.assertTrue(any('RX FIFO discard error(s) detected' in msg for msg in scan_log_messages['error']))
        self.assertTrue(any('[0, 1, 0, 0]' in msg for msg in scan_log_messages['error']))

        # Second RX has soft errors, they should be logged
        scan_log_handler.reset()
        bench_config['general']['abort_on_rx_error'] = False
        with self.TestScanSerial(bench_config=bench_config) as scan:
            scan.start()

        self.assertFalse('Aborting run...' in scan_log_messages['error'])
        self.assertTrue(any('RX FIFO discard error(s) detected' in msg for msg in scan_log_messages['error']))
        self.assertTrue(any('[0, 1, 0, 0]' in msg for msg in scan_log_messages['error']))

    def test_fifo_readout_buffer(self):

        with self.TestScanSerial(bench_config=self.bench_config) as scan:
            scan.start()

        self.assertFalse(scan.fifo_readout.fill_buffer)
        self.assertTrue(scan.fifo_readout.get_data_buffer('rx0').shape[0] == 0)

        TestScanBuffer = create_test_scan(parallel=False, fill_buffer=True)
        with TestScanBuffer(bench_config=self.bench_config) as scan:
            scan.start()

        self.assertTrue(scan.fifo_readout.fill_buffer)
        self.assertTrue(np.all(scan.fifo_readout.get_data_buffer('rx0')[:10] == np.array(range(10), np.uint32)))



if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFifoReadout)
    unittest.TextTestRunner(verbosity=2).run(suite)
