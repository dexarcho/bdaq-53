`ifndef DESERIALIZER_AURORA_TOP__SV
 `define DESERIALIZER_AURORA_TOP__SV

`include "rtl/eoc/aurora/DeserializerPhaseDet.sv"
`include "rtl/eoc/aurora/DeserializerGearbox4to66.sv"
`include "rtl/eoc/aurora/DeserializerFindSyncHeader.sv"
`include "rtl/eoc/aurora/DeserializerDescrambler.sv"
`include "rtl/eoc/aurora/MergerUpscaleBlocks.sv"
`include "rtl/eoc/aurora/MergerRRArbitrer.sv"

module DeserializerAuroraTop (
   input wire Reset_b,
   input wire ClkDes,
   input wire [3:0][3:0] Q,

   input wire ClkAurora,

   input wire [3:0] ActiveLanes,
   output logic ChannelUp,
   
   AuroraDataInterface.out DataOutput,

   AuroraDataInterface.out MonitorOutput);
   

   logic [0:55] KWordOut;
   logic [0:3] 	KWordNumber;
   logic 	KWordValid;
   
   logic [0:63] DataOut;
   logic 	DataValid;
   
   logic [3:0] 	DeserializedData;
   logic       DeserializedValid;
   
   DeserializerPhaseDet PhaseDet (
				  .Qa(Q[0]),
				  .Qb(Q[1]),
				  .Qc(Q[2]),
				  .Qd(Q[3]),
				  .Reset_b(Reset_b),
				  .Clk160(ClkDes),
				  .DesData(DeserializedData),
				  .Valid(DeserializedValid)
				  );

   logic [65:0] DesData66;
   logic 	DesDataValid;
   
   DeserializerGearbox4to66 Gearbox (
				     .Data4(DeserializedData),
				     .Rst_b(Reset_b),
				     .Clk(ClkDes),
				     .Data66(DesData66),
				     .DataValid(DesDataValid)
				     );

   logic [65:0] SyncData66;
   logic 	SyncDataValid;

   
   DeserializerFindSyncHeader Header (
				      .Data66(DesData66),
				      .DataValid(DesDataValid),
				      .Clk(ClkDes),
				      .Rst_b(Reset_b),
				      .SyncData66(SyncData66),
				      .SyncDataValid(SyncDataValid)
				      );

   logic [65:0] AuroraBlock;
   logic 	AuroraBlockValid;

   DeserializerDescrambler Descrambler (
					.DataIn(SyncData66[65:2]),
					.SyncBits({<<{SyncData66[1:0]}}),
					.Ena(SyncDataValid),
					.Clk(ClkDes),
				       	.Rst_b(Reset_b),
					.DataOut(AuroraBlock)
					);

   always @(posedge ClkDes)
     AuroraBlockValid = SyncDataValid;

   always @(posedge ClkDes)
    begin
	if (Reset_b == 1'b0)
	  ChannelUp = 1'b0;
	else if (AuroraBlockValid)
	    ChannelUp = 1'b1;
    end

   logic isAuroraData;

   assign isAuroraData = (AuroraBlock[1:0] == 2'b10) & AuroraBlockValid;

   logic alwaysRead;

   always @(posedge ClkAurora)
     begin
	if (Reset_b == 1'b0)
	  alwaysRead = 1'b0;
	else
	  alwaysRead = 1'b1;
     end
   
   SingleCdcFifo #(.DSIZE(64)) DataFIFO (
					.wdata({<<{AuroraBlock[65:2]}}),
					.wput(isAuroraData),
					.wrst_b(Reset_b),
					.wclk(ClkDes),

					.rdata(DataOut),
					.rrdy(DataValid),
					.rrst_b(Reset_b),
					.rget(alwaysRead),
					.rclk(ClkAurora)
					);   

   MergerUpscaleBlocks UpscaleBlocks (
				      .Reset_b(Reset_b),
				      .Clock(ClkAurora),
				      .ActiveLanes(ActiveLanes),
				      .DataIn(DataOut),
				      .DataValidIn(DataValid),
				      .DataOut(DataOutput.DataEOC),
				      .DataMask(DataOutput.DataMask),
				      .DataEmpty(DataOutput.DataEOC_empty),
				      .DataRead(DataOutput.DataEOC_read)
				      );


   
   logic isKWord;
   logic [3:0] numKWord;
   logic [59:0] KWordPacked;
   logic [0:7] reverted_btf;
   assign reverted_btf = {<<{AuroraBlock[9:2]}};

   always_comb
     begin
	case(reverted_btf)
	  8'hD2: numKWord = 4'd0;
	  8'h99: numKWord = 4'd1;
	  8'h55: numKWord = 4'd2;
	  8'hB4: numKWord = 4'd3;
	  8'hCC: numKWord = 4'd4;
	  default: numKWord = 4'b1111;	  
	endcase // case (AuroraBlock[9:2]	      
     end
   
   assign isKWord = ((AuroraBlock[1:0] == 2'b01) && (numKWord != 4'b1111)) & AuroraBlockValid;
   
   SingleCdcFifo #(.DSIZE(60)) KWordFIFO (
					 .wdata({numKWord,AuroraBlock[65:10]}), // adapt to FIFO Endiassness 
					 .wput(isKWord),
					 .wrst_b(Reset_b),
					 .wclk(ClkDes),

					 .rdata(KWordPacked),
					 .rrdy(KWordValid),
					 .rclk(ClkAurora),
					 .rrst_b(Reset_b),
					 .rget(MonitorOutput.DataEOC_read)
					 );
   
   
   // Revert Endiassness at the output
   assign KWordOut = {<<{KWordPacked[55:0]}};
   assign KWordNumber = {<<{KWordPacked[59:56]}};

   // expand output to match data size
   logic [0:7] expanded_btf;

   always_comb
     begin
	case(KWordNumber)
	  4'd0: expanded_btf = 8'hD2;
	  4'd1: expanded_btf = 8'h99;
	  4'd2: expanded_btf = 8'h55;
	  4'd3: expanded_btf = 8'hB4;
	  4'd4: expanded_btf = 8'hCC;
	  default: expanded_btf = 8'hA1;	  
	endcase // case (AuroraBlock[9:2]	      
     end

   assign MonitorOutput.DataEOC = {expanded_btf,KWordOut,expanded_btf,KWordOut,expanded_btf,KWordOut,expanded_btf,KWordOut};
   assign MonitorOutput.DataEOC_empty = ~KWordValid;
   
endmodule // DeserializerAuroraTop


`endif //  `ifndef DESERIALIZER_AURORA_TOP__SV
