#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import yaml
import numpy as np

MODULES_FOLDER = os.path.join(os.path.dirname(__file__), os.pardir, 'modules')
MODULE_DESCRIPTION_FILE = os.path.join(MODULES_FOLDER, 'module_types.yaml')


class ModuleType(object):
    ''' Class specifying a multi-chip module type defined in modules/module_types.yaml '''

    def __init__(self, module_type_name, chip_type_name=None):
        ''' Loads module type from the module definition file '''

        self._chip_type = chip_type_name

        with open(MODULE_DESCRIPTION_FILE) as f:
            module_descriptions = yaml.full_load(f)
        if module_type_name in module_descriptions:
            self.module_description = module_descriptions[module_type_name]
        else:
            raise ValueError("Module type '%s' is not supported." % (module_type_name))

        # load chip_id map and chip rotation map
        self.chip_id_map = np.array(self.module_description['chip_id'])
        self.chip_rot_map = np.array(self.module_description['chip_rotation'])

        # Check if the loaded module type has a valid format
        if len(self.chip_id_map.shape) != 2:
            raise ValueError("Chip_id map of module must be rectangular.")
        if len(set(self.chip_id_map.flatten())) != self.chip_id_map.size:
            raise ValueError("Chip_id map can not have duplicate chip_ids.")
        if self.chip_id_map.shape != self.chip_rot_map.shape:
            raise ValueError("Chip_id map and rotation map of module must have same shape.")
        if not any(all(rot % 2 == res for rot in self.chip_rot_map.flatten()) for res in (0, 1)):
            raise ValueError("Rotation map of module can only have either even or odd digits.")

    def get_chip_ids(self):
        ''' returns all chip_ids of module'''
        return self.chip_id_map.flatten()

    def switch_axis(self):
        ''' returns whether the axis should be switched '''
        return self.chip_rot_map[0, 0] % 2 == 1

    def get_size(self, chip_cols, chip_rows):
        ''' returns the size (rows, cols) of the pixel map as stored in the module file
            for given chip size
        '''
        shape = self.chip_id_map.shape
        return chip_cols * shape[1], chip_rows * shape[0]

    def get_real_size(self, chip_cols, chip_rows):
        ''' returns the size (rows, cols) of the pixel map as displayed in plotting
            for given chip size
        '''
        shape = self.chip_id_map.shape
        n_side = self.get_map_extension()
        chip_cols += n_side['left'] + n_side['right']
        chip_rows += n_side['top'] + n_side['bottom']
        if self.switch_axis():
            return chip_rows * shape[1], chip_cols * shape[0]
        else:
            return chip_cols * shape[1], chip_rows * shape[0]

    def concatenate_maps(self, maps, dtype=None):
        ''' returns concatenated maps.
            Maps must be supplied as a 2D array, which matches the chip_id_map
            Maps may be supplied as arrays or as pytables carrays
        '''

        m0 = maps[0][0][:]
        maps[0][0] = m0
        if dtype is None:
            dtype = m0.dtype
        chip_shape = m0.shape
        shape = list(chip_shape)
        shape[:2] = self.get_size(*chip_shape[:2])
        combined = np.zeros(shape, dtype=dtype)
        for r, row in enumerate(maps):
            for c, m in enumerate(row):
                sl = (slice(c * chip_shape[0], (c + 1) * chip_shape[0]),
                      slice(r * chip_shape[1], (r + 1) * chip_shape[1]))
                combined[sl] = m[:]
        return combined

    def transform_to_real_layout(self, map, extendet_maps=True, fill_value=0):
        ''' Transforms the histogram to real layout on module
            One map including all individual chip maps must be supplied
            Chip maps get optionally extendet if needed
            Chip maps get rotated according to module definition
            In the end they are concatenated again
        '''
        m_shape = map.shape
        c_shape = (m_shape[0] // len(self.chip_id_map[0]), m_shape[1] // len(self.chip_id_map))
        r_shape = (*self.get_real_size(c_shape[0], c_shape[1]), *m_shape[2:])
        f_shape = (r_shape[0] // len(self.chip_id_map[0]), r_shape[1] // len(self.chip_id_map))

        class Map():
            def __init__(self_m):
                self_m.m = map
                self_m.shape = r_shape
                self_m.dtype = self_m.m.dtype

            def __getitem__(self_m, *args):
                combined = np.zeros(r_shape, dtype=self_m.dtype)
                for r, row in enumerate(self.chip_rot_map):
                    for c, rot in enumerate(row):
                        si = (slice(c * c_shape[0], (c + 1) * c_shape[0]), slice(r * c_shape[1], (r + 1) * c_shape[1]))
                        sf = (slice(c * f_shape[0], (c + 1) * f_shape[0]), slice(r * f_shape[1], (r + 1) * f_shape[1]))
                        m = self_m.m[si]
                        if extendet_maps:
                            m = self.extend_map(m, fill_value=fill_value)
                        combined[sf] = np.rot90(m, rot)
                return combined[args]

        return Map()

    def get_chip_config(self, chip_id):
        ''' returns a confing of the chip whith given chip_id '''
        return self.module_description['chips'].get(chip_id, {})

    def get_low_occupancy(self, chip_id):
        ''' returns a list of sclices specifying reagions wich may have low occupancys '''
        regions = self.get_chip_config(chip_id).get('low_occupancy', [])
        ranges = [(slice(*r[0]), slice(*r[1])) for r in regions]
        return ranges

    def get_max_corner_size(self):
        ''' returns the maximum number of rows at the corners wich may have bigger pixels '''
        return self.module_description.get('big_corner_pixels', {}).get('max_n', 0)

    def get_big_corners(self, chip_id):
        ''' returns a selection of ['left', 'right', 'top', 'bottom']
            indicating which corners may have bigger pixels
        '''
        pos = np.where(self.chip_id_map == chip_id)
        row, col = (p[0] for p in pos)
        rot = self.chip_rot_map[row, col]
        dic = self.module_description.get('big_corner_pixels', {})
        corner_rows = dic.get('rows', [])
        corner_cols = dic.get('cols', [])
        selection = [(col + abs(i - (rot // 2) % 2)) in corner_cols for i in (0, 1)]
        selection += [(row + abs(i - (rot // 2) % 2)) in corner_rows for i in (0, 1)]
        if rot % 2 == 0:
            strings = ['left', 'right', 'top', 'bottom']
        else:
            strings = ['bottom', 'top', 'left', 'right']
        return [string for sel, string in zip(selection, strings) if sel]

    def get_map_extension(self):
        ''' returns a dicts specifying how many rows / columns should be added
            to the corresponding sides: ['left', 'right', 'top', 'bottom']
        '''
        chip_types = self.module_description.get('extend_maps', {})
        em = chip_types.get(self._chip_type, {})
        return {side: em.get(side, 0) for side in ['left', 'right', 'top', 'bottom']}

    def extend_map(self, map_to_extend, fill_value=0):
        ''' returns the map extendet to each sides as specified '''
        n_side = self.get_map_extension()
        old_shape = map_to_extend.shape
        new_shape = list(old_shape)
        new_shape[0] += n_side['left'] + n_side['right']
        new_shape[1] += n_side['top'] + n_side['bottom']
        if any(os != ns for os, ns in zip(old_shape, new_shape)):
            section = (slice(n_side['left'], n_side['left'] + old_shape[0]), slice(n_side['top'], n_side['top'] + old_shape[1]))
            extendet_map = np.full_like(map_to_extend, fill_value=fill_value, shape=new_shape)
            extendet_map[section] = map_to_extend
            return extendet_map
        else:
            return map_to_extend
