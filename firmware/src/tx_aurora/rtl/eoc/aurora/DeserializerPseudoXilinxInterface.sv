`ifndef DESERIALIZER_PSEUDO_XILINX_INTERFACE
 `define DESERIALIZER_PSEUDO_XILINX_INTERFACE

 `include "rtl/eoc/aurora/DeserializerModule.sv"
 `include "rtl/eoc/fifos/SingleCdcFifo.sv"
 `include "rtl/eoc/aurora/MergerUpscaleBlocks.sv"

module DeserializerPseudoXilinxInterface (
   input wire Reset_b,
   input wire [1:0] RX, //unused
   input wire SerialIn,

   output bit UserClk,

   output logic ChannelUp,

   output logic [0:63] DataOut,
   output logic DataValid,

   output logic [0:55] KWordOut,
   output logic [0:3] KWordNumber,
   output logic KWordValid
   
);

   bit 		clkfast = 1'b0;
   bit 		clkdelay;		
   bit 		clkslow = 1'b0;

   timeunit 1ps ;
   timeprecision 100fs ;


   // Incoming serial data at 1.28GHz
   parameter period = 782 ;   // ps
   parameter jitter =  50 ;   // ps - unused


   always #( 0.5*0.5*period ) clkfast = ~ clkfast ;
   always #( 2*period ) clkslow = ~ clkslow ; 
   always #( 50 ) clkdelay = clkfast;
   always #( 16*period ) UserClk = ~UserClk; // **WARN: Q: just chosen ~x2 faster than clkslow
   
      
   
   logic [3:0] Qa, Qb, Qc, Qd;
   

   logic [65:0] AuroraBlock;
   logic 	AuroraBlockValid;
   
   
   DeserializerModule DUTDesModule (
				    .SerialIn(SerialIn),
				    .ClkSampling(clkdelay),
				    .ClkParallel(clkslow),
				    .AuroraData(AuroraBlock),
				    .AuroraDataValid(AuroraBlockValid),
				    .Reset_b(Reset_b));
  

   always @(posedge clkslow)
    begin
	if (Reset_b == 1'b0)
	  ChannelUp = 1'b0;
	else if (AuroraBlockValid)
	    ChannelUp = 1'b1;
    end

   logic isAuroraData;

   assign isAuroraData = (AuroraBlock[1:0] == 2'b10) & AuroraBlockValid;
   
   SingleCdcFifo #(.DSIZE(64)) DataFIFO (
					.wdata({<<{AuroraBlock[65:2]}}),
					.wput(isAuroraData),
					.wrst_b(Reset),
					.wclk(clkslow),

					.rdata(DataOut),
					.rrdy(DataValid),
					.rrst_b(~Reset),
					.rget(1'b1),
					.rclk(UserClk)
					);

   MergerUpscaleBlocks mergerupscale (
				      .Reset_b(Reset_b),
				      .Clock(UserClk),
				      .DataIn(DataOut),
				      .DataValidIn(DataValid),
				      .ActiveLanes(4'b1011)
				      );
 
   
   logic isKWord;
   logic [3:0] numKWord;
   logic [59:0] KWordPacked;
   logic [0:7] reverted_btf;
   assign reverted_btf = {<<{AuroraBlock[9:2]}};
   always_comb
     begin
	case(reverted_btf)
	  8'hD2: numKWord = 4'd0;
	  8'h99: numKWord = 4'd1;
	  8'h55: numKWord = 4'd2;
	  8'hB4: numKWord = 4'd3;
	  8'hCC: numKWord = 4'd4;
	  default: numKWord = 4'b1111;	  
	endcase // case (AuroraBlock[9:2]	      
     end
   
   assign isKWord = ((AuroraBlock[1:0] == 2'b01) && (numKWord != 4'b1111)) & AuroraBlockValid;
   
   SingleCdcFifo #(.DSIZE(60)) KWordFIFO (
					 .wdata({numKWord,AuroraBlock[55:0]}), // adapt to FIFO Endiassness 
					 .wput(isKWord),
					 .wrst_b(Reset),
					 .wclk(clkslow),

					 .rdata(KWordPacked),
					 .rrdy(KWordValid),
					 .rclk(UserClk),
					 .rrst_b(~Reset),
					 .rget(1'b1)
					 );
   
   // Revert Endiassness at the output
   assign KWordOut = {<<{KWordPacked[55:0]}};
   assign KWordNumber = {<<{KWordPacked[59:56]}};
   
   
   // Revert Endiassness at the output
   assign KWordOut = {<<{KWordPacked[55:0]}};
   assign KWordNumber = {<<{KWordPacked[59:56]}};
   
   
endmodule // DeserializerPseudoXilinxInterface


`endif
