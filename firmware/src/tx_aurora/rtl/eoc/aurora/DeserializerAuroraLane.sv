`ifndef DESERIALIZER_AURORA_LANE__SV
 `define DESERIALIZER_AURORA_LANE__SV

`include "rtl/eoc/aurora/DeserializerPhaseDet.sv"
`include "rtl/eoc/aurora/DeserializerGearbox4to66.sv"
`include "rtl/eoc/aurora/DeserializerFindSyncHeader.sv"
`include "rtl/eoc/aurora/DeserializerDescrambler.sv"
`include "rtl/eoc/aurora/MergerRRArbitrer.sv"

module DeserializerAuroraLane (
   input wire Reset_b,
   input wire ResetDes_b,
   input wire ClkDes,
   input wire [3:0][3:0] Q,

   input wire ClkAurora,

   output logic LaneUp,

   output logic [65:0] BlockOut,
   output logic BlockValid
);

   logic 	resetDesPhase_b;
   logic 	resetDesGearbox_b;

   always @(posedge ClkDes)
     begin
	resetDesPhase_b <= ResetDes_b;
	resetDesGearbox_b <= ResetDes_b;
  end
   
   logic [3:0] 	DeserializedData;
   logic       DeserializedValid;

   
   DeserializerPhaseDet PhaseDet (
				  .Qa(Q[0]),
				  .Qb(Q[1]),
				  .Qc(Q[2]),
				  .Qd(Q[3]),
				  .Reset_b(resetDesPhase_b),
				  .Clk160(ClkDes),
				  .DesData(DeserializedData),
				  .Valid(DeserializedValid)
				  );

   logic [65:0] DesData66;
   logic [65:0] DesData66_clkDes;
   logic 	DesDataValid;
   logic 	DesDataValid_clkDes;
   logic 	DesDataRead;
   
   
   DeserializerGearbox4to66 Gearbox (
				     .Data4(DeserializedData),
				     .Rst_b(resetDesGearbox_b),
				     .Clk(ClkDes),
				     .Data66(DesData66_clkDes),
				     .DataValid(DesDataValid_clkDes)
				     );

   OutputCdcFifoResetB #( .DSIZE(66), .ASIZE(2) ) GearboxFIFO (
        .rempty ( fifoEmpty ),
        .wdata  ( DesData66_clkDes ),
        .winc   ( DesDataValid_clkDes ),
        .wclk   ( ClkDes ),
        .wrst_b   ( ResetDes_b ),
        .rdata  ( DesData66 ), 
        .wfull  ( ),
        .rinc   ( DesDataRead & ~fifoEmpty ),
        .rclk   ( ClkAurora ),
        .rrst_b   ( Reset_b )  

    ) ;

   always @(posedge ClkAurora)
     if (Reset_b == '0)
       DesDataRead = '0;
     else
       DesDataRead = '1;

   logic [65:0] SyncData66;
   logic 	SyncDataValid;

   always @(posedge ClkAurora)
     DesDataValid = ~fifoEmpty;
   
   DeserializerFindSyncHeader Header (
				      .Data66(DesData66),
				      .DataValid(DesDataValid),
				      .Clk(ClkAurora),
				      .Rst_b(Reset_b),
				      .SyncData66(SyncData66),
				      .SyncDataValid(SyncDataValid)
				      );

   logic [65:0] AuroraBlock;
   logic 	AuroraBlockValid;

   DeserializerDescrambler Descrambler (
					.DataIn(SyncData66[65:2]),
					.SyncBits({<<{SyncData66[1:0]}}),
					.Ena(SyncDataValid),
					.Clk(ClkAurora),
				       	.Rst_b(Reset_b),
					.DataOut(AuroraBlock)
					);

   always @(posedge ClkAurora)
     AuroraBlockValid = SyncDataValid;

   always @(posedge ClkAurora)
    begin
	if (Reset_b == 1'b0)
	  LaneUp = 1'b0;
	else if (AuroraBlockValid)
	  LaneUp = 1'b1;
    end

   assign BlockValid = AuroraBlockValid;
   assign BlockOut = {<<{AuroraBlock[65:0]}};
   

     
endmodule // DeserializerAuroraLane


`endif //  `ifndef DESERIALIZER_AURORA_LANE__SV
