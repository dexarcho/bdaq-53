#!/bin/bash
export SIM=questa

if [ -d "unisims" ] && [ -d "secureip" ]; then
    echo "Directories unisims, secureip already exist"
else
  if [ ! -z "$RD53B_SRC" ]; then
    echo "Compiling unisims, secureip and tsmc libs"
    vlog -work tsmc $RD53B_SRC/questa_sources/tcbn65lp.vp
    vlog -work tsmc $RD53B_SRC/questa_sources/tcbn65lplvt.vp
    vlog -work tsmc $RD53B_SRC/questa_sources/tcbn65lpbwp12tlvt_pwr.vp
  else
    echo "Compiling unisims and secureip libs"
  fi
    vlog -work unisims $XILINX_VIVADO/data/verilog/src/unisims/*.v
    vlog -work unisims $XILINX_VIVADO/data/verilog/src/retarget/*.v

    vlog -work secureip -f $XILINX_VIVADO/data/secureip/gtxe2_common/gtxe2_common_cell.list.f
    vlog -work secureip -f $XILINX_VIVADO/data/secureip/gtxe2_channel/gtxe2_channel_cell.list.f
fi