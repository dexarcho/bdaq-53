
`ifndef AURORA_PRBS_GENERATOR__SV   // include guard
`define AURORA_PRBS_GENERATOR__SV

`timescale 1ns/ 1ps 

module AuroraPrbsGenerator
	#(
   parameter      WIDTH =20,
                  TAP1 = 15,
                  TAP2 = 14
   ) (
   // Outputs
   PRBS_Out,
   // Input
   Clk, Rst_b
   );


   output reg [WIDTH - 1:0] PRBS_Out;
   input              Clk, Rst_b;

   reg [WIDTH-1:0]    prbs;
   reg [WIDTH-1:0]    d;
   wire Clk;
 

   genvar                            i;
   generate
     for (i=0; i<WIDTH; i++) begin
       always_comb
         PRBS_Out[i] <= prbs[WIDTH-i-1];
       end
   endgenerate

   always @ (posedge Clk)
     begin
     if (Rst_b == 1'b0)
        begin
        prbs <= {WIDTH{1'b1}};
        end
     else
       begin
          prbs <= d;// les nouvelles valeurs des registres des polynomes
       end // else: !if(Rst)
     end

   always_comb
     begin
          d = prbs;  
          repeat (WIDTH)
	    begin
		  d= {d[WIDTH-2:0],d[TAP1-1]^d[TAP2-1]};
		  //d= {d[TAP1-1]^d[TAP2-1],d[WIDTH-1:1]};
	    end          
       end
endmodule 

`endif
