#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
import unittest
import logging
import yaml
import shutil

from os.path import dirname, join, abspath
from bdaq53.scans.test_registers import RegisterTest

from bdaq53.tests import utils

local_configuration = {
    'lanes': [1, 2, 3, 4]
}


class TestAuroraLanes(unittest.TestCase):
    def test_test_registers(self):
        ''' Enable ITkPixV1 settings'''
        with open(join(abspath(join(dirname(__file__), '..', '..', '..')), 'testbench.yaml'), 'r') as tbf:
            tb_dict = yaml.full_load(tbf)
        tb_dict['modules']['module_0']['fe_0']['chip_type'] = '"CROCv1"'
        tb_dict['modules']['module_0']['fe_0']['chip_id'] = 15

        with open(join(abspath(join(dirname(__file__), '..', '..', '..')), 'testbench.yaml'), 'w') as tbf:
            yaml.dump(tb_dict, tbf)

        logging.info('Starting digital scan test')
        self.sim_dc = 21
        for lanes in local_configuration.pop('lanes'):
            logging.info('Test register write/read with %i Aurora Lane(s)' % (lanes))
            self.test = RegisterTest(bdaq_conf=utils.setup_cocotb(self.sim_dc, chip='CROCv1', rx_lanes=lanes), scan_config={'ignore': list(range(3, 138))})
            self.test.scan()
            self.test.analyze()
            self.test.close()

    def tearDown(self):
        utils.close_sim()
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
