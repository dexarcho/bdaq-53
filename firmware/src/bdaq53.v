/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute of Bonn University
 * ------------------------------------------------------------
 */

/**
 *  THE HARDWARE PLATFORM is set by the run.tcl script and written to the Vivado project options
 *
 *  Supported DAQ hardware:
 *    1. BDAQ53 base board with Enclustra Mercury+ KX2 FPGA module (experimental support for KX1)
 *    2. USBPIX3(MIO3,MMC3) base board with Enclustra Mercury KX1 FPGA module.
 *    3. Xilinx KC705 evaluation board with the CERN single chip FMC adapter card
 *
 *  FPGA and configuration memory model numbers
 *    BDAQ53 (KX2):  XC7K160T-2-FFG676-I    (select xc7k160tffg676-2)    Quad-SPI flash: S25FL512S
 *    USBPIX3 (KX1): XC7K160T-1-FBG676-C    (select xc7k160tfbg676-1)    Quad-SPI flash: MT25QL256 or S25FL512S depending on the KX1 revision
 *    KC705:         XC7K325T-2-FFG900-C    (select xc7k325tffg900-2)    Quad-SPI flash: MT25QL128
 **/

`timescale 1ns / 1ps
`default_nettype wire

//`define USE_TCP_TO_BUS

`ifdef USE_TCP_TO_BUS
    `include "utils/tcp_to_bus.v"
`else
    `include "utils/rbcp_to_bus.v"
`endif

`include "utils/clock_divider.v"

`ifndef 10G
    `include "utils/fifo_32_to_8.v"

    `include "SiTCP/TIMER.v"
    `include "SiTCP/SiTCP_XC7K_32K_BBT_V110.V"
    `include "SiTCP/WRAP_SiTCP_GMII_XC7K_32K.V"
    
    // Use GMII instead of RGMII for KC705
    `ifdef KC705
        `define GMII
    `else
        `include "utils/rgmii_io.v"
    `endif
`endif

module bdaq53
    #(
        // FIRMWARE VERSION
        parameter VERSION_MAJOR = 8'd0,
        parameter VERSION_MINOR = 8'd0,
        parameter VERSION_PATCH = 8'd0
    )(
        input wire RESET_BUTTON,
        input wire MGT_REFCLK0_P, MGT_REFCLK0_N,
    `ifdef KC705
        input wire CLKSi570_P, CLKSi570_N,          // aurora drp clock
        output wire SI5324_RST,
        output wire SI5342_FMC_RST,
        input wire Si5324_N, Si5324_P,              // aurora reference clock
        input wire SMA_MGT_REF_CLK_N, SMA_MGT_REF_CLK_P,
        input wire GPIO_SW_C,                       // resets aurora pll
        input wire [3:0] GPIO_DIP_SW,               // DIP switch
        output wire USER_SMA_P, USER_SMA_N,         // used for CMD
        output wire CMD_FMC_LPC_P, CMD_FMC_LPC_N,              //used for cmd FMC
        output wire XADC_GPIO_23_P, XADC_GPIO_23_N,
        output wire DP2_EN_LPC, DP1_EN_LPC,         // controls the data direction of lane 2 and 3 (0: FPGA --> RD53A, 1: FPGA <-- RD53A)

        inout wire DP_GPIO_LANE0_LPC_P, DP_GPIO_LANE0_LPC_N,  // HITOR0
        inout wire DP_GPIO_LANE1_LPC_P, DP_GPIO_LANE1_LPC_N,  // HITOR1
        inout wire DP_GPIO_LANE2_LPC_P, DP_GPIO_LANE2_LPC_N,  // HITOR2
        inout wire DP_GPIO_LANE3_LPC_P, DP_GPIO_LANE3_LPC_N,  // HITOR3

        output wire DP1_EXT_CMD_CLK_P, DP1_EXT_CMD_CLK_N,       // for CDR bypass mode
        output wire DP1_EXT_SER_CLK_P, DP1_EXT_SER_CLK_N,       // for CDR bypass mode
    `else
        input wire USER_BUTTON,                     // resets aurora pll
        input wire clkin,                           // main fpga clock source
        output wire MGT_REF_SEL,                    // switch between Si570 and external reference clock
    `endif
    `ifdef BDAQ53
        // DP_ML ("DP2") and mini Displayport (mDP) connected to SelectIOs
        inout wire [3:0] DP_GPIO_P, DP_GPIO_N,  // HITOR0...3
        inout wire [3:0] mDP_GPIO_P, mDP_GPIO_N,  // HITOR4...7
        output wire DP_GPIO_AUX_P, DP_GPIO_AUX_N,
        
        // DP_ML connected to MGTs
        output wire DP_ML_AUX_P, DP_ML_AUX_N,       // Multi-lane DP

        // 2-row PMOD header for general purpose IOs
        inout wire [7:0]  PMOD,

        // NTC
        output wire [2:0] NTC_MUX,
        
        `ifndef _KX1
            // DP_ML connected to MGTs
            output wire [2:0] DP_SL_AUX_P, DP_SL_AUX_N, // Single-lane DP
    
            // Displayport "slow control" signals
            output wire [3:0] GPIO_RESET,
            input wire [3:0]  GPIO_SENSE,
    
            // SiTCP EEPROM
            output wire EEPROM_CS, EEPROM_SK, EEPROM_DI,
            input wire EEPROM_DO,
        `endif
    `elsif USBPIX3
        // RJ45 block. Port "D" on top side, usually used for Mimosa plane 3
        inout wire [3:0] RJ45_HITOR_P,
        inout wire [3:0] RJ45_HITOR_N,

        // 2-row PMOD header for general purpose IOs
        inout wire [7:0]  PMOD,
    `endif

        input wire CLK200_P, CLK200_N,

    // Ethernet

`ifdef 10G
    // SFP+
        output wire       SFP_TX_DISABLE,
        input wire        SFP_RX_N,
        input wire        SFP_RX_P,
        output wire       SFP_TX_N,
        output wire       SFP_TX_P,
    `ifdef BDAQ53
        input wire        MGT_REFCLK3_P,  // Si511 156.25 MHz oscillator
        input wire        MGT_REFCLK3_N,  // Si511 156.25 MHz oscillator
//    `elsif KC705
//        input wire        SMA_MGT_REF_CLK_P,  // clock provided by SMA loop cable
//        input wire        SMA_MGT_REF_CLK_N,  // clock provided by SMA loop cable
    `endif
`else
    `ifdef GMII
        inout  wire       mdio_phy_mdio,
        output wire       mdio_phy_mdc,
        output wire       phy_rst_n,
        input  wire       gmii_crs,
        input  wire       gmii_col,
        output wire [7:0] gmii_txd,
        output wire       gmii_tx_clk,
        output wire       gmii_tx_en,
        output wire       gmii_tx_er,
        input  wire [7:0] gmii_rxd,
        input  wire       gmii_rx_clk,
        input  wire       gmii_rx_er,
        input  wire       gmii_rx_dv,
    `else
        output wire [3:0] rgmii_txd,
        output wire       rgmii_tx_ctl,
        output wire       rgmii_txc,
        input  wire [3:0] rgmii_rxd,
        input  wire       rgmii_rx_ctl,
        input  wire       rgmii_rxc,
        output wire       mdio_phy_mdc,
        inout  wire       mdio_phy_mdio,
        output wire       phy_rst_n,
    `endif
`endif


    // Aurora RX lanes
    `ifdef _FMC_LPC
        input wire [3:0] MGT_RX_FMC_LPC_P, MGT_RX_FMC_LPC_N,
    `elsif BDAQ53
        `ifdef _KX1
            input wire [3:0]  MGT_RX_P, MGT_RX_N,
        `else
            input wire [7:0]  MGT_RX_P, MGT_RX_N,
        `endif
    `else
        input wire [3:0]  MGT_RX_P, MGT_RX_N,
    `endif

    // Aurora TX lanes (used for CMD in special cases)
    `ifdef USBPIX3
        output wire [0:0] MGT_TX_P, MGT_TX_N,
    `elsif KC705
        `ifdef _SMA
            output wire [0:0] MGT_TX_P, MGT_TX_N,
        `elsif _FMC_LPC
            output wire [0:0] MGT_TX_FMC_LPC_P, MGT_TX_FMC_LPC_N,
        `endif
    `endif

        // Debug LEDs
        output wire [7:0] LED,

    // Fan speed control for the KC705
    `ifdef KC705
        output wire SM_FAN_PWM,
    `endif

        input wire RJ45_TRIGGER,
        output wire LEMO_TX1, // Busy signal for TLU

    `ifndef KC705
        // TLU
        input wire RJ45_RESET,
        // LEMO
        output wire LEMO_TX0,
        input wire LEMO_RX0,
        input wire LEMO_RX1,
    `endif

        // I2C bus
        inout wire I2C_SDA,
        output wire I2C_SCL
    );


    wire RESET_N;
    wire RST;
    wire AURORA_RESET;
    wire BUS_CLK_PLL, CLK200PLL, CLK125PLLTX, CLK125PLLTX90, CLK125PLLRX;
    wire PLL_FEEDBACK, LOCKED;
    wire clkin1;
    wire REFCLK_OUT;   // Internal copy of the MGT ref clock
    wire BYPASS_MODE;
    wire BYPASS_MODE_RESET = 0;
    wire BYPASS_CDR = 0;
    wire BYPASS_CMD_CLK_EN;
    wire BYPASS_EXT_SER_CLK_EN;

// Clock OUTPUT driver
    wire MGT_REF_CLK_P, MGT_REF_CLK_N;
    OBUFDS #(
        .IOSTANDARD("LVDS_25"), // Specify the output I/O standard
        .SLEW("FAST")           // Specify the output slew rate
    ) i_OBUFDS_MGT_REF (
        .O(MGT_REF_CLK_P),      // Diff_p output (connect directly to top-level port)
        .OB(MGT_REF_CLK_N),     // Diff_n output (connect directly to top-level port)
        .I(REFCLK_OUT)         // Buffer input
    );

`ifdef KC705
    assign RESET_N = ~RESET_BUTTON;
    assign AURORA_RESET = GPIO_SW_C;
    wire MGT_REF_SEL = 0;   // dummy signal for the ref clk mux only present on the mmc3 hardware
    localparam CONF_CLKIN1_PERIOD = 5.000;
    localparam CONF_CLKFBOUT_MULT = 5;
    localparam CONF_CLKFBOUT_MULT2 = 8;
    localparam CLKOUT0_DIVIDE_VAL = 6;

    wire clkin_buf;
    IBUFDS #(
        .DIFF_TERM("FALSE"),       // Differential Termination
        .IBUF_LOW_PWR("FALSE"),    // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
    ) IBUFDS_inst (
        .O(clkin_buf),  // Buffer output
        .I(CLK200_P),  // Diff_p buffer input (connect directly to top-level port)
        .IB(CLK200_N) // Diff_n buffer input (connect directly to top-level port)
    );

    assign clkin1 = clkin_buf;
    assign SI5324_RST = !RST;
    assign SI5342_FMC_RST = !RST;
`else
    assign RESET_N = RESET_BUTTON;
    assign AURORA_RESET = ~USER_BUTTON;
    localparam CONF_CLKIN1_PERIOD = 10.000;
    localparam CONF_CLKFBOUT_MULT = 10;
    localparam CONF_CLKFBOUT_MULT2 = 16;
    assign clkin1 = clkin;
    localparam CLKOUT0_DIVIDE_VAL = 7;
`endif

// Clock signals only needed for the 10G interface 
`ifdef 10G
    wire TEN_GIG_REF_CLK_P, TEN_GIG_REF_CLK_N;
    `ifdef KC705
        assign TEN_GIG_REF_CLK_P = CLKSi570_P;  // SMA_MGT_REF_CLK_P;
        assign TEN_GIG_REF_CLK_N = CLKSi570_N;  // SMA_MGT_REF_CLK_N;
    `else
        assign TEN_GIG_REF_CLK_P = MGT_REFCLK3_P;
        assign TEN_GIG_REF_CLK_N = MGT_REFCLK3_N;
    `endif
`endif

    PLLE2_BASE #(
        .BANDWIDTH("OPTIMIZED"),    // OPTIMIZED, HIGH, LOW
        .CLKFBOUT_MULT(CONF_CLKFBOUT_MULT),         // Multiply value for all CLKOUT, (2-64)
        .CLKFBOUT_PHASE(0.0),       // Phase offset in degrees of CLKFB, (-360.000-360.000).
        .CLKIN1_PERIOD(CONF_CLKIN1_PERIOD),     // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).

        .CLKOUT0_DIVIDE(CLKOUT0_DIVIDE_VAL),         // Divide amount for CLKOUT0 (1-128)
        .CLKOUT0_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT0_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

        .CLKOUT1_DIVIDE(5),         // Divide amount for CLKOUT0 (1-128)
        .CLKOUT1_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT1_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

        .CLKOUT2_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
        .CLKOUT2_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT2_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

        .CLKOUT3_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
        .CLKOUT3_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT3_PHASE(90.0),       // Phase offset for CLKOUT0 (-360.000-360.000).

        .CLKOUT4_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
        .CLKOUT4_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT4_PHASE(-5.625),     // Phase offset for CLKOUT0 (-360.000-360.000).     // resolution is 45°/[CLKOUTn_DIVIDE]
        //-65 -> 0?; - 45 -> 39;  -25 -> 100; -5 -> 0;

        .CLKOUT5_DIVIDE(10),        // Divide amount for CLKOUT0 (1-128)
        .CLKOUT5_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT5_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

        .DIVCLK_DIVIDE(1),          // Master division value, (1-56)
        .REF_JITTER1(0.0),          // Reference input jitter in UI, (0.000-0.999).
        .STARTUP_WAIT("FALSE")      // Delay DONE until PLL Locks, ("TRUE"/"FALSE")
    )
    PLLE2_BASE_inst (              // VCO = 1 GHz
        .CLKOUT0(BUS_CLK_PLL),     // 142.857 MHz BUS_CLK_PLL
        .CLKOUT1(CLK200PLL),       // 200 MHz for MIG
        .CLKOUT2(CLK125PLLTX),     // 125 MHz
        .CLKOUT3(CLK125PLLTX90),
        .CLKOUT4(CLK125PLLRX),
        .CLKOUT5(),         // 100 MHz drp clock for the Aurora core

        .CLKFBOUT(PLL_FEEDBACK),

        .LOCKED(LOCKED),           // 1-bit output: LOCK

        // Input clock
        .CLKIN1(clkin1),

        // Control Ports
        .PWRDWN(0),
        .RST(!RESET_N),

        // Feedback
        .CLKFBIN(PLL_FEEDBACK)
    );

    wire PLL_FEEDBACK_2, LOCKED_2;
    wire CLK320PLL, CLK160PLL, CLK40PLL;
    PLLE2_BASE #(
        .BANDWIDTH("OPTIMIZED"),    // OPTIMIZED, HIGH, LOW
        .CLKFBOUT_MULT(CONF_CLKFBOUT_MULT2),         // Multiply value for all CLKOUT, (2-64)
        .CLKFBOUT_PHASE(0.0),       // Phase offset in degrees of CLKFB, (-360.000-360.000).
        .CLKIN1_PERIOD(CONF_CLKIN1_PERIOD),     // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).

        .CLKOUT0_DIVIDE(5),         // Divide amount for CLKOUT0 (1-128)
        .CLKOUT0_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT0_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

        .CLKOUT1_DIVIDE(10),         // Divide amount for CLKOUT0 (1-128)
        .CLKOUT1_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT1_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

        .CLKOUT2_DIVIDE(40),         // Divide amount for CLKOUT0 (1-128)
        .CLKOUT2_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT2_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

        .CLKOUT3_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
        .CLKOUT3_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT3_PHASE(90.0),       // Phase offset for CLKOUT0 (-360.000-360.000).

        .CLKOUT4_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
        .CLKOUT4_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT4_PHASE(-5.625),     // Phase offset for CLKOUT0 (-360.000-360.000).     // resolution is 45°/[CLKOUTn_DIVIDE]
        //-65 -> 0?; - 45 -> 39;  -25 -> 100; -5 -> 0;

        .CLKOUT5_DIVIDE(10),        // Divide amount for CLKOUT0 (1-128)
        .CLKOUT5_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
        .CLKOUT5_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

        .DIVCLK_DIVIDE(1),          // Master division value, (1-56)
        .REF_JITTER1(0.0),          // Reference input jitter in UI, (0.000-0.999).
        .STARTUP_WAIT("FALSE")      // Delay DONE until PLL Locks, ("TRUE"/"FALSE")
    )
    PLLE2_BASE_inst_2 (              // VCO = 1 GHz
        .CLKOUT0(CLK320PLL),     // 320 MHz
        .CLKOUT1(CLK160PLL),     // 160 MHz
        .CLKOUT2(CLK40PLL),     // 40 MHz
        .CLKOUT3(),
        .CLKOUT4(),
        .CLKOUT5(),

        .CLKFBOUT(PLL_FEEDBACK_2),

        .LOCKED(LOCKED_2),           // 1-bit output: LOCK

        // Input clock
        .CLKIN1(clkin1),

        // Control Ports
        .PWRDWN(0),
        .RST(!RESET_N),

        // Feedback
        .CLKFBIN(PLL_FEEDBACK_2)
    );


    assign RST = !RESET_N | !LOCKED | !LOCKED_2;
`ifdef KC705
    assign DP1_EN_LPC = ~BYPASS_MODE;  //GPIO_DIP_SW[0];   //"enable": Lanes 0..3 are inputs (L0..3 --> FPGA). "disable": Lanes 0,1 are inputs, Lanes 2,3 are outputs (L0,1 --> FPGA, L2,3 <-- FPGA)
    assign DP2_EN_LPC = 1;   //"enable"=1: Lanes 0..3 are inputs (L0..3 --> FPGA). "disable": Lanes 0,1 are inputs, Lanes 2,3 are outputs (L0,1 --> FPGA, L2,3 <-- FPGA)
`endif
    wire BUS_CLK, CLK125TX, CLK125TX90, CLK125RX, CLKCMD, CLK320, CLK160, CLK40;

`ifdef 10G
    BUFG BUFG_inst_BUS_CKL (.O(BUS_CLK), .I(CLK156M) );
`else
    BUFG BUFG_inst_BUS_CKL (.O(BUS_CLK), .I(BUS_CLK_PLL) );
    `ifdef GMII
        assign CLK125RX = gmii_rx_clk;
        BUFG BUFG_inst_CLK125TX (.O(CLK125TX), .I(CLK125PLLTX));
        ODDR IOB_GTX (.C(CLK125TX), .CE(1'b1), .D1(1'b1), .D2(1'b0), .R(1'b0), .S(1'b0), .Q(gmii_tx_clk));
    `else
        BUFG BUFG_inst_CLK125RX (.O(CLK125RX), .I(rgmii_rxc));
        BUFG BUFG_inst_CLK125TX (.O(CLK125TX), .I(CLK125PLLTX));
        BUFG BUFG_inst_CLK125TX90 (.O(CLK125TX90), .I(CLK125PLLTX90));
    `endif
`endif

    BUFG BUFG_inst_CLKCMDENC (.O(CLKCMD), .I(REFCLK_OUT));

    BUFG BUFG_inst_CLK320 (.O(CLK320), .I(CLK320PLL));
    BUFG BUFG_inst_CLK160 (.O(CLK160), .I(CLK160PLL));
    BUFG BUFG_inst_CLK40 (.O(CLK40), .I(CLK40PLL));

    wire RX_CLK_IN_P, RX_CLK_IN_N;
    wire INIT_CLK_IN_P, INIT_CLK_IN_N;  // 50...200 MHz


`ifdef KC705
    assign INIT_CLK_IN_P = CLKSi570_P;
    assign INIT_CLK_IN_N = CLKSi570_N;
    `ifdef EXT_AURORA_REF_CLK   // external LVDS reference clock input, connected to the "MGT_CLK" SMA inputs
        assign RX_CLK_IN_P = SMA_MGT_REF_CLK_P;
        assign RX_CLK_IN_N = SMA_MGT_REF_CLK_N;
    `else                       // programmable on-board oscillator
        assign RX_CLK_IN_P = Si5324_P;
        assign RX_CLK_IN_N = Si5324_N;
    `endif
`else
    assign INIT_CLK_IN_P = CLK200_P;        // from 200 MHz oscillator
    assign INIT_CLK_IN_N = CLK200_N;
    assign RX_CLK_IN_P   = MGT_REFCLK0_P;   // from Si570
    assign RX_CLK_IN_N   = MGT_REFCLK0_N;
`endif


`ifdef 10G
//------------------------------------------------------------------------------
//	10GbE SiTCP interface
//------------------------------------------------------------------------------
    wire			SiTCPXG_OPEN_REQ;
    wire			SiTCPXG_ESTABLISHED;
    wire			SiTCPXG_CLOSE_REQ;
    wire			SiTCPXG_CLOSE_ACK;
    wire			SiTCPXG_TX_AFULL;
    wire	[63:0]	SiTCPXG_TX_D;
    wire	[3:0]	SiTCPXG_TX_B;
    wire	[15:0]	SiTCPXG_RX_SIZE;
    wire			SiTCPXG_RX_CLR_ENB;
    wire			SiTCPXG_RX_CLR_REQ;
    wire	[15:0]	SiTCPXG_RX_RADR;
    wire	[15:0]	SiTCPXG_RX_WADR;
    wire	[ 7:0]	SiTCPXG_RX_WENB;
    wire	[63:0]	SiTCPXG_RX_WDAT;

    wire	[63:0]	xgmii_rxd;
    wire	[7:0]	xgmii_rxc;
    wire	[7:0]	xgmii_txc;
    wire	[63:0]	xgmii_txd;

    wire            intSfpTxDisable;
    assign	SFP_TX_DISABLE	= ~intSfpTxDisable;

    wire			drp_req;
    wire	[15:0]	drp_daddr;
    wire			drp_den;
    wire	[15:0]	drp_di;
    wire	[15:0]	drp_drpdo;
    wire			drp_drdy;
    wire			drp_dwe;

    wire            CLK156M;
    wire            SiTCP_RST;
    wire            RBCP_ACK, RBCP_ACT, RBCP_WE, RBCP_RE;
    wire [7:0]      RBCP_WD, RBCP_RD;
    wire [31:0]     RBCP_ADDR;
    wire            RST_EEPROM;

    wire BUS_RST_CLK156M;
    cdc_pulse_sync start_pulse_sync (.clk_in(BUS_CLK), .pulse_in(BUS_RST), .clk_out(CLK156M), .pulse_out(BUS_RST_CLK156M));

    ten_gig_eth_pcs_pma ten_gig_eth_pcs_pma	(
        .refclk_p					(TEN_GIG_REF_CLK_P          ),  // input wire refclk_p
        .refclk_n					(TEN_GIG_REF_CLK_N          ),  // input wire refclk_n
        .reset						(0                 			),	// input wire reset
        .coreclk_out				(CLK156M					),	// output wire coreclk_out
        .txp						(SFP_TX_P					),	// output wire txp
        .txn						(SFP_TX_N					),	// output wire txn
        .rxp						(SFP_RX_P					),	// input wire rxp
        .rxn						(SFP_RX_N					),	// input wire rxn
        .xgmii_txd					(xgmii_txd[63:0]			),	// input wire [63 : 0] xgmii_txd
        .xgmii_txc					(xgmii_txc[7:0]				),	// input wire [7 : 0] xgmii_txc
        .xgmii_rxd					(xgmii_rxd[63:0]			),	// output wire [63 : 0] xgmii_rxd
        .xgmii_rxc					(xgmii_rxc[7:0]				),	// output wire [7 : 0] xgmii_rxc
// MDIO
        .mdc						(1'b1						),	// input wire mdc
        .mdio_in					(1'b1						),	// input wire mdio_in
        .mdio_out					(							),	// output wire mdio_out
        .mdio_tri					(							),	// output wire mdio_tri
        .prtad						(5'd0						),	// input wire [4 : 0] prtad
// SPF+ I/F
        .signal_detect				(1'b1						),	// input wire signal_detect
        .tx_fault					(1'b0						),	// input wire tx_fault
        .tx_disable					(intSfpTxDisable			),	// output wire tx_disable
// drp_interface_ports
        .dclk						(CLK156M					),	// input wire dclk	<DRP clock 156MHz>
        .drp_req					(drp_req					),	// output wire drp_req
        .drp_gnt					(drp_req					),	// input wire drp_gnt
// core_gt_drp_interface
        .drp_daddr_i				(drp_daddr[15:0]			),	// input wire [15 : 0] drp_daddr_i
        .drp_den_i					(drp_den					),	// input wire drp_den_i
        .drp_di_i					(drp_di[15:0]				),	// input wire [15 : 0] drp_di_i
        .drp_drpdo_o				(drp_drpdo[15:0]			),	// output wire [15 : 0] drp_drpdo_o
        .drp_drdy_o					(drp_drdy					),	// output wire drp_drdy_o
        .drp_dwe_i					(drp_dwe					),	// input wire drp_dwe_i
// core_gt_drp_interface
        .drp_daddr_o				(drp_daddr[15:0]			),	// output wire [15 : 0] drp_daddr_o
        .drp_den_o					(drp_den					),	// output wire drp_den_o
        .drp_di_o					(drp_di[15:0]				),	// output wire [15 : 0] drp_di_o
        .drp_drpdo_i				(drp_drpdo[15:0]			),	// input wire [15 : 0] drp_drpdo_i
        .drp_drdy_i					(drp_drdy					),	// input wire drp_drdy_i
        .drp_dwe_o					(drp_dwe					),	// output wire drp_dwe_o
//Miscellaneous port
        .core_status				(							),	// output wire [7 : 0] core_status
        .sim_speedup_control		(1'b0						),	// input wire sim_speedup_control
        .pma_pmd_type				(3'b111						),	// input wire [2 : 0] pma_pmd_type
        .resetdone_out				(							),	// output wire resetdone_out
        .rxrecclk_out				(							),	// output wire rxrecclk_out
        .txusrclk_out				(							),	// output wire txusrclk_out
        .txusrclk2_out				(							),	// output wire txusrclk2_out
        .areset_datapathclk_out		(							),	// output wire areset_datapathclk_out
        .gttxreset_out				(							),	// output wire gttxreset_out
        .gtrxreset_out				(							),	// output wire gtrxreset_out
        .txuserrdy_out				(							),	// output wire txuserrdy_out
        .reset_counter_done_out		(							),	// output wire reset_counter_done_out
        .qplllock_out				(							),	// output wire qplllock_out
        .qplloutclk_out				(							),	// output wire qplloutclk_out
        .qplloutrefclk_out			(							)	// output wire qplloutrefclk_out
    );
    assign SiTCPXG_RX_SIZE = 16'hffff;
    WRAP_SiTCPXG_XC7K_128K	#(
        .RxBufferSize				("LongLong"					)	// "Byte":8bit width ,"Word":16bit width ,"LongWord":32bit width , "LongLong":64bit width
    ) WRAP_SiTCPXG_XC7K_128K (
        .REG_FPGA_VER				(0                          ),  // in   : User logic Version(For example, the synthesized date)
        .REG_FPGA_ID				(0                          ),  // in   : User logic ID (We recommend using the lower 4 bytes of the MAC address.)
        //		==== System I/F ====
        .FORCE_DEFAULTn				(0                          ),  // in   : Force to set default values
        .XGMII_CLOCK				(CLK156M					),	// in	: XGMII Clock 156.25MHz
        .RSTs						(0                			),	// in	: System reset (Sync.)
        //		==== XGMII I/F ====
        .XGMII_RXC					(xgmii_rxc[7:0]				),	// in	: Rx control[7:0]
        .XGMII_RXD					(xgmii_rxd[63:0]			),	// in	: Rx data[63:0]
        .XGMII_TXC					(xgmii_txc[7:0]				),	// out  : Control bits[7:0]
        .XGMII_TXD					(xgmii_txd[63:0]			), 	// out  : Data[63:0]
        //		==== 93C46 I/F ====
        `ifdef BDAQ53
        .EEPROM_CS					(EEPROM_CS					),	// out	: Chip select
        .EEPROM_SK					(EEPROM_SK	    			),	// out	: Serial data clock
        .EEPROM_DI					(EEPROM_DI  				),	// out	: Serial write data
        .EEPROM_DO					(EEPROM_DO					),	// in	: Serial read data
        `endif
        //		==== User I/F ====
        .SiTCP_RESET_OUT			(SiTCP_RST  				),	// out	: System reset for user's module
        //			--- RBCP ---
        .RBCP_ACT					(RBCP_ACT					),	// out	: Indicates that bus access is active.
        .RBCP_ADDR					(RBCP_ADDR[31:0]			),	// out	: Address[31:0]
        .RBCP_WE					(RBCP_WE					),	// out	: Write enable
        .RBCP_WD					(RBCP_WD[7:0]				),	// out	: Data[7:0]
        .RBCP_RE					(RBCP_RE					),	// out	: Read enable
        .RBCP_ACK					(RBCP_ACK					),	// in	: Access acknowledge
        .RBCP_RD					(RBCP_RD[7:0]				),	// in	: Read data[7:0]
        //			--- TCP ---
        .USER_SESSION_OPEN_REQ		(SiTCPXG_OPEN_REQ			),	// in	: Request for opening the new session
        .USER_SESSION_ESTABLISHED	(SiTCPXG_ESTABLISHED		),	// out	: Establish of a session
        .USER_SESSION_CLOSE_REQ		(SiTCPXG_CLOSE_REQ			),	// out	: Request for closing session.
        .USER_SESSION_CLOSE_ACK		(SiTCPXG_CLOSE_ACK			),	// in	: Acknowledge for USER_SESSION_CLOSE_REQ.
        .USER_TX_D					(SiTCPXG_TX_D[63:0]			),	// in	: Write data
        .USER_TX_B					(SiTCPXG_TX_B[3:0]			),	// in	: Byte length of USER_TX_DATA(Set to 0 if not written)
        .USER_TX_AFULL				(SiTCPXG_TX_AFULL			),	// out	: Request to stop TX
        .USER_RX_SIZE				(SiTCPXG_RX_SIZE[15:0]		),	// in	: Receive buffer size(byte) caution:Set a value of 4000 or more and (memory size-16) or less
        .USER_RX_CLR_ENB			(SiTCPXG_RX_CLR_ENB			),	// out	: Receive buffer Clear Enable
        .USER_RX_CLR_REQ			(SiTCPXG_RX_CLR_REQ			),	// in	: Receive buffer Clear Request
        .USER_RX_RADR				(SiTCPXG_RX_RADR[15:0]		),	// in	: Receive buffer read address in bytes (unused upper bits are set to 0)
        .USER_RX_WADR				(SiTCPXG_RX_WADR[15:0]		),	// out	: Receive buffer write address in bytes (lower 3 bits are not connected to memory)
        .USER_RX_WENB				(SiTCPXG_RX_WENB[ 7:0]		),	// out	: Receive buffer byte write enable (big endian)
        .USER_RX_WDAT				(SiTCPXG_RX_WDAT[63:0]		)	// out	: Receive buffer write data (big endian)
    );
`else  // 1G SiTCP interface
    // -------  MDIO interface  ------- //
        wire   mdio_gem_mdc;
        wire   mdio_gem_i;
        wire   mdio_gem_o;
        wire   mdio_gem_t;
        wire   link_status;
        wire  [1:0] clock_speed;
        wire   duplex_status;

    `ifndef GMII
        // -------  RGMII interface  ------- //
        wire   gmii_tx_clk;
        wire   gmii_tx_en;
        wire  [7:0] gmii_txd;
        wire   gmii_tx_er;
        wire   gmii_crs;
        wire   gmii_col;
        wire   gmii_rx_clk;
        wire   gmii_rx_dv;
        wire  [7:0] gmii_rxd;
        wire   gmii_rx_er;

        rgmii_io rgmii
        (
            .rgmii_txd(rgmii_txd),
            .rgmii_tx_ctl(rgmii_tx_ctl),
            .rgmii_txc(rgmii_txc),

            .rgmii_rxd(rgmii_rxd),
            .rgmii_rx_ctl(rgmii_rx_ctl),

            .gmii_txd_int(gmii_txd),        // Internal gmii_txd signal.
            .gmii_tx_en_int(gmii_tx_en),
            .gmii_tx_er_int(gmii_tx_er),
            .gmii_col_int(gmii_col),
            .gmii_crs_int(gmii_crs),
            .gmii_rxd_reg(gmii_rxd),        // RGMII double data rate data valid.
            .gmii_rx_dv_reg(gmii_rx_dv),    // gmii_rx_dv_ibuf registered in IOBs.
            .gmii_rx_er_reg(gmii_rx_er),    // gmii_rx_er_ibuf registered in IOBs.

            .eth_link_status(link_status),
            .eth_clock_speed(clock_speed),
            .eth_duplex_status(duplex_status),

            // FOllowing are generated by DCMs
            .tx_rgmii_clk_int(CLK125TX),    // Internal RGMII transmitter clock.
            .tx_rgmii_clk90_int(CLK125TX90),// Internal RGMII transmitter clock w/ 90 deg phase
            .rx_rgmii_clk_int(CLK125RX),    // Internal RGMII receiver clock

            .reset(!phy_rst_n)
        );
    `endif

    // Instantiate tri-state buffer for MDIO
        IOBUF i_iobuf_mdio(
            .O(mdio_gem_i),
            .IO(mdio_phy_mdio),
            .I(mdio_gem_o),
            .T(mdio_gem_t));


    // -------  SiTCP module  ------- //
        wire TCP_OPEN_ACK, TCP_CLOSE_REQ;
        wire TCP_RX_WR, TCP_TX_WR;
        wire TCP_TX_FULL, TCP_ERROR;
        wire [7:0] TCP_RX_DATA, TCP_TX_DATA;
        wire [15:0] TCP_RX_WC;
        wire RBCP_ACK, RBCP_ACT, RBCP_WE, RBCP_RE;
        wire [7:0] RBCP_WD, RBCP_RD;
        wire [31:0] RBCP_ADDR;
        wire SiTCP_RST;
        wire EEPROM_CS_int, EEPROM_SK_int, EEPROM_DI_int, EEPROM_DO_int;

    `ifdef KC705
        localparam CONST_phy_addr = 5'd7;
    `else
        localparam CONST_phy_addr = 5'd3;
    `endif

    // connect the physical EEPROM pins only for the BDAQ53
    `ifdef BDAQ53
        `ifndef _KX1
        assign EEPROM_CS = EEPROM_CS_int;
        assign EEPROM_SK = EEPROM_SK_int;
        assign EEPROM_DI = EEPROM_DI_int;
        assign EEPROM_DO_int = EEPROM_DO;
        `endif
    `endif

    // use PMOD[3:0] to set the IP address
        wire [3:0] IP_ADDR_SEL;
        wire FORCE_DEFAULTn;
    `ifdef BDAQ53
        assign PMOD[7:4] = 4'hf;
        assign IP_ADDR_SEL = {PMOD[0], PMOD[1], PMOD[2], PMOD[3]};
        assign FORCE_DEFAULTn = 1'b0;
    `elsif USBPIX3
        assign PMOD[7:4] = 4'hf;
        assign IP_ADDR_SEL = {PMOD[0], PMOD[1], PMOD[2], PMOD[3]};
        assign FORCE_DEFAULTn = 1'b0;
    `elsif KC705
        assign IP_ADDR_SEL = {GPIO_DIP_SW[0], GPIO_DIP_SW[1], GPIO_DIP_SW[2], GPIO_DIP_SW[3]};
    `else
        assign IP_ADDR_SEL = 4'h0;
        assign FORCE_DEFAULTn = 1'b0;
    `endif

    WRAP_SiTCP_GMII_XC7K_32K #(
        .TIM_PERIOD (8'd142)
    ) sitcp (
        .CLK(BUS_CLK)               ,    // in    : System Clock >129MHz
        .RST(RST)                   ,    // in    : System reset
        // Configuration parameters
        .FORCE_DEFAULTn(FORCE_DEFAULTn), // in    : Load default parameters
        .EXT_IP_ADDR({8'd192, 8'd168, |{IP_ADDR_SEL} ? 8'd10 + IP_ADDR_SEL : 8'd10, 8'd12})  ,    //IP address[31:0] default: 192.168.10.12. If jumpers are set: 192.168.[11..25].12
        .EXT_TCP_PORT(16'd24)       ,    // in    : TCP port #[15:0]
        .EXT_RBCP_PORT(16'd4660)    ,    // in    : RBCP port #[15:0]
        .PHY_ADDR(CONST_phy_addr)   ,    // in    : PHY-device MIF address[4:0]
        // EEPROM
        .EEPROM_CS(EEPROM_CS_int)   ,    // out    : Chip select
        .EEPROM_SK(EEPROM_SK_int)   ,    // out    : Serial data clock
        .EEPROM_DI(EEPROM_DI_int)   ,    // out    : Serial write data
        .EEPROM_DO(EEPROM_DO_int)   ,    // in     : Serial read data
        // user data, initial values are stored in the EEPROM, 0xFFFF_FC3C-3F
        .USR_REG_X3C()              ,    // out    : Stored at 0xFFFF_FF3C
        .USR_REG_X3D()              ,    // out    : Stored at 0xFFFF_FF3D
        .USR_REG_X3E()              ,    // out    : Stored at 0xFFFF_FF3E
        .USR_REG_X3F()              ,    // out    : Stored at 0xFFFF_FF3F
        // MII interface
        .GMII_RSTn(phy_rst_n)       ,    // out    : PHY reset
        .GMII_1000M(1'b1)           ,    // in    : GMII mode (0:MII, 1:GMII)
        // TX
        .GMII_TX_CLK(CLK125TX)      ,    // in    : Tx clock
        .GMII_TX_EN(gmii_tx_en)     ,    // out    : Tx enable
        .GMII_TXD(gmii_txd)         ,    // out    : Tx data[7:0]
        .GMII_TX_ER(gmii_tx_er)     ,    // out    : TX error
        // RX
        .GMII_RX_CLK(CLK125RX)      ,    // in    : Rx clock
        .GMII_RX_DV(gmii_rx_dv)     ,    // in    : Rx data valid
        .GMII_RXD(gmii_rxd)         ,    // in    : Rx data[7:0]
        .GMII_RX_ER(gmii_rx_er)     ,    // in    : Rx error
        .GMII_CRS(gmii_crs)         ,    // in    : Carrier sense
        .GMII_COL(gmii_col)         ,    // in    : Collision detected
        // Management IF
        .GMII_MDC(mdio_phy_mdc)     ,    // out    : Clock for MDIO
        .GMII_MDIO_IN(mdio_gem_i)   ,    // in    : Data
        .GMII_MDIO_OUT(mdio_gem_o)  ,    // out    : Data
        .GMII_MDIO_OE(mdio_gem_t)   ,    // out    : MDIO output enable
        // User I/F
        .SiTCP_RST(SiTCP_RST)       ,    // out    : Reset for SiTCP and related circuits
        // TCP connection control
        .TCP_OPEN_REQ(1'b0)         ,    // in    : Reserved input, shoud be 0
        .TCP_OPEN_ACK(TCP_OPEN_ACK) ,    // out    : Acknowledge for open (=Socket busy)
        .TCP_ERROR(TCP_ERROR)       ,    // out    : TCP error, its active period is equal to MSL
        .TCP_CLOSE_REQ(TCP_CLOSE_REQ)   ,    // out    : Connection close request
        .TCP_CLOSE_ACK(TCP_CLOSE_REQ)   ,    // in    : Acknowledge for closing
        // FIFO I/F
        .TCP_RX_WC(TCP_RX_WC)       ,    // in    : Rx FIFO write count[15:0] (Unused bits should be set 1)
        .TCP_RX_WR(TCP_RX_WR)       ,    // out   : Write enable
        .TCP_RX_DATA(TCP_RX_DATA)   ,    // out   : Write data[7:0]
        .TCP_TX_FULL(TCP_TX_FULL)   ,    // out   : Almost full flag
        .TCP_TX_WR(TCP_TX_WR)       ,    // in    : Write enable
        .TCP_TX_DATA(TCP_TX_DATA)   ,    // in    : Write data[7:0]
        // RBCP
        .RBCP_ACT(RBCP_ACT)         ,    // out   : RBCP active
        .RBCP_ADDR(RBCP_ADDR)       ,    // out   : Address[31:0]
        .RBCP_WD(RBCP_WD)           ,    // out   : Data[7:0]
        .RBCP_WE(RBCP_WE)           ,    // out   : Write enable
        .RBCP_RE(RBCP_RE)           ,    // out   : Read enable
        .RBCP_ACK(RBCP_ACK)         ,    // in    : Access acknowledge
        .RBCP_RD(RBCP_RD)                // in    : Read data[7:0]
    );
`endif

// -------  BUS SIGNALING  ------- //
    wire BUS_WR, BUS_RD, BUS_RST;
    wire [31:0] BUS_ADD;
    wire [7:0] BUS_DATA;
    assign BUS_RST = SiTCP_RST;

`ifdef USE_TCP_TO_BUS
    wire INVALID;

    tcp_to_bus i_tcp_to_bus(
        .BUS_RST(BUS_RST),
        .BUS_CLK(BUS_CLK),

        // SiTCP TCP RX
        .TCP_RX_WC(TCP_RX_WC),
        .TCP_RX_WR(TCP_RX_WR),
        .TCP_RX_DATA(TCP_RX_DATA),

        // SiTCP RBCP (UDP)
        .RBCP_ACT(RBCP_ACT),
        .RBCP_ADDR(RBCP_ADDR),
        .RBCP_WD(RBCP_WD),
        .RBCP_WE(RBCP_WE),
        .RBCP_RE(RBCP_RE),
        .RBCP_ACK(RBCP_ACK),
        .RBCP_RD(RBCP_RD),

        // Basil bus
        .BUS_WR(BUS_WR),
        .BUS_RD(BUS_RD),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA),

        .INVALID(INVALID)
    );
`else
    rbcp_to_bus irbcp_to_bus(
        .BUS_RST(BUS_RST),
        .BUS_CLK(BUS_CLK),

        .RBCP_ACT(RBCP_ACT),
        .RBCP_ADDR(RBCP_ADDR),
        .RBCP_WD(RBCP_WD),
        .RBCP_WE(RBCP_WE),
        .RBCP_RE(RBCP_RE),
        .RBCP_ACK(RBCP_ACK),
        .RBCP_RD(RBCP_RD),
    
        .BUS_WR(BUS_WR),
        .BUS_RD(BUS_RD),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA[7:0])
    );
`endif    


// HitOr
    wire [7:0] HITOR_P;
    wire [7:0] HITOR_N;
    wire [7:0] HITOR_BUF;
    wire [7:0] HITOR;  // HITOR contains all four individual HitOrs from the different pixel locations
    wire [7:0] IO_BUF_SIGNAL;
    wire [7:0] IO_BUF_SIGNAL_inter;
    wire [7:0] IO_BUF_T;

`ifdef BDAQ53
    localparam n_hitors = 8;
    assign HITOR_P = {mDP_GPIO_P[3:0], DP_GPIO_P[3:0]};
    assign HITOR_N = {mDP_GPIO_N[3:0], DP_GPIO_N[3:0]};
    assign HITOR = HITOR_BUF[7:0];
`elsif USBPIX3
    localparam n_hitors = 4;
    assign HITOR_P = RJ45_HITOR_P;
    assign HITOR_N = RJ45_HITOR_N;
    assign HITOR = {4'h0, HITOR_BUF[3:0]};
`elsif KC705
    localparam n_hitors = 4;
    assign HITOR_P = {DP_GPIO_LANE3_LPC_P, DP_GPIO_LANE2_LPC_P, DP_GPIO_LANE1_LPC_P, DP_GPIO_LANE0_LPC_P};
    assign HITOR_N = {DP_GPIO_LANE3_LPC_N, DP_GPIO_LANE2_LPC_N, DP_GPIO_LANE1_LPC_N, DP_GPIO_LANE0_LPC_N};
    assign HITOR = {4'h0, HITOR_BUF[3:0]};
`else
    localparam n_hitors = 0;
    assign HITOR = 0;
`endif

    wire AURORA_CLK_OUT, AURORA_CLK_OUT_REG;
    wire [3:0] DATA_MERGING_OUTPUT;  // Aurora data from the TX core in FW, used for data merging tests
    wire [3:0] DATA_MERGING_ENABLE;  // Output enable signals, used for data merging tests
    wire [7:0] CLK_IO_ODDR;
    assign IO_BUF_T = {~DATA_MERGING_ENABLE, ~BYPASS_MODE, ~BYPASS_MODE, ~BYPASS_MODE, ~BYPASS_MODE};
    assign IO_BUF_SIGNAL_inter = {DATA_MERGING_OUTPUT, BYPASS_MODE_RESET, BYPASS_CMD_CLK_EN ? CLKCMD : 1'b0, BYPASS_EXT_SER_CLK_EN ? AURORA_CLK_OUT : 1'b0, BYPASS_CDR};
    assign CLK_IO_ODDR = {DATA_MERGING_CLK, DATA_MERGING_CLK, DATA_MERGING_CLK, DATA_MERGING_CLK, BUS_CLK, CLKCMD, AURORA_CLK_OUT, BUS_CLK};

//    assign IO_BUF_T = {~DATA_MERGING_ENABLE, ~DATA_MERGING_ENABLE};
//    assign IO_BUF_SIGNAL = {DATA_MERGING_OUTPUT, DATA_MERGING_OUTPUT};

    genvar gen_hitor;
    generate
        if (n_hitors>0) begin
            for (gen_hitor=0; gen_hitor<n_hitors; gen_hitor=gen_hitor+1) begin : generate_hitor_ibufds
                ODDR oddr_hitor_gen(
                    .D1(IO_BUF_SIGNAL_inter[gen_hitor]), .D2(IO_BUF_SIGNAL_inter[gen_hitor]),
                    .C(CLK_IO_ODDR[gen_hitor]), .CE(1'b1), .R(1'b0), .S(1'b0),
                    .Q(IO_BUF_SIGNAL[gen_hitor])
                );
                IOBUFDS #(
                    .DIFF_TERM("TRUE"),
                    .IBUF_LOW_PWR("FALSE"),
                    .IOSTANDARD("LVDS_25")
                ) IOBUFDS_inst_HITOR0 (
                    .O(HITOR_BUF[gen_hitor]),
                    .IO(HITOR_P[gen_hitor]),
                    .IOB(HITOR_N[gen_hitor]),
                    .T(IO_BUF_T[gen_hitor]),
                    .I(IO_BUF_SIGNAL[gen_hitor])
                );
            end
        end
    endgenerate


// ----- bdaq53 CORE ----- //
    wire CMD_OUT_CH0, CLKCMD_REG;
    wire CMD_OUTPUT_EN;
    wire [7:0] AUR_RX_LANE_UP;
    wire [3:0] RX_MULTILLANE_UP;
    wire [7:0] AUR_RX_CHANNEL_UP;
    wire [7:0] AUR_PLL_LOCKED;
    wire [31:0] FIFO_DATA;
    wire FIFO_WRITE, FIFO_EMPTY, FIFO_FULL;
    wire DATA_MERGING_CLK;

// signals for simulation testbench
    wire BUS_BYTE_ACCESS;
    wire CMD_DATA, CMD_WRITING;

`ifdef BDAQ53
    `ifndef _KX1
        wire [3:0] GPIO_RESET_inverted;
        assign GPIO_RESET = ~GPIO_RESET_inverted;   // invert for the low-active opto-coupler
    `endif
`endif

// LEMO RX
    wire LEMO_TRIGGER, MULTI_PURPOSE;
`ifndef KC705
    assign LEMO_TRIGGER = LEMO_RX0;  // trigger input via LEMO
    assign MULTI_PURPOSE = LEMO_RX1;  // multi purpose input (to TLU)
`else
    assign LEMO_TRIGGER = 0;
    assign MULTI_PURPOSE = 0;
`endif

// LEMO TX
    wire RJ45_BUSY;
    wire RJ45_CLK;
    wire CMD_LOOP_START_PULSE;
    wire [1:0] LEMO_MUX_TX1, LEMO_MUX_TX0, LEMO_MUX_RX1, LEMO_MUX_RX0;
`ifndef KC705
    assign LEMO_TX0 = LEMO_MUX_TX0[1] ? (LEMO_MUX_TX0[0] ? 1'b0 : 1'b0) : (LEMO_MUX_TX0[0] ? CMD_LOOP_START_PULSE : RJ45_CLK);
    assign LEMO_TX1 = LEMO_MUX_TX1[1] ? (LEMO_MUX_TX1[0] ? 1'b0 : 1'b0) : (LEMO_MUX_TX1[0] ? 1'b0 : RJ45_BUSY);
`else
    wire RJ45_RESET, LEMO_TX0;  // Have only TX1 as output in case of KC705
    assign RJ45_RESET = 0;
    assign LEMO_TX0 = 0;
    assign LEMO_TX1 = RJ45_BUSY;
`endif


    bdaq53_core #(
        .VERSION_MAJOR(VERSION_MAJOR),
        .VERSION_MINOR(VERSION_MINOR),
        .VERSION_PATCH(VERSION_PATCH)
    ) i_bdaq53_core (
        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),
        .BUS_BYTE_ACCESS(BUS_BYTE_ACCESS),

        // Clocks from oscillators and mux select
        .INIT_CLK(BUS_CLK),
        .DRP_CLK(BUS_CLK),
        .RX_CLK_IN_P(RX_CLK_IN_P),
        .RX_CLK_IN_N(RX_CLK_IN_N),
    `ifdef KC705
        .MGT_REF_SEL(),
    `else
        .MGT_REF_SEL(MGT_REF_SEL),
    `endif

        // PLL
        .CLK_CMD(CLKCMD),
        .REFCLK_OUT(REFCLK_OUT),
        .AURORA_CLK_OUT(AURORA_CLK_OUT),
        .AURORA_RESET(AURORA_RESET),

    // Aurora lanes
    `ifdef _FMC_LPC
        .MGT_RX_P(MGT_RX_FMC_LPC_P),
        .MGT_RX_N(MGT_RX_FMC_LPC_N),
    `else
        .MGT_RX_P(MGT_RX_P),
        .MGT_RX_N(MGT_RX_N),
    `endif

    `ifdef USBPIX3    // USBPIX3 uses MGTs
        .TX_P(MGT_TX_P),
        .TX_N(MGT_TX_N),
    `endif
    `ifdef KC705      // KC705 uses MGTs
        `ifdef _SMA
            .TX_P(MGT_TX_P),
            .TX_N(MGT_TX_N),
        `elsif _FMC_LPC
            .TX_P(MGT_TX_FMC_LPC_P),
            .TX_N(MGT_TX_FMC_LPC_N),
        `endif
    `endif
    `ifdef BDAQ53     // BDAQ53 uses SelectIOs
        .TX_P(),
        .TX_N(),
    `endif

        .DATA_MERGING_OUTPUT(DATA_MERGING_OUTPUT),
        .DATA_MERGING_ENABLE(DATA_MERGING_ENABLE),
        .AURORA_SER_CLK(),
        .AURORA_SER_CLK_OUT(DATA_MERGING_CLK),

        // CMD encoder
        .CMD_WRITING(CMD_WRITING),
        .CMD_DATA(CMD_DATA),
        .CMD_OUT(CMD_OUT_CH0),
        .CMD_OUTPUT_EN(CMD_OUTPUT_EN),
        .CMD_LOOP_START_PULSE(CMD_LOOP_START_PULSE),

        .BYPASS_MODE(BYPASS_MODE),

        // HitOr lines
        .HITOR(HITOR),

    `ifdef BDAQ53
        //PMOD SIGNAL
        .PMOD(),
        // Displayport "slow control" signals
        .GPIO_RESET(GPIO_RESET_inverted),
        .GPIO_SENSE(GPIO_SENSE),
        .NTC_MUX(NTC_MUX),
    `endif

    `ifndef USBPIX3
        .BYPASS_MODE_RESET(BYPASS_MODE_RESET),
        .BYPASS_CDR(BYPASS_CDR),
        .BYPASS_CMD_CLK_EN(BYPASS_CMD_CLK_EN),
        .BYPASS_EXT_SER_CLK_EN(BYPASS_EXT_SER_CLK_EN),
    `endif

    // Debug signals
    `ifdef USBPIX3
        .DEBUG_TX0(),
        .DEBUG_TX1(),
    `else
        .DEBUG_TX0(),
        .DEBUG_TX1(),
    `endif

        .RX_LANE_UP(AUR_RX_LANE_UP),
        .RX_MULTILLANE_UP(RX_MULTILLANE_UP),
        .RX_CHANNEL_UP(AUR_RX_CHANNEL_UP),
        .PLL_LOCKED(AUR_PLL_LOCKED),

        // I2C bus
        .I2C_SDA(I2C_SDA),
        .I2C_SCL(I2C_SCL),

        // FIFO control signals (TX FIFO of DAQ)
        .FIFO_DATA(FIFO_DATA),
        .FIFO_WRITE(FIFO_WRITE),
        .FIFO_EMPTY(FIFO_EMPTY),
        .FIFO_FULL(FIFO_FULL),

        // TLU
        .RJ45_TRIGGER(RJ45_TRIGGER),
        .RJ45_RESET(RJ45_RESET),
        .RJ45_BUSY(RJ45_BUSY),
        .RJ45_CLK(RJ45_CLK),

        // TRIGGER
        .LEMO_TRIGGER(LEMO_TRIGGER),
        .MULTI_PURPOSE(MULTI_PURPOSE),

        // TDC
        .CLK320(CLK320),
        .CLK160(CLK160),
        .CLK40(CLK40),

        .LEMO_MUX({LEMO_MUX_TX1, LEMO_MUX_TX0, LEMO_MUX_RX1, LEMO_MUX_RX0})
    );


// Command encoder outputs
    wire CMD_FMC_OUT_REG;
    wire [3:0] CMD_P, CMD_N;
    wire CMD_CLK_OUT_P, CMD_CLK_OUT_N;
    wire AURORA_CLK_OUT_P, AURORA_CLK_OUT_N;

`ifdef KC705
    localparam nr_of_cmd_outputs = 2;

    wire [nr_of_cmd_outputs-1:0] CMD_OUT;
    assign CMD_OUT = {~CMD_OUT_CH0, CMD_OUT_CH0};

    assign CMD_FMC_LPC_P = CMD_P[0];
    assign CMD_FMC_LPC_N = CMD_N[0];

    assign USER_SMA_P = CMD_P[1];
    assign USER_SMA_N = CMD_N[1];

    assign DP1_EXT_CMD_CLK_P = CMD_CLK_OUT_P;
    assign DP1_EXT_CMD_CLK_N = CMD_CLK_OUT_N;
    assign DP1_EXT_SER_CLK_P = AURORA_CLK_OUT_P;
    assign DP1_EXT_SER_CLK_N = AURORA_CLK_OUT_N;
`elsif BDAQ53
    `ifdef _KX1
        localparam nr_of_cmd_outputs = 1;
        wire [nr_of_cmd_outputs-1:0] CMD_OUT;
        assign CMD_OUT = ~CMD_OUT_CH0;
    `else
        localparam nr_of_cmd_outputs = 4;
        wire [nr_of_cmd_outputs-1:0] CMD_OUT;
        assign CMD_OUT = {~CMD_OUT_CH0, ~CMD_OUT_CH0, ~CMD_OUT_CH0, ~CMD_OUT_CH0};
        assign DP_SL_AUX_P[2:0] = CMD_P[3:1];
        assign DP_SL_AUX_N[2:0] = CMD_N[3:1];
        assign DP_GPIO_AUX_P = CMD_CLK_OUT_P;
        assign DP_GPIO_AUX_N = CMD_CLK_OUT_N;
    `endif
        assign DP_ML_AUX_P = CMD_P[0];
        assign DP_ML_AUX_N = CMD_N[0];
`elsif USBPIX3
    localparam nr_of_cmd_outputs = 0;
    wire [nr_of_cmd_outputs-1:0] CMD_OUT;
`else
    localparam nr_of_cmd_outputs = 0;
    wire [nr_of_cmd_outputs-1:0] CMD_OUT;
`endif

    wire [nr_of_cmd_outputs-1:0] CMD_OUT_REG;

    genvar i;
    generate
        for (i=0; i<nr_of_cmd_outputs; i=i+1) begin : generate_cmd_obufds
            ODDR oddr_cmd_gen(
                .D1(CMD_OUT[i]), .D2(CMD_OUT[i]),
                .C(CLKCMD), .CE(1'b1), .R(1'b0), .S(1'b0),
                .Q(CMD_OUT_REG[i])
            );

            OBUFDS #(
                .IOSTANDARD("LVDS"),    // Specify the output I/O standard
                .SLEW("FAST")           // Specify the output slew rate
            ) i_OBUFDS_CMD_DATA_gen (
                .O(CMD_P[i]),           // Diff_p output (connect directly to top-level port)
                .OB(CMD_N[i]),          // Diff_n output (connect directly to top-level port)
                .I(CMD_OUT_REG[i])      // Buffer input
            );
        end
    endgenerate

`ifdef 10G
`ifdef KC705
    wire CLK156M_REG;
    ODDR oddr_156M(
        .D1(1'b1), .D2(1'b0),
        .C(CLK156M), .CE(1'b1), .R(1'b0), .S(1'b0),
        .Q(CLK156M_REG)
    );

    OBUFDS #(
        .IOSTANDARD("LVDS"),    // Specify the output I/O standard
        .SLEW("FAST")           // Specify the output slew rate
    ) i_OBUFDS_USER_SMA_CLK (
        .O(USER_SMA_P),   // Diff_p output (connect directly to top-level port)
        .OB(USER_SMA_N),  // Diff_n output (connect directly to top-level port)
        .I(CLK156M_REG)      // Buffer input
    );
`endif
`endif

    ODDR oddr_aurora(
        .D1(1'b1), .D2(1'b0),
        .C(AURORA_CLK_OUT), .CE(1'b1), .R(1'b0), .S(1'b0),
        .Q(AURORA_CLK_OUT_REG)
    );

    OBUFDS #(
        .IOSTANDARD("LVDS"),    // Specify the output I/O standard
        .SLEW("FAST")           // Specify the output slew rate
    ) i_OBUFDS_AURORA_CLK (
        .O(AURORA_CLK_OUT_P),   // Diff_p output (connect directly to top-level port)
        .OB(AURORA_CLK_OUT_N),  // Diff_n output (connect directly to top-level port)
        .I(AURORA_CLK_OUT_REG)      // Buffer input
    );

    ODDR oddr_cmd_clk(
        .D1(1'b1), .D2(1'b0),
        .C(CLKCMD), .CE(1'b1), .R(1'b0), .S(1'b0),
        .Q(CLKCMD_REG)
    );

    OBUFDS #(
        .IOSTANDARD("LVDS"),    // Specify the output I/O standard
        .SLEW("FAST")           // Specify the output slew rate
    ) i_OBUFDS_CMD_CLK (
        .O(CMD_CLK_OUT_P),      // Diff_p output (connect directly to top-level port)
        .OB(CMD_CLK_OUT_N),     // Diff_n output (connect directly to top-level port)
        .I(CLKCMD_REG)          // Buffer input
    );


// -------  MODULES for fast data readout(FIFO) - cdc_fifo is for timing reasons
    wire [31:0] cdc_data_out;
    wire tcp_fifo_full, cdc_fifo_empty;

cdc_syncfifo #(
    .DSIZE(32), .ASIZE(3)
) cdc_syncfifo_i (
        .rdata(cdc_data_out),
        .wfull(FIFO_FULL),
        .rempty(cdc_fifo_empty),
        .wdata(FIFO_DATA),
        .winc(FIFO_WRITE), .wclk(BUS_CLK), .wrst(BUS_RST),
        .rinc(!tcp_fifo_full), .rclk(BUS_CLK), .rrst(BUS_RST)
);

`ifdef 10G
    wire [31:0] TCP10G_TX_DATA_LOW;
    reg [63:0] TCP10G_TX_DATA;
    wire TCP_TX_WR;
    reg TCP10G_WORD_CNT = 0;
    wire TCP_TX_WR_ready;
    reg SiTCPXG_TX_VALID = 0;

    gerneric_fifo #(
        .DATA_SIZE(32),
        .DEPTH(128*1024)
    )  fifo_10G (
        .clk(CLK156M),
        .reset(BUS_RST),
        .write(!cdc_fifo_empty),
        .read(TCP_TX_WR),
        .data_in(cdc_data_out),
        .full(tcp_fifo_full),
        .empty(FIFO_EMPTY),
        .data_out(TCP10G_TX_DATA_LOW),
        .size()
        );

    assign TCP_TX_WR = SiTCPXG_ESTABLISHED & !SiTCPXG_TX_AFULL & !FIFO_EMPTY;

    `define sitcp_32bit

    `ifndef sitcp_32bit
        always @ (posedge CLK156M) begin
            if (TCP_TX_WR) begin
                if (TCP10G_WORD_CNT == 0)
                    TCP10G_TX_DATA[63:32] <= TCP10G_TX_DATA_LOW;
                else begin
                    TCP10G_TX_DATA[31:0] <= TCP10G_TX_DATA_LOW;
                    SiTCPXG_TX_VALID <= 1;
                end
                TCP10G_WORD_CNT <= ~TCP10G_WORD_CNT;
            end
            else
                SiTCPXG_TX_VALID <= 0;
        end

        // 64 bit bus
        assign TCP_TX_WR_ready = SiTCPXG_ESTABLISHED & !SiTCPXG_TX_AFULL & !FIFO_EMPTY;
//        assign SiTCPXG_TX_B = (TCP_TX_WR & TCP10G_WORD_CNT) ? 4'd8 : 4'd0;
        assign SiTCPXG_TX_B = SiTCPXG_TX_VALID ? 4'd8 : 4'd0;
        assign SiTCPXG_TX_D = {TCP10G_TX_DATA[7:0], TCP10G_TX_DATA[15:8], TCP10G_TX_DATA[23:16], TCP10G_TX_DATA[31:24],
                            TCP10G_TX_DATA[39:32], TCP10G_TX_DATA[47:40], TCP10G_TX_DATA[55:48], TCP10G_TX_DATA[63:56]};  // byte swapping
    `else
    /*
        always @ (posedge CLK156M) begin
            if (TCP_TX_WR) begin
                TCP10G_TX_DATA <= TCP10G_TX_DATA_LOW;
                SiTCPXG_TX_VALID <= 1;
                end
            else
                SiTCPXG_TX_VALID <= 0;
        end
    */

        // 32 bit bus
//        assign SiTCPXG_TX_B = SiTCPXG_TX_VALID ? 4'd4 : 4'd0;
//        assign SiTCPXG_TX_D = {TCP10G_TX_DATA[7:0], TCP10G_TX_DATA[15:8], TCP10G_TX_DATA[23:16], TCP10G_TX_DATA[31:24], 32'h00000000};
        assign SiTCPXG_TX_B = TCP_TX_WR ? 4'd4 : 4'd0;
        assign SiTCPXG_TX_D = {TCP10G_TX_DATA_LOW[7:0], TCP10G_TX_DATA_LOW[15:8], TCP10G_TX_DATA_LOW[23:16], TCP10G_TX_DATA_LOW[31:24], 32'h00000000};
    `endif 

    // TCP connection
    assign SiTCPXG_CLOSE_ACK = SiTCPXG_CLOSE_REQ;  // close  the TCP connection uppon request
//    assign SiTCPXG_OPEN_REQ = ~SiTCPXG_ESTABLISHED & ~SiTCPXG_CLOSE_REQ;   // open and hold a connection as long as it is not actively closed
//    assign SiTCPXG_OPEN_REQ = (~SiTCPXG_ESTABLISHED | SiTCPXG_OPEN_REQ) & ~SiTCPXG_CLOSE_REQ;   // open and hold a connection as long as it is not actively closed

`else
    fifo_32_to_8 #(
        .DEPTH(128*1024)
    ) i_data_fifo (
        .RST(BUS_RST),
        .CLK(BUS_CLK),

        .WRITE(!cdc_fifo_empty),
        .READ(TCP_TX_WR),
        .DATA_IN(cdc_data_out),
        .FULL(tcp_fifo_full),
        .EMPTY(FIFO_EMPTY),
        .DATA_OUT(TCP_TX_DATA)
        );
    assign TCP_TX_WR = !TCP_TX_FULL & !FIFO_EMPTY;
`endif


// Status LEDs
    wire [7:0] LED_int;
//assign LED_int = {TCP_OPEN_ACK, TCP_RX_WR, TCP_TX_WR, FIFO_FULL, AUR_RX_LANE_UP[0], AUR_RX_CHANNEL_UP[0], AUR_PLL_LOCKED, LOCKED};
`ifdef _1LANE
   assign LED_int = {AUR_RX_LANE_UP[0], AUR_RX_LANE_UP[1], AUR_RX_LANE_UP[2], AUR_RX_LANE_UP[3], FIFO_FULL, AUR_RX_LANE_UP[6], AUR_RX_LANE_UP[5], AUR_RX_LANE_UP[4]};
`else
    assign LED_int = {RX_MULTILLANE_UP[0], RX_MULTILLANE_UP[1], RX_MULTILLANE_UP[2], RX_MULTILLANE_UP[3], FIFO_FULL, 1'b0, 1'b0, & AUR_PLL_LOCKED[3:0]};
`endif

`ifdef KC705
    assign LED = LED_int;
`else
    assign LED = ~LED_int;  // BDAQ uses active-low LED drivers
`endif


// Reduce the fan speed (KC705)
`ifdef KC705
    clock_divider #(
        .DIVISOR(142860)
    ) i_clock_divisor_fan (
        .CLK(BUS_CLK),
        .RESET(1'b0),
        .CE(),
        .CLOCK(SM_FAN_PWM)
    );
`endif

endmodule

