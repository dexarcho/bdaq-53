#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan sends triggers without injection
    into enabled pixels to identify noisy pixels.
'''

import zlib  # workaround

import numpy as np

from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis_utils as au

local_configuration = {
    # Scan parameters
    'start_row'    : 0,
    'stop_row'     : 192,
    'maskfile'     : None,
    
    # Noise occupancy scan parameters
    'n_triggers'   : 1e5,

    # Threshold tuning parameters
    'VTH_name'     : 'Vthreshold_LIN',
    'VTH_start'    : 350,
    'start_column' : 128,
    'stop_column'  : 264,
    'VTH_step'     : 1,
    
    'limit'        : 1.0,
    
    }


class NoiseThresholdTuning(ScanBase):
    scan_id = "noise_threshold_tuning"

    def take_data(self):
        
        error = False
        # pbar = tqdm(total=len(self.steps)*50000)        
        with self.readout(scan_param_id=1, fill_buffer=True, clear_buffer=True):
            for i in self.steps:
                self.chip.write_command(self.trigger_data, repetitions=50000)
                # pbar.update(50000)
        # pbar.close()
        
        # if self.chip['rx'].LOST_COUNT:  
        if any(self.fifo_readout.get_rx_fifo_discard_count()):
            error = True
            self.chip['rx'].RESET_COUNTERS = 1
            
            self.chip['rx'].RESET = 1 #temporary fix needs firmware update
            while not self.chip['rx'].RX_READY:
                pass
        
        dqdata = self.fifo_readout.data
        rawdata = np.concatenate([item[0] for item in dqdata])

        (hits, hist_occ, hist_tot, hist_rel_bcid, hist_trigger_id,
         hist_scurve, hist_event_status,
         hist_bcid_error) = au.init_outs(n_hits=len(rawdata) * 4, n_scan_params=1)

        (n_hits, event_number, chunk_offset, trg_id, prev_trg_number) = au.interpret_data(
            rawdata=rawdata,
            hits=hits,
            hist_occ=hist_occ,
            hist_tot=hist_tot,
            hist_rel_bcid=hist_rel_bcid,
            hist_trigger_id=hist_trigger_id,
            hist_scurve=hist_scurve,
            hist_event_status=hist_event_status,
            hist_bcid_error=hist_bcid_error,
            scan_param_id=0)
        
        return np.count_nonzero(hist_occ) , hist_occ, error
            
    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192,
             n_triggers=1e6, wait_cycles=200, limit=1, **kwargs):
       
        
        self.n_pixels = (stop_column - start_column) * (stop_row - start_row)
        n_triggers = int(n_triggers)
        if n_triggers > 50000:
            self.steps = range(0, n_triggers, 50000)
        else:
            self.steps = [n_triggers]
            
        # disable injection
        self.chip.enable_macro_col_cal(macro_cols=None) 
        self.chip.injection_mask[:, :] = False
        
        self.trigger_data = self.chip.send_trigger(trigger=0b1111, write=False) * 8  # effectively we send x32 triggers
        self.trigger_data += self.chip.write_sync_01(write=False) * wait_cycles
        
        self.logger.info('Starting scan...')     
        
        n_pixels = (stop_column - start_column) * (stop_row - start_row)
        min_tdac, max_tdac, scan_len = self.get_tdac_range(start_column, stop_column)
        
        self.TDAC_mask = np.zeros((400, 192), dtype=int)
        if min_tdac < 0:
            optimal_tdac_mean = 0
            self.TDAC_mask[start_column:stop_column, start_row:stop_row] = min_tdac
        else:
            optimal_tdac_mean = 7
            self.TDAC_mask[start_column:stop_column, start_row:stop_row] = max_tdac - 1
            
        self.chip.tdac_mask[:, :] = self.TDAC_mask
        self.chip.write_masks()
        
        for vth in range(kwargs.get('VTH_start'), 0, -1 * kwargs.get('VTH_step', 1)):
            self.vth = vth
            # self.logger.info('Set %s to %i' % (kwargs.get('VTH_name'), vth))
            # self.chip.set_dacs(**kwargs)
            self.chip.write_register(kwargs.get('VTH_name'), vth)  # write threshold

            iteration = 0
            while True:
                n_total_hits, hit_occ, error = self.take_data()
                
                something_to_change = False
                for col in range(start_column, stop_column):
                    for row in range(start_row, stop_row):
                        if hit_occ[col, row]:
                            something_to_change = True
                            if col >= 128 and col < 264:
                                if self.TDAC_mask[col, row] > 0:
                                    self.TDAC_mask[col, row] -= 1
                                else:
                                    self.chip.enable_mask[col, row] = False
                            if col >= 264 and col <= 400:
                                if self.TDAC_mask[col, row] < 15:
                                    self.TDAC_mask[col, row] += 1
                                else:
                                    self.chip.enable_mask[col, row] = False

                self.chip.tdac_mask[:, :] = self.TDAC_mask
                
                if something_to_change:
                    self.chip.write_masks(range(start_column, stop_column))  # can only update what has changed to sepeed up

                this_tdac = self.TDAC_mask[start_column:stop_column, :].flatten()
                mean_tdac = np.mean(this_tdac)
                if min_tdac < 0:
                    this_tdac = this_tdac + 15
                bc = np.bincount(this_tdac, minlength=scan_len)
                n_disabled_pixels = (self.chip.enable_mask[start_column:stop_column, start_row:stop_row] == False).sum()
                r_disabled_pixels = float(n_disabled_pixels) / float(n_pixels) * 100.
                np.set_printoptions(linewidth=120)
                self.logger.info('Pixel hit: %d | TDAC distribution in iteration %i at %s = %i:\n%s\nMean TDAC is %1.2f | %i pixels are disabled. This corresponds to %1.2f%% of %i total pixels. | max_occ %d' % (
                    n_total_hits, iteration, kwargs.get('VTH_name'), self.vth, str(bc), mean_tdac, n_disabled_pixels, r_disabled_pixels, n_pixels, np.max(hit_occ)))

                iteration += 1
                if n_total_hits < 0.001 * n_pixels and np.max(hit_occ) < 5 and not error:  # no errors ? this is bad?
                    break

            if r_disabled_pixels >= limit or (mean_tdac >= optimal_tdac_mean and min_tdac < 0) or (mean_tdac <= optimal_tdac_mean and min_tdac == 0)  :
                break
       
        self.disable_mask = self.chip.enable_mask
        self.save_disable_mask(update=False)
        self.save_tdac_mask()

if __name__ == "__main__":
    scan = NoiseThresholdTuning()
    scan.start(**local_configuration)

