#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    ITkPix self trigger scan (triggers are generated inside chip itself). Only working for ITkPix.
'''


from bdaq53.scans.scan_ext_trigger import ExtTriggerScan
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 384,

    # Stop conditions (choose one)
    'scan_timeout': 60,          # Timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time

    'trigger_latency': 482,     # Latency of trigger in units of 25 ns (BCs). For 8 triggers. Use 484 for 4 triggers, 485 for 2 triggers, and 486 for 1 trigger
    'trigger_length': 8,       # Length of trigger command (amount of consecutive BCs are read out)

    # Trigger configuration
    'bench': {'TLU': {
        'TRIGGER_MODE': 0,      # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 0     # Selecting trigger input: HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
    }
    }
}


class ITkPixSelfTriggerScan(ExtTriggerScan):
    scan_id = 'self_trigger_scan'

    def _configure(self, scan_timeout=60, trigger_length=32, trigger_latency=482, start_column=0, stop_column=400, start_row=0, stop_row=192, **kwargs):
        '''
        Parameters
        ----------
        scan_timeout : int / False
            Scan time in seconds. Set to False for no limit.
        trigger_length : int
            Amount of BCIDs to read out on every trigger.
        trigger_latency : int
            Latency of trigger in units of 25ns.
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''

        super(ITkPixSelfTriggerScan, self)._configure(scan_timeout=scan_timeout, trigger_length=trigger_length, trigger_latency=trigger_latency, start_column=start_column, stop_column=stop_column, start_row=start_row, stop_row=stop_row, **kwargs)

        # Scan works only with ITkPix
        if self.chip.chip_type.lower() == 'rd53a':
            raise RuntimeError('This scan is not working with the RD53A readout chip!')

        # Enable Hitor
        self.chip.masks['hitbus'][:] = self.chip.masks['enable'][:]
        self.chip.masks.update(force=True)

        # Configure self-trigger
        self.chip.registers['SelfTriggerConfig_0'].write(trigger_length + (trigger_latency << 5))  # number of consecutive triggers and latenecy
        self.chip.registers['SelfTriggerConfig_1'].write(0b110001)  # Enable self trigger, set digital threshold
        self.chip.registers['HitOrPatternLUT'].write(0xFFFE)  # all HITOR pattern combinations
        self.chip.registers['TriggerConfig'].write(496)  # Latency. FIXME: adjust such that this can be set to 500.

    def _scan(self, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, fraction=0.99, use_tdc=False, **_):
        self.enable_hitor(True)
        super(ITkPixSelfTriggerScan, self)._scan(scan_timeout=scan_timeout, **_)
        self.enable_hitor(False)

        self.chip.registers['SelfTriggerConfig_1'].write(0b010001)  # Disable self trigger

    def _analyze(self):
        self.configuration['bench']['analysis']['cluster_hits'] = True
        self.configuration['bench']['analysis']['store_hits'] = True
        self.configuration['bench']['analysis']['align_method'] = 1  # No TLU words for event building
        self.configuration['bench']['analysis']['analyze_tdc'] = self.configuration['scan'].get('use_tdc', False)

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


if __name__ == '__main__':
    with ITkPixSelfTriggerScan(scan_config=scan_configuration) as scan:
        scan.start()
