/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype wire

`ifdef _1LANE
    // This scheme has to be adapted to work with the 1-lane version
`elsif _2LANE
    `include "2lanes/exdes/aurora_64b66b_2lanes_exdes.v"
`elsif _3LANE
    `include "3lanes/exdes/aurora_64b66b_3lanes_exdes.v"
`elsif _4LANE
    `include "4lanes/exdes/aurora_64b66b_4lanes_exdes.v"
`endif

// output format #ID (as parameter IDENTIFIER + 1 frame start + 16 bit data)

module rx_aurora_64b66b_core
#(
    parameter ABUSWIDTH = 16,
    parameter IDENTIFIER = 0,
    parameter AURORA_LANES = 4,
    parameter AURORA_CHANNELS = 1
    )(
    input wire [1:0] CHIP_TYPE,
    input wire [AURORA_LANES-1:0] RXP, RXN,
    output reg RX_CLK,

    input wire GTXQ0_P, GTXQ0_N,
    input wire INIT_CLK,
    input wire DRP_CLK_IN,
    output wire REFCLK_OUT,

    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,   //output format #ID (IDENTIFIER + 1 frame start + 16 bit data)

    input wire USERK_FIFO_READ,
    output wire USERK_FIFO_EMPTY,
    output wire [31:0] USERK_FIFO_DATA,

    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,

    output wire RX_READY,
    output wire LOST_ERROR,
    output wire RX_LANE_UP,
    output wire RX_CHANNEL_UP,
    output wire [AURORA_LANES-1:0] RX_MULTILLANE_UP,
    output wire PLL_LOCKED,

    output wire RX_SOFT_ERROR,
    output wire RX_HARD_ERROR,

    input wire AURORA_RESET
);

localparam VERSION = 4;
localparam RX_CHANNELS = 1;

wire SOFT_RST;
assign SOFT_RST = (BUS_ADD==0 && BUS_WR);

wire RST;
assign RST = BUS_RST | SOFT_RST;

reg RESET_LOGIC;    // reset only the logic and the fifos

wire USER_CLK;

reg CONF_EN;
reg [1:0] CONF_USER_K_FILTER_MODE;
localparam _USERK_BLOCK = 2'h0;
localparam _USERK_FILTER = 2'h1;
localparam _USERK_PASS = 2'h2;
reg [7:0] USER_K_FILTER_MASK_1, USER_K_FILTER_MASK_2, USER_K_FILTER_MASK_3;
reg [7:0] LOST_DATA_CNT = 8'd0;
reg [31:0] FRAME_COUNTER = 32'd0;
reg RESET_COUNTERS;
wire RESET_COUNTERS_pulse;

always @(posedge BUS_CLK) begin
    if(RST) begin
        CONF_EN <= 1;
        CONF_USER_K_FILTER_MODE <= _USERK_BLOCK;
        USER_K_FILTER_MASK_1 <= 8'h00;
        USER_K_FILTER_MASK_2 <= 8'h01;
        USER_K_FILTER_MASK_3 <= 8'h02;
        RESET_COUNTERS <= 0;
        RESET_LOGIC <= 0;
    end
    else if(BUS_WR) begin
        if(BUS_ADD == 2) begin
            CONF_USER_K_FILTER_MODE <= BUS_DATA_IN[7:6];
            CONF_EN <= BUS_DATA_IN[0];
        end
        else if(BUS_ADD == 4)
            USER_K_FILTER_MASK_1  <= BUS_DATA_IN;
        else if(BUS_ADD == 5)
            USER_K_FILTER_MASK_2  <= BUS_DATA_IN;
        else if(BUS_ADD == 6)
            USER_K_FILTER_MASK_3  <= BUS_DATA_IN;
        else if(BUS_ADD == 7) begin
            RESET_COUNTERS <= BUS_DATA_IN[0];
            RESET_LOGIC <= BUS_DATA_IN[1];
        end
    end
end


// reset pulse for the (error) counters
pulse_gen_rising reset_counters (.clk_in(BUS_CLK), .in(RESET_COUNTERS), .out(RESET_COUNTERS_pulse));

// count the sent data packaged (in units of 32 bits)
always@(posedge BUS_CLK) begin
    if(FIFO_READ & !FIFO_EMPTY & (FRAME_COUNTER < 24'hffffff))
        FRAME_COUNTER <= FRAME_COUNTER + 1;
    if (RESET_COUNTERS_pulse)
        FRAME_COUNTER <= 32'b0;
end

// count soft errors (count rising edged)
reg [7:0] RX_SOFT_ERROR_COUNTER;
reg RX_SOFT_ERROR_reg;
wire RX_SOFT_ERROR_SYNC;
cdc_pulse_sync cdc_soft_error (.clk_in(USER_CLK), .pulse_in(RX_SOFT_ERROR), .clk_out(BUS_CLK), .pulse_out(RX_SOFT_ERROR_SYNC));
always@(posedge BUS_CLK) begin
    if (RESET_COUNTERS_pulse | RST | RESET_LOGIC)
        RX_SOFT_ERROR_COUNTER <= 32'b0;
    else if(RX_SOFT_ERROR_SYNC & !RX_SOFT_ERROR_reg & (RX_SOFT_ERROR_COUNTER < 8'hff))
        RX_SOFT_ERROR_COUNTER <= RX_SOFT_ERROR_COUNTER + 1;
    RX_SOFT_ERROR_reg <= RX_SOFT_ERROR_SYNC;
end


// count hard errors (count rising edged)
reg [7:0] RX_HARD_ERROR_COUNTER;
reg RX_HARD_ERROR_reg;
wire RX_HARD_ERROR_SYNC;
cdc_pulse_sync cdc_hard_error (.clk_in(USER_CLK), .pulse_in(RX_HARD_ERROR), .clk_out(BUS_CLK), .pulse_out(RX_HARD_ERROR_SYNC));
always@(posedge BUS_CLK) begin
    if (RESET_COUNTERS_pulse | RST | RESET_LOGIC)
        RX_HARD_ERROR_COUNTER <= 32'b0;
    else if(RX_HARD_ERROR_SYNC & !RX_HARD_ERROR_reg & (RX_HARD_ERROR_COUNTER < 8'hff))
        RX_HARD_ERROR_COUNTER <= RX_HARD_ERROR_COUNTER + 1;
    RX_HARD_ERROR_reg <= RX_HARD_ERROR_SYNC;
end


always @(posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        else if(BUS_ADD == 2)
            BUS_DATA_OUT <= {CONF_USER_K_FILTER_MODE, RX_HARD_ERROR_SYNC&PLL_LOCKED, RX_SOFT_ERROR_SYNC&PLL_LOCKED, PLL_LOCKED, RX_LANE_UP, RX_READY, CONF_EN};
        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= LOST_DATA_CNT;
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= USER_K_FILTER_MASK_1;
        else if(BUS_ADD == 5)
            BUS_DATA_OUT <= USER_K_FILTER_MASK_2;
        else if(BUS_ADD == 6)
            BUS_DATA_OUT <= USER_K_FILTER_MASK_3;
        else if(BUS_ADD == 7)
            BUS_DATA_OUT <= {4'b0, 1'b0/*GTX_TX_MODE*/, 1'b0, RESET_LOGIC, RESET_COUNTERS};
        else if(BUS_ADD == 8)
            BUS_DATA_OUT <= FRAME_COUNTER[7:0];
        else if(BUS_ADD == 9)
            BUS_DATA_OUT <= FRAME_COUNTER[15:8];
        else if(BUS_ADD == 10)
            BUS_DATA_OUT <= FRAME_COUNTER[23:16];
        else if(BUS_ADD == 11)
            BUS_DATA_OUT <= FRAME_COUNTER[31:24];
        else if(BUS_ADD == 12)
            BUS_DATA_OUT <= RX_SOFT_ERROR_COUNTER[7:0];
        else if(BUS_ADD == 13)
            BUS_DATA_OUT <= RX_HARD_ERROR_COUNTER[7:0];
        else if(BUS_ADD == 14)
            BUS_DATA_OUT <= {AURORA_LANES[3:0], RX_CHANNELS[3:0]};
    else
        BUS_DATA_OUT <= 8'b0;
    end
end


wire CONF_EN_SYNC;
cdc_reset_sync rst_conf_en_sync (.clk_in(BUS_CLK), .pulse_in(CONF_EN), .clk_out(USER_CLK), .pulse_out(CONF_EN_SYNC));

// Aurora init
reg pma_init_r;
reg [4:0] pma_init_cnt;
always@ (posedge BUS_CLK) begin
    `ifdef USBPIX3
        if (AURORA_RESET | RST)     // reset only once if the GTX transmitter is used, in order to not lose the link?
    `else
        if (AURORA_RESET | RST)
    `endif
            pma_init_cnt <= 5'hf;
    if (pma_init_cnt > 5'h0) begin
        pma_init_r <= 1'b1;
        pma_init_cnt <= pma_init_cnt - 1;
    end
    else
       pma_init_r <= 1'b0;
end


// AXI STREAM INTERFACE
wire RX_TLAST;
wire RX_TVALID;
wire [64*AURORA_LANES-1:0] RX_TDATA;
wire [8*AURORA_LANES-1:0]  RX_TKEEP;
wire [7:0] USER_K_ERR;
wire [64*AURORA_LANES-1:0] USER_K_DATA;
wire USER_K_VALID;


// ---- aurora core -----//
assign RX_LANE_UP = &{RX_MULTILLANE_UP};
assign RX_READY = RX_CHANNEL_UP & RX_LANE_UP;

wire RST_USER_SYNC;
cdc_reset_sync rst_pulse_user_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(USER_CLK), .pulse_out(RST_USER_SYNC));

wire RST_LOGIC_USER_SYNC;
cdc_reset_sync rst_logic_pulse_user_sync (.clk_in(BUS_CLK), .pulse_in(RESET_LOGIC), .clk_out(USER_CLK), .pulse_out(RST_LOGIC_USER_SYNC));

reg RX_TFIRST;
always@(posedge USER_CLK)
    if(RST_USER_SYNC | RST_LOGIC_USER_SYNC)
        RX_TFIRST <= 1;
    else if(RX_TVALID & RX_TLAST)
        RX_TFIRST <= 1;
    else if(RX_TVALID)
        RX_TFIRST <= 0;

// Generate pulse on rising edge of USERK_RX_TFIRST
reg USER_K_VALID_delayed;
reg USERK_RX_TFIRST;
wire USERK_RX_TFIRST_COMB;
//assign USERK_RX_TFIRST_COMB = USER_K_VALID & !USER_K_VALID_delayed;
always@(posedge USER_CLK) begin
    if(RST_USER_SYNC)
        USERK_RX_TFIRST <= 0;
    else if(USER_K_VALID & !USER_K_VALID_delayed)
        USERK_RX_TFIRST <= 1;
    else
        USERK_RX_TFIRST <= 0;
    USER_K_VALID_delayed <= USER_K_VALID;
end

localparam count_ones_logwidth = $clog2(8*AURORA_LANES);
reg [count_ones_logwidth:0] count_ones;
integer idx;

always @* begin
  count_ones = {8*AURORA_LANES{1'b0}};
  for( idx = 0; idx<8*AURORA_LANES; idx = idx + 1) begin
    count_ones = count_ones + RX_TKEEP[idx];
  end
end


localparam RX_TFIRST_OFFSET = 64*AURORA_LANES;
wire [7:0] bytemask;
wire [count_ones_logwidth-2:0] bytemask_bytes;
assign bytemask_bytes = count_ones[count_ones_logwidth:2];

// CDC for DATA
localparam DATA_SIZE_FIFO = (count_ones_logwidth-1)+1+(64*AURORA_LANES);

reg [count_ones_logwidth-1:0] byte_cnt, byte_cnt_prev, bytestoread, bytestoread_prev;           // pointers for fifo access
wire [DATA_SIZE_FIFO-1:0] data_to_cdc, cdc_data_out, cdc_data_out_val, cdc_data_out_buffer;
wire cdc_wfull, cdc_fifo_empty, read_fifo_cdc;
reg [16:0] data_out;
wire [16:0] fifo_data_out_byte [(4*AURORA_LANES-1):0];
reg [16:0] fifo_data_out_byte_buf [(4*AURORA_LANES-1):0];


assign data_to_cdc = {bytemask_bytes, RX_TFIRST, RX_TDATA[(64*AURORA_LANES-1):0]};
assign read_fifo_cdc = !cdc_fifo_empty && (bytestoread==0 && bytestoread_prev!=0);
always@(*) begin
 if (CHIP_TYPE == 2'b00) begin
    data_out = {cdc_data_out[RX_TFIRST_OFFSET] && byte_cnt==1 ,fifo_data_out_byte[byte_cnt_prev & 4'hf][15:0]};
end
 if (CHIP_TYPE == 2'b01) begin
    data_out = {fifo_data_out_byte[byte_cnt_prev & 4'hf][16:0]};
 end
end
// CDC FIFO between Aurora- and BUS clock domains
cdc_syncfifo #(.DSIZE(DATA_SIZE_FIFO), .ASIZE(12-(AURORA_LANES/2))) cdc_syncfifo_i
(
    .rdata(cdc_data_out),
    .wfull(cdc_wfull),
    .rempty(cdc_fifo_empty),
    .wdata(data_to_cdc),
    .winc(RX_TVALID & CONF_EN_SYNC), .wclk(USER_CLK), .wrst(RST_USER_SYNC | RST_LOGIC_USER_SYNC),
    .rinc(read_fifo_cdc), .rclk(BUS_CLK), .rrst(RST | RESET_LOGIC)
    );


// CDC for USER K
localparam DATA_USERK_SIZE_FIFO = 1+(64*AURORA_LANES);

reg [count_ones_logwidth-1:0] userk_byte_cnt, userk_byte_cnt_prev, userk_bytestoread, userk_bytestoread_prev;           // pointers for fifo access
wire [DATA_USERK_SIZE_FIFO-1:0] userk_data_to_cdc, userk_cdc_data_out;
wire userk_wfull, userk_cdc_fifo_empty, userk_read_fifo_cdc;
wire [16:0] userk_data_out;
wire [16:0] userk_fifo_data_out_byte [(4*AURORA_LANES-1):0];
reg [16:0] userk_fifo_data_out_byte_buf [(4*AURORA_LANES-1):0];

// Filter to seperate monitor data from register data etc
wire USER_K_FILTER_PASSED;
wire [AURORA_LANES-1:0] USER_K_FILTER_PASSED_block;
wire [7:0] USER_K_HEADER [AURORA_LANES-1:0];
wire [63:0] USER_K_WORD [AURORA_LANES-1:0];
genvar filter_block;
generate
    for (filter_block=0; filter_block<AURORA_LANES; filter_block=filter_block+1) begin
        assign USER_K_HEADER[filter_block] = USER_K_DATA[64*filter_block+7:64*filter_block];
        assign USER_K_WORD[filter_block] = USER_K_DATA[64*filter_block+63:64*filter_block];
        assign USER_K_FILTER_PASSED_block[filter_block] = (((USER_K_HEADER[filter_block] == USER_K_FILTER_MASK_1)
                                                          |(USER_K_HEADER[filter_block] == USER_K_FILTER_MASK_2)
                                                          |(USER_K_HEADER[filter_block] == USER_K_FILTER_MASK_3))
                                                          && ( USER_K_WORD[filter_block][33:24] != 9'h1ff )
                                                          ) ? 1 : 0;
    end
endgenerate
assign USER_K_FILTER_PASSED = | {USER_K_FILTER_PASSED_block};

// Write to CDC fifo after applying the filter
assign USERK_RX_TFIRST_COMB = USER_K_VALID & !USER_K_VALID_delayed & (
    ( (CONF_USER_K_FILTER_MODE == _USERK_FILTER) & USER_K_FILTER_PASSED)
    | (CONF_USER_K_FILTER_MODE == _USERK_PASS));

assign userk_data_to_cdc = {1'b0, USER_K_DATA}; //USERK_RX_TFIRST
assign userk_read_fifo_cdc = !userk_cdc_fifo_empty && (userk_bytestoread==0 && userk_bytestoread_prev!=0);
assign userk_data_out = {userk_cdc_data_out[RX_TFIRST_OFFSET] && userk_byte_cnt==0 ,userk_fifo_data_out_byte[userk_byte_cnt_prev & 4'hf][15:0]};

// CDC fifo for the USER_K data
cdc_syncfifo #(.DSIZE(DATA_USERK_SIZE_FIFO), .ASIZE(4)) userk_cdc_syncfifo_i
(
    .rdata(userk_cdc_data_out),
    .wfull(userk_wfull),
    .rempty(userk_cdc_fifo_empty),
    .wdata(userk_data_to_cdc),
    .winc(USERK_RX_TFIRST_COMB & CONF_EN_SYNC), .wclk(USER_CLK), .wrst(RST_USER_SYNC | RST_LOGIC_USER_SYNC),
    .rinc(userk_read_fifo_cdc), .rclk(BUS_CLK), .rrst(RST | RESET_LOGIC)
  );


// Generic FIFO for handling RX bursts
wire write_out_fifo, fifo_full;
wire [23:0] cdc_data;
reg  [23:0] cdc_data_delayed;
assign cdc_data = {7'b0, data_out};
assign write_out_fifo = byte_cnt_prev != 0;//(byte_cnt != 0 || byte_cnt_prev != 0);

always@(posedge BUS_CLK) begin
    cdc_data_delayed <= cdc_data;
end

gerneric_fifo #(.DATA_SIZE(24), .DEPTH(1024))  fifo_i
(   .clk(BUS_CLK), .reset(RST),
    .write(write_out_fifo),
    .read(FIFO_READ),
    .data_in(cdc_data_delayed),
    .full(fifo_full),
    .empty(FIFO_EMPTY),
    .data_out(FIFO_DATA[23:0]),
    .size()
    );


// Generic FIFO for handling USERK bursts
wire userk_write_out_fifo, userk_fifo_full;
wire [23:0] userk_cdc_data;
reg  [23:0] userk_cdc_data_delayed;
assign userk_cdc_data = {7'b0, userk_data_out};
assign userk_write_out_fifo = userk_byte_cnt_prev != 0;

always@(posedge BUS_CLK) begin
    userk_cdc_data_delayed <= userk_cdc_data;
end

gerneric_fifo #(.DATA_SIZE(24), .DEPTH(128))  userk_fifo_i
(   .clk(BUS_CLK), .reset(RST),
    .write(userk_write_out_fifo),
    .read(USERK_FIFO_READ),
    .data_in(userk_cdc_data_delayed),
    .full(userk_fifo_full),
    .empty(USERK_FIFO_EMPTY),
    .data_out(USERK_FIFO_DATA[23:0]),
    .size()
);

// Aurora receiver
`ifdef _2LANE
    aurora_64b66b_2lanes_exdes
`elsif _3LANE
    aurora_64b66b_3lanes_exdes
`elsif _4LANE
    aurora_64b66b_4lanes_exdes
`endif
    aurora_frame (
    // Error signals from Aurora
    .RX_HARD_ERR(RX_HARD_ERROR),
    .RX_SOFT_ERR(RX_SOFT_ERROR),
    .RX_LANE_UP(RX_MULTILLANE_UP),
    .RX_CHANNEL_UP(RX_CHANNEL_UP),
    .PLL_LOCKED(PLL_LOCKED),

    .INIT_CLK(INIT_CLK),

    .REFCLK_OUT(REFCLK_OUT),

    .PMA_INIT(pma_init_r),

    .GTXQ0_P(GTXQ0_P),
    .GTXQ0_N(GTXQ0_N),

    .RXP(RXP),
    .RXN(RXN),

    // Error signals from the Local Link packet checker
    .DATA_ERR_COUNT(),

    //USER_K
    .USER_K_ERR(USER_K_ERR),
    .USER_K_DATA(USER_K_DATA),
    .USER_K_VALID(USER_K_VALID),

    // User IO
    .RESET(RST_USER_SYNC),

    .DRP_CLK_IN(DRP_CLK_IN),
    .USER_CLK(USER_CLK),
    .RX_TDATA(RX_TDATA),
    .RX_TVALID(RX_TVALID),
    .RX_TKEEP(RX_TKEEP),
    .RX_TLAST(RX_TLAST)
);


// FIFO access pointer: RX  DATA
always@(posedge BUS_CLK)
    if(RST | RESET_LOGIC) begin
        byte_cnt <= 0;
        bytestoread <= 0;
        bytestoread_prev <= 0;
    end
    else begin
        byte_cnt_prev <= byte_cnt;
        bytestoread_prev <= bytestoread;
        if(!cdc_fifo_empty && !fifo_full && bytestoread_prev == 0 ) begin
        bytestoread <= {cdc_data_out[DATA_SIZE_FIFO-1:DATA_SIZE_FIFO-(count_ones_logwidth-1)], 1'b0};     //"Left shift" by 1, because we count in "half-words"
        byte_cnt <= byte_cnt + 1;
        end
        else begin
            if (!fifo_full & byte_cnt < bytestoread)
        byte_cnt <= byte_cnt + 1;
    else begin
        byte_cnt <= 0;
        bytestoread <= 0;
    end
        end
    end


// FIFO access pointer: USER DATA
always@(posedge BUS_CLK)
    if(RST | RESET_LOGIC) begin
        userk_bytestoread <= 0;
        userk_bytestoread_prev <= 0;
    end
    else begin
        userk_byte_cnt_prev <= userk_byte_cnt;
        userk_bytestoread_prev <= userk_bytestoread;
        if(!userk_cdc_fifo_empty && !userk_fifo_full && userk_bytestoread_prev == 0 ) begin
        userk_bytestoread <= 4*AURORA_LANES;
        userk_byte_cnt <= userk_byte_cnt + 1;
        end
        else begin
            if (!userk_fifo_full & userk_byte_cnt < userk_bytestoread)
        userk_byte_cnt <= userk_byte_cnt + 1;
    else begin
        userk_byte_cnt <= 0;
        userk_bytestoread <= 0;
    end
        end
    end


// Resorting of received data frames
genvar aurora_ch;
generate
    for (aurora_ch=0; aurora_ch<AURORA_LANES; aurora_ch=aurora_ch+1) begin
        always@(*)begin
            if (CHIP_TYPE == 0) begin //RD53A
                fifo_data_out_byte_buf[(4*aurora_ch)+0] = {1'b0, cdc_data_out[ aurora_ch*64+31 : aurora_ch*64+16 ]};
                fifo_data_out_byte_buf[(4*aurora_ch)+1] = {1'b0, cdc_data_out[ aurora_ch*64+15 : aurora_ch*64    ]};
                fifo_data_out_byte_buf[(4*aurora_ch)+2] = {1'b0, cdc_data_out[ aurora_ch*64+63 : aurora_ch*64+48 ]};
                fifo_data_out_byte_buf[(4*aurora_ch)+3] = {1'b0, cdc_data_out[ aurora_ch*64+47 : aurora_ch*64+32 ]};

                userk_fifo_data_out_byte_buf[(4*aurora_ch)+0] = {1'b0, userk_cdc_data_out[ aurora_ch*64+31 : aurora_ch*64+16 ]};
                userk_fifo_data_out_byte_buf[(4*aurora_ch)+1] = {1'b0, userk_cdc_data_out[ aurora_ch*64+15 : aurora_ch*64    ]};
                userk_fifo_data_out_byte_buf[(4*aurora_ch)+2] = {1'b0, userk_cdc_data_out[ aurora_ch*64+63 : aurora_ch*64+48 ]};
                userk_fifo_data_out_byte_buf[(4*aurora_ch)+3] = {1'b0, userk_cdc_data_out[ aurora_ch*64+47 : aurora_ch*64+32 ]};
            end
            if (CHIP_TYPE == 1) begin //ITkPixV1
                fifo_data_out_byte_buf[(4*aurora_ch)+0] = {1'b1, cdc_data_out[ aurora_ch*64+63 : aurora_ch*64+48 ]};
                fifo_data_out_byte_buf[(4*aurora_ch)+1] = {1'b0, cdc_data_out[ aurora_ch*64+47 : aurora_ch*64+32 ]};
                fifo_data_out_byte_buf[(4*aurora_ch)+2] = {1'b0, cdc_data_out[ aurora_ch*64+31 : aurora_ch*64+16 ]};
                fifo_data_out_byte_buf[(4*aurora_ch)+3] = {1'b0, cdc_data_out[ aurora_ch*64+15 : aurora_ch*64+0 ]};

                userk_fifo_data_out_byte_buf[(4*aurora_ch)+0] = {1'b1, userk_cdc_data_out[ aurora_ch*64+63 : aurora_ch*64+48 ]};
                userk_fifo_data_out_byte_buf[(4*aurora_ch)+1] = {1'b0, userk_cdc_data_out[ aurora_ch*64+47 : aurora_ch*64+32 ]};
                userk_fifo_data_out_byte_buf[(4*aurora_ch)+2] = {1'b0, userk_cdc_data_out[ aurora_ch*64+31 : aurora_ch*64+16 ]};
                userk_fifo_data_out_byte_buf[(4*aurora_ch)+3] = {1'b0, userk_cdc_data_out[ aurora_ch*64+15 : aurora_ch*64+0 ]};
            end
        end
    end
endgenerate


generate
    for (aurora_ch=0; aurora_ch<AURORA_LANES; aurora_ch=aurora_ch+1) begin
        assign fifo_data_out_byte[(4*aurora_ch)+0] = fifo_data_out_byte_buf[(4*aurora_ch)+0];
        assign fifo_data_out_byte[(4*aurora_ch)+1] = fifo_data_out_byte_buf[(4*aurora_ch)+1];
        assign fifo_data_out_byte[(4*aurora_ch)+2] = fifo_data_out_byte_buf[(4*aurora_ch)+2];
        assign fifo_data_out_byte[(4*aurora_ch)+3] = fifo_data_out_byte_buf[(4*aurora_ch)+3];

        assign userk_fifo_data_out_byte[(4*aurora_ch)+0] = userk_fifo_data_out_byte_buf[(4*aurora_ch)+0];
        assign userk_fifo_data_out_byte[(4*aurora_ch)+1] = userk_fifo_data_out_byte_buf[(4*aurora_ch)+1];
        assign userk_fifo_data_out_byte[(4*aurora_ch)+2] = userk_fifo_data_out_byte_buf[(4*aurora_ch)+2];
        assign userk_fifo_data_out_byte[(4*aurora_ch)+3] = userk_fifo_data_out_byte_buf[(4*aurora_ch)+3];
        end
endgenerate


// Add the USERK_RX_TFIRST bit
assign userk_fifo_data_out_byte[2*AURORA_LANES-1][16] = userk_cdc_data_out[64*AURORA_LANES];

always@(posedge USER_CLK) begin
    if(RST_USER_SYNC | RST_LOGIC_USER_SYNC)
        LOST_DATA_CNT <= 0;
    else if (cdc_wfull && RX_TVALID && LOST_DATA_CNT != -1)
        LOST_DATA_CNT <= LOST_DATA_CNT +1;
end

// Assign the additional 8 bits for channel identification etc.
assign FIFO_DATA[31:24] = IDENTIFIER[7:0];
assign USERK_FIFO_DATA[31:24] = {8'h00};

assign LOST_ERROR = LOST_DATA_CNT != 0;

endmodule
