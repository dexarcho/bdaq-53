#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script should be called once after powering-up the chip (end after each power-cycle).

    It is based on digital injection scan. It injects a digital pulse
    into pixel matrix for 8 times within one latency time in order to set TOT with
    code 0 and decrease the current.
'''
from bdaq53.system.scan_base import ScanBase
import os

decrease_current_config = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 384
    # 'maskfile': 'REF/ref_mask.h5'  # Use proper reference files after tuning
}


class LowCurrent(ScanBase):
    scan_id = 'low_current'

    def decrease_current(self, cal_edge_width=1, cal_edge_dly=2, latency=124, wait_cycles=200, trigger_pattern=0xffffffff, repetitions=8):
        with self.readout():
            indata = self.chip.write_cal(cal_edge_mode=1, cal_edge_width=cal_edge_width, cal_edge_dly=cal_edge_dly, write=False)
            self.chip.write_command(indata, repetitions=repetitions)
        self.log.success("Current is decreased.")

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        '''
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update(force=True)
        self.chip.setup_digital_injection()

    def _scan(self, n_injections=8, **_):
        '''
        Digital scan main loop
        Parameters
        ----------
        n_injections : int
            Number of injections.
        '''
        self.decrease_current(repetitions=n_injections)


if __name__ == '__main__':
    with LowCurrent(scan_config=decrease_current_config) as scan:
        scan.start()
        os.system('mv output_data/module_0/chip_0/chip_0.log output_data/module_0/chip_0/' + scan.run_name + '.log')
