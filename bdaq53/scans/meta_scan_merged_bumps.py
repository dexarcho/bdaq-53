#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Tune global and local threshold and scan for merged bump bonds.
    Also refer to scan_merged_bumps.py.

    Note:
    To obtain correct results the sensor has to be reverse biased for this scan.
'''

from bdaq53.scans.tune_global_threshold import GDACTuning
from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.scans.scan_merged_bumps import MergedBumpsScan


# Target threshold
target_DVCAL = 273  # 3000e

scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    'VCAL_MED': 500,
    'DVCAL_tuned': target_DVCAL
}

tuning_configuration = {
    'maskfile': None,

    'VCAL_MED': 500,
    'VCAL_HIGH': 500 + target_DVCAL
}


if __name__ == '__main__':
    tuning_configuration['start_column'] = scan_configuration['start_column']
    tuning_configuration['stop_column'] = scan_configuration['stop_column']
    tuning_configuration['start_row'] = scan_configuration['start_row']
    tuning_configuration['stop_row'] = scan_configuration['stop_row']

    with GDACTuning(scan_config=tuning_configuration) as global_tuning:
        global_tuning.start()

    with TDACTuning(scan_config=tuning_configuration) as local_tuning:
        local_tuning.start()

    with MergedBumpsScan(scan_config=scan_configuration) as merged_bumps_scan:
        merged_bumps_scan.start()
