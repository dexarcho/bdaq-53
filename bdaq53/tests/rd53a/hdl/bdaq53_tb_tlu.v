/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute, University of Bonn
 * ------------------------------------------------------------
 */

`timescale 1ns / 1ps

`include "glbl.v"

`define BDAQ53
//`define KC705


`ifdef INCLUDE_DUT
    `include "sim/common/cells_models_sim.v"
    `include "sim/rd53a/dut/CgWrapper_sim.v"
    `include "sim/rd53a/dut/DelayCell_sim.v"
    `include "src/verilog/top/RD53A.sv"
`endif

`include "bram_fifo/bram_fifo.v"
`include "bram_fifo/bram_fifo_core.v"
`include "utils/clock_multiplier.v"
`include "utils/clock_divider.v"
`include "utils/bus_to_ip.v"

module tlu_model (
    input wire SYS_CLK, SYS_RST, TLU_CLOCK, TLU_BUSY, ENABLE, TRIG,
    output wire TLU_TRIGGER, TLU_RESET
);

reg [14:0] TRIG_ID;
//reg TRIG;
wire VETO;
integer seed;

initial
    seed = 0;


//always@(posedge SYS_CLK) begin
//    if(SYS_RST)
//        TRIG <= 0;
//    else if($random(seed) % 100 == 10 && !VETO && ENABLE)
//       TRIG <= 1;
//    else
//        TRIG <= 0;
//end

always@(posedge SYS_CLK) begin
    if(SYS_RST)
        TRIG_ID <= 0;
    else if(TRIG)
        TRIG_ID <= TRIG_ID + 1;
end

localparam WAIT_STATE = 0, TRIG_STATE = 1, READ_ID_STATE = 2;

reg [1:0] state, state_next;
always@(posedge SYS_CLK)
    if(SYS_RST)
        state <= WAIT_STATE;
    else
        state <= state_next;

always@(*) begin
    state_next = state;

    case(state)
        WAIT_STATE:
            if(TRIG)
                state_next = TRIG_STATE;
        TRIG_STATE:
            if(TLU_BUSY)
                state_next = READ_ID_STATE;
        READ_ID_STATE:
            if(!TLU_BUSY)
                state_next = WAIT_STATE;
        default : state_next = WAIT_STATE;
    endcase
end

assign VETO = (state != WAIT_STATE) || (state == WAIT_STATE && TLU_CLOCK == 1'b1);

reg [15:0] TRIG_ID_SR;
initial TRIG_ID_SR = 0;
always@(posedge TLU_CLOCK or posedge TRIG)
    if(TRIG)
        TRIG_ID_SR <= {TRIG_ID, 1'b0};
    else
        TRIG_ID_SR <= {1'b0, TRIG_ID_SR[15:1]};

assign TLU_TRIGGER = (state == TRIG_STATE) | (TRIG_ID_SR[0] & TLU_BUSY);

assign TLU_RESET = 0;

endmodule


module tb (
    input wire          BUS_CLK,
    input wire          BUS_RST,
    input wire  [31:0]  BUS_ADD,
    inout wire  [31:0]  BUS_DATA,
    input wire          BUS_RD,
    input wire          BUS_WR,
    output wire         BUS_BYTE_ACCESS,

    output wire         CLK_BX,
    input wire [384*400-1:0] HIT,
    input wire          TRIGGER,
    output wire         RESET_TB
);

localparam DUT_1_CHIP_ID = 3'h0;
localparam DUT_2_CHIP_ID = 3'h1;

// -------  MODULE ADREESSES  ------- //
localparam PULSE_TEST_BASEADDR = 16'h0900;
localparam PULSE_TEST_HIGHADDR = 16'h1000 - 1;

localparam FIFO_BASEADDR = 32'h8000;
localparam FIFO_HIGHADDR = 32'h9000-1;

localparam FIFO_BASEADDR_DATA = 32'h8000_0000;
localparam FIFO_HIGHADDR_DATA = 32'h9000_0000;

localparam ABUSWIDTH = 32;
assign BUS_BYTE_ACCESS = BUS_ADD < 32'h8000_0000 ? 1'b1 : 1'b0;


// ----- Clock (mimics a PLL) -----
localparam PLL_MUL            = 5;
localparam PLL_DIV_BUS_CLK    = 7;
localparam PLL_DIV_CLK250     = 4;
localparam PLL_DIV_CLK125TX   = 8;
localparam PLL_DIV_CLK125TX90 = 8;
localparam PLL_DIV_CLK125RX   = 8;
localparam PLL_DIV_CLK_CMD    = 25;
localparam PLL_DIV_BX_CLK     = 4;
localparam PLL_LOCK_DELAY     = 1000*1000;

wire PLL_VCO, CLK250, CLK125TX, CLK125TX90, CLK125RX, CLKCMD, CLKBUS, CMD_CLK, BX_CLK;
clock_multiplier #( .MULTIPLIER(PLL_MUL)  ) i_clock_multiplier( .CLK(BUS_CLK),                      .CLOCK(PLL_VCO)  );
clock_divider #(.DIVISOR(PLL_DIV_CLK250)  ) i_clock_divisor_1 ( .CLK(PLL_VCO), .RESET(1'b0), .CE(), .CLOCK(CLK250)   );
clock_divider #(.DIVISOR(PLL_DIV_CLK125TX)) i_clock_divisor_2 ( .CLK(PLL_VCO), .RESET(1'b0), .CE(), .CLOCK(CLK125TX) );
clock_divider #(.DIVISOR(PLL_DIV_CLK125RX)) i_clock_divisor_3 ( .CLK(PLL_VCO), .RESET(1'b0), .CE(), .CLOCK(CLK125RX) );
clock_divider #(.DIVISOR(PLL_DIV_CLK_CMD) ) i_clock_divisor_4 ( .CLK(PLL_VCO), .RESET(1'b0), .CE(), .CLOCK(CLKCMD)   );
clock_divider #(.DIVISOR(PLL_DIV_BUS_CLK) ) i_clock_divisor_5 ( .CLK(PLL_VCO), .RESET(1'b0), .CE(), .CLOCK(CLKBUS)   );
clock_divider #(.DIVISOR(PLL_DIV_BX_CLK)  ) i_clock_divisor_bx( .CLK(CMD_CLK), .RESET(1'b0), .CE(), .CLOCK(BX_CLK));

// GTX reference clock: 160 MHz for 1.28 Gbit/s. Has to match IP core configuration!
localparam CLK_MGT_REF_PERIOD  = 6.25*1000;
reg CLK_MGT_REF=0;
initial CLK_MGT_REF = 1'b0;
always #(CLK_MGT_REF_PERIOD / 2) CLK_MGT_REF = !CLK_MGT_REF;

// INIT clock for the Aurora core
localparam INIT_CLK_PERIOD = 6.4*1000;
reg INIT_CLK=0;
initial INIT_CLK = 1'b0;
always #(INIT_CLK_PERIOD / 2) INIT_CLK = !INIT_CLK;


reg LOCKED;
initial begin
    LOCKED = 1'b0;
    #(PLL_LOCK_DELAY) LOCKED = 1'b1;
end
// -------------------------

wire BX_CLK_EN;
`ifdef INCLUDE_DUT
    // Get the clock from the chip
    wire CMD_BCR;
    assign CMD_CLK = dut_1.ACB.CDR_PLL.pll_clk_160MHz;
    assign CMD_BCR = dut_1.DCB.CommandDecoder.CmdBCR;
    //wire RESET_TB;
    assign RESET_TB = CMD_BCR;
    ODDR bx_clk_gate(.D1(BX_CLK_EN), .D2(1'b0), .C(BX_CLK), .CE(1'b1), .R(1'b0), .S(1'b0), .Q(CLK_BX) );
`else
    assign CMD_CLK = CLKCMD;
`endif


// Aurora / MGT clocks
wire MGT_REF_SEL=1;
wire MGT_REFCLK0_P, MGT_REFCLK0_N;
wire MGT_INITCLK_P, MGT_INITCLK_N;
assign MGT_REFCLK0_P    = CLK_MGT_REF  && MGT_REF_SEL;
assign MGT_REFCLK0_N    = ~CLK_MGT_REF && MGT_REF_SEL;
assign MGT_INITCLK_P = INIT_CLK;
assign MGT_INITCLK_N = ~INIT_CLK;


// -------  TB SIGNALS  ------- //
reg RESET = 0;
initial begin
    //repeat (100) @(posedge BUS_CLK);
    RESET <= 1;
    repeat (10) @(posedge BUS_CLK);
    RESET <= 0;
end


// -------  USER MODULES  ------- //
wire FIFO_FULL, FIFO_NOT_EMPTY, FIFO_WRITE, FIFO_EMPTY, FIFO_READ;
wire [31:0] FIFO_DATA;

bram_fifo
#(
    .BASEADDR(FIFO_BASEADDR),
    .HIGHADDR(FIFO_HIGHADDR),
    .BASEADDR_DATA(FIFO_BASEADDR_DATA),
    .HIGHADDR_DATA(FIFO_HIGHADDR_DATA),
    .ABUSWIDTH(32)
) i_out_fifo (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .FIFO_READ_NEXT_OUT(FIFO_READ),
    .FIFO_EMPTY_IN(!FIFO_WRITE),
    .FIFO_DATA(FIFO_DATA),

    .FIFO_NOT_EMPTY(FIFO_NOT_EMPTY),
    .FIFO_FULL(FIFO_FULL),
    .FIFO_NEAR_FULL(),
    .FIFO_READ_ERROR()
);


// ----- BDAQ53A CORE ----- //
wire AURORA_MMCM_NOT_LOCKED;
wire CMD_P, CMD_N, CMD_DATA;
wire CKL_SEL;
wire [3:0] OUT_DATA_P, OUT_DATA_N;
wire LEMO_TX0, LEMO_TX1;
wire EEPROM_CS, EEPROM_SK, EEPROM_DI, EEPROM_DO;
wire I2C_SDA, I2C_SCL;
wire TLU_BUSY, TLU_RESET;
wire TLU_CLOCK, TLU_TRIGGER;
wire HITOR;
wire CLK40;
wire CMD_OUT;

clock_divider #(
    .DIVISOR(4)
) i_clock_divisor_160MHz_to_40MHz (
    .CLK(CMD_CLK),
    .RESET(1'b0),
    .CE(),
    .CLOCK(CLK40)
);


bdaq53_core fpga(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),    // full 32 bit bus
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_BYTE_ACCESS(BUS_BYTE_ACCESS),

    // Clocks from oscillators and mux select
	.INIT_CLK(BUS_CLK),
    .RX_CLK_IN_P(MGT_REFCLK0_P), .RX_CLK_IN_N(MGT_REFCLK0_N),
    `ifdef KC705    .MGT_REF_SEL(),
    `else           .MGT_REF_SEL(MGT_REF_SEL),
    `endif

    // PLL
    .CLK_CMD(CMD_CLK),
    .REFCLK1_OUT(),
    .TX_OUT_CLK(),

    // Aurora lanes
    .MGT_RX_P({OUT_DATA_P}),
    .MGT_RX_N({OUT_DATA_N}),
    .TX_P(),
    .TX_N(),

    // CMD encoder
    .EXT_TRIGGER(TRIGGER),
    .CMD_DATA(CMD_DATA),
    .CMD_OUT(CMD_OUT),
    .CMD_WRITING(),

    `ifdef BDAQ53
        .GPIO_SENSE(4'b1110),
        .GPIO_RESET(),
    `endif

    // Debug signals
    .DEBUG_TX0(LEMO_TX0), .DEBUG_TX1(LEMO_TX1),    //debug signal copy of CMD

    // I2C bus
    .I2C_SDA(I2C_SDA), .I2C_SCL(I2C_SCL),

    // FIFO cpontrol signals (TX FIFO of DAQ)
    .FIFO_DATA(FIFO_DATA),
    .FIFO_WRITE(FIFO_WRITE),
    .FIFO_EMPTY(!FIFO_NOT_EMPTY),
    .FIFO_FULL(FIFO_FULL),

    .BX_CLK_EN(BX_CLK_EN),
    .DUT_RESET(DUT_RESET),
    .AURORA_RESET(0),
    .RESET_TB(),             // <------------------- REMOVE?

    // TLU
    .RJ45_TRIGGER(TLU_TRIGGER),
    .RJ45_RESET(TLU_RESET),
    .RJ45_BUSY_LEMO_TX1(TLU_BUSY),
    .RJ45_CLK_LEMO_TX0(TLU_CLOCK)
    );


`ifdef INCLUDE_DUT

    wire [3:0] DUT_1_SER_DATA1G_P, DUT_1_SER_DATA1G_N;
    wire [3:0] DUT_2_SER_DATA1G_P, DUT_2_SER_DATA1G_N;

    // swap bit order, like display cables do
    assign DUT_1_OUT_DATA_P = { DUT_1_SER_DATA1G_P[0], DUT_1_SER_DATA1G_P[1], DUT_1_SER_DATA1G_P[2], DUT_1_SER_DATA1G_P[3] };
    assign DUT_1_OUT_DATA_N = { DUT_1_SER_DATA1G_N[0], DUT_1_SER_DATA1G_N[1], DUT_1_SER_DATA1G_N[2], DUT_1_SER_DATA1G_N[3] };
    assign DUT_2_OUT_DATA_P = { DUT_2_SER_DATA1G_P[0], DUT_2_SER_DATA1G_P[1], DUT_2_SER_DATA1G_P[2], DUT_2_SER_DATA1G_P[3] };
    assign DUT_2_OUT_DATA_N = { DUT_2_SER_DATA1G_N[0], DUT_2_SER_DATA1G_N[1], DUT_2_SER_DATA1G_N[2], DUT_2_SER_DATA1G_N[3] };


    //--------------- Begin INSTANTIATION Template ---------------//


    wire por_out_b ;             // use this to monitor internally-generated POR signal
    wire [7:0] hit_or ;          // use this to monitor Hit-ORs


    wire VDD_PLL = 1'b1 ;        // **NOTE: power connectivity is modeled for CDR/PLL block !
    wire GND_PLL = 1'b0 ;

    wire VDD_CML = 1'b1 ;        // **NOTE: power connectivity is modeled for CML drivers !
    wire GND_CML = 1'b0 ;


    RD53A  dut_1 (


       //------------------------------   DIGITAL INTERFACE   ------------------------------//

       //
       // Power-On Resets (POR)
       //
       .POR_EXT_CAP_PAD     (             RESET     ),
       .POR_OUT_B_PAD       (             por_out_b ),
       //.POR_BGP_PAD         (                       ),

       //
       // Clock Data Recovery (CDR) input command/data stream [SLVS]
       //
       .CMD_P_PAD           (    CMD_DATA /*m_cmd_if.serial_in  */),
       .CMD_N_PAD           (   ~CMD_DATA /*~m_cmd_if.serial_in */),


       //
       // 4x general-purpose SLVS outputs, including Hit-ORs
       //
       .GPLVDS0_P_PAD       (             hit_or[0] ),
       .GPLVDS0_N_PAD       (                       ),

       .GPLVDS1_P_PAD       (             hit_or[1] ),
       .GPLVDS1_N_PAD       (                       ),

       .GPLVDS2_P_PAD       (             hit_or[2] ),
       .GPLVDS2_N_PAD       (                       ),

       .GPLVDS3_P_PAD       (             hit_or[3] ),
       .GPLVDS3_N_PAD       (                       ),

       //
       // general purpose monitor output [CMOS]
       //
       .STATUS_PAD          (                       ),

       //
       // 4x serial output data links @ 1.28 Gb/s [CML]
       //
       .GTX0_P_PAD          ( DUT_1_SER_DATA1G_P[0] /*DUT_OUT_DATA_P */),
       .GTX0_N_PAD          ( DUT_1_SER_DATA1G_N[0] /*DUT_OUT_DATA_N */),

       .GTX1_P_PAD          ( DUT_1_SER_DATA1G_P[1] ),
       .GTX1_N_PAD          ( DUT_1_SER_DATA1G_N[1] ),

       .GTX2_P_PAD          ( DUT_1_SER_DATA1G_P[2] ),
       .GTX2_N_PAD          ( DUT_1_SER_DATA1G_N[2] ),

       .GTX3_P_PAD          ( DUT_1_SER_DATA1G_P[3] ),
       .GTX3_N_PAD          ( DUT_1_SER_DATA1G_N[3] ),

       //
       // single serial output data link @ 5 Gb/s [GWT]
       //
       //.GWTX_P_PAD          (                       ),
       //.GWTX_N_PAD          (                       ),

       //
       // external 3-bit hard-wired local chip address [CMOS]
       //
       .CHIPID0_PAD         ( DUT_1_CHIP_ID[0]      ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .CHIPID1_PAD         ( DUT_1_CHIP_ID[1]      ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .CHIPID2_PAD         ( DUT_1_CHIP_ID[2]      ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

       //
       // **BACKUP: single 1.28 Gb/s output line [SLVS]
       //
       .GTX0LVDS_P_PAD      (                       ),
       .GTX0LVDS_N_PAD      (                       ),

       //
       // **BACKUP: bypass/debug MUX controls [CMOS]
       //
       .BYPASS_CMD_PAD      (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .BYPASS_CDR_PAD      ( 1'b0                  ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .DEBUG_EN_PAD        ( 1'b0                  ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

       //
       // **BACKUP: external clocks [SLVS]
       //
       .EXT_CMD_CLK_P_PAD   (  CMD_CLK              ),
       .EXT_CMD_CLK_N_PAD   ( ~CMD_CLK              ),

       .EXT_SER_CLK_P_PAD   ( 1'b0), // CMD_CLK ), //CLK_DATAX20          ),
       .EXT_SER_CLK_N_PAD   ( 1'b1), //~CMD_CLK ), //~CLK_DATAX20          ),

       //
       // **BACKUP: JTAG [CMOS]
       //
       .JTAG_TRST_B_PAD     (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .JTAG_TCK_PAD        (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .JTAG_TMS_PAD        (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-UP   CONFIGURATION IN CMOS PADS !
       .JTAG_TDI_PAD        (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-UP   CONFIGURATION IN CMOS PADS !
       .JTAG_TDO_PAD        (                       ),

       //
       // **BACKUP: external trigger [CMOS]
       //
       .EXT_TRIGGER_PAD     (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

       //
       // **BACKUP: auxiliary external CalEdge/CalDly injection signals [CMOS]
       //
       .INJ_STRB0_PAD       (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .INJ_STRB1_PAD       (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

       //
       // spares [CMOS]
       //
       //.SPARE0_PAD          (                       ),
       //.SPARE1_PAD          (                       ),


       //------------------------------   ANALOG INTERFACE   ------------------------------//

       //
       // pixel inputs
       //
       .ANA_HIT             ( HIT /*AnalogHitInt /* */),

       //
       // external calibration
       //
       .IREF_TRIM0_PAD      (                  1'b0 ),
       .IREF_TRIM1_PAD      (                  1'b0 ),
       .IREF_TRIM2_PAD      (                  1'b0 ),
       .IREF_TRIM3_PAD      (                  1'b1 ),

       //
       // **BACKUP: external DC calibration levels
       //
       .VINJ_HI_PAD         (                       ),
       .VINJ_MID_PAD        (                       ),

       //
       // monitoring
       //
       .IMUX_OUT_PAD        (                       ),
       .VMUX_OUT_PAD        (                       ),

       //
       // **BACKUP: external ADC reference voltages
       //
       .VREF_ADC_IN_PAD     (                       ),
       .VREF_ADC_OUT_PAD    (                       ),

       //
       // **BACKUP: bias currents
       //
       .IREF_IN_PAD         (                       ),
       .IREF_OUT_PAD        (                       ),


       //---------------------------   POWER/GROUND INTERFACE   ---------------------------//

       //
       // ANALOG Shunt-LDO
       //
       .SLDO_REXT_A_PAD     (                       ),
       .SLDO_RINT_A_PAD     (                       ),
       .SLDO_VREF_A_PAD     (                       ),
       .SLDO_VOFFSET_A_PAD  (                       ),
       .SLDO_VDDSHUNT_A_PAD (                       ),

       //
       // DIGITAL Shunt-LDO
       //
       .SLDO_REXT_D_PAD     (                       ),
       .SLDO_RINT_D_PAD     (                       ),
       .SLDO_VREF_D_PAD     (                       ),
       .SLDO_VOFFSET_D_PAD  (                       ),
       .SLDO_VDDSHUNT_D_PAD (                       ),

       //
       // ANALOG core power/ground
       //
       //.VDDA                (                       ),
       //.GNDA                (                       ),
       .VINA_PAD            (                       ),

       //
       // DIGITAL core power/ground
       //
       //.VDDD                (                       ),
       //.GNDD                (                       ),
       .VIND_PAD            (                       ),

       //
       // global substrate
       //
       //.VSUB                (                       ),

       //
       // dedicated PLL power/ground rails
       //
       .VDD_PLL_PAD         (               VDD_PLL ),                    // **NOTE: power connectivity is modeled for CDR/PLL block !
       .GND_PLL_PAD         (               GND_PLL ),

       //
       // dedicated CML driver power/ground rails
       //
       .VDD_CML_PAD         (               VDD_CML ),                    // **NOTE: power connectivity is modeled for CML drivers !
       .GND_CML_PAD         (               GND_PLL ),

       //
       // dedicated 5 Gb/s SER power/ground rails
       //
       //.GWT_VDD_PAD         (                       ),
       //.GWT_VSS_PAD         (                       ),
       //.GWT_VDDHS_PAD       (                       ),
       //.GWT_GNDHS_PAD       (                       ),
       //.GWT_VDDHS_CORE_PAD  (                       ),
       //.GWT_GNDHS_CORE_PAD  (                       ),

       //
       // ground for detector guard-ring pads
       //
       .DET_GRD0_PAD        (                       ),
       .DET_GRD1_PAD        (                       )

       ) ;

    RD53A  dut_2 (


       //------------------------------   DIGITAL INTERFACE   ------------------------------//

       //
       // Power-On Resets (POR)
       //
       .POR_EXT_CAP_PAD     (             RESET     ),
       .POR_OUT_B_PAD       (             por_out_b ),
       //.POR_BGP_PAD         (                       ),

       //
       // Clock Data Recovery (CDR) input command/data stream [SLVS]
       //
       .CMD_P_PAD           (    CMD_DATA /*m_cmd_if.serial_in  */),
       .CMD_N_PAD           (   ~CMD_DATA /*~m_cmd_if.serial_in */),


       //
       // 4x general-purpose SLVS outputs, including Hit-ORs
       //
       .GPLVDS0_P_PAD       (             hit_or[4] ),
       .GPLVDS0_N_PAD       (                       ),

       .GPLVDS1_P_PAD       (             hit_or[5] ),
       .GPLVDS1_N_PAD       (                       ),

       .GPLVDS2_P_PAD       (             hit_or[6] ),
       .GPLVDS2_N_PAD       (                       ),

       .GPLVDS3_P_PAD       (             hit_or[7] ),
       .GPLVDS3_N_PAD       (                       ),

       //
       // general purpose monitor output [CMOS]
       //
       .STATUS_PAD          (                       ),

       //
       // 4x serial output data links @ 1.28 Gb/s [CML]
       //
       .GTX0_P_PAD          ( DUT_2_SER_DATA1G_P[0] /*DUT_OUT_DATA_P */),
       .GTX0_N_PAD          ( DUT_2_SER_DATA1G_N[0] /*DUT_OUT_DATA_N */),

       .GTX1_P_PAD          ( DUT_2_SER_DATA1G_P[1] ),
       .GTX1_N_PAD          ( DUT_2_SER_DATA1G_N[1] ),

       .GTX2_P_PAD          ( DUT_2_SER_DATA1G_P[2] ),
       .GTX2_N_PAD          ( DUT_2_SER_DATA1G_N[2] ),

       .GTX3_P_PAD          ( DUT_2_SER_DATA1G_P[3] ),
       .GTX3_N_PAD          ( DUT_2_SER_DATA1G_N[3] ),

       //
       // single serial output data link @ 5 Gb/s [GWT]
       //
       //.GWTX_P_PAD          (                       ),
       //.GWTX_N_PAD          (                       ),

       //
       // external 3-bit hard-wired local chip address [CMOS]
       //
       .CHIPID0_PAD         ( DUT_2_CHIP_ID[0]      ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .CHIPID1_PAD         ( DUT_2_CHIP_ID[1]      ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .CHIPID2_PAD         ( DUT_2_CHIP_ID[2]      ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

       //
       // **BACKUP: single 1.28 Gb/s output line [SLVS]
       //
       .GTX0LVDS_P_PAD      (                       ),
       .GTX0LVDS_N_PAD      (                       ),

       //
       // **BACKUP: bypass/debug MUX controls [CMOS]
       //
       .BYPASS_CMD_PAD      (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .BYPASS_CDR_PAD      ( 1'b0                  ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .DEBUG_EN_PAD        ( 1'b0                  ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

       //
       // **BACKUP: external clocks [SLVS]
       //
       .EXT_CMD_CLK_P_PAD   (  CMD_CLK              ),
       .EXT_CMD_CLK_N_PAD   ( ~CMD_CLK              ),

       .EXT_SER_CLK_P_PAD   ( 1'b0), // CMD_CLK ), //CLK_DATAX20          ),
       .EXT_SER_CLK_N_PAD   ( 1'b1), //~CMD_CLK ), //~CLK_DATAX20          ),

       //
       // **BACKUP: JTAG [CMOS]
       //
       .JTAG_TRST_B_PAD     (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .JTAG_TCK_PAD        (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .JTAG_TMS_PAD        (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-UP   CONFIGURATION IN CMOS PADS !
       .JTAG_TDI_PAD        (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-UP   CONFIGURATION IN CMOS PADS !
       .JTAG_TDO_PAD        (                       ),

       //
       // **BACKUP: external trigger [CMOS]
       //
       .EXT_TRIGGER_PAD     (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

       //
       // **BACKUP: auxiliary external CalEdge/CalDly injection signals [CMOS]
       //
       .INJ_STRB0_PAD       (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !
       .INJ_STRB1_PAD       (                       ),                    // **NOTE: LEAVE UNCONNECTED TO CHECK PULL-DOWN CONFIGURATION IN CMOS PADS !

       //
       // spares [CMOS]
       //
       //.SPARE0_PAD          (                       ),
       //.SPARE1_PAD          (                       ),


       //------------------------------   ANALOG INTERFACE   ------------------------------//

       //
       // pixel inputs
       //
       .ANA_HIT             ( HIT /*AnalogHitInt /* */),

       //
       // external calibration
       //
       .IREF_TRIM0_PAD      (                  1'b0 ),
       .IREF_TRIM1_PAD      (                  1'b0 ),
       .IREF_TRIM2_PAD      (                  1'b0 ),
       .IREF_TRIM3_PAD      (                  1'b1 ),

       //
       // **BACKUP: external DC calibration levels
       //
       .VINJ_HI_PAD         (                       ),
       .VINJ_MID_PAD        (                       ),

       //
       // monitoring
       //
       .IMUX_OUT_PAD        (                       ),
       .VMUX_OUT_PAD        (                       ),

       //
       // **BACKUP: external ADC reference voltages
       //
       .VREF_ADC_IN_PAD     (                       ),
       .VREF_ADC_OUT_PAD    (                       ),

       //
       // **BACKUP: bias currents
       //
       .IREF_IN_PAD         (                       ),
       .IREF_OUT_PAD        (                       ),


       //---------------------------   POWER/GROUND INTERFACE   ---------------------------//

       //
       // ANALOG Shunt-LDO
       //
       .SLDO_REXT_A_PAD     (                       ),
       .SLDO_RINT_A_PAD     (                       ),
       .SLDO_VREF_A_PAD     (                       ),
       .SLDO_VOFFSET_A_PAD  (                       ),
       .SLDO_VDDSHUNT_A_PAD (                       ),

       //
       // DIGITAL Shunt-LDO
       //
       .SLDO_REXT_D_PAD     (                       ),
       .SLDO_RINT_D_PAD     (                       ),
       .SLDO_VREF_D_PAD     (                       ),
       .SLDO_VOFFSET_D_PAD  (                       ),
       .SLDO_VDDSHUNT_D_PAD (                       ),

       //
       // ANALOG core power/ground
       //
       //.VDDA                (                       ),
       //.GNDA                (                       ),
       .VINA_PAD            (                       ),

       //
       // DIGITAL core power/ground
       //
       //.VDDD                (                       ),
       //.GNDD                (                       ),
       .VIND_PAD            (                       ),

       //
       // global substrate
       //
       //.VSUB                (                       ),

       //
       // dedicated PLL power/ground rails
       //
       .VDD_PLL_PAD         (               VDD_PLL ),                    // **NOTE: power connectivity is modeled for CDR/PLL block !
       .GND_PLL_PAD         (               GND_PLL ),

       //
       // dedicated CML driver power/ground rails
       //
       .VDD_CML_PAD         (               VDD_CML ),                    // **NOTE: power connectivity is modeled for CML drivers !
       .GND_CML_PAD         (               GND_PLL ),

       //
       // dedicated 5 Gb/s SER power/ground rails
       //
       //.GWT_VDD_PAD         (                       ),
       //.GWT_VSS_PAD         (                       ),
       //.GWT_VDDHS_PAD       (                       ),
       //.GWT_GNDHS_PAD       (                       ),
       //.GWT_VDDHS_CORE_PAD  (                       ),
       //.GWT_GNDHS_CORE_PAD  (                       ),

       //
       // ground for detector guard-ring pads
       //
       .DET_GRD0_PAD        (                       ),
       .DET_GRD1_PAD        (                       )

       ) ;

    //---------------- End INSTANTIATION Template ---------------
`endif

assign HITOR = |{hit_or[7], hit_or[6], hit_or[5], hit_or[4], hit_or[3], hit_or[2], hit_or[1], hit_or[0]};
// ---- aurora core for simulation -----//
localparam   CLOCKPERIOD_1 = 6.25;

reg     gsr_r;
reg     gts_r;

assign  glbl.GSR = gsr_r;
assign  glbl.GTS = gts_r;

initial begin
  gts_r = 1'b0;
    gsr_r = 1'b1;
    #(200*CLOCKPERIOD_1);
    gsr_r = 1'b0;
    #(1600*CLOCKPERIOD_1);
end

initial begin
    `ifdef INCLUDE_DUT
        `ifdef INCA
            //
            // DUT_1
            //
            $shm_open("/tmp/rd53a_dut_1.shm");
            //$shm_probe("ASM");
            //$shm_probe(tb.dut_1.DCB.DataConcentrator, "A");
            $shm_probe(tb.dut_1, "A");
            $shm_probe(tb.dut_1.DCB, "ASM");
            $shm_probe(tb, "A");
            //$shm_probe(tb.dut_1.DCB.Aurora64b66b_Frame_Multilane_top, "A");
            $shm_probe(tb.dut_1.PixelArray, "A");
            $shm_probe(tb.fpga, "AS");
            $shm_probe(tb.fpga.i_aurora_rx, "A");
            //$shm_probe(tb.fpga.i_aurora_rx.i_aurora_rx_core, "A");
            //$shm_probe(tb.fpga.i_aurora_rx.i_aurora_rx_core.aurora_frame, "AS");
            //$shm_probe(tb.dut_1.DCB.CommandDecoder, "A");
            //$shm_probe(tb.dut_1.DCB.ChannelSync, "A");
            //$shm_probe(tb.dut_1.DCB.GlobalConfiguration, "A");
            $shm_probe(tb.fpga.i_cmd_rd53.i_cmd_rd53_core, "A");
            //$shm_probe(tb.dut_1.DCB.OutputCdcFifo, "A");
            //$shm_probe(tb.dut_1.DCB.AlignData, "A");
            //$shm_probe(tb.dut_1.PixelArray.chip_gen[20].column.iColumnReadControl, "A");
//            //
//            // DUT_2
//            //
//            $shm_open("/tmp/rd53a_dut_2.shm");
//            //$shm_probe("ASM");
//            //$shm_probe(tb.dut_2.DCB.DataConcentrator, "A");
//            $shm_probe(tb.dut_2, "A");
//            $shm_probe(tb.dut_2.DCB, "ASM");
//            $shm_probe(tb, "A");
//            //$shm_probe(tb.dut_2.DCB.Aurora64b66b_Frame_Multilane_top, "A");
//            $shm_probe(tb.dut_2.PixelArray, "A");
//            $shm_probe(tb.fpga, "AS");
//            $shm_probe(tb.fpga.i_aurora_rx, "A");
//            //$shm_probe(tb.fpga.i_aurora_rx.i_aurora_rx_core, "A");
//            //$shm_probe(tb.fpga.i_aurora_rx.i_aurora_rx_core.aurora_frame, "AS");
//            //$shm_probe(tb.dut_2.DCB.CommandDecoder, "A");
//            //$shm_probe(tb.dut_2.DCB.ChannelSync, "A");
//            //$shm_probe(tb.dut_2.DCB.GlobalConfiguration, "A");
//            $shm_probe(tb.fpga.i_cmd_rd53.i_cmd_rd53_core, "A");
//            //$shm_probe(tb.dut_2.DCB.OutputCdcFifo, "A");
//            //$shm_probe(tb.dut_2.DCB.AlignData, "A");
//            //$shm_probe(tb.dut_2.PixelArray.chip_gen[20].column.iColumnReadControl, "A");
        `endif
    `endif

    `ifdef KC705
        $dumpfile("/tmp/rd53daq_k.vcd");
    `elsif BDAQ53
        $dumpfile("/tmp/rd53daq_b.vcd");
    `endif
    $dumpvars(4);
end


// ----- Test pulser ----- //
wire TEST_PULSE;
pulse_gen
#(
    .BASEADDR(PULSE_TEST_BASEADDR),
    .HIGHADDR(PULSE_TEST_HIGHADDR)
) i_pulse_gen
(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(CLK40),
    .EXT_START(1'b0),
    .PULSE(TEST_PULSE)
);


// ----- TLU model ----- //
wire TRIGGER_ENABLE, TRIGGER_VETO;
assign TRIGGER_ENABLE = 1'b1;

tlu_model itlu_model (
    .SYS_CLK(BUS_CLK),
    .SYS_RST(BUS_RST),
    .ENABLE(TRIGGER_ENABLE),
    .TLU_CLOCK(TLU_CLOCK),
    .TLU_BUSY(TLU_BUSY),
    .TLU_TRIGGER(TLU_TRIGGER), // output TLU trigger signal
    .TLU_RESET(TLU_RESET),
    .TRIG(TEST_PULSE)  // input: generate trigger if HIGH
);

endmodule

