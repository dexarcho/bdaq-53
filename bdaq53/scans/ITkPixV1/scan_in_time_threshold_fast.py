#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script scans over different amounts of injected charge
    to find the effective in time threshold of the enabled pixels.
    It is ~40% faster than the standard scan_threshold.py script
    thanks to shortened injection times and the use of
    external trigger words for offline analysis.
    The shortened injection times requires the desoldering
    of C30 and C31 on the Single Chip Card of ITkPixV1
'''

from tqdm import tqdm
import numpy as np
import tables as tb
from shutil import copyfile
import os

from bdaq53.system.scan_base import ScanBase
from bdaq53.chips.shift_and_inject import shift_and_inject_fast, get_scan_loop_mask_steps
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.chips import ITkPixV1
import bdaq53.analysis.analysis_utils as au


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 384,

    'VCAL_MED': 500,
    'VCAL_HIGH_start': 600,
    'VCAL_HIGH_stop': 1400,
    'VCAL_HIGH_step': 20,

    'fine_delay': 0
}


class FastThresholdScanInTime(ScanBase):
    scan_id = 'fast_threshold_scan'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, TDAC=None, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        TDAC : int / np.ndarray
            If int: TDAC value to use for all enabled pixels, overwriting any existing mask.
            If np.ndarray: TDAC mask to use for enabled pixels. Has to have shape to match start_column:stop_column, start_row:stop_row
        '''
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()
        # Write TLU words on every CMD repetition. Needed for analysis.
        self.bdaq.trigger_on_cmd_loop_start()

        if TDAC is not None:
            if type(TDAC) == int:
                self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC
            elif type(TDAC) == np.ndarray:
                self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC[start_column:stop_column, start_row:stop_row]

        self.chip.masks.update(force=True)

    def _scan(self, n_injections=100, VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, CAL_EDGE_latency=9, wait_cycles=300, fine_delay=21, **_):
        '''
        Threshold scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        '''

        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        scan_param_id_range = range(0, len(vcal_high_range))

        self.log.info('Starting scan...')
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH_start, vcal_med=VCAL_MED, fine_delay=fine_delay)
        pbar = tqdm(total=get_scan_loop_mask_steps(scan=self) * len(vcal_high_range), unit=' Mask steps')
        with self.readout():
            shift_and_inject_fast(scan=self, n_injections=n_injections, vcal_high_range=vcal_high_range, scan_param_id_range=scan_param_id_range, pbar=pbar)

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            mean_thr = np.median(a.threshold_map[np.nonzero(a.threshold_map)])
            # mean_noise = np.median(a.noise_map[np.nonzero(a.noise_map)])
            if np.isfinite(mean_thr):
                self.log.info('Mean threshold is %i [Delta VCAL]' % (int(mean_thr)))
            else:
                self.log.warning('Mean threshold could not be determined!')

        # Determines which front end is active and sets the correct bcid cutting value
        rel_bcid = 13
        with tb.open_file(self.output_filename + '_interpreted.h5', 'r+') as in_file:
            run_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])
            threshold_map = in_file.root.ThresholdMap[:]
            hist_occ_in_time = in_file.root.HistRelBCID[:, :, :, rel_bcid]

            scan_range = [v - run_config['VCAL_MED'] for v in range(run_config['VCAL_HIGH_start'],
                                                                    run_config['VCAL_HIGH_stop'],
                                                                    run_config['VCAL_HIGH_step'])]

            in_file.create_carray(in_file.root, name='InTimeHistOcc',
                                  title='Occupancy Histogram',
                                  obj=hist_occ_in_time,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

            in_t_threshold_map, in_t_noise_map, in_t_chi2_map = au.fit_scurves_multithread(
                hist_occ_in_time.reshape((self.chip.masks.dimensions[1] * self.chip.masks.dimensions[0], -1)),
                scan_range,
                run_config['n_injections'],
                optimize_fit_range=False, rows=self.chip.masks.dimensions[1])

            in_file.create_carray(in_file.root, name='InTimeThresholdMap', title='Threshold Map',
                                  obj=in_t_threshold_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='InTimeNoiseMap', title='Noise Map', obj=in_t_noise_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            in_file.create_carray(in_file.root, name='InTimeChi2Map', title='Chi2 / ndf Map',
                                  obj=in_t_chi2_map,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        os.rename(self.output_filename + '_interpreted.h5', self.output_filename + '_inter.h5')
        copyfile(self.output_filename + '_inter.h5', self.output_filename + '_interpreted.h5')
        os.remove(self.output_filename + '_inter.h5')

        # Calculate overdrive
        try:
            overdrive = np.median(in_t_threshold_map[in_t_threshold_map > 0]) - np.median(
                threshold_map[threshold_map > 0])
            self.log.success('The estimated overdrive is {0} [Delta VCAL]'.format(int(overdrive)))
        except ValueError:
            overdrive = 0
            self.log.error('Overdrive estimation failed')

        self.plot_standard_plots(rel_bcid)
        # return overdrive, mean_thr, mean_noise

    def plot_standard_plots(self, rel_bcid=13):
        with tb.open_file(self.output_filename + '_interpreted.h5', 'r') as in_file_h5:
            in_t_hist_occ = in_file_h5.root.InTimeHistOcc[:]
            in_t_threshold_map = in_file_h5.root.InTimeThresholdMap[:]
            in_t_noise_map = in_file_h5.root.InTimeNoiseMap[:]
            fine_bcid_orig = in_file_h5.root.HistRelBCID[:, :, :, :]
            fine_bcid = np.zeros_like(fine_bcid_orig)
            fine_bcid[:, :, :, rel_bcid] = fine_bcid_orig[:, :, :, rel_bcid]

        with plotting.Plotting(analyzed_data_file=self.output_filename + '_interpreted.h5', save_png=False) as p:
            p.create_standard_plots()
            scan_parameter_name = '$\\Delta$ VCAL'
            electron_axis = True
            scan_parameter_range = [v - p.scan_config['VCAL_MED'] for v in
                                    range(p.scan_config['VCAL_HIGH_start'],
                                          p.scan_config['VCAL_HIGH_stop'] + 1,
                                          p.scan_config['VCAL_HIGH_step'])]
            min_tdac, max_tdac, range_tdac, _ = ITkPixV1.get_tdac_range(
                ITkPixV1.get_flavor(p.scan_config['stop_column'] - 1))
            p._plot_scurves(scurves=in_t_hist_occ.reshape((p.rows * p.cols, -1)).T,
                            scan_parameters=scan_parameter_range,
                            electron_axis=electron_axis,
                            scan_parameter_name=scan_parameter_name,
                            suffix='in_time_scurves')
            p._plot_distribution(in_t_threshold_map[in_t_threshold_map != 0 & ~p.enable_mask].T,
                                 plot_range=scan_parameter_range,
                                 electron_axis=electron_axis,
                                 x_axis_title=scan_parameter_name,
                                 title='In Time Threshold distribution for enabled pixels',
                                 print_failed_fits=True,
                                 suffix='in_t_threshold_distribution')
            p._plot_relative_bcid(hist=fine_bcid.sum(axis=(0, 1, 2)).T)
            p._plot_stacked_threshold(data=in_t_threshold_map[in_t_threshold_map != 0 & ~p.enable_mask].T,
                                      tdac_mask=p.tdac_mask[in_t_threshold_map != 0 & ~p.enable_mask].T,
                                      plot_range=scan_parameter_range,
                                      electron_axis=electron_axis,
                                      x_axis_title=scan_parameter_name,
                                      title='In Time Threshold distribution for enabled pixels',
                                      suffix='in_t_tdac_threshold_distribution',
                                      min_tdac=min(min_tdac, max_tdac),
                                      max_tdac=max(min_tdac, max_tdac),
                                      range_tdac=range_tdac)
            p._plot_occupancy(hist=np.ma.masked_array(in_t_threshold_map, p.enable_mask).T,
                              electron_axis=True,
                              z_label='In Time Threshold',
                              title='In Time Threshold',
                              use_electron_offset=True,
                              show_sum=False,
                              z_min=None,
                              z_max=None,
                              suffix='threshold_map')
            plot_range = None
            if in_t_noise_map[in_t_noise_map != 0 & ~p.enable_mask].T.shape[0] == 0:
                plot_range = [0, 10]

            p._plot_distribution(in_t_noise_map[in_t_noise_map != 0 & ~p.enable_mask].T,
                                 title='Noise distribution for enabled pixels',
                                 plot_range=plot_range,
                                 electron_axis=electron_axis,
                                 use_electron_offset=False,
                                 x_axis_title=scan_parameter_name,
                                 y_axis_title='# of hits',
                                 print_failed_fits=True,
                                 suffix='noise_distribution')

            p._plot_occupancy(hist=np.ma.masked_array(in_t_noise_map, p.enable_mask).T,
                              electron_axis=False,
                              use_electron_offset=False,
                              z_label='In Time Noise',
                              z_max='median',
                              title='In Time Noise',
                              show_sum=False,
                              suffix='noise_map')


if __name__ == '__main__':
    with FastThresholdScanInTime(scan_config=scan_configuration) as scan:
        scan.start()
