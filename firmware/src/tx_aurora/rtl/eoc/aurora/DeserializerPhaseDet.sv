`ifndef DESERIALIZER_PHASE_DET__SV
`define DESERIALIZER_PHASE_DET__SV

`timescale 1ns/1ps


module DeserializerPhaseDet
  (
   input wire [3:0] Qa,
   input wire [3:0] Qb,
   input wire [3:0] Qc,
   input wire [3:0] Qd,
   input wire Clk160,
   input wire Reset_b,
   output logic [3:0] DesData,
   output logic Valid);

   enum logic [1:0] 	{QA,QB,QC,QD} 	choice, chosen;
   logic 	foundTrans;

   logic [3:0] 	del_Qa;
   logic [3:0] 	del_Qb;
   logic [3:0] 	del_Qc;
   logic [3:0] 	del_Qd;

    DEL4 Qa0_del_dont_touch (.I(Qa[0]), .Z(del_Qa[0]));
    DEL4 Qa1_del_dont_touch (.I(Qa[1]), .Z(del_Qa[1]));
    DEL4 Qa2_del_dont_touch (.I(Qa[2]), .Z(del_Qa[2]));
    DEL4 Qa3_del_dont_touch (.I(Qa[3]), .Z(del_Qa[3]));

    DEL4 Qb0_del_dont_touch (.I(Qb[0]), .Z(del_Qb[0]));
    DEL4 Qb1_del_dont_touch (.I(Qb[1]), .Z(del_Qb[1]));
    DEL4 Qb2_del_dont_touch (.I(Qb[2]), .Z(del_Qb[2]));
    DEL4 Qb3_del_dont_touch (.I(Qb[3]), .Z(del_Qb[3]));

    DEL4 Qc0_del_dont_touch (.I(Qc[0]), .Z(del_Qc[0]));
    DEL4 Qc1_del_dont_touch (.I(Qc[1]), .Z(del_Qc[1]));
    DEL4 Qc2_del_dont_touch (.I(Qc[2]), .Z(del_Qc[2]));
    DEL4 Qc3_del_dont_touch (.I(Qc[3]), .Z(del_Qc[3]));

    DEL4 Qd0_del_dont_touch (.I(Qd[0]), .Z(del_Qd[0]));
    DEL4 Qd1_del_dont_touch (.I(Qd[1]), .Z(del_Qd[1]));
    DEL4 Qd2_del_dont_touch (.I(Qd[2]), .Z(del_Qd[2]));
    DEL4 Qd3_del_dont_touch (.I(Qd[3]), .Z(del_Qd[3]));

    // synopsys dc_script_begin
    // set_dont_touch *del_dont_touch
    // synopsys dc_script_end

   logic [3:0] 	Qa_int_notmr;
   logic [3:0] 	Qb_int_notmr;
   logic [3:0] 	Qc_int_notmr;
   logic [3:0] 	Qd_int_notmr;
   logic [3:0]  DesData_notmr;

   assign DesData = DesData_notmr;

   always_ff @(posedge Clk160)
     begin
	Qa_int_notmr <= del_Qa;
	Qb_int_notmr <= del_Qb;
	Qc_int_notmr <= del_Qc;
	Qd_int_notmr <= del_Qd;	
     end

   always_comb begin
      priority case ({Qc_int_notmr[0],Qd_int_notmr[0],Qa_int_notmr[0],Qb_int_notmr[0]}) inside
	4'b1100, 4'b0011 : begin 
	   choice = QB;
	   foundTrans = 1'b1;
	end
	4'b1110, 4'b0001: begin 
	   choice = QA;
	   foundTrans = 1'b1;
	end
	4'b1000, 4'b0111 : begin 
	   choice = QC;
	   foundTrans = 1'b1;
	end
	default : begin
	   choice = QD;
	   foundTrans = 1'b0;	   
	end
      endcase // case ({Qd[0],Qc[0],Qb[0],Qa[0])
			    
   end
   
   always_ff @(posedge Clk160)
     if (Reset_b == 1'b0)
       chosen <= QD;
     else if (foundTrans == 1'b1)
       unique case (choice)
	 QA : chosen <= QA;
	 QB : chosen <= QB;
	 QC : chosen <= QC;
       endcase // case (choice)

   always_ff @(posedge Clk160)
     if (foundTrans == 1'b1)
       unique case(choice)
	 QA : if (chosen == QA)
	   Valid <= 1'b1;
	 else
	   Valid <= 1'b0;
	 QB : if (chosen == QB)
	   Valid <= 1'b1;
	 else
	   Valid <= 1'b0;
	 QC : if (chosen == QC)
	   Valid <= 1'b1;
	 else
	   Valid <= 1'b0;
	 QD : if (chosen == QD)
	   Valid <= 1'b1;
	 else
	   Valid <= 1'b0;
                endcase // case (choice)
   
   always_ff @(posedge Clk160)
     unique case(chosen)
       QA : DesData_notmr <= {Qa_int_notmr[0],Qa_int_notmr[1],Qa_int_notmr[2],Qa_int_notmr[3]};
       QB : DesData_notmr <= {Qb_int_notmr[0],Qb_int_notmr[1],Qb_int_notmr[2],Qb_int_notmr[3]};
       QC : DesData_notmr <= {Qc_int_notmr[0],Qc_int_notmr[1],Qc_int_notmr[2],Qc_int_notmr[3]};
       QD : DesData_notmr <= {Qd_int_notmr[0],Qd_int_notmr[1],Qd_int_notmr[2],Qd_int_notmr[3]};       
     endcase // case (chosen)
   
endmodule // DeserializerPhaseDet
			  
`endif //  `ifndef DESERIALIZER_PHASE_DET__SV
