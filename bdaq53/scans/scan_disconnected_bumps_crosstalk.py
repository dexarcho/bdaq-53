#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This scan identifies pixels with disconnected bumps bonds by doing a crosstalk scan
    with maximal DVCAL and injecting in all eight neighbors of a pixel (except at FE boundaries).

    Note:
    To obtain correct results over the whole chip, including the "bad" pixels on the DIFF FE,
    it is recommended to use the custom feedback register settings from below.
    In addition, the sensor should *not* be reverse-biased, but ideally biased at zero current (0A),
    which corresponds to a slight forward voltage around ~1e1 mV.
    Before this scan the FEs have to be tuned (using default register settings!) to different thresholds
    in order to achieve correct results. You should tune to {SYNC: 2500e, LIN: 2000e, DIFF: 1500e}.
    The NoiseOccScan after tuning must be run with the same custom register settings as used here,
    especially for the DIFF FE! The noise occupancy level can be as high as ~1e-4. Make sure to use
    the same value here and in the NoiseOccScan. The noise occupancy scan may also be omitted.
'''

import scipy
import numpy as np
import tables as tb

from tqdm import tqdm

from bdaq53.system.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import analysis_utils as au
from bdaq53.analysis import plotting


scan_configuration = {
    'start_column': 0,
    'stop_column': 400,
    'start_row': 0,
    'stop_row': 192,

    'sensor_pixel': 'square',   # 'square' (50x50) or 'rect' (25x100) pixels?

    'n_injections': 200,
    'max_noise_occ': 1e-4,      # Target maximum noise occupancy from NoiseOccScan

    'VCAL_MED': 0,
    'VCAL_HIGH': 4095
}


class BumpConnCTalkScan(ScanBase):
    scan_id = "bump_connection_crosstalk_scan"

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, max_noise_occ=1e-6, certainty=1.0, VCAL_MED=0, VCAL_HIGH=4095, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        max_noise_occ : float
            Target maximum noise occupancy from NoiseOccScan
        certainty : float
            Expected/tolerable number of pixels wrongly classified 'poorly connected' instead of 'disconnected' ("false negatives").
            Does not cover "false positives" (connected pixels wrongly recognized as poorly/disconnected)!
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            VCAL_HIGH DAC value.
        '''

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()

        self.chip.masks.update(force=True)

        # Need to use modified register settings. Do not change.
        self.chip.registers['IBIAS_KRUM_SYNC'].write(23)
        self.chip.registers['KRUM_CURR_LIN'].write(12)
        self.chip.registers['VFF_DIFF'].write(15)
        self.chip.registers['PRECOMP_DIFF'].write(620)
        self.chip.registers['PRMP_DIFF'].write(450)

        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)

    def _scan(self, sensor_pixel='square', n_injections=100, VCAL_MED=0, VCAL_HIGH=4095, **_):
        '''
        Crosstalk scan main loop

        Parameters
        ----------
        n_injections : int
            Number of injections
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            VCAL_HIGH DAC value.
        '''
        self.log.info('Starting scan...')

        diff_first = True

        pbar = tqdm(total=self.chip.masks.get_mask_steps(pattern='ring_injection'), unit=' Mask steps')
        with self.readout():
            for fe, _ in self.chip.masks.shift(masks=['enable', 'injection'], pattern='ring_injection', cache=True, skip_empty=False):
                if fe == 'SYNC':
                    self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections, latency=122, wait_cycles=400)
                elif fe == 'LIN':
                    self.chip.inject_analog_single(repetitions=n_injections, latency=122, wait_cycles=400)
                else:
                    if diff_first:
                        diff_first = False
                        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED, fine_delay=2)
                    if sensor_pixel == 'square':
                        self.chip.inject_analog_single(repetitions=n_injections, latency=122, wait_cycles=15)   # 50x50 pixels
                    else:
                        self.chip.inject_analog_single(repetitions=n_injections, latency=124, wait_cycles=81)   # 25x100 pixels
                pbar.update(1)

        pbar.close()
        self.log.success('Scan finished')

    def _analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_data()

        with tb.open_file(a.analyzed_data_file, 'r+') as in_file:
            # Get scan parameters
            run_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])

            n_injections, max_noise_occ, certainty = run_config['n_injections'], run_config['max_noise_occ'], run_config['certainty']

            start_column = run_config['start_column']
            stop_column = run_config['stop_column']
            start_row = run_config['start_row']
            stop_row = run_config['stop_row']

            enable_mask = np.zeros(shape=(400, 192), dtype=bool)
            enable_mask[start_column:stop_column, start_row:stop_row] = True
            disable_mask = in_file.root.configuration_in.chip.use_pixel[:]

            enabled_pixels = np.logical_and(enable_mask, disable_mask)

            quantile_alpha = float(certainty) / 400 / 192

            # Estimate maximal number of noise hits
            result = scipy.optimize.root_scalar(f=au.pixel_hits_prob_root_more_n, args=(n_injections * 32 * max_noise_occ, quantile_alpha), method='bisect', bracket=[-0.1, min(n_injections, 150)], xtol=0.1)
            min_occupancy = int(result.root) + 1

            hist_occ = np.array(in_file.root.HistOcc[:]).reshape((400, 192))

            hist_bump_connected_raw = hist_occ > ((min_occupancy + n_injections) / 2.)

            hist_bump_connected = np.full(shape=hist_bump_connected_raw.shape, dtype=np.str, fill_value='-')

            hist_bump_connected[hist_bump_connected_raw] = 'c'      # Obviously connected bumps

            hist_bump_connected[np.invert(enabled_pixels)] = '*'    # Do not count non-enabled and disabled pixels

            def reevaluate_bump_connections():
                for col in range(hist_bump_connected_raw.shape[0]):
                    for row in range(hist_bump_connected_raw.shape[1]):

                        # Not yet confirmed disconnected
                        if hist_bump_connected[col, row] == '-':
                            count = 0   # Count connected neighboring bumps
                            for shift_c in (-1, 0, 1):
                                for shift_r in (-1, 0, 1):
                                    if not (shift_c == 0 and shift_r == 0):     # Do not count central point
                                        try:
                                            c = 1 if hist_bump_connected_raw[col + shift_c, row + shift_r] else 0
                                        except IndexError:
                                            c = 0
                                        count += c
                            if count > 0:
                                hist_bump_connected[col, row] = 'd'     # Disconnected bump, since at least one neighbor's injections should be observed as crosstalk
                            else:
                                hist_bump_connected[col, row] = '?'     # Unknown state, since all neighbors disconnected; cannot observe crosstalk in any case

                        # Spot disconnected bumps surrounding at least one connected bump
                        elif hist_bump_connected[col, row] == 'c':
                            for shift_c in (-1, 0, 1):
                                for shift_r in (-1, 0, 1):
                                    if not (shift_c == 0 and shift_r == 0):     # Skip central point
                                        try:
                                            if not hist_bump_connected_raw[col + shift_c, row + shift_r]:
                                                hist_bump_connected[col + shift_c, row + shift_r] = 'd'     # Disconnected bump
                                        except IndexError:
                                            pass

            # Iteratively determine bump connection states
            hist_bump_connected_old = np.copy(hist_bump_connected)
            num_iterations = 0
            while True:
                reevaluate_bump_connections()
                num_iterations += 1
                if np.array_equal(hist_bump_connected, hist_bump_connected_old):
                    break
                else:
                    if not ((hist_bump_connected_old == '-').any() or (hist_bump_connected_old == '*').any()):
                        raise RuntimeError('Somehow the algorithm does not terminate. Stop here...')
                hist_bump_connected_old = np.copy(hist_bump_connected)
            if (hist_bump_connected == '-').any():
                raise RuntimeError('There are still bumps without evaluated connection state.')

            conn_state_count = dict(zip(*np.unique(hist_bump_connected[enabled_pixels], return_counts=True)))
            num_connected_bumps = conn_state_count.get('c', 0)
            num_poorly_connected_bumps = conn_state_count.get('b', 0)
            num_disconnected_bumps = conn_state_count.get('d', 0)
            num_unknown_state_bumps = conn_state_count.get('?', 0)

            self.log.info('Found {0} connected pixels'.format(num_connected_bumps))
            self.log.info('Found {0} disconnected pixels'.format(num_disconnected_bumps))
            if num_poorly_connected_bumps > 0:
                self.log.info('Found {0} poorly connected pixels'.format(num_poorly_connected_bumps))
            if num_unknown_state_bumps > 0:
                self.log.info('Found {0} pixels of unknown connection state'.format(num_unknown_state_bumps))
            self.log.info('Ignored {0} non-enabled pixels'.format(np.count_nonzero(np.invert(enabled_pixels))))

            # Create array for bump connectivity plots
            hist_bump_bonds_connection = np.full(shape=hist_bump_connected.shape, fill_value=-12.)
            hist_bump_bonds_connection[hist_bump_connected == 'c'] = 1
            hist_bump_bonds_connection[hist_bump_connected == 'b'] = 0.5
            hist_bump_bonds_connection[hist_bump_connected == 'd'] = 0
            hist_bump_bonds_connection[hist_bump_connected == '?'] = -12    # PI * thumb ~ 12
            hist_bump_bonds_connection[hist_bump_connected == '*'] = -13

            in_file.create_carray(in_file.root, name='BumpConnectivityMap', title='Bump Connectivity', obj=hist_bump_bonds_connection,
                                  filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))

        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()

                p._plot_bump_connectivity_map(hist_bump_bonds_connection)

                p._plot_bump_connectivity_hist(connected_bumps=num_connected_bumps,
                                               num_poorly_connected_bumps=num_poorly_connected_bumps,
                                               disconnected_bumps=num_disconnected_bumps,
                                               unknown_state_bumps=num_unknown_state_bumps)


if __name__ == '__main__':
    with BumpConnCTalkScan(scan_config=scan_configuration) as scan:
        scan.start()
