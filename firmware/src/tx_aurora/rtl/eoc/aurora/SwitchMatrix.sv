`ifndef SWITCH_MATRIX__SV
 `define SWITCH_MATRIX__SV

module SwitchMatrix
  #(
    IOS = 4,
    WIDTH = 4
    )
   (
    input wire [IOS-1:0][$clog2(IOS)-1:0] Sel,

    input wire [IOS-1:0][WIDTH-1:0] In,
    output logic [IOS-1:0][WIDTH-1:0] Out
    );

   always_comb begin
      for (int chano = 0; chano < IOS; chano++) begin
	 Out[chano] = '0;
	 for (int chani = 0; chani < IOS; chani++) begin
	    if (Sel[chano] == chani[$clog2(IOS)-1:0])
	      Out[chano] = In[chani];
	 end
      end
   end
endmodule // SwitchMatrix

module SwitchMatrixUnpacked
  #(
    IOS = 4,
    WIDTH = 4
    )
   (
    input wire [IOS-1:0][$clog2(IOS)-1:0] Sel,

    input wire [WIDTH-1:0] In [IOS],
    output logic [WIDTH-1:0] Out [IOS]
    );

   always_comb begin
      for (int chano = 0; chano < IOS; chano++) begin
	 Out[chano] = '0;
	 for (int chani = 0; chani < IOS; chani++) begin
	    if (Sel[chano] == chani[$clog2(IOS)-1:0])
	      Out[chano] = In[chani];
	 end
      end
   end
endmodule // SwitchMatrix

`endif
