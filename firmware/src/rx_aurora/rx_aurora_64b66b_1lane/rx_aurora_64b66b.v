/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ns/1ps
`default_nettype wire

`include "rx_aurora_64b66b_1lane/rx_aurora_64b66b_core.v"
`include "rx_aurora/rx_aurora_64b66b_1lane/ip/exdes/aurora_64b66b_1lane_exdes.v"

module rx_aurora_64b66b
#(
    parameter BASEADDR = 16'h0000,
    parameter HIGHADDR = 16'h0000,
    parameter ABUSWIDTH = 16,
    parameter IDENTIFIER = 0,
    parameter AURORA_CHANNELS = 1
)(
    input wire [1:0] CHIP_TYPE,
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    inout wire [7:0] BUS_DATA,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,

    input wire [AURORA_CHANNELS-1:0] RXP, RXN,
    output wire TX_P, TX_N,

    input wire RX_CLK_IN_P, RX_CLK_IN_N,
    input wire INIT_CLK_IN,
    input wire DRP_CLK_IN,
    output wire REFCLK_OUT,
    output wire TX_OUT_CLK,
    output wire AURORA_CLK_OUT,
    output wire DATA_MERGING_CLK,

    input wire [AURORA_CHANNELS-1:0] FIFO_READ,
    output wire [AURORA_CHANNELS-1:0] FIFO_EMPTY,
    output wire [AURORA_CHANNELS-1:0][31:0] FIFO_DATA,

    output wire [AURORA_CHANNELS-1:0] LOST_ERROR,
    output wire [AURORA_CHANNELS-1:0] RX_LANE_UP,
    output wire [AURORA_CHANNELS-1:0] RX_CHANNEL_UP,
    output wire PLL_LOCKED,

    input wire CLK_CMD,
    input wire CMD_DATA,
    input wire CMD_OUTPUT_EN,

    input wire AURORA_RESET,

    output wire [AURORA_CHANNELS-1:0] RX_SOFT_ERROR,
    output wire [AURORA_CHANNELS-1:0] RX_HARD_ERROR
);

localparam AURORA_LANES = 1;

// Signals to connect the individual channels of the GTX receivers to the core
wire [AURORA_CHANNELS-1:0][63:0] AURORA_USER_K_DATA;
wire [AURORA_CHANNELS-1:0] AURORA_USER_K_VALID;
wire [AURORA_CHANNELS-1:0] AURORA_RST_USER_SYNC;
wire [AURORA_CHANNELS-1:0][63:0] AURORA_RX_TDATA;
wire [AURORA_CHANNELS-1:0][7:0] AURORA_RX_TKEEP;
wire [AURORA_CHANNELS-1:0] AURORA_RX_TVALID;
wire [AURORA_CHANNELS-1:0] AURORA_RX_TLAST;

wire [AURORA_CHANNELS-1:0] rx_channel_up_i;
assign RX_CHANNEL_UP = rx_channel_up_i;
wire [AURORA_CHANNELS-1:0] rx_lane_up_i;
assign RX_LANE_UP = rx_lane_up_i;
wire [AURORA_CHANNELS-1:0] AURORA_PMA_INIT;
wire USER_CLK;

wire [AURORA_CHANNELS-1:0][63:0] TX_DATA;

// Generate multiple cores with increasing base address. They share one 'external' Aurora receiver block
genvar _channel;
generate
    for (_channel = 0; _channel < AURORA_CHANNELS; _channel = _channel + 1) begin: aurora_gen
        wire IP_RD, IP_WR;
        wire [ABUSWIDTH-1:0] IP_ADD;
        wire [7:0] IP_DATA_IN;
        wire [7:0] IP_DATA_OUT;

        bus_to_ip #(
            .BASEADDR(BASEADDR + 16'h100*_channel),
            .HIGHADDR(HIGHADDR + 16'h100*_channel),
            .ABUSWIDTH(ABUSWIDTH)
        ) i_bus_to_ip
        (
            .BUS_RD(BUS_RD),
            .BUS_WR(BUS_WR),
            .BUS_ADD(BUS_ADD),
            .BUS_DATA(BUS_DATA),

            .IP_RD(IP_RD),
            .IP_WR(IP_WR),
            .IP_ADD(IP_ADD),
            .IP_DATA_IN(IP_DATA_IN),
            .IP_DATA_OUT(IP_DATA_OUT)
        );


        // Arbiter to select between hit- and monitoring data
        localparam ARB_WIDTH = 2;
        wire ARB_READY_OUT;
        wire ARB_WRITE_OUT;
        wire ARB_GRANT_USER_K, ARB_GRANT_HIT;
        wire [31:0] ARB_DATA_OUT, FIFO_HIT_DATA;
        wire FIFO_HIT_EMPTY;
        wire FIFO_USER_K_EMPTY;
        wire [31:0] FIFO_USER_K_DATA;

        rrp_arbiter
        #(
            .WIDTH(ARB_WIDTH)
        ) i_rrp_arbiter
        (
            .RST(BUS_RST),
            .CLK(BUS_CLK),

            .WRITE_REQ({!FIFO_USER_K_EMPTY, !FIFO_HIT_EMPTY}),  // indicate request to write data
            .HOLD_REQ({2'b00}),                                 // wait for writing for given stream (priority)
            .DATA_IN({FIFO_USER_K_DATA, FIFO_HIT_DATA}),        // incoming data for arbitration
            .READ_GRANT({ARB_GRANT_USER_K, ARB_GRANT_HIT}),     // indicate to stream that data has been accepted

            .READY_OUT(ARB_READY_OUT),  // indicates ready for outgoing stream
            .WRITE_OUT(ARB_WRITE_OUT),  // indicates will of write to outgoing stream
            .DATA_OUT(ARB_DATA_OUT)     // outgoing data stream
        );


        // map FIFO signals to arbiter
        assign ARB_READY_OUT = FIFO_READ[_channel];
        assign FIFO_DATA[_channel] = {ARB_GRANT_USER_K, ARB_DATA_OUT[23:0]};  // 16 bits of data, 8 bits frame information, 8 bits of additional IDs, flags etc
        assign FIFO_EMPTY[_channel] = ~ARB_WRITE_OUT;

        rx_aurora_64b66b_core
        #(
            .ABUSWIDTH(ABUSWIDTH),
            .IDENTIFIER(_channel),
            .AURORA_LANES(1),
            .AURORA_CHANNELS(AURORA_CHANNELS)
        ) i_aurora_rx_core (
            .CHIP_TYPE(CHIP_TYPE),
            .BUS_CLK(BUS_CLK),
            .BUS_RST(BUS_RST),
            .BUS_ADD(IP_ADD),
            .BUS_DATA_IN(IP_DATA_IN),
            .BUS_RD(IP_RD),
            .BUS_WR(IP_WR),
            .BUS_DATA_OUT(IP_DATA_OUT),

            .FIFO_READ(ARB_GRANT_HIT),
            .FIFO_EMPTY(FIFO_HIT_EMPTY),
            .FIFO_DATA(FIFO_HIT_DATA),

            .USERK_FIFO_READ(ARB_GRANT_USER_K),
            .USERK_FIFO_EMPTY(FIFO_USER_K_EMPTY),
            .USERK_FIFO_DATA(FIFO_USER_K_DATA),

            .LOST_ERROR(LOST_ERROR[_channel]),

            .RX_LANE_UP(rx_lane_up_i[_channel]),
            .RX_CHANNEL_UP(rx_channel_up_i[_channel]),
            .PLL_LOCKED(PLL_LOCKED),

            .CLK_CMD(CLK_CMD),
            .CMD_DATA(CMD_DATA),
            .CMD_OUTPUT_EN(CMD_OUTPUT_EN),
            .TX_DATA_OUT(TX_DATA),  //TODO: CMD via MGT

            .AURORA_RESET(AURORA_RESET),
            .AURORA_RST_USER_SYNC(AURORA_RST_USER_SYNC[_channel]),
            .AURORA_PMA_INIT(AURORA_PMA_INIT[_channel]),
            .AURORA_USER_CLK(USER_CLK),
            .AURORA_USER_K_DATA(AURORA_USER_K_DATA[_channel]),
            .AURORA_USER_K_VALID(AURORA_USER_K_VALID[_channel]),
            .RX_TDATA(AURORA_RX_TDATA[_channel]),
            .RX_TVALID(AURORA_RX_TVALID[_channel]),
            .RX_TKEEP(AURORA_RX_TKEEP[_channel]),
            .RX_TLAST(AURORA_RX_TLAST[_channel]),

            .RX_SOFT_ERROR(RX_SOFT_ERROR[_channel]),
            .RX_HARD_ERROR(RX_HARD_ERROR[_channel])
        );
    end
endgenerate


// The actual Aurora logic. This module includes a common support block (clock, reset) and up to 7 GTX receivers
aurora_64b66b_1lane_exdes #(
    .NR_OF_CHANNELS(AURORA_CHANNELS)
) aurora_frame (
    // Status signals from Aurora
    .RX_HARD_ERR(RX_HARD_ERROR),
    .RX_SOFT_ERR(RX_SOFT_ERROR),
    .DATA_ERR_COUNT(),
    .RX_LANE_UP(rx_lane_up_i),
    .RX_CHANNEL_UP(rx_channel_up_i),

    // Clocks, resets
    .INIT_CLK(INIT_CLK_IN),
    .GTXQ0_P(RX_CLK_IN_P),
    .GTXQ0_N(RX_CLK_IN_N),
    .DRP_CLK_IN(DRP_CLK_IN),
    .USER_CLK(USER_CLK),
    .PLL_LOCKED(PLL_LOCKED),
    .REFCLK1_OUT(REFCLK_OUT),
    .TX_OUT_CLK(TX_OUT_CLK),
    .AURORA_CLK_OUT(AURORA_CLK_OUT),
    .DATA_MERGING_CLK(DATA_MERGING_CLK),

    .RESET(|AURORA_RST_USER_SYNC[0]),  //TODO: Individual resets
    .PMA_INIT(AURORA_PMA_INIT[0]),  //TODO: Individual resets

    // Data IO
    .RXP(RXP),
    .RXN(RXN),

    .TX_P(TX_P),
    .TX_N(TX_N),
    `ifdef USBPIX3
        .TX_DATA(TX_DATA),  //TODO: Keep the CMD via MGT for USBPIX3?
    `else
        .TX_DATA(0),
    `endif

    .USER_K_ERR(),
    .USER_K_DATA(AURORA_USER_K_DATA),
    .USER_K_VALID(AURORA_USER_K_VALID),

    .RX_TDATA(AURORA_RX_TDATA),
    .RX_TVALID(AURORA_RX_TVALID),
    .RX_TKEEP(AURORA_RX_TKEEP),
    .RX_TLAST(AURORA_RX_TLAST)
);

endmodule
