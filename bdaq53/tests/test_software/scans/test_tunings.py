#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
import copy
import inspect
import importlib
import logging
import os
import shutil
import time
import unittest

import yaml

import bdaq53
from bdaq53.tests import utils
from bdaq53.tests import bdaq_mock

bdaq53_path = os.path.dirname(bdaq53.__file__)
bench_config = os.path.abspath(os.path.join(bdaq53_path, 'testbench.yaml'))


def _load_tuning(module_name):
    ''' Load the tuning class with scan_configuration and attach logger '''
    module = importlib.import_module(module_name)

    def filter_func(c):
        if 'tun' in c[0].lower():
            return True
        return False

    # Get tuning class in module
    for c in inspect.getmembers(module, inspect.isclass):
        if 'tun' in c[0].lower():
            cls = c[1]

    scan_config = module.scan_configuration

    # Catch scan output to check for errors reported
    scan_logger = logging.getLogger(cls.__name__)
    scan_log_handler = utils.MockLoggingHandler(level='DEBUG')
    scan_logger.addHandler(scan_log_handler)
    scan_log_messages = scan_log_handler.messages

    return cls, scan_config, scan_log_messages


class TestTunings(unittest.TestCase):
    ''' Test tuning scripts.

    Mainly useful to catch API related issues, not logical bugs.
    If errors are mentioned in the log or exceptions occur a test fails.
    Since data from chip is not simulated tuning results cannot make sense
    '''

    @classmethod
    def setUpClass(cls):
        super(TestTunings, cls).setUpClass()

        analysis_logger = logging.getLogger('Analysis')
        cls.analysis_log_handler = utils.MockLoggingHandler(level='DEBUG')
        analysis_logger.addHandler(cls.analysis_log_handler)
        cls.analysis_log_messages = cls.analysis_log_handler.messages

        # Use hardware mocks to be able to test without hardware
        cls.bhm = bdaq_mock.BdaqMock(n_chips=2)
        # Speed up testing time drastically, by not calling mask shifting
        cls.bhm.patch_function('bdaq53.chips.rd53a.RD53AMaskObject.update')
        cls.bhm.start()

        # Load standard bench config to change in test cases
        with open(bench_config) as f:
            cls.bench_config = yaml.full_load(f)
        cls.bench_config['general']['use_database'] = False  # deactivate failing feature

        # Use the second chip of a dual module to make setup more complex
        cls.bench_config['modules']['module_0']['chip_1'] = copy.deepcopy(cls.bench_config['modules']['module_0']['chip_0'])
        cls.bench_config['modules']['module_0']['chip_1']['chip_sn'] = '0x0002'
        cls.bench_config['modules']['module_0']['chip_1']['receiver'] = "rx1"
        cls.bench_config['modules']['module_0']['chip_1']['send_data'] = "tcp://127.0.0.1:5501"

    @classmethod
    def tearDownClass(cls):
        cls.bhm.stop()

    @classmethod
    def tearDown(cls):
        # Reset messages after each test
        cls.analysis_log_handler.reset()
        cls.bhm.reset()  # clear mock call storage to save RAM
        cls.bhm.patch_function('bdaq53.chips.rd53a.RD53AMaskObject.update')
        shutil.rmtree('output_data', ignore_errors=True)  # always delete output from previous test
        time.sleep(0.1)  # shutil.rmtree does not block until file is really deleted, https://bugs.python.org/issue22024

    def check_scan_success(self, scan_log_messages):
        ''' Check the log output if scan was successfull '''
        if scan_log_messages['error'] or self.analysis_log_messages['error']:
            return False
        if not any('All done!' in msg for msg in scan_log_messages['success']):
            return False
        return True

    def test_global_threshold_tuning(self):
        GDACTuning, scan_config, scan_log_messages, = _load_tuning('bdaq53.scans.tune_global_threshold')

        with GDACTuning(bench_config=self.bench_config, scan_config=scan_config) as tuning:
            tuning.start()

        self.assertTrue(self.check_scan_success(scan_log_messages))

    def test_local_threshold_tuning(self):
        TDACTuning, scan_config, scan_log_messages, = _load_tuning('bdaq53.scans.tune_local_threshold')

        with TDACTuning(bench_config=self.bench_config, scan_config=scan_config) as tuning:
            tuning.start()

        self.assertTrue(self.check_scan_success(scan_log_messages))

    def test_noise_threshold_tuning(self):
        NoiseTuning, scan_config, scan_log_messages, = _load_tuning('bdaq53.scans.tune_local_threshold_noise')

        with NoiseTuning(bench_config=self.bench_config, scan_config=scan_config) as tuning:
            tuning.start()

        self.assertTrue(self.check_scan_success(scan_log_messages))

    def test_tlu_tuning(self):
        TuneTlu, scan_config, scan_log_messages, = _load_tuning('bdaq53.scans.tune_tlu')
        scan_config['sleep'] = 0  # speed up test

        with TuneTlu(bench_config=self.bench_config, scan_config=scan_config) as tuning:
            tuning.start()

        self.assertTrue(self.check_scan_success(scan_log_messages))

    def test_tune_tot(self):
        TotTuning, scan_config, scan_log_messages, = _load_tuning('bdaq53.scans.tune_tot')

        with TotTuning(bench_config=self.bench_config, scan_config=scan_config) as tuning:
            tuning.start()

        self.assertTrue(self.check_scan_success(scan_log_messages))


if __name__ == '__main__':
    unittest.main()
