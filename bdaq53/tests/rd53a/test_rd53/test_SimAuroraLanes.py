#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import os
import unittest
import shutil

from bdaq53.tests import utils
from bdaq53.scans.test_registers import RegisterTest

dir_path = os.path.dirname(os.path.realpath(__file__))


local_configuration = {
    'lanes': [1, 4]
}


class TestAuroraLanes(unittest.TestCase):
    def test_test_registers(self):
        for lanes in local_configuration.pop('lanes'):
            logging.info('Test register write/read with %i Aurora Lane(s)' % (lanes))
            with RegisterTest(bdaq_conf=utils.setup_cocotb(rx_lanes=lanes), bench_config=os.path.join(dir_path, 'testbench_sim.yaml'), scan_config={'ignore': list(range(3, 138))}) as scan:
                scan.start()

    def tearDown(self):
        utils.close_sim()
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
