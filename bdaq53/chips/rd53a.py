#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import yaml
import os
import time
import math

import numpy as np
from numba import njit

from basil.utils.BitLogic import BitLogic

from bdaq53.chips.chip_base import ChipBase, RegisterObject, MaskObject
from bdaq53.analysis import analysis_utils
from bdaq53.analysis import rd53a_analysis as ana
from bdaq53.system import logger as logger

FLAVOR_COLS = {'SYNC': range(0, 128),
               'LIN': range(128, 264),
               'DIFF': range(264, 400)}


def get_flavor(col):
    for fe, cols in FLAVOR_COLS.items():
        if col in cols:
            return fe


def get_tdac_range(fe):
    if fe == 'SYNC':
        return 0, 0, 0, 0
    elif fe == 'LIN':
        return 15, 0, 16, -1
    elif fe == 'DIFF':
        return -15, 15, 31, 1


@njit(cache=True, fastmath=True)
def encode_rd53a(address, data):
    ''' Encodes address + data information into the custom RD53a 5bit values -> 8bit symbols protocoll '''

    # Lookup table, 5-bit values to 8-bit symbols, index = value
    symbols = [0b01101010, 0b01101100, 0b01110001, 0b01110010, 0b01110100, 0b10001011, 0b10001101, 0b10001110, 0b10010011, 0b10010101, 0b10010110,  # value 1 - 10
               0b10011001, 0b10011010, 0b10011100, 0b10100011, 0b10100101, 0b10100110, 0b10101001, 0b10101010, 0b10101100, 0b10110001,  # value 11 - 20
               0b10110010, 0b10110100, 0b11000011, 0b11000101, 0b11000110, 0b11001001, 0b11001010, 0b11001100, 0b11010001,   # value 21- 30
               0b11010010, 0b11010100]    # value 31 - 32

    word_1 = (address >> 4)  # 9-bit adressword
    word_2 = (address & 0b000001111) << 1 | (data & 0b1000000000000000) >> 15
    word_3 = (data >> 10) & 0b0000000000011111
    word_4 = (data >> 5) & 0b0000000000011111
    word_5 = data & 0b0000000000011111

    return [symbols[word_1], symbols[word_2], symbols[word_3], symbols[word_4], symbols[word_5]]


@njit(cache=True, fastmath=True)
def get_double_pixels_data(mask, enable, injection, hitbus, tdac, lin_gain_sel, dc_aligned=True):
    ''' Get array of register values for double-pixels selected by the mask array.

        Creates an 8-bit value for each selected pixel in the mask array.
        Since pixel registers are defined for neighboring double pixels a 16-bit entry for each double pixel is returned.
        dc_aligned: Boolean
            If set the order of returned words is looping over double pixels
            otherwise the order of returned loops the double pixel and then the double columns.

            Just changes the word order.
    '''
    n_pixel = mask.shape[0] * mask.shape[1] // 2
    data = np.zeros(shape=(n_pixel, ), dtype=np.uint16)
    i = 0
    col = 0
    value = np.uint8(0)  # noqa: F841

    def pixel_value(col, row):
        # 8-bit pixel register value is: TDAC HITB INJ EN = 00000 0 0 0
        value = (enable[col, row] | (injection[col, row] << 1) | (hitbus[col, row] << 2))
        # Linear flavour TDAC
        if col >= 128 and col < 264:
            value |= (((lin_gain_sel[col, row] << 4) | abs(tdac[col, row])) << 3)
        elif col >= 264:
            value |= (abs(tdac[col, row]) << 3)
            if tdac[col, row] < 0:  # sign bit
                value |= (1 << 7)
        return value

    while col < mask.shape[0]:
        for row in range(mask.shape[1]):
            if col % 2 == 0 and (mask[col, row] or (dc_aligned and mask[col + 1, row])):  # even columns
                data[i] = pixel_value(col, row)
                v = pixel_value(col + 1, row)
                data[i] |= (v << 8)
                i += 1
            elif col % 2 == 1 and mask[col, row] and not mask[col - 1, row]:    # odd columns
                data[i] = pixel_value(col - 1, row)
                v = pixel_value(col, row)
                data[i] |= (v << 8)
                i += 1
        if dc_aligned:
            col += 2
        else:
            col += 1

    return data[:i]


class RD53ACalibration(object):
    # Natural constants
    e = 1.6021766208e-19
    kb = 1.38064852e-23

    def __init__(self, calibration=None):
        if calibration is None:
            calibration = {}
        # Chip specific calibration constants
        try:
            self.e_conversion = {}
            self.e_conversion['slope'] = calibration['e_conversion'].get('slope', 10.4)
            self.e_conversion['slope_error'] = calibration['e_conversion'].get('slope_error', 0.10)
            self.e_conversion['offset'] = calibration['e_conversion'].get('offset', 180)
            self.e_conversion['offset_error'] = calibration['e_conversion'].get('offset_error', 60)
        except KeyError:
            self.e_conversion = {'slope': 10.4, 'slope_error': 0.10, 'offset': 180, 'offset_error': 60}

        self.ADC_a = calibration.get('ADC_a', 0.2)      # Conversion factors ADC counts - voltage: slope
        self.ADC_b = calibration.get('ADC_b', 15.5)     # Conversion factors ADC counts - voltage: offset

        self.t_sensors_calibrated = False
        Nf_global = calibration.get('Nf', 1.24)         # Chip specific conversion factors for on-chip T sensors: voltage - temperature
        if 'Nf_0' in calibration.keys():
            self.t_sensors_calibrated = True
        self.Nf_0 = calibration.get('Nf_0', Nf_global)  # Generally, there is one Nf per sensor. For a default fallback in case of
        self.Nf_1 = calibration.get('Nf_1', Nf_global)  # uncalibrated chips, all fall back to a global value.
        self.Nf_2 = calibration.get('Nf_2', Nf_global)
        self.Nf_3 = calibration.get('Nf_3', Nf_global)

    def return_all_values(self):
        values = {}
        values['e_conversion_slope'] = self.e_conversion['slope']
        values['e_conversion_slope_error'] = self.e_conversion['slope_error']
        values['e_conversion_offset'] = self.e_conversion['offset']
        values['e_conversion_offset_error'] = self.e_conversion['offset_error']
        values['ADC_a'] = self.ADC_a
        values['ADC_b'] = self.ADC_b
        values['Nf_0'] = self.Nf_0
        values['Nf_1'] = self.Nf_1
        values['Nf_2'] = self.Nf_2
        values['Nf_3'] = self.Nf_3

        return values

    def _dV_to_T(self, dV, sensor):
        Nf = eval('self.Nf_{0}'.format(sensor))
        return self.e / (Nf * self.kb * np.log(15)) * dV - 273.15

    def get_V_from_ADC(self, ADC, use_offset=True):
        if use_offset:
            return round(((self.ADC_a * ADC + self.ADC_b) / 1000), 3)
        else:
            return round(((self.ADC_a * ADC) / 1000), 3)

    def get_temperature_from_ADC(self, dADC, sensor):
        return round((self._dV_to_T(self.get_V_from_ADC(dADC, use_offset=False), sensor=sensor)), 3)


class RD53AMaskObject(MaskObject):
    def remap_to_global(self, col, row):
        ''' Returns global address from column and row '''
        core_col = int(col / 8)
        core_row = int(row / 8)
        region = 2 * (row % 8) + int((col % 8) / 4)
        pixel_pair = int((col % 4) / 2)
        pixel = col % 2

        return (core_col, core_row, region, pixel_pair, pixel)

    def remap_to_local(self, core_col, core_row, region, pixel_pair, pixel):
        ''' Returns column and row from global address'''
        col = core_col * 8 + (region % 2) * 4 + pixel_pair * 2 + pixel
        row = core_row * 8 + int(region / 2)

        return (col, row)

    def remap_to_registers(self, core_col, core_row, region, pixel_pair):
        region_bin = '{0:04b}'.format(region)
        region_col = '{0:#06b}{1:s}{2:01b}'.format(core_col, region_bin[3], pixel_pair)
        region_row = '{0:#06b}{1:s}'.format(core_row, region_bin[:3])

        return (region_col, region_row)

    def get_pixel_data(self, column, row):
        ''' Return register value for one given pixel '''
        enable = str(int(self['enable'][column, row]))
        injection_enable = str(int(self['injection'][column, row]))
        hitbus_enable = str(int(self['hitbus'][column, row]))

        if column >= 128 and column < 264:
            if self['tdac'][column, row] < 0:
                self['tdac'][column, row] = 0
            tdac = str(bin(int(self['lin_gain_sel'][column, row])))[2:] + str(bin(self['tdac'][column, row]))[2:].zfill(4)
        elif column >= 264:
            if self['tdac'][column, row] >= 0:
                tdac = str(bin(self['tdac'][column, row]))[2:].zfill(5)
            else:
                tdac = '1' + str(bin(abs(self['tdac'][column, row])))[2:].zfill(4)
        else:
            tdac = '00000'

        return tdac + hitbus_enable + injection_enable + enable

    def get_double_pixel_data(self, core_col, core_row, region, pixel_pair):
        ''' Return register value for given double pixel '''
        return int('0b' + self.get_pixel_data(*self.remap_to_local(core_col, core_row, region, pixel_pair, 1)) +
                   self.get_pixel_data(*self.remap_to_local(core_col, core_row, region, pixel_pair, 0)), 2)

    def reset_all(self):
        super(RD53AMaskObject, self).reset_all()
        # Add special treatment for default TDAC, since LIN and DIFF need different default values
        self['tdac'][128:264, :] = 7

    def update(self, force=False, broadcast=False):
        ''' Write the actual pixel register configuration

            Only write changes or the complete matrix using auto-row for speed up
        '''
        if force and self.chip.bdaq.board_version == 'SIMULATION':
            return []

        self._find_changes()

        indata = self.chip.registers['PIX_DEFAULT_CONFIG'].get_write_command(0)  # Disable default config

        if force:
            write_mask = np.ones(self.dimensions, bool)
        else:   # Find out which pixels need to be updated
            write_mask = self.to_write
        to_write = np.column_stack((np.where(write_mask)))
        n_changes = len(to_write)

        if n_changes < 4000:
            pix_reg_data = get_double_pixels_data(mask=write_mask,
                                                  enable=self['enable'],
                                                  injection=self['injection'],
                                                  hitbus=self['hitbus'],
                                                  tdac=self['tdac'],
                                                  lin_gain_sel=self['lin_gain_sel'],
                                                  dc_aligned=False)  # direct pixels addressing
        else:
            pix_reg_data = get_double_pixels_data(mask=np.ones_like(self['enable']),
                                                  enable=self['enable'],
                                                  injection=self['injection'],
                                                  hitbus=self['hitbus'],
                                                  tdac=self['tdac'],
                                                  lin_gain_sel=self['lin_gain_sel'],
                                                  dc_aligned=True)  # auto row aligns words at even columns

        data = []

        if n_changes < 4000:   # Write only double pixels which need to be updated
            self.chip.registers['PIX_MODE'].set(0)
            indata += self.chip.registers['PIX_MODE'].get_write_command(0)    # Disable broadcast, auto-col, auto_row

            indata += self.chip.write_sync_01(write=False)
            last_coords = (-1, -1, -1, -1, -1, -1)
            written = []
            index = 0
            for col, row in to_write:
                (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, row)
                (region_col, region_row) = self.remap_to_registers(core_col, core_row, region, pixel_pair)

                # Speedup block
                if (core_col, core_row, region, pixel_pair) in written:  # This pixel pair has already been updated
                    continue
                if col != last_coords[0]:  # Downshifting patterns always changes two pixels below each other
                    indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))
                if row != last_coords[1]:  # Sideshifting patterns always changes two pixels next to each other
                    indata += self.chip.registers['REGION_ROW'].get_write_command(int(region_row, 2))
                last_coords = (col, row, core_col, core_row, region, pixel_pair)
                written.append((core_col, core_row, region, pixel_pair))

                # Write pixel pair value
                dp_data = int(pix_reg_data[index])
                index += 1

                indata += self.chip.registers['PIX_PORTAL'].get_write_command(dp_data)

                if len(indata) > 4000:    # Write command to chip before it gets too long
                    self.chip.write_command(indata)
                    data.append(indata)
                    indata = self.chip.write_sync_01(write=False)

            self.chip.write_command(indata)
            data.append(indata)
        else:   # Use auto-row
            self.chip.registers['PIX_MODE'].set(0b001000)
            indata += self.chip.registers['PIX_MODE'].get_write_command(0b001000)    # Disable broadcast, enable auto-row

            indata += self.chip.write_sync_01(write=False)

            index = 0

            for col in range(0, 400, 2):   # Loop over every second column, since two neighboring pixels are always written together
                # Set column address to next column pair
                (core_col, core_row, region, pixel_pair, _) = self.remap_to_global(col, 0)
                (region_col, _) = self.remap_to_registers(core_col, core_row, region, pixel_pair)
                indata += self.chip.registers['REGION_COL'].get_write_command(int(region_col, 2))

                # Set row to 0
                indata += self.chip.registers['REGION_ROW'].get_write_command(0)

                for row in range(192):  # Loop over every row, write double pixels
                    dp_data = int(pix_reg_data[index])
                    index += 1

                    indata += self.chip.registers['PIX_PORTAL'].get_write_command(dp_data)

                    if len(indata) > 4000:  # Write command to chip before it gets too long
                        self.chip.write_command(indata)
                        data.append(indata)
                        indata = self.chip.write_sync_01(write=False)

            self.chip.write_command(indata)
            data.append(indata)

        # Set this mask as last mask to be able to find changes in next update()
        for name, mask in self.items():
            self.was[name][:] = mask[:]

        return data

    def apply_good_pixel_mask_diff(self):
        '''
        This function disables pixels with too high parasitic capacitance on the comparator output
        in the differntial front end. The matrix represents one digital core
        with 1 indicating enabled pixels and 0 indicating disabled pixels.
        '''
        self.chip.log.info('Applying good pixel mask to Differential Front-End')
        good_pixel_cell = np.transpose([[0, 0, 1, 1, 0, 0, 0, 0],
                                        [0, 0, 0, 1, 0, 0, 0, 0],
                                        [0, 0, 0, 1, 0, 0, 0, 0],
                                        [0, 0, 0, 1, 1, 0, 0, 0],
                                        [0, 0, 0, 1, 1, 0, 0, 0],
                                        [0, 0, 0, 1, 1, 0, 0, 0],
                                        [0, 0, 0, 1, 0, 0, 0, 0],
                                        [0, 0, 0, 0, 1, 0, 0, 0]])
        good_pixel_mask = np.tile(good_pixel_cell, (17, 24))
        self.disable_mask[264:, :] = np.logical_and(self.disable_mask[264:, :], good_pixel_mask)

    def load_logo_mask(self, masks=['injection']):
        from pylab import imread
        img = np.invert(imread(os.path.join(os.path.dirname(__file__), 'RD53A_logo.bmp')).astype('bool'))
        img_mask = np.zeros((400, 192), dtype=bool)

        img_mask[105:295, 1:191] = np.transpose(img[:, :, 0])
        img_mask[:, 0] = False
        img_mask[:, -1] = False
        img_mask[0:105, :] = False
        img_mask[295:400, :] = False

        for name in masks:
            self[name] = img_mask
        return img_mask


class RD53A(ChipBase):
    '''
    Main class for RD53A chip
    '''

    cmd_data_map = {
        0: 0b01101010,
        1: 0b01101100,
        2: 0b01110001,
        3: 0b01110010,
        4: 0b01110100,
        5: 0b10001011,
        6: 0b10001101,
        7: 0b10001110,
        8: 0b10010011,
        9: 0b10010101,
        10: 0b10010110,
        11: 0b10011001,
        12: 0b10011010,
        13: 0b10011100,
        14: 0b10100011,
        15: 0b10100101,
        16: 0b10100110,
        17: 0b10101001,
        18: 0b10101010,
        19: 0b10101100,
        20: 0b10110001,
        21: 0b10110010,
        22: 0b10110100,
        23: 0b11000011,
        24: 0b11000101,
        25: 0b11000110,
        26: 0b11001001,
        27: 0b11001010,
        28: 0b11001100,
        29: 0b11010001,
        30: 0b11010010,
        31: 0b11010100
    }

    trigger_map = {
        0: 0b00101011,
        1: 0b00101011,
        2: 0b00101101,
        3: 0b00101110,
        4: 0b00110011,
        5: 0b00110101,
        6: 0b00110110,
        7: 0b00111001,
        8: 0b00111010,
        9: 0b00111100,
        10: 0b01001011,
        11: 0b01001101,
        12: 0b01001110,
        13: 0b01010011,
        14: 0b01010101,
        15: 0b01010110
    }

    CMD_GLOBAL_PULSE = 0b01011100
    CMD_CAL = 0b01100011
    CMD_REGISTER = 0b01100110
    CMD_RDREG = 0b01100101
    CMD_NULL = 0b01101001
    CMD_ECR = 0b01011010
    CMD_BCR = 0b01011001
    CMD_SYNCH = 0b10000001
    CMD_SYNCL = 0b01111110
    CMD_SYNC = [0b10000001, 0b01111110]  # 0x(817E)

    macro_regs = [{'columns': range(0, 32),
                   'macro_columns': range(0, 16),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_1'},

                  {'columns': range(32, 64),
                   'macro_columns': range(16, 32),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_2'},

                  {'columns': range(64, 96),
                   'macro_columns': range(32, 48),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_3'},

                  {'columns': range(96, 128),
                   'macro_columns': range(48, 64),
                   'macro_name': 'EN_MACRO_COL_CAL_SYNC_4'},

                  {'columns': range(128, 160),
                   'macro_columns': range(64, 80),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_1'},

                  {'columns': range(160, 192),
                   'macro_columns': range(80, 96),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_2'},

                  {'columns': range(192, 224),
                   'macro_columns': range(96, 112),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_3'},

                  {'columns': range(224, 256),
                   'macro_columns': range(112, 128),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_4'},

                  {'columns': range(256, 264),
                   'macro_columns': range(128, 132),
                   'macro_name': 'EN_MACRO_COL_CAL_LIN_5'},

                  {'columns': range(264, 296),
                   'macro_columns': range(132, 148),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_1'},

                  {'columns': range(296, 328),
                   'macro_columns': range(148, 164),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_2'},

                  {'columns': range(328, 360),
                   'macro_columns': range(164, 180),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_3'},

                  {'columns': range(360, 392),
                   'macro_columns': range(180, 196),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_4'},

                  {'columns': range(392, 400),
                   'macro_columns': range(196, 200),
                   'macro_name': 'EN_MACRO_COL_CAL_DIFF_5'}]

    core_regs = [{'columns': range(0, 128),
                  'core_columns': range(0, 16),
                  'core_name': 'EN_CORE_COL_SYNC'},

                 {'columns': range(128, 256),
                  'core_columns': range(16, 32),
                  'core_name': 'EN_CORE_COL_LIN_1'},

                 {'columns': range(256, 264),
                  'core_columns': range(32, 33),
                  'core_name': 'EN_CORE_COL_LIN_2'},

                 {'columns': range(264, 392),
                  'core_columns': range(33, 49),
                  'core_name': 'EN_CORE_COL_DIFF_1'},

                 {'columns': range(392, 400),
                  'core_columns': range(49, 50),
                  'core_name': 'EN_CORE_COL_DIFF_2'}]

    voltage_mux = {'ADCbandgap': 0,
                   'CAL_MED_left': 1,
                   'CAL_HI_left': 2,
                   'TEMPSENS_1': 3,
                   'RADSENS_1': 4,
                   'TEMPSENS_2': 5,
                   'RADSENS_2': 6,
                   'TEMPSENS_4': 7,
                   'RADSENS_4': 8,
                   'VREF_VDAC': 9,
                   'VOUT_BG': 10,
                   'IMUX_out': 11,
                   'VCAL_MED': 12,
                   'VCAL_HIGH': 13,
                   'RADSENS_3': 14,
                   'TEMPSENS_3': 15,
                   'REF_KRUM_LIN': 16,
                   'Vthreshold_LIN': 17,
                   'VTH_SYNC': 18,
                   'VBL_SYNC': 19,
                   'VREF_KRUM_SYNC': 20,
                   'VTH_HI_DIFF': 21,
                   'VTH_LO_DIFF': 22,
                   'VIN_Ana_SLDO': 23,
                   'VOUT_Ana_SLDO': 24,
                   'VREF_Ana_SLDO': 25,
                   'VOFF_Ana_SLDO': 26,
                   'ground': 27,
                   'ground1': 28,
                   'VIN_Dig_SLDO': 29,
                   'VOUT_Dig_SLDO': 30,
                   'VREF_Dig_SLDO': 31,
                   'VOFF_Dig_SLDO': 32,
                   'ground2': 33}

    current_mux = {'IREF': 0,
                   'IBIASP1_SYNC': 1,
                   'IBIASP2_SYNC': 2,
                   'IBIAS_DISC_SYNC': 3,
                   'IBIAS_SF_SYNC': 4,
                   'ICTRL_SYNCT_SYNC': 5,
                   'IBIAS_KRUM_SYNC': 6,
                   'COMP_LIN': 7,
                   'FC_BIAS_LIN': 8,
                   'KRUM_CURR_LIN': 9,
                   'LDAC_LIN': 10,
                   'PA_IN_BIAS_LIN': 11,
                   'COMP_DIFF': 12,
                   'PRECOMP_DIFF': 13,
                   'FOL_DIFF': 14,
                   'PRMP_DIFF': 15,
                   'LCC_DIFF': 16,
                   'VFF_DIFF': 17,
                   'VTH1_DIFF': 18,
                   'VTH2_DIFF': 19,
                   'CDR_CP_IBIAS': 20,
                   'VCO_BUFF_BIAS': 21,
                   'VCO_IBIAS': 22,
                   'CML_TAP_BIAS0': 23,
                   'CML_TAP_BIAS1': 24,
                   'CML_TAP_BIAS2': 25}

    flavor_cols = FLAVOR_COLS

    def __init__(self, bdaq, chip_sn='0x0000', chip_id=8, receiver='rx0', config=None):
        self.log = logger.setup_derived_logger('RD53A - ' + chip_sn)
        self.bdaq = bdaq
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        if chip_id not in range(9):
            raise ValueError("Invalid Chip ID %r, use integer 0-8" % chip_id)

        self.chip_type = 'rd53a'
        self.chip_sn = chip_sn
        self.chip_id = chip_id
        self.receiver = receiver

        if config is None or len(config) == 0:
            self.log.warning("No explicit configuration supplied. Using 'rd53a_default.cfg.yaml'!")
            with open(os.path.join(os.path.dirname(__file__), 'rd53a_default.cfg.yaml'), 'r') as f:
                self.configuration = yaml.full_load(f)
        elif isinstance(config, dict):
            self.configuration = config
        elif isinstance(config, str) and os.path.isfile(config):
            with open(config) as f:
                self.configuration = yaml.full_load(f)
        else:
            raise TypeError('Supplied config has unknown format!')

        self.registers = RegisterObject(self, 'rd53a_registers.yaml')

        masks = {'enable': {'default': False},
                 'injection': {'default': False},
                 'hitbus': {'default': False},
                 'tdac': {'default': 0},
                 'lin_gain_sel': {'default': True},
                 'injection_delay': {'default': 351}}
        self.masks = RD53AMaskObject(self, masks, (400, 192))

        # Load disabled pixels from chip config
        if 'disable' in self.configuration.keys():
            for pix in self.configuration['disable']:
                self.masks.disable_mask[pix[0], pix[1]] = False

        self.calibration = RD53ACalibration(self.configuration['calibration'])

    def init(self, **_):
        if self.bdaq.board_version != 'SIMULATION':
            self.write_trimbits()   # Try to write trimbits for better communication
            self.init_communication()
            self.reset()
            if self.bdaq.enable_NTC:
                self.get_temperature_NTC()
            else:
                self.get_temperature_sensors()
        else:
            self.init_communication()

    def write_trimbits(self, verify=False):
        '''
        Write trimbits for all reference voltages to the chip, if they are supplied in the chip config.
        '''

        if 'trim' not in self.configuration:
            return

        vref_a_trim = self.configuration['trim'].get('VREF_A_TRIM', 16)
        vref_d_trim = self.configuration['trim'].get('VREF_D_TRIM', 16)
        self.registers['VOLTAGE_TRIM'].write(int('{0:05b}'.format(vref_a_trim) + '{0:05b}'.format(vref_d_trim), 2), verify=verify)

        mon_bg_trim = self.configuration['trim'].get('MON_BG_TRIM', 0)
        mon_adc_trim = self.configuration['trim'].get('MON_ADC_TRIM', 0)
        self.registers['MONITOR_CONFIG'].write(int('{0:05b}'.format(mon_bg_trim) + '{0:06b}'.format(mon_adc_trim), 2), verify=verify)

    def setup_aurora(self, tx_lanes=1, CB_Wait=255, CB_Send=1, only_cb=False, bypass_mode=False, **_):
        if bypass_mode:
            self.log.info("Switching chip to BYPASS MODE")
            self.registers['CDR_CONFIG'].set(int('0b'
                                                 + '1'  # CDR_SEL_DEL_CLK ***
                                                 + '00'  # CDR_PD_SEL[1:0]
                                                 + '0100'  # CDR_PD_DEL[3:0]
                                                 + '0'  # CDR_EN_GCK2
                                                 + '011'  # CDR_VCO_GAIN[2:0]
                                                 + '111',  # CDR_SEL_SER_CLK[2:0] ***
                                                 2))

        elif self.bdaq['system']['AURORA_RX_640M'] == 1:
            self.log.info("Chip is running at 640Mb/s")
            self.registers['CDR_CONFIG'].set(int('0b'
                                                 + '0'  # CDR_SEL_DEL_CLK
                                                 + '00'  # CDR_PD_SEL[1:0]
                                                 + '0100'  # CDR_PD_DEL[3:0]
                                                 + '0'  # CDR_EN_GCK2
                                                 + '011'  # CDR_VCO_GAIN[2:0]
                                                 + '001',  # CDR_SEL_SER_CLK[2:0]
                                                 2))
        else:
            self.log.info("Chip is running at 1.28Gb/s")
        if only_cb is False:
            self.log.debug("Aurora transmitter settings: Lanes=%u, CB_Wait=%u, CB_Send=%u", tx_lanes, CB_Wait, CB_Send)
            self.log.info("{} Aurora lanes active".format(tx_lanes))
            if tx_lanes == 4:
                self.registers['OUTPUT_CONFIG'].set(0b00111100)
                self.registers['CML_CONFIG'].set(0b00001111)
            elif tx_lanes == 3:
                self.registers['OUTPUT_CONFIG'].set(0b00011100)
                self.registers['CML_CONFIG'].set(0b00000111)
            elif tx_lanes == 2:
                self.registers['OUTPUT_CONFIG'].set(0b00001100)
                self.registers['CML_CONFIG'].set(0b00000011)
            elif tx_lanes == 1:
                self.registers['OUTPUT_CONFIG'].set(0b00000100)
                self.registers['CML_CONFIG'].set(0b00000001)
            else:
                self.log.error("Aurora lane configuration (1...4) must be specified")
        else:
            self.log.debug("Aurora settings: CB_Wait=%u, CB_Send=%u", CB_Wait, CB_Send)

        # Set CB frame distance and number
        self.registers['AURORA_CB_CONFIG0'].set(((CB_Wait << 4) | CB_Send & 0x0f) & 0xff)
        self.registers['AURORA_CB_CONFIG1'].set((CB_Wait & 0xfffff) >> 4)  # Set CB frame distance and number

        self.registers.write_all()

        # Reset Aurora and serializers
        self.send_global_pulse(bitnames=['reset_aurora', 'reset_serializer'], pulse_width=14)

    def init_communication(self, repetitions=None):
        self.log.info('Initializing communication...')

        # Configure cmd encoder
        self.bdaq['cmd'].reset()
        self.bdaq.set_chip_type_RD53A()
        time.sleep(0.1)
        self.write_command(self.write_sync(write=False) * 32)
        # Wait for PLL lock
        self.bdaq.wait_for_pll_lock(lock_receivers=self.receiver)

        self.setup_aurora(tx_lanes=self.bdaq.rx_lanes[self.receiver], bypass_mode=self.bdaq.configuration['hardware']['bypass_mode'])

        # Workaround for locking problems
        for _ in range(30):
            self.write_command(self.write_sync(write=False) * 64)
            self.write_ecr()
            try:
                self.bdaq.wait_for_aurora_sync(sync_receivers=self.receiver)
            except RuntimeError:
                self.registers.write_all(force=True)
            else:
                break

            self.write_command([0x00] * 1000, repetitions=10)
            time.sleep(0.01)
        else:
            self.bdaq.wait_for_aurora_sync(sync_receivers=self.receiver)

        self.log.success('Communication established')

    def write_global_pulse(self, width, write=True):
        # 0101_1100    ChipId<3:0>,0    Width<3:0>,0
        indata = [self.CMD_GLOBAL_PULSE] * 2  # [0b01011100]
        chip_id_bits = self.chip_id << 1
        indata += [self.cmd_data_map[chip_id_bits]]
        width_bits = width << 1
        indata += [self.cmd_data_map[width_bits]]

        if write:
            self.write_command(indata)

        return indata

    def write_cal(self, cal_edge_mode=0, cal_edge_width=10, cal_edge_dly=2, cal_aux_value=0, cal_aux_dly=0, write=True):
        '''
            Command to send a digital or analog injection to the chip.
            Digital or analog injection is selected globally via the INJECTION_SELECT register.

            For digital injection, only CAL_edge signal is relevant:
                - CAL_edge_mode switches between step (0) and pulse (0) mode
                - CAL_edge_dly is counted in bunch crossings. It sets the delay before the rising edge of the signal
                - CAL_edge_width is the duration of the pulse (only in pulse mode) and is counted in cycles of the 160MHz clock
            For analog injection, the CAL_aux signal is used as well:
                - CAL_aux_value is the value of the CAL_aux signal
                - CAL_aux_dly is counted in cycles of the 160MHz clock and sets the delay before the edge of the signal

            {ChipId[3:0],CalEdgeMode, CalEdgeDelay[2:0],CalEdgeWidth[5:4]}{CalEdgeWidth[3:0],CalAuxMode, CalAuxDly[4:0]}
        '''
        indata = [self.CMD_CAL] * 2
        chip_id_bits = self.chip_id << 1
        indata += [self.cmd_data_map[(chip_id_bits + cal_edge_mode)]]
        cal_edge_dly_bits = BitLogic.from_value(0, size=3)
        cal_edge_dly_bits[:] = cal_edge_dly
        cal_edge_width_bits = BitLogic.from_value(0, size=6)
        cal_edge_width_bits[:] = cal_edge_width
        cal_aux_dly_bits = BitLogic.from_value(0, size=5)
        cal_aux_dly_bits[:] = cal_aux_dly

        indata += [self.cmd_data_map[(cal_edge_dly_bits[2:0].tovalue() << 2) + cal_edge_width_bits[5:4].tovalue()]]
        indata += [self.cmd_data_map[(cal_edge_width_bits[3:0].tovalue() << 1) + cal_aux_value]]
        indata += [self.cmd_data_map[cal_aux_dly_bits[4:0].tovalue()]]

        if write:
            self.write_command(indata)

        return indata

    def _read_register(self, address, write=True):
        indata = [self.CMD_RDREG] * 2
        chip_id_bits = self.chip_id << 1
        indata += [self.cmd_data_map[chip_id_bits]]
        addr_bits = BitLogic.from_value(0, size=9 + 6)
        addr_bits[14:6] = address
        indata += [self.cmd_data_map[addr_bits[14:10].tovalue()]]
        indata += [self.cmd_data_map[addr_bits[9:5].tovalue()]]
        indata += [self.cmd_data_map[addr_bits[4:0].tovalue()]]
        if write:
            self.write_command(indata)
        return indata

    def _get_register_value(self, address, timeout=1000, tries=10):
        self.enable_monitor_data()
        self.send_global_pulse('reset_monitor_data', pulse_width=4)
        self.bdaq['FIFO']['RESET']
        if self.bdaq.board_version == 'SIMULATION':
            self.write_command(self.write_sync_01(write=False) * 1000)
        for _ in range(tries):
            self._read_register(address)

            for _ in range(timeout):
                if self.bdaq['FIFO'].get_FIFO_SIZE() > 0:
                    data = self.bdaq['FIFO'].get_data()
                    userk_data = analysis_utils.process_userk(ana.interpret_userk_data(data))
                    if len(userk_data) == 0:
                        continue
                    index_i = np.where(userk_data['Address'] == address)[0]
                    if len(index_i) == 0:
                        continue
                    return userk_data['Data'][index_i][0]
                self.write_command(self.write_sync_01(write=False) * 10)
            else:
                self.log.warning('Timeout while waiting for register response.')
        else:
            raise RuntimeError('Timeout while waiting for register response.')

    def _write_register(self, address, data, write=True):
        '''
            Sends write command to register with data

            Parameters:
            ----------
                address : int
                    Address of the register to be written to

            Returns:
            ----------
                indata : binarray
                    Boolean representation of register write command.
        '''

#         assert(isinstance(data, (int, np.int)) == np.issubdtype(type(data), np.signedinteger))

        if isinstance(data, (int, np.integer)):
            bits = 9 + 16
            data_to_send = data | (address << 16)
            bits_to_send = "{0:025b}".format(data_to_send)
            wr_reg_mode = 0
            chip_id_bits = self.chip_id << 1
            header = chip_id_bits + wr_reg_mode
            indata = [self.CMD_REGISTER, self.CMD_REGISTER, self.cmd_data_map[header]] + encode_rd53a(address, data)
        else:
            if(len(data) != 6):
                raise ValueError('Error while writing data %s to register %s:\n Command data size was not 1 or 6. It was: %s' % (data, address, len(data)))

            wr_reg_mode = 1
            bits = 9 + len(data) * 16

            data_to_send = (address << 6 * 16) | (data[0] << 5 * 16) | (data[1] << 4 * 16) | (data[2] << 3 * 16) | (data[3] << 2 * 16) | (data[4] << 1 * 16) | data[5]
            bits_to_send = "{0:0105b}".format(data_to_send)

            indata = [self.CMD_REGISTER] * 2
            chip_id_bits = self.chip_id << 1
            header = chip_id_bits + wr_reg_mode
            indata += [self.cmd_data_map[header]]

            for b in range(bits // 5):
                v = '0b' + bits_to_send[b * 5:b * 5 + 5]
                indata += [self.cmd_data_map[int(v, 2)]]

        if write:
            self.write_command(indata)

        return indata

    def write_null(self, write=True):
        indata = [self.CMD_NULL] * 2  # [0b01101001]
        if write:
            self.write_command(indata)
        return indata

    def write_ecr(self, write=True):
        indata = [self.CMD_ECR] * 2  # [0b01011010]
        if write:
            self.write_command(indata)
        return indata

    def write_bcr(self, write=True):
        indata = [self.CMD_BCR] * 2  # [0b01011001]
        if write:
            self.write_command(indata)
        return indata

    def write_sync(self, write=True, repetitions=1):
        indata = [self.CMD_SYNCH]
        indata += [self.CMD_SYNCL]
        if write:
            self.write_command(indata, repetitions=repetitions)
        return indata

    def write_sync_01(self, write=True, repetitions=1):
        indata = [0b01010101]
        indata += [0b01010101]
        if write:
            self.write_command(indata, repetitions=repetitions)
        return indata

    def send_trigger(self, trigger, tag=0, write=True):
        # Trigger is always followed by 5 Data bits
        indata = [self.trigger_map[trigger]]
        indata += [self.cmd_data_map[tag]]
        if write:
            self.write_command(indata)
        return indata

    def send_trigger_tag(self, trigger, trigger_tag, write=True):
        if trigger == 0:
            self.log.error("Illegal trigger number")
            return
        else:
            indata = [self.trigger_map[trigger]]
            indata += [trigger_tag]
            if write:
                self.write_command(indata)
            return indata

    def write_command(self, data, repetitions=1, wait_for_done=True, wait_for_ready=False):
        '''
            Write data to the command encoder.

            Parameters:
            ----------
                data : list
                    Up to [get_cmd_size()] bytes
                repetitions : integer
                    Sets repetitions of the current request. 1...2^16-1. Default value = 1.
                wait_for_ready : boolean
                    Wait for completion of preceding commands before sending the command.
                    This is usually on to allow software to run while commands are written. This speeds up all scans
                    by 10% - 20%.
                wait_for_done : boolean
                    Wait for completion after sending the command. Advisable if data to analyze from chip is returned.
        '''

        if isinstance(data[0], list):
            for indata in data:
                self.write_command(indata, repetitions, wait_for_done)
            return

        assert (0 < repetitions < 65536), "Repetition value must be 0<n<2^16"
        if repetitions > 1:
            self.log.debug("Repeating command %i times." % (repetitions))

        if wait_for_ready:
            while (not self.bdaq['cmd'].is_done()):
                pass

        self.bdaq['cmd'].set_data(data)
        self.bdaq['cmd'].set_size(len(data))
        self.bdaq['cmd'].set_repetitions(repetitions)
        self.bdaq['cmd'].start()

        if wait_for_done:
            while (not self.bdaq['cmd'].is_done()):
                pass

    def send_global_pulse(self, bitnames, pulse_width, global_pulse_route_file=None, write=True):
        '''
            Used to set a single or multiple bits of GLOBAL_PULSE_ROUTE register by name, using names defined in rd53a_global_pulse_route.yaml

            Parameters:
            ----------
                bitnames : str or list of str
                    Name(s) of the bit(s) to be set to 1
        '''

        if not global_pulse_route_file:
            global_pulse_route_file = os.path.join(self.proj_dir, 'chips' + os.sep + 'rd53a_global_pulse_route.yaml')

        with open(global_pulse_route_file, 'r') as infile:
            global_pulse_route_map = yaml.full_load(infile)

        if isinstance(bitnames, str):
            bitnames = [bitnames]

        listofnames = [bit['name'] for bit in global_pulse_route_map]

        for bitname in bitnames:
            if bitname not in listofnames:
                self.log.warning('Could not find %s in possible values for register GLOBAL_PULSE_ROUTE' % bitname)

        val = ''
        for i in range(16):
            if global_pulse_route_map[i]['name'] in bitnames:
                val += '1'
            else:
                val += '0'

        val = '0b' + val[::-1]

        indata = []
        if self.registers['GLOBAL_PULSE_ROUTE'].get() != int(val, 2):
            indata += self.registers['GLOBAL_PULSE_ROUTE'].get_write_command(int(val, 2))
        indata += self.write_global_pulse(width=pulse_width)
        if write:
            self.write_command(indata)
        return indata

    def reset(self):
        if self.bdaq.board_version == 'SIMULATION':
            return

        #  Set all registers to default
        self.registers.reset_all()
        for reg, val in self.configuration['registers'].items():
            self.registers[reg].set(val)
        self.registers.write_all()
        self.write_trimbits()

        self.masks.reset_all()  # Set all masks to default and
        self.masks.update(force=True)   # write all masks to chip

        self.send_global_pulse('sync_fe_autozeroing', pulse_width=14)    # FIXME: Do we need this?

        self.enable_core_col_clock(range(50))  # Enable clock on full chip
        self.enable_macro_col_cal(range(200))  # Enable analog calibration on full chip

    def generate_trigger_command(self, trigger_pattern):
        pattern = str(bin(trigger_pattern))[2:]
        cmd = []
        for i in range(math.ceil(len(pattern) / 4)):
            pat = pattern[i * 4:i * 4 + 4]
            cmd += self.send_trigger(int(pat, 2), write=False)
        return cmd

    def setup_digital_injection(self, fine_delay=9):
        self.registers['INJECTION_SELECT'].set(int('0b01{0:b}'.format(fine_delay), 2))  # Enable digital injection with zero delay
        self.registers['INJECTION_SELECT'].write(int('0b01{0:b}'.format(fine_delay), 2))  # Enable digital injection with zero delay

    def inject_digital(self, cal_edge_width=10, cal_edge_dly=2, latency=121, repetitions=1, wait_cycles=300, trigger_pattern=0xffffffff):
        '''
            Injects a digital pulse in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger, accepts values [117:124]
                repetitions : int
                    Number of times, the injection command is repeated, i.e. number of injections
        '''
        indata = self.write_sync_01(write=False) * 10
        indata += self.write_cal(cal_edge_width=cal_edge_width, cal_edge_dly=cal_edge_dly, cal_edge_mode=1, write=False)  # Injection
        indata += self.write_sync_01(write=False) * latency  # Wait for latency
        indata += self.generate_trigger_command(trigger_pattern=trigger_pattern)    # Send triggers
        if self.bdaq.board_version != 'SIMULATION':
            indata += self.write_sync_01(write=False) * wait_cycles  # Wait for data
        self.write_command(indata, repetitions=repetitions)
        return indata

    def setup_analog_injection(self, vcal_high, vcal_med, fine_delay=9, injection_mode=0, write=True):
        '''
            Sets all relevant registers to make the chip ready for analog injection and also arms the injection circuitry.

            ----------
            Parameters:
                vcal_high : int
                    VCAL_HIGH DAC value, defining the voltage for the higher injection point of the differential injection circuit
                vcal_med : int
                    VCAL_MED DAC value, defining the voltage for the lower injection point of the differential injection circuit
                fine_delay : int
                    Range from 0 - 15, defining the injection point within one bcid
                injection_mode : int
                    Can either be 0 or 1, Where 0 corresponds to uniform injection mode and 1 to alternating injection mode (high--> med for even pixels, med-->gnd odd pixels)
        '''
        indata = self.write_sync(write=False) * 10
        indata += self.registers['INJECTION_SELECT'].get_write_command(int('0b' + format(injection_mode, '01b') + '0' + format(fine_delay, '04b'), 2))  # Enable analog injection with configured fine delay
        indata += self.registers['VCAL_HIGH'].get_write_command(vcal_high)  # Set VCAL_HIGH
        indata += self.registers['VCAL_MED'].get_write_command(vcal_med)  # Set VCAL_MED
        indata += self.registers['GLOBAL_PULSE_ROUTE'].get_write_command(0b0100000000000000)  # With every injection reset command decoder to avoid SYNC getting stuck
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, write=False)  # CalEdge -> 0
        if write:
            self.write_command(indata)
        return indata

    def inject_analog_single(self, repetitions=1, latency=122, wait_cycles=300, send_ecr=False, trigger_pattern=0xffffffff, send_trigger=True, write=True):
        '''
            Injects a single analog pulse in all enabled pixels

            ----------
            Parameters:
                latency : int
                    Number of write_sync commands between injection and trigger
        '''

        indata = self.write_sync_01(write=False)

        if send_ecr:
            indata += self.write_global_pulse(width=8, write=False)  # Send AUTO_ZERO pulse
            indata += self.write_ecr(write=False)   # Send ECR for SYNC bug
            indata += self.write_ecr(write=False)   # Send ECR twice. This helps reducing the number of data errors introduced by ECR
            indata += self.write_sync_01(write=False) * 60

        indata += self.write_cal(cal_edge_mode=0, cal_edge_width=1, cal_edge_dly=0, write=False)  # CalEdge -> 1 (inject)
        indata += self.write_sync_01(write=False) * latency  # Wait for latency
        if send_trigger:
            indata += self.generate_trigger_command(trigger_pattern=trigger_pattern)    # Send triggers
        indata += self.write_cal(cal_edge_mode=1, cal_edge_width=0, cal_edge_dly=0, write=False)  # CalEdge -> 0

        if self.bdaq.board_version != 'SIMULATION':
            indata += self.write_sync_01(write=False) * wait_cycles  # Wait for data

        if write:
            self.write_command(indata, repetitions=repetitions)

        return indata

    def revive(self):
        ''' A RD53A state machine can get stuck. This leads to no hits being send out.
            Sending the event counter reset restores normal operation.

            This is a chip bug workaround.
        '''
        self.write_ecr()
        self.write_ecr()
        for _ in range(10):
            time.sleep(0.01)  # Needed to prevent non empty FIFO, since chip send data after ECR
            if self.bdaq['FIFO'].get_FIFO_SIZE() == 0:
                break

    def toggle_output_select(self, repetitions=1, latency=114, wait_cycles=400, fine_delay=9, send_ecr=False):
        '''
            Toggles between digital and analog injection. If the comparator is stuck high
            this creates a transient on hit_t and thus a hit.

            Note
            ----
            Chip implementation:
            assign hit_t = EnDigHit ? (CalEdge & cal_en) : hit ;
            assign HitOut = hit_t & hit_en;

            See also Figure 29 in RD53 manual.

            Parameters:
            ----------
                latency : int
                    Number of write_sync commands between injection and trigger, accepts values [117:124]
        '''

        before_setting = self.registers['INJECTION_SELECT'].get()

        self.registers['INJECTION_SELECT'].write(int('0b01{0:b}'.format(fine_delay), 2))

        indata = self.write_sync_01(write=False)

        if send_ecr:
            indata += self.write_global_pulse(width=8, write=False)
            indata += self.write_ecr(write=False)
            indata += self.write_ecr(write=False)
            indata += self.write_sync_01(write=False) * 60

        # Enable analog injection, this creates a rising edge if comparator output is stuck high
        indata += self.registers['INJECTION_SELECT'].get_write_command(int('0b00{0:b}'.format(fine_delay), 2))
        # Enable digital injection = falling edge for stuck high pixels
        indata += self.registers['INJECTION_SELECT'].get_write_command(int('0b01{0:b}'.format(fine_delay), 2))
        indata += self.write_sync_01(write=False) * latency  # Wait for latency
        indata += self.send_trigger(trigger=0b1111, write=False) * 8  # Trigger

        indata += self.write_sync_01(write=False) * wait_cycles
        self.write_command(indata, repetitions=repetitions)

        self.registers['INJECTION_SELECT'].write(before_setting)

    def enable_core_col_clock(self, core_cols=None, write=True):
        '''
            Enable clocking of given core columns. After POR everything is disabled.

            ----------
            Parameters:
                core_cols : list of int
                    A list of core columns to enable. Default = None disables everything
        '''

        # Disable everything
        indata = self.write_sync_01(write=False)
        indata += self.registers['EN_CORE_COL_SYNC'].get_write_command(0)
        indata += self.registers['EN_CORE_COL_LIN_1'].get_write_command(0)
        indata += self.registers['EN_CORE_COL_LIN_2'].get_write_command(0)
        indata += self.registers['EN_CORE_COL_DIFF_1'].get_write_command(0)
        indata += self.registers['EN_CORE_COL_DIFF_2'].get_write_command(0)

        if core_cols:
            indata += self.write_sync_01(write=False)
            for r in self.core_regs:
                bits = []
                for i, core_col in enumerate(r['core_columns']):
                    if core_col in core_cols:
                        bits.append(i)

                if not bits:
                    continue
                bits = list(set(bits))  # Remove duplicates

                data = ''
                for i in range(len(r['core_columns'])):
                    data += '1' if i in bits else '0'

                indata += self.registers[r['core_name']].get_write_command(int(data[::-1], 2))

        if write:
            self.write_command(indata)

        return indata

    def enable_macro_col_cal(self, macro_cols=None, write=True):
        '''
            Enable analog calibration of given macro (double-) columns. After POR everything is enabled.

            ----------
            Parameters:
                macro_cols : list of int
                    A list of macro columns to enable. Default = None disables everything
        '''

        # Disable everything
        indata = self.registers['EN_MACRO_COL_CAL_SYNC_1'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_SYNC_2'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_SYNC_3'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_SYNC_4'].get_write_command(0)

        indata += self.registers['EN_MACRO_COL_CAL_LIN_1'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_LIN_2'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_LIN_3'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_LIN_4'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_LIN_5'].get_write_command(0)

        indata += self.registers['EN_MACRO_COL_CAL_DIFF_1'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_DIFF_2'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_DIFF_3'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_DIFF_4'].get_write_command(0)
        indata += self.registers['EN_MACRO_COL_CAL_DIFF_5'].get_write_command(0)

        if not macro_cols:
            return

        # Enable given columns
        for r in self.macro_regs:
            bits = []
            for i, macro_col in enumerate(r['macro_columns']):
                if macro_col in macro_cols:
                    bits.append(i)

            if not bits:
                continue
            bits = list(set(bits))  # Remove duplicates

            data = ''
            for i in range(len(r['macro_columns'])):
                data += '1' if i in bits else '0'

            indata += self.registers[r['macro_name']].get_write_command(int(data[::-1], 2))

        if write:
            self.write_command(indata)

        return indata

    def enable_monitor_data(self):
        self.bdaq.set_monitor_filter(receivers=self.receiver, mode='filter')
        self.send_global_pulse('enable_monitoring', 4)
        self.log.debug('Monitor data enabled')

    def disable_monitor_data(self):
        self.bdaq.set_monitor_filter(receivers=self.receiver, mode='block')
        self.log.debug('Monitor data disabled')

    def get_ADC_value(self, name, measure):
        """
        Sends multiplexer settings and reads ADC measurement
        Output is in ADC LSB.

        Parameters:
        ----------
            name: str
                The name of the ADC register to be measured
            measure: str
                Either 'current', 'i', 'voltage' or 'v'; addresses the corresponding multiplexer

        Returns:
        ----------
            ADC value
        """
        # Helper variables
        _measure, _current, _voltage = measure.lower(), ('current', 'i'), ('voltage', 'v')

        if _measure not in _current + _voltage:
            raise ValueError("*measure* must be either 'voltage' ('v') or 'current' ('i')")

        try:
            if _measure in _current:
                bitstring = int('1' + format(self.current_mux[name], '06b') + format(11, '07b'), 2)
            else:
                bitstring = int('1' + format(32, '06b') + format(self.voltage_mux[name], '07b'), 2)
        except KeyError:
            raise ValueError("Invalid ADC register name {} for {} measurement".format(name, _measure))

        # Perform measurement and collect result
        val_adc = self._set_mux_read_adc(mux_val=bitstring)

        # ADC to physical value conversion
        val_real = None

        # FIXME: calibration for current measurement is missing
        if _measure in _voltage:
            val_real = self.calibration.get_V_from_ADC(val_adc)

        return val_adc, val_real

    def _set_mux_read_adc(self, mux_val):
        """

        Parameters
        ----------
        mux_val: int
            Value to write to multiplexer

        Returns
        -------
        ADC value LSB
        """

        # Reset Monitor Data
        self.send_global_pulse(bitnames='reset_monitor_data', pulse_width=4)

        # Reset ADC
        self.send_global_pulse(bitnames='reset_adc', pulse_width=4)

        # Select MUX
        self.registers['MONITOR_SELECT'].write(mux_val)

        # Start ADC conversion
        self.send_global_pulse(bitnames='adc_start_conversion', pulse_width=4)

        # Read and return ADC result
        return self.registers['MonitoringDataADC'].read()

    def get_chip_status(self):
        """
            Returns a map of all important chip parameters.
            Can only be called before or after a scan!
        """

        self.log.info('Recording chip status...')
        try:

            # Get voltages and currents
            voltages = {v_mon: self.get_ADC_value(v_mon, measure='v') for v_mon in self.voltage_mux}
            currents = {c_mon: self.get_ADC_value(c_mon, measure='i') for c_mon in self.current_mux}

        except RuntimeError as e:
            self.log.error('There was an error while receiving the chip status: %s' % e)
            voltages, currents = {}, {}

        return voltages, currents

    def get_ring_oscillators(self, pulse_width=8):
        '''
            Returns data of ring oscillators:
            {
                RING_OSC_i: {
                    raw_data: Raw register data, consisting of a cycle counter and the actual counter,
                    counter: Value of the actual counter (last 12 bits of register value),
                    frequency: Counter value devided by pulse length. In Hz.
                }
            }
        '''

        oscillators = {}
        for i in range(8):
            osc_data = {}
            self.write_command(self.write_sync(write=False) * 300)
            self.registers['RING_OSC_ENABLE'].write(0b11111111)
            self.registers['RING_OSC_' + str(i)].write(0b0)
            self.send_global_pulse('start_ringosc', pulse_width=pulse_width)
            self.write_command(self.write_sync(write=False) * 40)

            raw_data = self.registers['RING_OSC_' + str(i)].read()
            counter = int('{0:016b}'.format(raw_data)[-12:], 2)
            cycles = int('{0:016b}'.format(raw_data)[-16:-12], 2)
            frequency = counter / (cycles * 2**pulse_width / 160e6)

            osc_data['raw_data'] = raw_data
            osc_data['counter'] = counter
            osc_data['frequency'] = frequency

            oscillators['RING_OSC_' + str(i)] = osc_data

        return oscillators

    def get_temperature_sensors(self, samples=1, diode_current=14, log=True):
        sensors = {0: 'TEMPSENS_1', 1: 'TEMPSENS_2', 2: 'TEMPSENS_3', 3: 'TEMPSENS_4'}

        temp = {}
        try:
            for sensor_id, sensor in sensors.items():
                temp[sensor] = {}
                lower, upper = [], []

                for _ in range(samples):
                    bitstring = int('1' + format(diode_current, '04b') + '01' + format(diode_current, '04b') + '0', 2)
                    self.registers['SENSOR_CONFIG_0'].write(bitstring)
                    self.registers['SENSOR_CONFIG_1'].write(bitstring)
                    self.write_sync_01()
                    lower.append(self.get_ADC_value(sensor, measure='v')[0])

                    bitstring = int('1' + format(diode_current, '04b') + '11' + format(diode_current, '04b') + '1', 2)
                    self.registers['SENSOR_CONFIG_0'].write(bitstring)
                    self.registers['SENSOR_CONFIG_1'].write(bitstring)
                    self.write_sync_01()
                    upper.append(self.get_ADC_value(sensor, measure='v')[0])

                self.log.debug('Sensor: {0}\nLower: {0}\nUpper: {1}'.format(sensor, lower, upper))
                temp[sensor]['dADC'] = int(np.mean(upper) - np.mean(lower))
                temp[sensor]['temp'] = self.calibration.get_temperature_from_ADC(temp[sensor]['dADC'], sensor=sensor_id)

            mean_temp = round(np.mean([val['temp'] for val in temp.values()]), 0)
            t_error = 3 if self.calibration.t_sensors_calibrated else 6
            if log:
                self.log.info('Mean chip temperature is ({0} +- {1})°C'.format(int(mean_temp), t_error))

        except RuntimeError as e:
            temp = {0: -99, 1: -99, 2: -99, 3: -99}
            mean_temp = -99
            self.log.error('There was an error reading temperature sensors: %s' % e)
        return mean_temp, temp

    def get_temperature_NTC(self, log=True):
        T = self.bdaq.get_temperature_NTC(self.receiver)
        if log:
            self.log.info('NTC temperature is {0:1.2f}°C'.format(T))
        return T

    def get_temperature(self, log=True):
        if self.bdaq.enable_NTC:
            return self.get_temperature_NTC(log=log)
        else:
            return self.get_temperature_sensors(log=log)[0]

    def _az_setup(self, delay, repeat, width=6, synch=1):
        '''
            Auto-zeroing configuration for the synchronous front-end

            Parameters:
            ----------
                dalay: AZ command distance in [us]
                width: global pulse width (default: 6)
                repeat: How many times. 0 = infinite
                synch >= 0  for width<6  (the = value is the minimum amount of sync needed to correctly mask the HitOr. With more it still works but with higher dead-time
                synch >= 1  for width=6
                      >= 5  for width=7
                      >= 13 for width=8
                      >= 29 for width=9
        '''

        # WORKAROUND: FW AZ pulse needs to be sent to all connected chips. Since we only have one AZ pulser in FW, use broadcast chip id for these commands.
        original_id = self.chip_id
        self.chip_id = 8

        self.bdaq['pulse_gen_az'].reset()
        if self.bdaq['pulse_gen_az'].is_done():
            self.log.info('Setting auto-zeroing period to %s us' % delay)
            self.bdaq['pulse_gen_az'].set_delay(delay * 160)
            self.bdaq['pulse_gen_az'].set_width(1)
            self.bdaq['pulse_gen_az'].set_repeat(repeat)
        else:
            while not self.self.bdaq['pulse_gen_az'].is_done():
                self.log.warning('Auto-zeroing pluse generator is busy. Waiting...')
                time.sleep(0.1)

        indata = self.registers['EN_CORE_COL_SYNC'].get_write_command(0)
        indata += self.registers['GLOBAL_PULSE_ROUTE'].get_write_command(0b0100000000000000)
        indata += self.write_global_pulse(width=width, write=False)     # Send AZ, pulse width should be ~400 ns (2^n * 6.25 ns)
        indata += self.write_sync_01(write=False) * synch
        indata += self.registers['EN_CORE_COL_SYNC'].get_write_command(0xffff)
        indata += self.write_ecr(write=False)

        self.write_command(indata)

        self.chip_id = original_id  # Reset chip id to correct value from before

        self.bdaq['cmd'].set_az_veto_cycles(len(indata) * 8)

        return indata

    def _az_start(self):
        ''' Software-controlled start '''
        self.log.debug('Starting auto-zeroing')
        self.bdaq['pulse_gen_az'].start()

    def _az_stop(self):
        ''' Software-controlled stop '''
        self.log.debug('Stoping auto-zeroing')
        self.bdaq['pulse_gen_az'].reset()

    def get_flavor(self, col):
        return get_flavor(col)

    def get_tdac_range(self, fe):
        return get_tdac_range(fe)

    def configure_ptot(self):
        raise NotImplementedError('RD53A has no PTOT mode!')

    def write_trigger_latency(self, latency):
        self.registers['LATENCY_CONFIG'].write(latency)

    def get_trigger_latency(self):
        return self.registers['LATENCY_CONFIG'].get()


if __name__ == '__main__':
    rd53a_chip = RD53A()
    rd53a_chip.init()
