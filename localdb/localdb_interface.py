# LocalDB interface for BDAQ53
# To be used with the localDB Docker stack provided by BDAQ53.
# Also setups the database that is usually not needed when using the CLI

import os
import time
import socket
import math
import logging
import yaml
import json
import hashlib

import pymongo
import gridfs

import numpy as np
import tables as tb
from bson import ObjectId
from datetime import datetime

from bdaq53.system import logger
from bdaq53.system.scan_base import fill_dict_from_conf_table

from bdaq53.analysis import analysis_utils as au
from bdaq53.manage_configuration import convert_config_bdaq_to_yarr

bdaq53_localdb_path = os.path.dirname(__file__)

LOCALDB_HOST = "127.0.0.1"
LOCALDB_PORT = 27017  # default mongodb port
DB_VERSION = 1.01


class LocalDBError(Exception):
    pass


class DBNotInitializedError(Exception):
    pass


class UploadDataError(Exception):
    pass


class ScanExistsError(Exception):
    pass


class DBReset(Exception):
    pass


class LocalDBinterface(object):
    def __init__(self, host, port, debug=False, dummy=False, _reset_db=None):
        self.debug = debug
        self.dummy = dummy

        self.db_initialized = False
        self.log = logger.setup_derived_logger("LocalDBinterface", level=logging.DEBUG if debug else logging.INFO)
        self._setup_connection(host, port, _reset_db=_reset_db)

        if self.dummy:
            self.log.warning("Running in dummy mode! Nothing will be written do localDB.")

    def _setup_connection(self, host, port, _reset_db=None):
        self.host = host
        self.port = port
        client = pymongo.MongoClient(host=host, port=port, serverSelectionTimeoutMS=3000)
        self.log.debug(client.server_info())
        self.log.success("Connected to localDB at {0}:{1}".format(host, port))

        self.DB_localdb = client["localdb"]
        self.DB_localdbtools = client["localdbtools"]
        self.FS_localdb = gridfs.GridFS(self.DB_localdb)

        # Reset all data if _reset_db flag
        if _reset_db is not None:
            self._reset_database(_reset_db)

        # Check if DB is initialized
        dbs = client.list_database_names()
        self.log.debug("Available databases:\n{}".format(dbs))
        if "localdb" not in dbs:
            self.log.error("DB 'localdb' not found!")
            # Raise exception, since this requires user intervention
            raise LocalDBError("Please download institution and module types info from the ITkPD via the viewer application GUI (Admin page)!")

        collections = self.DB_localdb.list_collection_names()
        self.log.debug("Available collections in DB 'localdb':\n{}".format(collections))
        tools_collections = self.DB_localdbtools.list_collection_names()
        self.log.debug("Available collections in DB 'localdbtools':\n{}".format(tools_collections))

        if not ("institution" in collections and "pd.institution" in collections):
            self.log.error("Collection 'institutions' not found!")
            # Raise exception, since this requires user intervention
            raise LocalDBError("Please download institution and module types info from the ITkPD via the viewer application GUI (Admin page)!")

        # Check for everything else except for fs.chunks
        # if 'chip' not in collections or 'component' not in collections or 'componentTestRun' not in collections or 'config' not in collections or 'fs.files' not in collections or 'testRun' not in collections or 'user' not in collections:
        if "component" not in collections or "componentTestRun" not in collections or "fs.files" not in collections or "testRun" not in collections:
            self.log.warning("localDB instance is not yet initialized!")
        else:
            self.db_initialized = True

    def init(self, user=None, institution=None):
        """
            Initialize localDB if necessary.
            Define self.institution and self.user variables from provided parameters or config file created by 'setup_localdb.sh'.
        """

        # If DB not initialized, create necessary collections and indexes
        if not self.db_initialized:
            raise DBNotInitializedError('Not initialized. Run "bdaq_localdb --init" ')

        # Load local configuration
        try:
            with open(os.path.join(bdaq53_localdb_path, ".localdb.yaml")) as c:
                config = yaml.safe_load(c)
        except FileNotFoundError:
            config = None

        # Define self.institution and self.user
        if institution is not None:
            self.institution = self._get_institution(institution)
        elif config is not None:
            self.institution = self._get_institution(config["user"]["institution"])
        else:
            raise RuntimeError("No institution defined!")
        self.log.debug("Selected institution:\n{}".format(self.institution))

        if user is not None:
            self.user = self._get_user(user)
        elif config is not None:
            self.user = self._get_user(config["user"]["name"])
        else:
            raise RuntimeError("No user defined!")
        self.log.debug("Selected user:\n{}".format(self.user))

    def _reset_database(self, mode):
        """ Drop all non-default information from the database. Recommended only for debugging. """

        self.log.critical("CAREFUL, this will drop all information from localDB in 3s...")
        self.log.critical("3...")
        time.sleep(1)
        self.log.critical("2...")
        time.sleep(1)
        self.log.critical("1...")
        time.sleep(1)

        if mode in ["soft", "data"]:
            self.DB_localdb["fs.files"].drop()
            self.DB_localdb["fs.chunks"].drop()
            self.DB_localdb["chip"].remove({})
            self.DB_localdb["config"].remove({})
            self.DB_localdb["componentTestRun"].remove({})
            self.DB_localdb["testRun"].remove({})
            self.DB_localdbtools["viewer.query"].remove({})
            self._write_index(self.DB_localdbtools["viewer.query"], {"runId": "config", "timeStamp": datetime.utcnow()})
        elif mode in ["hard", "all"]:
            client = pymongo.MongoClient(host=self.host, port=self.port, serverSelectionTimeoutMS=3000)
            client.drop_database("localdb")

            self.DB_localdbtools["QC.module.types"].drop()
            self.DB_localdbtools["QC.status"].drop()
            self.DB_localdbtools["viewer.query"].remove({})

            self._write_index(self.DB_localdbtools["viewer.query"], {"runId": "config", "timeStamp": datetime.utcnow()})

        raise DBReset("LocalDB was reset to default values!")

    def _get_institution(self, code):
        """ Return institution object from localDB for given institution code """

        if "institution" not in self.DB_localdb.list_collection_names():
            # No institution collection defined -> Database is not yet initialized!
            raise DBNotInitializedError("No institutions defined in the localDB yet. Make sure to download the list of institutions from the PDB via the GUI.")

        for institution in self.DB_localdb.institution.find():  # List all existing institutions
            try:
                if institution["code"] == code:
                    self.log.debug("Institution {} found in localDB.".format(code))
                    return institution
            except KeyError:  # There is no code if unknown institution uploaded through YARR?!
                pass
        else:  # Institution code does not exist
            raise LocalDBError("Institution not found: {}".format(code))

    def _get_user(self, name):
        """
            Return user object from localDB for given user name.
            Create a fresh user object if user not yet defined in localDB.
        """

        fresh_user = {"userName": name, "USER": name, "institution": self.institution["code"], "description": "default", "HOSTNAME": socket.getfqdn(), "userType": "readWrite"}

        userlist = list(self.DB_localdb.user.find())
        self.log.debug("Available users in DB 'localdb':\n{}".format(userlist))
        for user in userlist:  # List all existing users
            # Check if user name from local config file already exists...
            if user["userName"] == name or user["USER"] == name:
                self.log.debug("User {} found in localDB.".format(name))
                return user
        else:  # Create new user
            self.log.warning("Username '{}' not found in localDB. Creating new user...".format(name))
            oid = self._write_index(self.DB_localdb.user, fresh_user)
            return self.DB_localdb.user.find_one({"_id": ObjectId(oid)})

    def _write_index(self, collection, data):
        if self.dummy:
            return "123"

        def _update_sys(data):
            if "sys" not in data.keys():
                data["sys"] = {"cts": datetime.utcnow(), "mts": datetime.utcnow(), "rev": 0}
            else:
                data["sys"]["mts"] = datetime.utcnow()
                data["sys"]["rev"] += 1
            return data

        data = _update_sys(data)
        if "dbVersion" not in data.keys():
            data["dbVersion"] = DB_VERSION

        if "_id" in data.keys():
            result = collection.replace_one({"_id": data["_id"]}, data)
            if result.modified_count == 1:
                return data["_id"]
            else:
                raise
        else:
            return str(collection.insert_one(data).inserted_id)

    def _put_json_as_file(self, data, filename):
        if self.dummy:
            return "123"

        binary = json.dumps(data, indent=4).encode("utf-8")
        sha = hashlib.sha256(binary).hexdigest()
        return str(self.FS_localdb.put(binary, filename=filename, dbVersion=DB_VERSION, hash=sha))

    def _put_string_as_file(self, data, filename):
        if self.dummy:
            return "123"

        binary = data.encode("utf-8")
        sha = hashlib.sha256(binary).hexdigest()
        return str(self.FS_localdb.put(binary, filename=filename, dbVersion=DB_VERSION, hash=sha))

    def upload_scan_for_chip(self, in_file):
        """
            Upload a scan to the localDB based on _interpreted.h5 file.

            Parameters:
            ----------
            in_file : str / path-like
                Path to the '_interpreted.h5' file of the scan to upload
        """

        implemented_scans = ["digital_scan", "analog_scan", "threshold_scan", "global_threshold_tuning", "local_threshold_tuning"]

        data = {}

        def _load_chip_config(node):
            chip_config = au.ConfigDict()
            chip_config["settings"] = fill_dict_from_conf_table(node.settings)
            chip_config["calibration"] = fill_dict_from_conf_table(node.calibration)
            chip_config["trim"] = fill_dict_from_conf_table(node.trim)
            chip_config["registers"] = fill_dict_from_conf_table(node.registers)
            chip_config["module"] = fill_dict_from_conf_table(node.module)
            chip_config["use_pixel"] = node.use_pixel[:]
            chip_config["masks"] = {}
            for n in node.masks:
                chip_config["masks"][n.name] = n[:]
            return chip_config

        raw_data_file = in_file.replace("_interpreted", "")
        interpreted_data_file = raw_data_file.replace(".h5", "_interpreted.h5")

        with tb.open_file(raw_data_file, "r") as f:
            chip_config_in = _load_chip_config(node=f.root.configuration_in.chip)

        # Read configuration objects from interpreted h5 file
        bench_config, scan_config = au.ConfigDict(), au.ConfigDict()
        with tb.open_file(interpreted_data_file, "r") as f:
            configuration_out = f.root.configuration_out
            for node in configuration_out.bench._f_list_nodes():
                bench_config[node.name] = fill_dict_from_conf_table(node)
            for node in configuration_out.scan._f_list_nodes():
                scan_config[node.name] = fill_dict_from_conf_table(node)

            chip_config = _load_chip_config(node=f.root.configuration_out.chip)

            # Read scan data according to scan_id
            run_name = scan_config["run_config"]["run_name"]
            scan_id = scan_config["run_config"]["scan_id"]
            timestamp = "_".join(scan_config["run_config"]["run_name"].split("_")[:2])

            if scan_id not in implemented_scans:
                raise NotImplementedError("LocalDB upload for {} is not yet implemented!".format(scan_id))

            # Basic data for all scans
            if scan_id in ["digital_scan", "analog_scan", "threshold_scan"]:
                HistRelBCID = f.root.HistRelBCID[:]
                HistOcc = f.root.HistOcc[:]
                # HistTot = f.root.HistTot[:]
            if scan_id in ["threshold_scan"]:
                ThresholdMap = f.root.ThresholdMap[:]
                NoiseMap = f.root.NoiseMap[:]
                Chi2Map = f.root.Chi2Map[:]

        def _convert_to_e(dac, use_offset=True):
            if use_offset:
                e = dac * chip_config["calibration"]["e_conversion_slope"] + chip_config["calibration"]["e_conversion_offset"]
                de = math.sqrt((dac * chip_config["calibration"]["e_conversion_slope_error"]) ** 2 + chip_config["calibration"]["e_conversion_offset_error"] ** 2)
            else:
                e = dac * chip_config["calibration"]["e_conversion_slope"]
                de = dac * chip_config["calibration"]["e_conversion_slope_error"]
            return e, de

        # Check if scan_id is already in DB
        run_data = self.DB_localdb.testRun.find_one({"exec": run_name})
        if run_data is not None:
            self.log.info("testRun {} already exists in localDB! Adding this scan to the existing testRun".format(run_name))
            testRun = str(run_data["_id"])

            component_run_data = self.DB_localdb.componentTestRun.find_one({"testRun": testRun, "name": chip_config["settings"]["chip_sn"]})
            if component_run_data is not None:
                self.log.error("componentTestRun {0} for chip {1} already exists in localDB!".format(run_name, chip_config["settings"]["chip_sn"]))
                if not self.dummy:
                    raise ScanExistsError("componentTestRun {0} for chip {1} already exists in localDB!".format(run_name, chip_config["settings"]["chip_sn"]))
        else:
            testRun = None

        self.log.info("Preparing upload for {}...".format(run_name))

        # Prepare chip data -> localdb.chip

        # Check if chip already in DB...
        query = {"name": chip_config["settings"]["chip_sn"], "chipType": scan_config["run_config"]["chip_type"].upper(), "componentType": "front-end_chip"}
        chip_data = self.DB_localdb.chip.find_one(query)

        # If not, prepare new object
        if chip_data is None:
            chip_data = {"name": chip_config["settings"]["chip_sn"], "chipId": chip_config["settings"]["chip_id"], "chipType": scan_config["run_config"]["chip_type"].upper(), "componentType": "front-end_chip"}

        self.log.debug("Chip data:\n{}".format(chip_data))
        oid_chip = self._write_index(self.DB_localdb.chip, chip_data)

        # Prepare and upload files -> localdb.fs.files, localdb.fs.chunks

        if scan_id in ["digital_scan", "analog_scan", "threshold_scan"]:
            # Enable mask map
            enable_mask = chip_config["use_pixel"]
            enable_mask[: scan_config["scan_config"]["start_column"], :] = False
            enable_mask[scan_config["scan_config"]["stop_column"] :, :] = False
            enable_mask[:, : scan_config["scan_config"]["start_row"]] = False
            enable_mask[:, scan_config["scan_config"]["stop_row"] :] = False

            enmask = {"Data": enable_mask.astype(float).tolist(), "Name": "EnMask", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0, "x": {"AxisTitle": "Column", "Bins": 400, "High": 400.5, "Low": 0.5}, "y": {"AxisTitle": "Row", "Bins": 192, "High": 192.5, "Low": 0.5}, "z": {"AxisTitle": "Enable"}}  # FIXME: Hardcoded for RD53A  # FIXME: Hardcoded for RD53A  # FIXME: Hardcoded for RD53A  # FIXME: Hardcoded for RD53A
            oid_fs_EnMask = self._put_json_as_file(enmask, filename="EnMask.json")
            if self.debug:
                with open("debug_EnMask.json", "w") as f:
                    json.dump(enmask, f, indent=4)

            # Occupancy map
            occ_data = HistOcc.sum(axis=2).astype(float).tolist()
            occ = {"Data": occ_data, "Name": "OccupancyMap", "Overflow": 0.0, "Type": "Histo2d", "Underflow": 0.0, "x": {"AxisTitle": "Column", "Bins": 400, "High": 400.5, "Low": 0.5}, "y": {"AxisTitle": "Row", "Bins": 192, "High": 192.5, "Low": 0.5}, "z": {"AxisTitle": "Hits"}}  # FIXME: Hardcoded for RD53A  # FIXME: Hardcoded for RD53A  # FIXME: Hardcoded for RD53A  # FIXME: Hardcoded for RD53A
            oid_fs_OccupancyMap = self._put_json_as_file(occ, filename="OccupancyMap.json")
            if self.debug:
                with open("debug_OccupancyMap.json", "w") as f:
                    json.dump(occ, f, indent=4)

            # Relative BCID histogram
            bcid = {"Data": HistRelBCID.sum(axis=(0, 1, 2)).astype(float).tolist(), "Name": "L1Dist", "Overflow": 15.0, "Type": "Histo1d", "Underflow": 0.0, "x": {"AxisTitle": "L1Id", "Bins": 32, "High": 31.5, "Low": -0.5}, "y": {"AxisTitle": "Hits"}, "z": {"AxisTitle": "z"}}  # FIXME: Hardcoded trigger window  # FIXME: Hardcoded trigger window
            oid_fs_L1Dist = self._put_json_as_file(bcid, filename="L1Dist.json")
            if self.debug:
                with open("debug_L1Dist.json", "w") as f:
                    json.dump(bcid, f, indent=4)

        if scan_id in ["threshold_scan"]:
            # Threshold distribution
            plot_range = [v - scan_config["scan_config"]["VCAL_MED"] for v in range(scan_config["scan_config"]["VCAL_HIGH_start"], scan_config["scan_config"]["VCAL_HIGH_stop"] + 1, scan_config["scan_config"]["VCAL_HIGH_step"])]
            hist, bins = np.histogram(np.ravel(ThresholdMap[enable_mask]), bins=plot_range)

            plot_range = [_convert_to_e(v)[0] for v in plot_range]

            contents = "Histo1d\nThresholdDist-0\nThreshold [e]\nNumber of Pixels\nz\n{nbins} {lower_limit} {upper_limit}\n2 16\n{data}".format(nbins=len(bins) - 1, lower_limit=plot_range[0], upper_limit=plot_range[-1], data=" ".join(hist.astype(str).tolist()))

            oid_fs_ThresholdDist_0 = self._put_string_as_file(contents, filename="ThresholdDist-0.dat")
            if self.debug:
                with open("debug_ThresholdDist-0.dat", "w") as f:
                    f.write(contents)

            # Threshold map
            contents = "Histo2d\nThresholdMap-0\nColumn\nRow\nThreshold [e]\n400 0.5 400.5\n192 0.5 192.5\n0 0\n"  # FIXME: Hardcoded for RD53A
            for row in range(192):
                for col in range(400):
                    contents += " " + str(_convert_to_e(ThresholdMap[col, row])[0])
                contents += "\n"

            oid_fs_ThresholdMap_0 = self._put_string_as_file(contents, filename="ThresholdMap-0.dat")
            if self.debug:
                with open("debug_ThresholdMap-0.dat", "w") as f:
                    f.write(contents)

            # Noise distribution
            data = NoiseMap[enable_mask]
            diff = np.amax(data) - np.amin(data)
            plot_range = np.arange(np.amin(data), np.amax(data) + diff / 100.0, diff / 100.0)
            hist, bins = np.histogram(np.ravel(data), bins=plot_range)

            plot_range = [_convert_to_e(v, use_offset=False)[0] for v in plot_range]

            contents = "Histo1d\nNoiseDist-0\nNoise [e]\nNumber of Pixels\nz\n{nbins} {lower_limit} {upper_limit}\n2 16\n{data}".format(nbins=len(bins) - 1, lower_limit=plot_range[0], upper_limit=plot_range[1], data=" ".join(hist.astype(str).tolist()))

            oid_fs_NoiseDist_0 = self._put_string_as_file(contents, filename="NoiseDist-0.dat")
            if self.debug:
                with open("debug_NoiseDist-0.dat", "w") as f:
                    f.write(contents)

            # Noise map
            contents = "Histo2d\nNoiseMap-0\nColumn\nRow\nNoise [e]\n400 0.5 400.5\n192 0.5 192.5\n0 0\n"  # FIXME: Hardcoded for RD53A
            for row in range(192):
                for col in range(400):
                    contents += " " + str(_convert_to_e(NoiseMap[col, row], use_offset=False)[0])
                contents += "\n"

            oid_fs_NoiseMap_0 = self._put_string_as_file(contents, filename="NoiseMap-0.dat")
            if self.debug:
                with open("debug_NoiseMap-0.dat", "w") as f:
                    f.write(contents)

            # Chi2 distribution
            data = Chi2Map[enable_mask]
            diff = np.amax(data) - np.amin(data)
            plot_range = np.arange(np.amin(data), np.amax(data) + diff / 100.0, diff / 100.0)
            hist, bins = np.histogram(np.ravel(data), bins=plot_range)

            contents = "Histo1d\nChi2Dist-0\nFit Chi2 / ndf\nNumber of Pixels\nz\n{nbins} {lower_limit} {upper_limit}\n2 16\n{data}".format(nbins=len(bins) - 1, lower_limit=plot_range[0], upper_limit=plot_range[1], data=" ".join(hist.astype(str).tolist()))

            oid_fs_Chi2Dist_0 = self._put_string_as_file(contents, filename="Chi2Dist-0.dat")
            if self.debug:
                with open("debug_Chi2Dist-0.dat", "w") as f:
                    f.write(contents)

            # Chi2 map
            contents = "Histo2d\nChi2Map-0\nColumn\nRow\nFit Chi2/ndf\n400 0.5 400.5\n192 0.5 192.5\n0 0\n"  # FIXME: Hardcoded for RD53A
            for row in range(192):
                for col in range(400):
                    contents += " " + str(Chi2Map[col, row])
                contents += "\n"

            oid_fs_Chi2Map_0 = self._put_string_as_file(contents, filename="Chi2Map-0.dat")
            if self.debug:
                with open("debug_Chi2Map-0.dat", "w") as f:
                    f.write(contents)

            # Scurve plot
            plot_range = [v - scan_config["scan_config"]["VCAL_MED"] for v in range(scan_config["scan_config"]["VCAL_HIGH_start"], scan_config["scan_config"]["VCAL_HIGH_stop"] + 1, scan_config["scan_config"]["VCAL_HIGH_step"])]

            data = []
            for i in range(len(plot_range) - 1):
                scan_param_data = [v[i] for v in HistOcc[enable_mask]]
                hist, bins = np.histogram(scan_param_data, range(101))
                data.append(hist.tolist())

            scurve_0_json = {"Data": data, "Name": "sCurve-0", "Type": "Histo2d", "Underflow": 0.0, "Overflow": 0.0, "x": {"AxisTitle": "Vcal", "Bins": len(plot_range) - 1, "High": plot_range[-1] - 0.5, "Low": plot_range[0] - 0.5}, "y": {"AxisTitle": "Occupancy", "Bins": 100, "High": 100.5, "Low": 0.5}, "z": {"AxisTitle": "Number of pixels"}}

            oid_fs_sCurve_0 = self._put_json_as_file(scurve_0_json, filename="sCurve-0.json")
            if self.debug:
                with open("debug_sCurve-0.json", "w") as f:
                    json.dump(scurve_0_json, f, indent=4)

        # Prepare config data -> localdb.config and config files -> localdb.fs.files, localdb.fs.chunks

        # ctrlCfg.json
        # This file contains no relevant information for BDAQ53. Use hardcoded example.
        ctrlCfg_json = {"ctrlCfg": {"cfg": {"cmdPeriod": 6.250000073038109e-09, "idle": {"word": 1768515945}, "pulse": {"interval": 500, "word": 1549575846}, "rxPolarity": 0, "specNum": 0, "spiConfig": 541200, "sync": {"interval": 32, "word": 2172551550}, "trigConfig": None, "txPolarity": 0}, "type": "spec"}}
        oid_fs_ctrlCfg = self._put_json_as_file(ctrlCfg_json, "ctrlCfg.json")
        config_ctrlCfg = {"filename": "ctrlCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "ctrlCfg", "format": "fs.files", "data_id": oid_fs_ctrlCfg}
        oid_ctrlCfg = self._write_index(self.DB_localdb.config, config_ctrlCfg)
        if self.debug:
            with open("debug_ctrlCfg.json", "w") as f:
                json.dump(ctrlCfg_json, f, indent=4)

        # dbCfg.json
        # Use mostly hardcoded info for now.
        dbCfg_json = {"QC": False, "auth": "default", "dbName": "localdb", "environment": ["vddd_voltage", "vddd_current", "vdda_voltage", "vdda_current", "vddcom_voltage", "vddcom_current", "hv_voltage", "hv_current", "temperature"], "hostIp": self.host, "hostPort": self.port, "ssl": {"CAFile": "null", "PEMKeyFile": "null", "enabled": False}, "tls": {"CAFile": "null", "CertificateKeyFile": "null", "enabled": False}}
        oid_fs_dbCfg = self._put_json_as_file(dbCfg_json, "dbCfg.json")
        config_dbCfg = {"filename": "dbCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "dbCfg", "format": "fs.files", "data_id": oid_fs_dbCfg}
        oid_dbCfg = self._write_index(self.DB_localdb.config, config_dbCfg)
        if self.debug:
            with open("debug_dbCfg.json", "w") as f:
                json.dump(dbCfg_json, f, indent=4)

        # siteCfg.json
        siteCfg_json = {"insitution": self.institution["institution"]}
        oid_fs_siteCfg = self._put_json_as_file(siteCfg_json, "siteCfg.json")
        config_siteCfg = {"filename": "siteCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "siteCfg", "format": "fs.files", "data_id": oid_fs_siteCfg}
        oid_siteCfg = self._write_index(self.DB_localdb.config, config_siteCfg)
        if self.debug:
            with open("debug_siteCfg.json", "w") as f:
                json.dump(siteCfg_json, f, indent=4)

        # userCfg.json
        userCfg_json = {"description": "default", "institution": self.institution["code"], "userName": self.user["userName"]}
        oid_fs_userCfg = self._put_json_as_file(userCfg_json, "userCfg.json")
        config_userCfg = {"filename": "userCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "userCfg", "format": "fs.files", "data_id": oid_fs_userCfg}
        oid_userCfg = self._write_index(self.DB_localdb.config, config_userCfg)
        if self.debug:
            with open("debug_userCfg.json", "w") as f:
                json.dump(userCfg_json, f, indent=4)

        # chipCfg.json (beforeCfg)
        chipCfg_json = convert_config_bdaq_to_yarr(chip_config_in)
        oid_fs_chipCfg = self._put_json_as_file(chipCfg_json, "chipCfg.json")
        config_chipCfg = {"filename": "chipCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "beforeCfg", "format": "fs.files", "data_id": oid_fs_chipCfg}
        oid_chipCfg_before = self._write_index(self.DB_localdb.config, config_chipCfg)
        if self.debug:
            with open("debug_chipCfg.json", "w") as f:
                json.dump(chipCfg_json, f, indent=4)

        # chipCfg.json (afterCfg)
        # This file is not uploaded (at least for an analog scan). The index in testRun links to the chipCfg.json.before
        config_chipCfg = {"filename": "chipCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "afterCfg", "format": "fs.files", "data_id": oid_fs_chipCfg}
        oid_chipCfg_after = self._write_index(self.DB_localdb.config, config_chipCfg)

        # scanCfg
        # TODO: Partially hardcoded for analog scan...
        # TODO: Define fully custom BDAQ fromat to be able to reproduce with BDAQ?
        scanCfg_json = {"scan": {"histogrammer": {"0": {"algorithm": "OccupancyMap", "config": {}}, "1": {"algorithm": "TotMap", "config": {}}, "2": {"algorithm": "Tot2Map", "config": {}}, "3": {"algorithm": "L1Dist", "config": {}}, "4": {"algorithm": "HitsPerEvent", "config": {}}, "n_count": 5}, "loops": [{"config": {"max": 64, "min": 0, "step": 1}, "loopAction": "Rd53aMaskLoop"}, {"config": {"max": 50, "min": 0, "step": 1, "nSteps": 5}, "loopAction": "Rd53aCoreColLoop"}, {"config": {"count": 100, "delay": 48, "extTrigger": False, "frequency": 30000, "noInject": False, "time": 0}, "loopAction": "Rd53aTriggerLoop"}, {"loopAction": "StdDataLoop"}], "name": scan_id, "prescan": {"InjEnDig": 0, "LatencyConfig": 48, "GlobalPulseRt": chip_config_in["registers"]["GLOBAL_PULSE_ROUTE"]}}}
        if scan_id in ["analog_scan"]:
            scanCfg_json["scan"].update({"analysis": {"0": {"algorithm": "OccupancyAnalysis", "config": {"createMask": True}}, "1": {"algorithm": "L1Analysis"}, "n_count": 2}})
            scanCfg_json["scan"].update({"prescan": {"InjVcalDiff": 0, "InjVcalHigh": scan_config["scan_config"]["VCAL_HIGH"], "InjVcalMed": scan_config["scan_config"]["VCAL_MED"]}})
        if scan_id in ["threshold_scan"]:
            scanCfg_json["scan"].update({"analysis": {"0": {"algorithm": "ScurveFitter"}, "1": {"algorithm": "OccupancyAnalysis"}, "n_count": 2}})
            scanCfg_json["scan"].update({"loops": {"config": {"max": scan_config["scan_config"]["VCAL_HIGH_stop"] - scan_config["scan_config"]["VCAL_MED"], "min": scan_config["scan_config"]["VCAL_HIGH_start"] - scan_config["scan_config"]["VCAL_MED"], "step": scan_config["scan_config"]["VCAL_HIGH_step"], "parameter": "InjVcalDiff"}, "loopAction": "Rd53aParameterLoop"}})
            scanCfg_json["scan"].update({"prescan": {"InjVcalDiff": 0, "InjVcalHigh": 1000, "InjVcalMed": 1000}})
        oid_fs_scanCfg = self._put_json_as_file(scanCfg_json, "scanCfg.json")
        config_scanCfg = {"filename": "scanCfg.json", "chipType": scan_config["run_config"]["chip_type"].upper(), "title": "scanCfg", "format": "fs.files", "data_id": oid_fs_scanCfg}
        oid_scanCfg = self._write_index(self.DB_localdb.config, config_scanCfg)
        if self.debug:
            with open("debug_scanCfg.json", "w") as f:
                json.dump(scanCfg_json, f, indent=4)

        ### Prepare run data -> localdb.testRun
        if testRun is not None:
            oid_testRun = testRun
        else:
            run_data = {"exec": run_name, "runNumber": 1, "stopwatch": {"analysis": 0, "config": 0, "processing": 0, "scan": 0}, "testType": scan_config["run_config"]["scan_id"], "timestamp": timestamp, "targetCharge": -1, "targetTot": -1, "QC": False, "stage": "...", "chipType": scan_config["run_config"]["chip_type"].upper(), "address": str(self.institution["_id"]), "user_id": str(self.user["_id"]), "environment": False, "passed": True, "qcTest": False, "qaTest": False, "summary": False, "startTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "finishTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "ctrlCfg": oid_ctrlCfg, "dbCfg": oid_dbCfg, "siteCfg": oid_siteCfg, "userCfg": oid_userCfg, "scanCfg": oid_scanCfg, "plots": []}  # FIXME: What to do with this field?  # FIXME: What to do with this field?  # FIXME: Extract times from log file  # FIXME: What does this mean?  # FIXME: localDB introduces a new index in localdb.institution with no code but address (MAC) and HOSTNAME  # FIXME: What does this mean?  # FIXME: Hardcoded?  # FIXME: Hardcoded?  # FIXME: Hardcoded?  # FIXME: What does this mean?  # FIXME: Extract from log file

            if scan_id in ["digital_scan", "analog_scan"]:
                run_data["plots"].extend(["L1Dist", "OccupancyMap", "EnMask"])
            if scan_id in ["threshold_scan"]:
                run_data["plots"].extend(["ThresholdDist-0", "ThresholdMap-0", "NoiseDist-0", "NoiseMap-0", "Chi2Dist-0", "Chi2Map-0", "sCurve-0"])
            if scan_id in ["global_threshold_tuning", "local_threshold_tuning"]:
                run_data["targetCharge"] = int(_convert_to_e(scan_config["scan_config"]["VCAL_HIGH"] - scan_config["scan_config"]["VCAL_MED"])[0], 0)

            self.log.debug("testRun data:\n{}".format(run_data))
            oid_testRun = self._write_index(self.DB_localdb.testRun, run_data)

        ### Prepare component test run data -> localdb.componentTestRun

        componentRun_data = {"config": chip_config["settings"]["chip_config_file"], "enable": 1, "locked": 0, "rx": chip_config["settings"]["receiver"], "tx": 0, "name": chip_config["settings"]["chip_sn"], "geomId": chip_config["settings"]["chip_id"], "component": "...", "chip": oid_chip, "testRun": oid_testRun, "environment": "...", "beforeCfg": oid_chipCfg_before, "afterCfg": oid_chipCfg_after}  # TODO: This needs to be tied to the components table, i guess
        if scan_id in ["digital_scan", "analog_scan", "threshold_scan"]:
            componentRun_data.update({"attachments": [{"code": oid_fs_EnMask, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "EnMask", "description": "describe", "contentType": "json", "filename": "EnMask.json"}, {"code": oid_fs_OccupancyMap, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "OccupancyMap", "description": "describe", "contentType": "json", "filename": "OccupancyMap.json"}, {"code": oid_fs_L1Dist, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "L1Dist", "description": "describe", "contentType": "json", "filename": "L1Dist.json"}]})
        if scan_id in ["threshold_scan"]:
            componentRun_data["attachments"].extend(
                [
                    {"code": oid_fs_ThresholdDist_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "ThresholdDist-0", "description": "describe", "contentType": "dat", "filename": "ThresholdDist-0.dat"},
                    {"code": oid_fs_ThresholdMap_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "ThresholdMap-0", "description": "describe", "contentType": "dat", "filename": "ThresholdMap-0.dat"},
                    {"code": oid_fs_NoiseDist_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "NoiseDist-0", "description": "describe", "contentType": "dat", "filename": "NoiseDist-0.dat"},
                    {"code": oid_fs_NoiseMap_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "NoiseMap-0", "description": "describe", "contentType": "dat", "filename": "NoiseMap-0.dat"},
                    {"code": oid_fs_Chi2Dist_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "Chi2Dist-0", "description": "describe", "contentType": "dat", "filename": "Chi2Dist-0.dat"},
                    {"code": oid_fs_Chi2Map_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "Chi2Map-0", "description": "describe", "contentType": "dat", "filename": "Chi2Map-0.dat"},
                    {"code": oid_fs_sCurve_0, "dateTime": datetime.strptime(timestamp, "%Y%m%d_%H%M%S"), "title": "sCurve-0", "description": "describe", "contentType": "json", "filename": "sCurve-0.json"},
                ]
            )
        self.log.debug("componentTestRun data:\n{}".format(componentRun_data))
        oid_componentTestRun = self._write_index(self.DB_localdb.componentTestRun, componentRun_data)

        self.log.success("Successfully uploaded scan {}".format(run_name))
        # chip_sn_atlas = '20UPGFC{0:07d}'.format(int(chip_config['settings']['chip_sn'], 16))

        return oid_componentTestRun, oid_testRun
