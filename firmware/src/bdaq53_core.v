/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute of Bonn University
 * ------------------------------------------------------------
 */

`timescale 1ns / 1ps
`default_nettype wire

`include "cmd_rd53/cmd_rd53.v"
`include "cmd_rd53/cmd_rd53_core.v"

`ifdef _1LANE
    `include "rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b.v"
`else
    `include "rx_aurora/rx_aurora_64b66b.v"
`endif

`ifdef INCLUDE_AURORA_TX
    `include "tx_aurora/tx_aurora.v"
    `include "tx_aurora/rtl/eoc/aurora/AuroraMultilaneTop.sv"
`endif

`include "xadc.v"

// include basil modules
`include "i2c/i2c.v"
`include "i2c/i2c_core.v"
`include "gpio/gpio.v"
`include "gpio/gpio_core.v"
`include "utils/cdc_pulse_sync.v"
`include "utils/cdc_reset_sync.v"
`include "utils/cdc_syncfifo.v"
`include "utils/generic_fifo.v"
`include "utils/pulse_gen_rising.v"
`include "utils/bus_to_ip.v"
`include "utils/ddr_des.v"
`include "rrp_arbiter/rrp_arbiter.v"
`include "includes/log2func.v"
`include "spi/spi.v"
`include "spi/spi_core.v"
`include "spi/blk_mem_gen_8_to_1_2k.v"
`include "utils/CG_MOD_pos.v"
`include "utils/flag_domain_crossing.v"
`include "utils/3_stage_synchronizer.v"

`include "tlu/tlu_controller.v"
`include "tlu/tlu_controller_core.v"
`include "tlu/tlu_controller_fsm.v"

`include "pulse_gen/pulse_gen.v"
`include "pulse_gen/pulse_gen_core.v"

`include "tdc_s3/tdc_s3.v"
`include "tdc_s3/tdc_s3_core.v"


// Aurora RX channels
`ifdef RTL_SIM
    `ifdef _1LANE
        localparam N_AURORA_RX = 7;
    `else
        localparam N_AURORA_RX = 1;
    `endif
`elsif BDAQ53
    `ifdef _1LANE
        `ifdef _KX1
            localparam N_AURORA_RX = 4;  // The KX1 can only handle the multi-lane DP1 port
        `else
            `ifdef 10G
                localparam N_AURORA_RX = 4;  // RX channel reduction due to resource conflicts with the 10G interface
            `else
                localparam N_AURORA_RX = 7;  // The KX2 supports 4 channels on DP1 and 1 channel on DP2..4
            `endif
        `endif
    `else
        localparam N_AURORA_RX = 1;
    `endif
`elsif KC705
    localparam N_AURORA_RX = 1;
`elsif USBPIX3
    localparam N_AURORA_RX = 1;
`else
    localparam N_AURORA_RX = 0;
`endif

// TDC channels
localparam N_TDC_CHANNELS = 4;

// Chips with HitOR
localparam N_HITOR_CHIPS = 2;


module bdaq53_core #(
    // FIRMWARE VERSION
    parameter VERSION_MAJOR = 8'd0,
    parameter VERSION_MINOR = 8'd0,
    parameter VERSION_PATCH = 8'd0
)(
    input wire          BUS_CLK,
    input wire          BUS_RST,
    input wire  [31:0]  BUS_ADD,
    inout wire  [7:0]   BUS_DATA,
    input wire          BUS_RD,
    input wire          BUS_WR,
    output wire         BUS_BYTE_ACCESS,

    // Clocks from oscillators and mux select
    input wire INIT_CLK,
    input wire RX_CLK_IN_P, RX_CLK_IN_N,
    input wire DRP_CLK,
    output wire MGT_REF_SEL,
    output wire REFCLK_OUT,
    output wire AURORA_CLK_OUT,

    // PLL
    input wire CLK_CMD,

    // Aurora lanes
    input wire [7:0] MGT_RX_P, MGT_RX_N,
    output wire TX_P, TX_N,
    
    // Data merging signals
    output wire [3:0] DATA_MERGING_OUTPUT,
    output wire [3:0] DATA_MERGING_ENABLE,
    output wire       AURORA_SER_CLK,
    output wire       AURORA_SER_CLK_OUT,

    // CMD encoder
    input wire EXT_TRIGGER,
    output wire CMD_DATA, CMD_OUT, CMD_WRITING, CMD_OUTPUT_EN,
    output reg BYPASS_MODE = 0,
    output wire BYPASS_MODE_RESET,
    output wire BYPASS_CDR,
    output wire CMD_LOOP_START_PULSE,
    output reg BYPASS_CMD_CLK_EN = 0,
    output reg BYPASS_EXT_SER_CLK_EN = 0,

    // 4 individual HitOr lines (per chip) from the different pixel locations
    input wire [7:0] HITOR,

    // Displayport control signals
    output wire [3:0] GPIO_RESET,
    input wire [3:0] GPIO_SENSE,

    // NTC
    output wire [2:0] NTC_MUX,

    // Debug signals
    output wire [N_AURORA_RX-1:0] RX_LANE_UP,
    output wire [N_AURORA_RX-1:0] RX_CHANNEL_UP,
    output wire [N_AURORA_RX-1:0] PLL_LOCKED,
    output wire DEBUG_TX0, DEBUG_TX1,
    output wire [3:0] RX_MULTILLANE_UP,

    // I2C bus
    inout wire I2C_SDA,
    inout wire I2C_SCL,

    // FIFO control signals (TX FIFO of DAQ)
    output wire [31:0] FIFO_DATA,
    output wire FIFO_WRITE,
    input wire FIFO_EMPTY,
    input wire FIFO_FULL,

    input wire AURORA_RESET,
    output wire [N_AURORA_RX-1:0]RX_SOFT_ERROR,
    output wire [N_AURORA_RX-1:0]RX_HARD_ERROR,
    output wire [3:0] PMOD,

    // TLU
    input wire RJ45_TRIGGER, RJ45_RESET,
    output wire RJ45_BUSY,
    output wire RJ45_CLK,

    // TRIGGER
    input wire LEMO_TRIGGER,
    input wire MULTI_PURPOSE,

    // TDC
    input wire CLK320,
    input wire CLK160,
    input wire CLK40,

    output wire [7:0] LEMO_MUX

/*
    // DDR3 Memory Interface
    output wire [14:0]ddr3_addr,
    output wire [2:0] ddr3_ba,
    output wire       ddr3_cas_n,
    output wire       ddr3_ras_n,
    output wire       ddr3_ck_n, ddr3_ck_p,
    output wire [0:0] ddr3_cke,
    output wire       ddr3_reset_n,
    inout  wire [7:0] ddr3_dq,
    inout  wire       ddr3_dqs_n, ddr3_dqs_p,
    output wire [0:0] ddr3_dm,
    output wire       ddr3_we_n,
    output wire [0:0] ddr3_cs_n,
    output wire [0:0] ddr3_odt,
*/
);


// CLOCKS

wire [1:0] CHIP_TYPE;
// CONNECTOR ID
localparam CON_SMA = 8'd0;
localparam CON_FMC_LPC = 8'd1;
localparam CON_FMC_HPC = 8'd2;
localparam CON_DP = 8'd3;
localparam CON_SIM = 8'd4;

// BOARD ID
localparam SIM = 8'd0;
localparam BDAQ53 = 8'd1;
localparam USBPIX3 = 8'd2;
localparam KC705 = 8'd3;

`ifdef RTL_SIM
    localparam BOARD = SIM;
    localparam CONNECTOR = CON_SIM;
`elsif BDAQ53
    localparam BOARD = BDAQ53;
    localparam CONNECTOR = CON_DP;
`elsif USBPIX3
    localparam BOARD = USBPIX3;
    localparam CONNECTOR = CON_SMA;
`elsif KC705
    localparam BOARD = KC705;
    `ifdef _FMC_LPC
        localparam CONNECTOR = CON_FMC_LPC;
    `elsif _SMA
        localparam CONNECTOR = CON_SMA;
    `endif
`endif

// BOARD OPTIONS
// bit0: 640Mb/s RX, bit1: ...
`ifdef _RX640
    localparam AURORA_RX_640M = 1'b1;
`else
    localparam AURORA_RX_640M = 1'b0;
`endif

// BOARD CONFIGURATION
reg SI570_IS_CONFIGURED = 1'b0;

// VERSION/BOARD READBACK
localparam VERSION = 1; // Module version


// -------  MODULE ADREESSES  ------- //
localparam BDAQ_SYSTEM_BASEADDR = 32'h0000;
localparam BDAQ_SYSTEM_HIGHADDR = 32'h0100;

localparam TLU_BASEADDR = 32'h0600;
localparam TLU_HIGHADDR = 32'h0700-1;

localparam PULSER_TRIG_BASEADDR = 32'h0700;
localparam PULSER_TRIG_HIGHADDR = 32'h0800-1;

localparam PULSER_VETO_BASEADDR = 32'h0800;
localparam PULSER_VETO_HIGHADDR = 32'h0900-1;

localparam I2C_BASEADDR  = 32'h1000;
localparam I2C_HIGHADDR  = 32'h1100-1;

localparam GPIO_DM_BASEADDR = 32'h1e00;
localparam GPIO_DM_HIGHADDR = 32'h1f00-1;

localparam GPIO_XADC_VPVN_BASEADDR = 32'h1f00;
localparam GPIO_XADC_VPVN_HIGHADDR = 32'h2000-1;

localparam GPIO_XADC_FPGA_TEMP_BASEADDR = 32'h2000;
localparam GPIO_XADC_FPGA_TEMP_HIGHADDR = 32'h2100-1;

localparam GPIO_BDAQ_CONTROL_BASEADDR = 32'h2100;
localparam GPIO_BDAQ_CONTROL_HIGHADDR = 32'h2200-1;

localparam SPI_BASEADDR = 32'h2200;
localparam SPI_HIGHADDR = 32'h2300-1;

localparam PULSE_AZ_BASEADDR = 32'h2300;
localparam PULSE_AZ_HIGHADDR = 32'h2400-1;

localparam TDC_BASEADDR = 32'h2400;
localparam TDC_HIGHADDR = 32'h2500-1;

// 3 additional TDC modules are instantiated with base addresses 0h2500 to 0h2700
// Reserved: 32'h2400 ... 32'h2800-1

localparam PULSE_CMD_START_LOOP_BASEADDR = 32'h2800;
localparam PULSE_CMD_START_LOOP_HIGHADDR = 32'h2900-1;

localparam PULSER_EXT_TRIG_BASEADDR = 32'h2900;
localparam PULSER_EXT_TRIG_HIGHADDR = 32'h3000-1;

localparam AURORA_RX_BASEADDR = 32'h6000;
localparam AURORA_RX_HIGHADDR = 32'h6100-1;

// 6 additional Aurora modules can be instantiated with base addresses 0h6000 to 0h6600
// Reserved: 32'h6000 ... 32'h6700-1

localparam CMD_RD53_BASEADDR = 32'h9000;
localparam CMD_RD53_HIGHADDR = 32'hb000-1;

// System config
localparam ABUSWIDTH = 32;
wire BDAQ_SYSTEM_RD, BDAQ_SYSTEM_WR;
wire [ABUSWIDTH-1:0] BDAQ_SYSTEM_ADD;
wire [7:0] BDAQ_SYSTEM_DATA_IN;
reg [7:0] BDAQ_SYSTEM_DATA_OUT;

bus_to_ip #( .BASEADDR(BDAQ_SYSTEM_BASEADDR), .HIGHADDR(BDAQ_SYSTEM_HIGHADDR), .ABUSWIDTH(ABUSWIDTH) ) i_bus_to_ip_bdaq
(
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),

    .IP_RD(BDAQ_SYSTEM_RD),
    .IP_WR(BDAQ_SYSTEM_WR),
    .IP_ADD(BDAQ_SYSTEM_ADD),
    .IP_DATA_IN(BDAQ_SYSTEM_DATA_IN),
    .IP_DATA_OUT(BDAQ_SYSTEM_DATA_OUT)
);

reg [7:0] BUS_DATA_OUT_REG;
always @ (posedge BUS_CLK) begin
    if(BDAQ_SYSTEM_RD) begin
        if(BDAQ_SYSTEM_ADD == 0)
            BDAQ_SYSTEM_DATA_OUT <= VERSION;
        else if(BDAQ_SYSTEM_ADD == 1)
            BDAQ_SYSTEM_DATA_OUT <= VERSION_MINOR;
        else if(BDAQ_SYSTEM_ADD == 2)
            BDAQ_SYSTEM_DATA_OUT <= VERSION_MAJOR;
        else if(BDAQ_SYSTEM_ADD == 3)
            BDAQ_SYSTEM_DATA_OUT <= BOARD;
        else if(BDAQ_SYSTEM_ADD == 4)
            BDAQ_SYSTEM_DATA_OUT <= N_AURORA_RX;
        else if(BDAQ_SYSTEM_ADD == 5)
            BDAQ_SYSTEM_DATA_OUT <= {3'd0, BYPASS_EXT_SER_CLK_EN, BYPASS_CMD_CLK_EN, BYPASS_MODE, SI570_IS_CONFIGURED, AURORA_RX_640M};
        else if(BDAQ_SYSTEM_ADD == 6)
            BDAQ_SYSTEM_DATA_OUT <= CONNECTOR;
        else
            BDAQ_SYSTEM_DATA_OUT <= 0;
    end
end

always @ (posedge BUS_CLK)
    if(BDAQ_SYSTEM_WR) begin
        if(BDAQ_SYSTEM_ADD == 5) begin
            BYPASS_EXT_SER_CLK_EN <= BDAQ_SYSTEM_DATA_IN[4];
            BYPASS_CMD_CLK_EN <= BDAQ_SYSTEM_DATA_IN[3];
            BYPASS_MODE <= BDAQ_SYSTEM_DATA_IN[2];
            SI570_IS_CONFIGURED <= BDAQ_SYSTEM_DATA_IN[1];
        end
    end

// -------  USER MODULES  ------- //

// SPI module to acces external ADC and DAC with clock divider
wire SPI_CLK;
clock_divider #(
    .DIVISOR(4)
) i_clock_divisor_spi (
    .CLK(BUS_CLK),
    .RESET(1'b0),
    .CE(),
    .CLOCK(SPI_CLK)
);

wire SEN_inverted;
assign PMOD[3] = ~SEN_inverted;
spi #(
    .BASEADDR(SPI_BASEADDR),
    .HIGHADDR(SPI_HIGHADDR),
    .ABUSWIDTH(16),
    .MEM_BYTES(2)
) i_spi (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .SPI_CLK(SPI_CLK),

    .SCLK(PMOD[0]),
    .SDO(),
    .SDI(PMOD[2]),
    .EXT_START(),

    .SEN(SEN_inverted),
    .SLD()
);

// GPIO module to access general base-board features
wire [23:0] IO_CONTROL;
wire [2:0] HITOR_COMBINE_EN;
assign HITOR_COMBINE_EN = IO_CONTROL[23:21];
assign MGT_REF_SEL = ~IO_CONTROL[19];   // invert, because the default value '0' should correspond to the internal clock
assign LEMO_MUX = IO_CONTROL[18:11];
assign NTC_MUX = IO_CONTROL[10:8];
assign GPIO_RESET = IO_CONTROL[7:4];
assign IO_CONTROL[3:0] = GPIO_SENSE;

gpio #(
    .BASEADDR(GPIO_BDAQ_CONTROL_BASEADDR),
    .HIGHADDR(GPIO_BDAQ_CONTROL_HIGHADDR),
    .ABUSWIDTH(32),
    .IO_WIDTH(24),
    .IO_DIRECTION(24'hff_fff0)
) i_gpio_control (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO(IO_CONTROL)
);

wire [15:0] MEASURED_FPGA_TEMP;

gpio #(
    .BASEADDR(GPIO_XADC_FPGA_TEMP_BASEADDR),
    .HIGHADDR(GPIO_XADC_FPGA_TEMP_HIGHADDR),
    .ABUSWIDTH(32),
    .IO_WIDTH(16),
    .IO_DIRECTION(16'h0000)
) i_gpio_xadc_fpga_temp (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO(MEASURED_FPGA_TEMP)
);

wire [15:0] MEASURED_VPVN;

gpio #(
    .BASEADDR(GPIO_XADC_VPVN_BASEADDR),
    .HIGHADDR(GPIO_XADC_VPVN_HIGHADDR),
    .ABUSWIDTH(32),
    .IO_WIDTH(16),
    .IO_DIRECTION(16'h0000)
) i_gpio_xadc_vpvn (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO(MEASURED_VPVN)
);

// ------ XADC module for NTC (and FPGA-internal) temperature measurements ------ //
xadc_ug480 i_xadc_ug480(
    .VAUXP(),
    .VAUXN(),
    .RESET(BUS_RST),
    .ALM(),
    .DCLK(BUS_CLK),
    .MEASURED_TEMP(MEASURED_FPGA_TEMP),
    .MEASURED_VPVN(MEASURED_VPVN),
    .MEASURED_VCCINT(),
    .MEASURED_VCCAUX(),
    .MEASURED_VCCBRAM(),
    .MEASURED_AUX0(),
    .MEASURED_AUX1(),
    .MEASURED_AUX2(),
    .MEASURED_AUX3()
);

// ------ I²C module with clock generator ------ //
(* KEEP = "{TRUE}" *)
wire I2C_CLK;

clock_divider #(
    .DIVISOR(1600)
) i_clock_divisor_i2c (
    .CLK(BUS_CLK),
    .RESET(1'b0),
    .CE(),
    .CLOCK(I2C_CLK)
);


localparam I2C_MEM_BYTES = 32;

i2c #(
    .BASEADDR(I2C_BASEADDR),
    .HIGHADDR(I2C_HIGHADDR),
    .ABUSWIDTH(32),
    .MEM_BYTES(I2C_MEM_BYTES)
)  i_i2c (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .I2C_CLK(I2C_CLK),
    .I2C_SDA(I2C_SDA),
    .I2C_SCL(I2C_SCL)
);


// ----- Pulser for trigger command----- //
wire EXT_TRIGGER_PULSE;
wire EXT_START_VETO;
wire EXT_START_PULSE_TRIG;

pulse_gen #(
    .BASEADDR(PULSER_TRIG_BASEADDR),
    .HIGHADDR(PULSER_TRIG_HIGHADDR),
    .ABUSWIDTH(32)
) i_pulse_gen_trig (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(CLK_CMD),
    .EXT_START(EXT_START_PULSE_TRIG  & ~EXT_START_VETO),   // don't send triggers during the az phase (for sync fe)
    .PULSE(EXT_TRIGGER_PULSE)
);


// ----- Pulser for random ext. trigger----- //
wire PULSER_EXT_TRIG;
pulse_gen #(
    .BASEADDR(PULSER_EXT_TRIG_BASEADDR),
    .HIGHADDR(PULSER_EXT_TRIG_HIGHADDR),
    .ABUSWIDTH(32)
) i_pulse_gen_ext_trig (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(CLK_CMD),
    .EXT_START(1'b0),
    .PULSE(PULSER_EXT_TRIG)
);


// ----- Auto-zeroing pulse generator ----- //
wire AZ_PULSE;
pulse_gen
#(
    .BASEADDR(PULSE_AZ_BASEADDR),
    .HIGHADDR(PULSE_AZ_HIGHADDR),
    .ABUSWIDTH(32)
    ) i_pulse_gen_az
(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(BUS_CLK),
    .EXT_START(1'b0),
    .PULSE(AZ_PULSE)
    );


// ----- Command encoder ----- //
wire EXT_START_PIN;
wire CMD_EXT_START_ENABLED;
wire AZ_VETO_FLAG, AZ_VETO_TLU_PULSE;
assign EXT_START_VETO = AZ_VETO_FLAG;   // don't send triggers during the az phase (for sync fe)
wire CMD_LOOP_START;

cmd_rd53 #(
    .BASEADDR(CMD_RD53_BASEADDR),
    .HIGHADDR(CMD_RD53_HIGHADDR),
    .ABUSWIDTH(32)
) i_cmd_rd53 (
    .CHIP_TYPE(CHIP_TYPE),
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .EXT_START_PIN(EXT_START_PIN),
    .EXT_START_ENABLED(CMD_EXT_START_ENABLED),
    .EXT_TRIGGER(EXT_TRIGGER_PULSE), // length of EXT_TRIGGER_PULSE determines how many frames will be read out

    .AZ_PULSE(AZ_PULSE),
    .AZ_VETO_TLU_PULSE(AZ_VETO_TLU_PULSE),
    .AZ_VETO_FLAG(AZ_VETO_FLAG),

    .CMD_WRITING(CMD_WRITING),
    .CMD_LOOP_START(CMD_LOOP_START),
    .CMD_CLK(CLK_CMD),
    .CMD_OUTPUT_EN(CMD_OUTPUT_EN),
    .CMD_SERIAL_OUT(CMD_DATA),
    .CMD_OUT(CMD_OUT),

    .BYPASS_MODE_RESET(BYPASS_MODE_RESET),
    .BYPASS_CDR(BYPASS_CDR)
);


// ----- Arbiter ----- //
wire TLU_FIFO_EMPTY;
wire [N_AURORA_RX-1:0] AURORA_FIFO_EMPTY;
wire TLU_FIFO_PEEMPT_REQ;
wire [N_TDC_CHANNELS-1:0] TDC_FIFO_EMPTY;
wire [N_AURORA_RX-1:0][31:0] AURORA_FIFO_DATA;
wire [31:0] TLU_FIFO_DATA;
wire [31:0] TDC_FIFO_DATA [N_TDC_CHANNELS-1:0];
wire ARB_READY_OUT, ARB_WRITE_OUT;
wire [N_AURORA_RX-1:0] ARB_GRANT_AURORA_RX;
wire ARB_GRANT_TLU_FIFO;
wire [N_TDC_CHANNELS-1:0] ARB_GRANT_TDC_FIFO;
wire [31:0] ARB_DATA_OUT;


// ----- CMD_START_LOOP -> TDC pulse generator ----- //
pulse_gen
#(
    .BASEADDR(PULSE_CMD_START_LOOP_BASEADDR),
    .HIGHADDR(PULSE_CMD_START_LOOP_HIGHADDR),
    .ABUSWIDTH(32)
    ) i_pulse_gen_cmd_start_loop
(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(CLK_CMD),
    .EXT_START(CMD_LOOP_START),
    .PULSE(CMD_LOOP_START_PULSE)
    );


rrp_arbiter #(
    .WIDTH(1 + N_TDC_CHANNELS + N_AURORA_RX)
) i_rrp_arbiter (
    .RST(BUS_RST),
    .CLK(BUS_CLK),
    .WRITE_REQ({
        !TDC_FIFO_EMPTY[3],
        !TDC_FIFO_EMPTY[2],
        !TDC_FIFO_EMPTY[1],
        !TDC_FIFO_EMPTY[0],
        ~AURORA_FIFO_EMPTY,
        !TLU_FIFO_EMPTY}),
    .HOLD_REQ(TLU_FIFO_PEEMPT_REQ),  // wait for writing for given stream (priority)
    .DATA_IN({
        TDC_FIFO_DATA[3],
        TDC_FIFO_DATA[2],
        TDC_FIFO_DATA[1],
        TDC_FIFO_DATA[0],
        AURORA_FIFO_DATA,
        TLU_FIFO_DATA}),  // incoming data for arbitration
    .READ_GRANT({
        ARB_GRANT_TDC_FIFO,
        ARB_GRANT_AURORA_RX,
        ARB_GRANT_TLU_FIFO}),  // indicate to stream that data has been accepted
    .READY_OUT(ARB_READY_OUT),  // indicates ready for outgoing stream (input)
    .WRITE_OUT(ARB_WRITE_OUT),  // indicates will of write to outgoing stream
    .DATA_OUT(ARB_DATA_OUT)  // outgoing data stream
);

assign ARB_READY_OUT = !FIFO_FULL;


// ----- TLU module ----- //
wire TLU_FIFO_READ;
wire TRIGGER_ACKNOWLEDGE_FLAG; // to TLU FSM
wire TRIGGER_ACCEPTED_FLAG;
wire [31:0] TIMESTAMP;
wire TLU_BUSY, TLU_CLOCK;

wire [3:0] LEMO_TRIGGER_FROM_TDC;
wire [3:0] TDC_IN_FROM_TDC;

// Combine all four HITOR lines into one line
wire HITOR_COMBINED_DP_ML_5, HITOR_COMBINED_mP;
assign HITOR_COMBINED_DP_ML_5 = HITOR_COMBINE_EN[0] ? | HITOR[3:0] : 0;
assign HITOR_COMBINED_mP = HITOR_COMBINE_EN[1] ? | (~HITOR[7:4]) : 0;

tlu_controller #(
    .BASEADDR(TLU_BASEADDR),
    .HIGHADDR(TLU_HIGHADDR),
    .DIVISOR(32),
    .ABUSWIDTH(32),
    .WIDTH(10),
    .TLU_TRIGGER_MAX_CLOCK_CYCLES(32)
) i_tlu_controller (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .TRIGGER_CLK(CLK_CMD),  // TLU module operates with 160 MHz command clock

    .FIFO_READ(TLU_FIFO_READ),
    .FIFO_EMPTY(TLU_FIFO_EMPTY),
    .FIFO_DATA(TLU_FIFO_DATA),

    .FIFO_PREEMPT_REQ(TLU_FIFO_PEEMPT_REQ),

    .TRIGGER({PULSER_EXT_TRIG, CMD_LOOP_START, TDC_IN_FROM_TDC, MULTI_PURPOSE, LEMO_TRIGGER_FROM_TDC[0], HITOR_COMBINED_mP, HITOR_COMBINED_DP_ML_5}),  // HITOR (TDC loop-through), RX1, RX0 (TDC loop-through), HITOR (mDP), HITOR (DP)
    .TRIGGER_VETO({8'b0, AZ_VETO_FLAG, FIFO_FULL}),

    .TRIGGER_ACKNOWLEDGE(TRIGGER_ACKNOWLEDGE_FLAG),
    .TRIGGER_ACCEPTED_FLAG(TRIGGER_ACCEPTED_FLAG),
    .EXT_TRIGGER_ENABLE(CMD_EXT_START_ENABLED),

    .TLU_TRIGGER(RJ45_TRIGGER),
    .TLU_RESET(RJ45_RESET),
    .TLU_BUSY(TLU_BUSY),
    .TLU_CLOCK(TLU_CLOCK),

    .TIMESTAMP(TIMESTAMP)
);

assign RJ45_BUSY = TLU_BUSY;
assign RJ45_CLK = TLU_CLOCK;

// ----- Pulser for TLU veto----- //
wire EXT_START_PULSE_VETO;
assign EXT_START_PULSE_VETO = TRIGGER_ACCEPTED_FLAG;
assign EXT_START_PULSE_TRIG = TRIGGER_ACCEPTED_FLAG;
wire VETO_TLU_PULSE;
assign AZ_VETO_TLU_PULSE = VETO_TLU_PULSE;

// set acknowledge when veto returns to low
pulse_gen_rising i_pulse_gen_rising_tlu_veto(.clk_in(CLK_CMD), .in(~VETO_TLU_PULSE), .out(TRIGGER_ACKNOWLEDGE_FLAG));

pulse_gen #(
    .BASEADDR(PULSER_VETO_BASEADDR),
    .HIGHADDR(PULSER_VETO_HIGHADDR),
    .ABUSWIDTH(32)
) i_pulse_gen_veto (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .PULSE_CLK(CLK_CMD),
    .EXT_START(EXT_START_PULSE_VETO),
    .PULSE(VETO_TLU_PULSE)
);


// ----- TDC module (individual for all four HitOr lines)----- //
localparam CLKDV = 4;  // division factor from 160 MHz clock to DC_CLK (here 40 MHz)
wire [N_TDC_CHANNELS-1:0] TDC_FIFO_READ;
// Do not generate TDC module for KC705.
`ifndef KC705
    wire [CLKDV*4-1:0] FAST_TRIGGER_OUT;
    wire [1:0] TRIG_FAST_OUT;
    // First TDC module: creates fast sampled trigger signal to use it for other TDC modules.
    tdc_s3 #(
        .BASEADDR(TDC_BASEADDR),
        .HIGHADDR(TDC_HIGHADDR),
        .ABUSWIDTH(32),
        .CLKDV(CLKDV),
        .DATA_IDENTIFIER(4'b0001),
        .FAST_TDC(1),
        .FAST_TRIGGER(1),
        .BROADCAST(0)   // generate for first TDC module the 640MHz sampled trigger signal and share it with other TDC mddules (broadcast)
    ) i_tdc (
        .CLK320(CLK320),    // 320 MHz
        .CLK160(CLK160),    // 160 MHz
        .DV_CLK(CLK40),     // 40 MHz
        .TDC_IN(HITOR[0]),  // HITOR_loc_combined[0]
        .TDC_OUT(TDC_IN_FROM_TDC[0]),
        .TRIG_IN(LEMO_TRIGGER),
        .TRIG_OUT(LEMO_TRIGGER_FROM_TDC[0]),

        // input/output trigger signals for broadcasting mode
        .FAST_TRIGGER_IN(16'b0),
        .FAST_TRIGGER_OUT(FAST_TRIGGER_OUT),  // collect 640 MHz sampled trigger signal to pass it to other TDC modules

        .FIFO_READ(TDC_FIFO_READ[0]),
        .FIFO_EMPTY(TDC_FIFO_EMPTY[0]),
        .FIFO_DATA(TDC_FIFO_DATA[0]),

        .BUS_CLK(BUS_CLK),
        .BUS_RST(BUS_RST),
        .BUS_ADD(BUS_ADD),
        .BUS_DATA(BUS_DATA),
        .BUS_RD(BUS_RD),
        .BUS_WR(BUS_WR),

        .ARM_TDC(1'b0),
        .EXT_EN(1'b0),

        .TIMESTAMP(TIMESTAMP[15:0])
    );

    // Additional TDC modules: Use the fast sampled trigger signal from first TDC module.
    genvar i;
    generate
      for (i=1; i<N_TDC_CHANNELS; i=i+1) begin: tdc_gen
        tdc_s3 #(
            .BASEADDR(TDC_BASEADDR + 32'h0100*i),
            .HIGHADDR(TDC_HIGHADDR + 32'h0100*i),
            .ABUSWIDTH(32),
            .CLKDV(CLKDV),
            .DATA_IDENTIFIER(4'b0001 + i),
            .FAST_TDC(1),
            .FAST_TRIGGER(1),
            .BROADCAST(1)   // generate for first TDC module the 640MHz sampled trigger signal and share it with other TDC mddules (broadcast)
        ) i_tdc (
            .CLK320(CLK320),    // 320 MHz
            .CLK160(CLK160),    // 160 MHz
            .DV_CLK(CLK40),     // 40 MHz
            .TDC_IN(HITOR[i]),  // HITOR_loc_combined[i]
            .TDC_OUT(TDC_IN_FROM_TDC[i]),
            // Use FAST_TRIGGER_IN as input for trigger signal
            .TRIG_IN(1'b0),
            .TRIG_OUT(),

            // input/output trigger signals for broadcasting mode
            .FAST_TRIGGER_IN(FAST_TRIGGER_OUT),  // Use the already existing 640 MHz sampled trigger signal from first module as FAST TRIGGER (broadcast)
            .FAST_TRIGGER_OUT(LEMO_TRIGGER_FROM_TDC[i]),

            .FIFO_READ(TDC_FIFO_READ[i]),
            .FIFO_EMPTY(TDC_FIFO_EMPTY[i]),
            .FIFO_DATA(TDC_FIFO_DATA[i]),

            .BUS_CLK(BUS_CLK),
            .BUS_RST(BUS_RST),
            .BUS_ADD(BUS_ADD),
            .BUS_DATA(BUS_DATA),
            .BUS_RD(BUS_RD),
            .BUS_WR(BUS_WR),

            .ARM_TDC(1'b0),
            .EXT_EN(1'b0),

            .TIMESTAMP(TIMESTAMP[15:0])
        );
      end
    endgenerate
`else
    // TDC module for KC705. Only possible to sample HitOr. Trigger distance measurement is not possbile since have no trigger signal.
    genvar i;
    generate
      for (i = 0; i < 4; i = i + 1) begin: tdc_gen
        tdc_s3 #(
            .BASEADDR(TDC_BASEADDR + 32'h0100*i),
            .HIGHADDR(TDC_HIGHADDR + 32'h0100*i),
            .ABUSWIDTH(32),
            .CLKDV(CLKDV),
            .DATA_IDENTIFIER(4'b0001 + i),
            .FAST_TDC(1),
            .FAST_TRIGGER(0), // Cannot use FAST_TRIGGER mode for KC705, since have no external TRIGGER signal.
            .BROADCAST(0)
        ) i_tdc (
            .CLK320(CLK320),    // 320 MHz
            .CLK160(CLK160),    // 160 MHz
            .DV_CLK(CLK40),     // 40 MHz
            .TDC_IN(HITOR[i]),
            .TDC_OUT(TDC_IN_FROM_TDC[i]),
            .TRIG_IN(LEMO_TRIGGER),
            .TRIG_OUT(LEMO_TRIGGER_FROM_TDC[0]),

            .FIFO_READ(TDC_FIFO_READ[i]),
            .FIFO_EMPTY(TDC_FIFO_EMPTY[i]),
            .FIFO_DATA(TDC_FIFO_DATA[i]),

            .BUS_CLK(BUS_CLK),
            .BUS_RST(BUS_RST),
            .BUS_ADD(BUS_ADD),
            .BUS_DATA(BUS_DATA),
            .BUS_RD(BUS_RD),
            .BUS_WR(BUS_WR),

            .ARM_TDC(1'b0),
            .EXT_EN(1'b0),

            .TIMESTAMP(TIMESTAMP[15:0])
        );
      end
    endgenerate
`endif


// ----- AURORA ----- //
wire [N_AURORA_RX-1:0] AURORA_RX_FIFO_READ;
wire [N_AURORA_RX-1:0] AURORA_RX_FIFO_EMPTY;
wire [N_AURORA_RX-1:0][31:0] AURORA_RX_FIFO_DATA;
wire [N_AURORA_RX-1:0] AUR_LOST_ERR;
wire DATA_MERGING_CLK;

rx_aurora_64b66b #(
    .BASEADDR(AURORA_RX_BASEADDR),
    .HIGHADDR(AURORA_RX_HIGHADDR),
    .ABUSWIDTH(32),
    .IDENTIFIER(),
    .AURORA_CHANNELS(N_AURORA_RX)
) i_aurora_rx (
    .CHIP_TYPE(CHIP_TYPE),
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .FIFO_READ(AURORA_RX_FIFO_READ),
    .FIFO_EMPTY(AURORA_RX_FIFO_EMPTY),
    .FIFO_DATA(AURORA_RX_FIFO_DATA),

// RD53A lane '0' is mapped to MGT receiver '3', to revert the lane swapping in the Displayport cable.
// Since USBPix3 only allows access to MGT0 and MGT1, we have to connect RD53A lane 0 to MGT0
    .AURORA_RESET(AURORA_RESET),
    .LOST_ERROR(AUR_LOST_ERR),

    .RX_SOFT_ERROR(RX_SOFT_ERROR),
    .RX_HARD_ERROR(RX_HARD_ERROR),

    .RX_LANE_UP(RX_LANE_UP),
    .RX_CHANNEL_UP(RX_CHANNEL_UP),
    .PLL_LOCKED(PLL_LOCKED),

    .REFCLK_OUT(REFCLK_OUT),
    .DRP_CLK_IN(DRP_CLK),

    `ifdef _1LANE
        `ifdef USBPIX3
            .RXP(MGT_RX_P[0]),
            .RXN(MGT_RX_N[0]),
        `else
            `ifdef BDAQ53
                // The 4 lanes of the multilane DP connector 'DP1' are connected in reverse, to encounter the lane-reversing of DP cables
                .RXP({MGT_RX_P[6], MGT_RX_P[5], MGT_RX_P[4], MGT_RX_P[0], MGT_RX_P[1], MGT_RX_P[2], MGT_RX_P[3] }),
                .RXN({MGT_RX_N[6], MGT_RX_N[5], MGT_RX_N[4], MGT_RX_N[0], MGT_RX_N[1], MGT_RX_N[2], MGT_RX_N[3]}),
                .DATA_MERGING_CLK(DATA_MERGING_CLK),
            `else
                .RXP(MGT_RX_P),
                .RXN(MGT_RX_N),
            `endif
      `endif
      .TX_P(TX_P),
      .TX_N(TX_N),
      .RX_CLK_IN_P(RX_CLK_IN_P),
      .RX_CLK_IN_N(RX_CLK_IN_N),
      .INIT_CLK_IN(INIT_CLK),
      .AURORA_CLK_OUT(AURORA_CLK_OUT),

      .CLK_CMD(CLK_CMD),
      .CMD_DATA(CMD_DATA),
      .CMD_OUTPUT_EN(CMD_OUTPUT_EN)
    `else
        .RXP(MGT_RX_P),
        .RXN(MGT_RX_N),
        .RX_CLK(),

        .GTXQ0_P(RX_CLK_IN_P),
        .GTXQ0_N(RX_CLK_IN_N),
        .INIT_CLK_IN(INIT_CLK),
        .RX_MULTILLANE_UP(RX_MULTILLANE_UP)
    `endif
);

// FIFO for Aurora data
wire [N_AURORA_RX-1:0] AURORA_FIFO_FULL;

genvar gen_aurora_fifo;
generate
    for (gen_aurora_fifo=0; gen_aurora_fifo<N_AURORA_RX; gen_aurora_fifo=gen_aurora_fifo+1) begin : generate_aurora_fifo
        gerneric_fifo #(
            .DATA_SIZE(32),
            .DEPTH(512*8)
        ) aurora_fifo_i (
            .clk(BUS_CLK), .reset(BUS_RST),
            .write(!AURORA_RX_FIFO_EMPTY[gen_aurora_fifo]),
            .read(ARB_GRANT_AURORA_RX[gen_aurora_fifo]),
            .data_in(AURORA_RX_FIFO_DATA[gen_aurora_fifo]),
            .full(AURORA_FIFO_FULL[gen_aurora_fifo]),
            .empty(AURORA_FIFO_EMPTY[gen_aurora_fifo]),
            .data_out(AURORA_FIFO_DATA[gen_aurora_fifo]),
            .size()
        );
    end
endgenerate

assign FIFO_WRITE = ARB_WRITE_OUT;
assign FIFO_DATA = ARB_DATA_OUT;
assign AURORA_RX_FIFO_READ = ~AURORA_FIFO_FULL;
assign TLU_FIFO_READ = ARB_GRANT_TLU_FIFO;
assign TDC_FIFO_READ = ARB_GRANT_TDC_FIFO;



// Aurora transmitter for data merging tests
wire [79:0] gpio_data_merging;

`ifdef RTL_SIM
    assign AURORA_SER_CLK = gpio_data_merging[77] ? ~CLK320 : CLK320;  // AURORA_CLK_OUT CLK320 CLK_CMD
`else
    assign AURORA_SER_CLK = gpio_data_merging[77] ? ~DATA_MERGING_CLK: DATA_MERGING_CLK;  // generated by Aurora RX PLL
`endif

gpio #(
    .BASEADDR(GPIO_DM_BASEADDR),
    .HIGHADDR(GPIO_DM_HIGHADDR),
    .ABUSWIDTH(32),
    .IO_WIDTH(80),
    .IO_DIRECTION({8'hff, 8'hff, 64'hffffffffffffffff})
) i_gpio_data_merging (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO(gpio_data_merging)
);

`ifdef INCLUDE_AURORA_TX
    tx_aurora #(
    ) i_aurora_tx (
        .BUS_CLK(BUS_CLK),
        .AURORA_SER_CLK(AURORA_SER_CLK),
    
        .RX_CHANNEL_UP(RX_CHANNEL_UP),
        .TX_CONTROL(gpio_data_merging),
        .DATA_MERGING_OUTPUT(DATA_MERGING_OUTPUT),
        .DATA_MERGING_ENABLE(DATA_MERGING_ENABLE),
        .AURORA_SER_CLK_OUT(AURORA_SER_CLK_OUT)
    );
`endif

endmodule
