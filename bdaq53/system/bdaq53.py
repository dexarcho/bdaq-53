#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
import collections
import yaml
import os
import time
import math
import struct
import numpy as np

from basil.dut import Dut
from bdaq53.system.aurora_rx import aurora_rx
from bdaq53.system.analog_monitoring_board import MonitoringBoard
try:
    from bdaq53.system import logger
except ImportError:
    pass

import pkg_resources
VERSION = pkg_resources.get_distribution("bdaq53").version


class BDAQ53(Dut):
    '''
    Main class for BDAQ53 readout system
    '''

    def __init__(self, conf=None, bench_config=None):
        self.log = logger.setup_main_logger()
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.configuration = {}

        try:
            if bench_config is None:
                bench_config = os.path.join(self.proj_dir, 'testbench.yaml')
            with open(bench_config) as f:
                self.configuration = yaml.full_load(f)
        except TypeError:
            self.configuration = bench_config

        # Calibration settings, e.g. resistor values for NTC readout on BDAQ board
        self.calibration = self.configuration.get('calibration', {})
        self.enable_NTC = self.configuration['hardware'].get('enable_NTC', False)

        # Receivers in use
        self.receivers = []
        chip_cfgs = self.get_chips_cfgs()
        for chip_cfg in chip_cfgs:
            if chip_cfg['receiver'] not in self.receivers:
                self.receivers.append(chip_cfg['receiver'])
            else:
                raise RuntimeError('Receiver {0} is used multiple times in the testbench configuration!'.format(chip_cfg['receiver']))

        if not conf:
            conf = os.path.join(self.proj_dir, 'system' + os.sep + 'bdaq53.yaml')
        self.log.debug("Loading configuration file from %s" % conf)

        # Flag indicating of tlu module is enabled.
        self.tlu_module_enabled = False

        super(BDAQ53, self).__init__(conf)

    def init(self, **kwargs):
        super(BDAQ53, self).init()

        self.fw_version, self.board_version, self.board_options, self.connector_version, self.num_rx_channels = self['system'].get_daq_version()
        self.log.success('Found board %s with %s running firmware version %s' % (self.board_version, self.connector_version, self.fw_version))
        self.log.info('Board has %d Aurora receiver channels' % self.num_rx_channels)

        if self.fw_version != VERSION.split('.')[0] + '.' + VERSION.split('.')[1]:  # Compare only the first two blocks
            raise Exception("Firmware version (%s) is different than software version (%s)! Please update." % (self.fw_version, VERSION))

        # Initialize analog monitoring board
        if self.configuration['periphery'].get('analog_monitoring_board', False):
            self.monitoring_board = MonitoringBoard(self['i2c'])
            self.monitoring_board.init()
            self.log.info("Analog Monitoring Board succesfully initialized.")

        # Add existing Aurora receivers depending on readout board
        self.rx_channels = {}
        for i in range(self.num_rx_channels):
            self.rx_channels['rx%d' % i] = aurora_rx(self['intf'], {'name': 'rx%d' % i, 'type': 'bdaq53.aurora_rx', 'interface': 'intf',
                                                                    'base_addr': 0x6000 + i * 0x0100})
            self.rx_channels['rx%d' % i].init()

        self.rx_lanes = {}
        for recv in self.receivers:
            t_rx_lanes = self.rx_channels[recv].get_rx_config()
            self.rx_lanes[recv] = t_rx_lanes

        # Configure cmd encoder
        self.set_cmd_clk(frequency=160.0)
        self['cmd'].reset()
        time.sleep(0.1)

        # Wait for the chip (model) PLL to lock before establishing a link
        if self.board_version == 'SIMULATION':
            for _ in range(100):
                self.rx_channels['rx0'].get_rx_ready()

        # Configure the Aurora links
        self.wait_for_pll_lock()
        self.setup_aurora()

        self.print_powered_dp_connectors()

    def set_cmd_clk(self, frequency=160.0, force=False):
        if self.board_version in {'BDAQ53', 'USBPix3'}:
            if self['system']['SI570_IS_CONFIGURED'] == 0 or force is True:
                from basil.HL import si570
                si570_conf = {'name': 'si570', 'type': 'bdaq53.si570', 'interface': 'intf', 'base_addr': 0xba, 'init': {'frequency': frequency}}
                bdaq53a_clk_gen = si570.si570(self['i2c'], si570_conf)
                self['cmd'].set_output_en(False)
                for receiver in self.receivers:
                    self.rx_channels[receiver].reset()
                time.sleep(0.1)
                bdaq53a_clk_gen.init()
                time.sleep(0.1)
                self['cmd'].set_output_en(True)
                self['system']['SI570_IS_CONFIGURED'] = 1
            else:
                self.log.info('Si570 oscillator is already configured')
        elif self.board_version == 'KC705':
            self._kc705_setup_si5324(frequency=frequency)
        elif self.board_version == 'SIMULATION':
            pass

    def get_chips_cfgs(self):
        module_cfgs = {k: v for k, v in self.configuration['modules'].items() if 'identifier' in v.keys()}
        chip_cfgs = []
        for mod_cfg in module_cfgs.values():
            for k, v in mod_cfg.items():
                if isinstance(v, collections.abc.Mapping) and 'chip_sn' in v:  # Detect chips defined in testbench by the definition of a chip serial number
                    chip_cfgs.append(v)
        return chip_cfgs

    def set_mgt_ref(self, value):
        '''Controls the clock multiplexer chip, which is used for providing the Aurora RX reference clock'''
        if value == "int":
            logger.info('MGT: Switching to on-board (Si570) oscillator')
            self['BDAQ_CONTROL']['MGT_REF_SEL'] = 0
            self['BDAQ_CONTROL'].write()
        elif value == "ext":
            logger.info('MGT: Switching to external (SMA) clock source')
            self['BDAQ_CONTROL']['MGT_REF_SEL'] = 1
            self['BDAQ_CONTROL'].write()

    def get_mgt_ref(self):
        value = self['BDAQ_CONTROL']['MGT_REF_SEL']
        if value == 0:
            return 'int'
        elif value == 1:
            return 'ext'

    def _kc705_set_i2c_mux(self, value):
        ''' Configure the I2C MUX and returns the base address of the selected device '''
        _i2c_mux_map = {
            'Si570': (0x01, 0x5d),
            'FMC_HPC': (0x02, 0x00),
            'FMC_LPC': (0x04, 0x00),
            'I2C_EEPROM': (0x08, 0x54),
            'SFP_MODULE': (0x10, 0x50),
            'ADV7511': (0x20, 0x39),
            'DDR3_SODIMM': (0x40, (0x50, 0x18)),
            'Si5324': (0x80, 0x68)
        }
        if value in _i2c_mux_map:
            self['i2c'].write(0xe8, [_i2c_mux_map[value][0]])
            self.log.debug('I2C mux set to: %s' % value)
        else:
            self.log.error('I2C mux setting invalid: %s' % value)

        base_addr = _i2c_mux_map[value][1]
        self.log.debug('I2C base address: %s' % hex(base_addr))

        return base_addr

    def _kc705_setup_si5324(self, frequency=160):
        ''' Calculated register values for Si5324 have to be modified in order to work!
            N1_HS, NC1_LS, N2_HS, N2_LS, N32, BWSEL '''
        _si5324_f_map = {
            200: (7, 4, 10, 112000, 22857, 2),
            180: (7, 4, 10, 100800, 22857, 2),
            170: (5, 6, 10, 102000, 22857, 2),
            160: (8, 4, 10, 102400, 22857, 2),
            150: (9, 4, 10, 33250, 7037, 2),
            140: (9, 4, 10, 100800, 22857, 2),
            120: (11, 4, 11, 32000, 7619, 2),
            100: (9, 4, 11, 32000, 15238, 2)
        }

        self._kc705_set_i2c_mux('Si5324')

        def si5324_read(addr):
            self['i2c'].write(0xd0, [addr])
            return self['i2c'].read(0xd0, 1)[0]

        def si5324_write(addr, data):
            self['i2c'].write(0xd0, [addr, data & 0xff])

        # Based on: https://github.com/m-labs/si5324_test/blob/master/firmware/runtime/si5324.c
        self['i2c'].write(0xd0, [134])
        ident = struct.unpack(">H", bytearray(self['i2c'].read(0xd0, 2)))[0]
        if ident != 0x0182:
            raise ValueError("Si5324 chip didn't respond.")

        # Select XA/XB input
        si5324_write(0, si5324_read(0) | 0x40)  # Free running mode=1, CKOUT_ALWAYS_ON = 0
        si5324_write(11, 0x41)  # Disable CLKIN1
        si5324_write(6, 0x0F)  # Disable CKOUT2 (SFOUT2_REG=001), set CKOUT1 to LVDS (SFOUT1_REG=111)
        si5324_write(21, si5324_read(21) & 0xfe)  # CKSEL_PIN = 0
        si5324_write(3, 0x55)  # CKIN2 selected, SQ_ICAL=1

        if frequency in _si5324_f_map:
            register_set = _si5324_f_map[frequency]

            N1_HS = register_set[0] - 4
            NC1_LS = register_set[1] - 1
            N2_HS = register_set[2] - 4
            N2_LS = register_set[3] - 1
            N32 = register_set[4] - 1
            BWSEL = register_set[5]

            self.log.debug('Si5324: Setting registers to %s' % str(register_set))
        else:
            self.log.error('Si5324: No valid frequency specified: %u' % frequency)

        si5324_write(2, (si5324_read(2) & 0x0f) | (BWSEL << 4))
        si5324_write(25, N1_HS << 5)
        si5324_write(31, NC1_LS >> 16)
        si5324_write(32, NC1_LS >> 8)
        si5324_write(33, NC1_LS)
        si5324_write(40, (N2_HS << 5) | (N2_LS >> 16))
        si5324_write(41, N2_LS >> 8)
        si5324_write(42, N2_LS)
        si5324_write(46, N32 >> 16)
        si5324_write(47, N32 >> 8)
        si5324_write(48, N32)
        si5324_write(137, si5324_read(137) | 0x01)  # FASTLOCK=1
        si5324_write(136, 0x40)  # ICAL=1

        time.sleep(0.1)

        LOS1_INT = (si5324_read(129) & 0x02) == 0
        LOSX_INT = (si5324_read(129) & 0x01) == 0
        LOL_INT = (si5324_read(130) & 0x01) == 0

        self.log.debug('Si5324: Has input: %d' % (LOS1_INT))
        self.log.debug('Si5324: Has xtal %d:' % (LOSX_INT))
        self.log.debug('Si5324: Locked: %d' % (LOL_INT))

        self.log.info('Si5324: Clock set to %u MHz.' % frequency)

        if LOL_INT is False:
            self.log.warning('Si5324: Not locked.')

    def _kc705_get_temperature_NTC_CERNFMC(self):
        ''' Measure the temperature of the SCC NTC using the CERN FMC card connected to KC705 board'''

        if self.board_version != 'KC705':
            raise RuntimeError('_kc705_get_temperature_NTC_CERNFMC() is only available with KC705 and CERN FMC card')

        # -----constants-------------
        # FIXME: move to calibration object?
        ntc_adc_vdd = 2.5  # ntc_adc_vdd of the ADC
        ntc_R1 = 39000  # Resistance 1 of the voltage divider, in series with NTC in FMC-card
        ntc_R25C = 10e3  # NTC constant
        ntc_T25 = 298.15
        ntc_beta = 3435  # Beta NTC constant
        ntc_adc_lsb = 0.001  # ntc_adc_lsb value of the ADC for normal configuration. Can be changed by changing the configuration register.
        # ---------------------------

        ntc_adc_base_address = 0x90

        # Read the values of the 1 bit ADC in CERN-FMC card:
        if self.connector_version == 'FMC_HPC':
            self._kc705_set_i2c_mux('FMC_HPC')
            self.log.debug('I2C mux: FMC HPC selected')
        elif self.connector_version == 'FMC_LPC':
            self._kc705_set_i2c_mux('FMC_LPC')
            self.log.debug('I2C mux: FMC LPC selected')
        else:
            raise RuntimeError('_kc705_get_temperature_NTC_CERNFMC() is only available with KC705 and CERN FMC card')

        self['i2c'].write(ntc_adc_base_address, [0b00000001])  # address of ADC and write the addresss pointer register to point the configuration register(default 0x8583)
        self['i2c'].write(ntc_adc_base_address, [0b00000001, 0x85, 0x83])  # Reset the ADC to start adc_raw single conversion. with this config (which is the default) in the conversion register we will read the voltage drop between the terminals of the NTC resistor.

        self['i2c'].write(ntc_adc_base_address, [0b00000000])
        adc_raw = self['i2c'].read(ntc_adc_base_address, 2)  # read two bytes of the conversion register of adc

        self.log.debug('NTC ADC raw data: %s' % (hex(adc_raw[0]) + ' ' + hex(adc_raw[1])))
        adc = (((adc_raw[0] << 8) | adc_raw[1]) >> 4)
        V_adc = ntc_adc_lsb * adc
        R_ntc = (ntc_R1 * V_adc) / (ntc_adc_vdd - V_adc)
        T_ntc = (1.0 / ((1.0 / ntc_T25) + ((1.0 / ntc_beta) * (np.log(R_ntc / ntc_R25C))))) - (ntc_T25 - 25)

        return round(T_ntc, 3)

    def set_LEMO_MUX(self, connector='LEMO_MUX_TX0', value=0):
        '''
        Sets the multiplexer in order to select which signal is routed to LEMO ports. So far only used
        for LEMO_TX ports.

        Parameters
        ----------
        connector : string
            Name of the LEMO connector. Possible names: LEMO_MUX_TX1, LEMO_MUX_TX0
        value : int
            Value specifying the multiplexer state. Default is 0.
            LEMO_TX_0: not used (3), not used (2), CMD_LOOP_START_PULSE (1), RJ45_CLK (0)
            LEMO_TX_1: not used (3), not used (2), not used (1), RJ45_BUSY (0)
        '''

        # TODO:  LEMO_MUX_RX1 and LEMO_MUX_RX0 not yet used
        # According to FW. None means not used.
        lemo_tx0_signals = ['RJ45_CLK', 'CMD_LOOP_START_PULSE', None, None]
        lemo_tx1_signals = ['RJ45_BUSY', None, None, None]
        if connector in ('LEMO_MUX_TX1', 'LEMO_MUX_TX0') and value in range(4):
            self['BDAQ_CONTROL'][connector] = value
            self['BDAQ_CONTROL'].write()
            if 'TX0' in connector:
                signal = lemo_tx0_signals[value]
            if 'TX1' in connector:
                signal = lemo_tx1_signals[value]
            self.log.info('%s set to %s (%s)' % (connector, value, signal))
        else:
            self.log.error('%s or %s are invalid' % (connector, value))

    def _bdaq_set_NTC_MUX(self, connector=0):
        '''
        Sets the NTC readout route on the BDAQ board
        '''
        _BDAQ_ntc_mux_map = {
            # Connector ID: MUX channel
            0: 2,  # DP1 (DP connector '1')
            1: 3,  # DP3 (DP connector '2')
            2: 4,  # DP4 (DP connector '3')
            3: 5,  # DP5 (DP connector '4')
            4: 7,  # DP2 (DP connector '5', hitor)
            5: 6,  # mDP (miniDP connector)
            6: 0,  # NTC_RJ45_A
            7: 1   # NTC_RJ45_B
        }
        _BDAQ_receiver_connector_map = {
            # Receiver: Connector ID
            'rx0': 0,   # DP1 (DP connector '1')
            'rx1': 0,   # DP1 (DP connector '1')
            'rx2': 0,   # DP1 (DP connector '1')
            'rx3': 0,   # DP1 (DP connector '1')
            'rx4': 1,   # DP3 (DP connector '2')
            'rx5': 2,   # DP4 (DP connector '3')
            'rx6': 3    # DP5 (DP connector '4')
        }

        if isinstance(connector, str):
            connector = _BDAQ_receiver_connector_map[connector]

        self['BDAQ_CONTROL']['NTC_MUX'] = _BDAQ_ntc_mux_map[connector]
        self['BDAQ_CONTROL'].write()
        self.log.debug('Set NTC_MUX to %s', _BDAQ_ntc_mux_map[connector])
        time.sleep(0.001)

    def _bdaq_get_temperature_NTC(self, connector=0, NTC_type='TDK_NTCG163JF103FT1'):
        if not self.enable_NTC:
            raise ValueError('Please mount the correct resistors to your Bdaq board and enable the NTC in the testbench.yaml')

        if NTC_type == 'TDK_NTCG16H' or 'TDK_NTCG163JF103FT1':
            R_RATIO = np.array([18.85, 14.429, 11.133, 8.656, 6.779, 5.346, 4.245, 3.393, 2.728, 2.207, 1.796, 1.47, 1.209, 1.0, 0.831, 0.694, 0.583, 0.491, 0.416, 0.354, 0.302, 0.259, 0.223, 0.192, 0.167, 0.145, 0.127, 0.111, 0.0975, 0.086, 0.076, 0.0674, 0.0599, 0.0534])
            B_CONST = np.array([3140, 3159, 3176, 3194, 3210, 3226, 3241, 3256, 3270, 3283, 3296, 3308, 3320, 3332, 3343, 3353, 3363, 3373, 3382, 3390, 3399, 3407, 3414, 3422, 3428, 3435, 3441, 3447, 3453, 3458, 3463, 3468, 3473, 3478])
            TEMP = np.arange(-40 + 273.15, 130 + 273.15, 5)
            R0 = 10000  # R at 25C
        else:
            raise ValueError('NTC_type %s is not supported.' % NTC_type)

        self._bdaq_set_NTC_MUX(connector=connector)
        vpvn_raw = self['gpio_xadc_vpvn'].get_data()  # reads VP - VN voltage generated by NTC readout circuit on BDAQ53 pcb vpvn_raw is /16 becauses XADC is 12bit
        if all(v == 255 for v in vpvn_raw):
            self.log.warning('BDAQ ADC in saturation! Raw values: {}'.format(vpvn_raw))
        Vmeas = float((vpvn_raw[1] + vpvn_raw[0] * 256) / 16) * 1 / (2 ** 12 - 1)

        if 0.26 < Vmeas < 0.28:  # Very old BDAQ: No MUX, resistors, ... mounted
            self.log.warning('NTC measurement is ambiguous! Are you sure you have all necessary devices mounted on your BDAQ board?')
        elif 0.017 < Vmeas < 0.02:  # Wrong resistor values or solder jumpers open
            self.log.warning('NTC measurement is ambiguous! Are you sure you have the correct resistors mounted on your BDAQ board?')

        # r = NTC resistance, is measured by unity gain opamp on BDAQ53 PCB
        # R names taken from BDAQ53 PCB rev. 1.1, adjust to fit input xADC input rage [0-1V]
        # Vmeas = VntcP - VntcN

        R16 = float(self.calibration['bdaq_ntc']['R16'])
        R17 = float(self.calibration['bdaq_ntc']['R17'])
        R19 = float(self.calibration['bdaq_ntc']['R19'])
        VCC_3V3 = 3.3

        VntcP = VCC_3V3 * R19 / (R17 + R19)
        VntcN = VntcP - Vmeas

        r = VntcN * R16 / (VCC_3V3 - VntcN)

        r_ratio = r / R0
        arg = np.argwhere(R_RATIO <= r_ratio)

        if len(arg) == 0:
            j = -1
        else:
            j = arg[0]

        k = 1.0 / (math.log(r_ratio / R_RATIO[j]) / B_CONST[j] + 1 / TEMP[j])[0]
        self.log.debug("Temperature of NTC %s connected to connector %s: %.2f [°C]", NTC_type, connector, k - 273.15)

        return round(k - 273.15, 3)

    def _bdaq_get_temperature_FPGA(self):
        '''
        Return temperature of BDAQ FPGA
        '''
        # temp_raw is /16 becauses XADC is 12bit
        # -0x767) * 0.123 - 40 from XADC manual
        fpga_temp_raw = self['gpio_xadc_fpga_temp'].get_data()
        fpga_temp = (float(fpga_temp_raw[1] + fpga_temp_raw[0] * 256) / 16 - 0x767) * 0.123 - 40

        self.log.debug('Internal FPGA temperature: %.2f [°C]', fpga_temp)

        return round(fpga_temp, 3)

    def get_temperature_NTC(self, connector=0):
        '''
            Returns the NTC temperature measured at the selected connector.
            For KC705 the connector parameter takes no effect.

            Parameters:
            ----------
                connector : int or str
                    Determines which connector on the BDAQ board shall be used to readout the NTC.
                    Must be either the receiver name as string (eg. 'rx0') or the actual integer connector ID.
        '''
        if self.board_version == 'KC705':
            return self._kc705_get_temperature_NTC_CERNFMC()
        elif self.board_version == 'BDAQ53':
            return self._bdaq_get_temperature_NTC(connector=connector)
        else:
            self.log.error('NTC readout is not not supported on this hardware platform.')

    def get_temperature_FPGA(self):
        if self.board_version == 'BDAQ53':
            return self._bdaq_get_temperature_FPGA()
        else:
            self.log.error('FPGA temperature readout is not not supported on this hardware platform.')

    def get_DP_SENSE(self, DP_ID):
        ''' Read back the vddd_sense lines to identify powered chips '''
        if self.board_version == 'BDAQ53':
            sense = self['BDAQ_CONTROL'].get_data()
            self.log.debug('Slow_control sense: %s' % bin(sense[0]))
            if 0 <= DP_ID < 4:
                return ((sense[0] & (1 << DP_ID)) is False)
            else:
                self.log.error('Invalid DP_ID (0..3)')
        else:
            self.log.error('RD53A slow control is only available for BDAQ53 hardware')
            return False

    def set_DP_RESET(self, DP_ID, value):
        ''' Controls the POR lines. if set to 1, the POR is pulled to local chip ground via opto-coupler '''
        if self.board_version == 'BDAQ53':
            if 0 <= DP_ID < 4:
                self['BDAQ_CONTROL'].set_data([value << (4 + DP_ID)])
            else:
                self.log.error('Invalid DP_ID (0..3)')
        else:
            self.log.warning('RD53A slow control is only available for BDAQ53 hardware')
            return False

    def print_powered_dp_connectors(self):
        ''' Report the VDDD_SENSE signal status for the given Displayport connectors'''
        if self.board_version == 'BDAQ53':
            for i in range(4):
                if self.get_DP_SENSE(i):
                    self.log.info('VDDD_SENSE detected at Displayport ID %i' % i)

    def dp_power_on_reset(self, DP_ID):
        ''' Short reset pulse for given DP_ID '''
        self.set_DP_RESET(DP_ID, True)
        time.sleep(0.1)
        self.set_DP_RESET(DP_ID, False)

    def set_monitor_filter(self, receivers=None, mode='filter'):
        if receivers is None:
            receivers = self.receivers
        else:
            if not isinstance(receivers, (list,)):
                receivers = [receivers]

        for receiver in receivers:
            self.rx_channels[receiver].set_USER_K_FILTER_MASK_1(0x00)   # only allow frames containing register data
            self.rx_channels[receiver].set_USER_K_FILTER_MASK_2(0x01)   # only allow frames containing register data
            self.rx_channels[receiver].set_USER_K_FILTER_MASK_3(0x02)   # only allow frames containing register data
            self.rx_channels[receiver].set_USER_K_FILTER_MODE(mode)     # set the filter mode
        self.log.debug('USER_K filter set to %s' % mode)

    def write_digilent_dac(self, value):
        ''' Writes integer upto 16 bits to the externally via PMOD connected Digilent DAC '''
        if self.board_version == 'BDAQ53':
            byts = []
            value = int(value)
            for i in range(0, 2):
                byts.append(value >> (i * 8) & 0xff)
            byts.reverse()
            self['spi_dac'].set_data(byts, addr=0)
            self['spi_dac'].start()
        else:
            self.log.warning('Digilent DAC is only available for BDAQ53 hardware')
            return False

    def setup_aurora(self):
        if self.configuration['hardware']['bypass_mode'] is True:
            if (self.board_version == 'KC705' and self.connector_version == 'FMC_LPC' and self['system']['AURORA_RX_640M']) or (self.board_version == 'SIMULATION') or (self.board_version == 'BDAQ53'):
                self.log.info("Switching board to BYPASS MODE @ 640Mb/s")
                # bypass cdr & bypass reset lanes are switched in firmware
                self['system'].enable_bypass_clocks()
                self['system']['BYPASS_MODE'] = True
                self['cmd'].set_bypass_cdr(True)
                self['cmd'].set_bypass_reset(False)
                self['cmd'].set_bypass_reset(False)
                self['cmd'].set_bypass_reset(False)
                self['cmd'].set_bypass_reset(True)
            else:
                raise NotImplementedError('Bypass mode is only supported for the KC705+FMC_LPC readout hardware @ 640Mb/s')
        else:
            self['system'].disable_bypass_clocks()
            self['system']['BYPASS_MODE'] = False
            if self['system']['AURORA_RX_640M']:
                self.log.info("Aurora receiver(s) running at 640Mb/s")
            else:
                self.log.info("Aurora receiver(s) running at 1.28Gb/s")

    def wait_for_aurora_sync(self, sync_receivers=None, timeout=1000):
        '''
            Wait for Aurora sync on all receivers given in sync_receivers.
            Wait for Aurora sync on all receivers in use if no sync_receivers are specified.

            Parameters:
            ----------
            sync_receivers : str or list of str or None
                    Each string defines one receiver named as in the readout board configuration (e.g. 'rx0')
        '''
        self.log.debug("Waiting for Aurora sync...")
        times = 0

        if sync_receivers is None:
            receivers = self.receivers
        else:
            if isinstance(sync_receivers, (list,)):
                receivers = sync_receivers
            else:
                receivers = [sync_receivers]

        while times < timeout and any((self.rx_channels[receiver].get_rx_ready() == 0) for receiver in receivers):
            times += 1

        rx_ready = [(self.rx_channels[receiver].get_rx_ready() == 1) for receiver in receivers]

        if all(rx_ready):
            self.log.debug("Aurora link(s) synchronized")
            return True
        else:  # FIXME: can be annoying for multi chip modules
            self['cmd'].reset()
            timed_out_rcv = ''
            for receiver, ready in zip(receivers, rx_ready):
                if ready == 0:
                    timed_out_rcv += receiver + ' '
            timed_out_rcv = timed_out_rcv[:-1]
            raise RuntimeError('Timeout while waiting for Aurora Sync on %s.' % timed_out_rcv)

    def wait_for_pll_lock(self, lock_receivers=None, timeout=1000):
        '''
            Wait for PLL lock on all receivers given in lock_receivers.
            Wait for PLL lock on all receivers in use if no lock_receivers are specified.

            Parameters:
            ----------
            lock_receivers : str or list of str
                    Each string defines one receiver named as in the readout board configuration (e.g. 'rx0')
        '''
        self.log.debug("Waiting for PLL lock...")
        times = 0

        if lock_receivers is None:
            receivers = self.receivers
        else:
            if isinstance(lock_receivers, (list,)):
                receivers = lock_receivers
            else:
                receivers = [lock_receivers]
        while times < timeout and any((self.rx_channels[receiver].get_pll_locked() == 0) for receiver in receivers):
            times += 1

        pll_locked = [(self.rx_channels[receiver].get_pll_locked() == 1) for receiver in receivers]

        if all(pll_locked):
            self.log.debug("PLL(s) locked")
            return True
        else:
            timed_out_rcv = ''
            for receiver, locked in zip(receivers, pll_locked):
                if locked == 0:
                    timed_out_rcv += receiver + ' '
            timed_out_rcv = timed_out_rcv[:-1]
            raise RuntimeError('Timeout while waiting for PLL to lock on %s.' % timed_out_rcv)

    def get_trigger_counter(self):
        return self['tlu']['TRIGGER_COUNTER']

    def set_chip_type_RD53A(self):
        ''' Defines chip type RD53A '''
        self['cmd'].set_chip_type(0)

    def set_chip_type_ITkPixV1(self):
        ''' Defines chip type ITkPixV1 '''
        self['cmd'].set_chip_type(1)

    def enable_auto_sync(self):
        '''Enables automatic sending of sync commands'''
        self['cmd'].set_auto_sync(1)

    def disable_auto_sync(self):
        '''Disables automatic sending of sync commands'''
        self['cmd'].set_auto_sync(0)

    def configure_hitor_inputs(self, active_ports_conf=0b000):
        '''
            Configure which HitOr display port connectors are active and get or-ed to the common virtual HitOr port,
            which is forwarded to TLU and TDC modules. Use individual bits to encode which ports to activate.
            At maximum two ports can be activated (0b11) since maximally two ports are available on the BDAQ board.
        '''
        if self.board_version == 'BDAQ53':
            active_ports_conf = active_ports_conf & 0b011   # Two available ports
        else:
            active_ports_conf = active_ports_conf & 0b001   # Assume max. one available port
        self.log.debug('Configure HitOr: ' + str(bin(active_ports_conf)))
        self['BDAQ_CONTROL']['HITOR_COMBINE_EN'] = active_ports_conf
        self['BDAQ_CONTROL'].write()

    def configure_tdc_modules(self, modules=(0, 1, 2, 3)):
        self.log.info('Configuring TDC modules {0}...'.format(modules))
        for module in modules:
            self['tdc_hitor{0}'.format(module)].EN_WRITE_TIMESTAMP = self.configuration['TDC'].get('EN_WRITE_TIMESTAMP', 1)
            self['tdc_hitor{0}'.format(module)].EN_TRIGGER_DIST = self.configuration['TDC'].get('EN_TRIGGER_DIST', 1)
            self['tdc_hitor{0}'.format(module)].EN_NO_WRITE_TRIG_ERR = self.configuration['TDC'].get('EN_NO_WRITE_TRIG_ERR', 1)
            self['tdc_hitor{0}'.format(module)].EN_INVERT_TDC = self.configuration['TDC'].get('EN_INVERT_TDC', 0)
            self['tdc_hitor{0}'.format(module)].EN_INVERT_TRIGGER = self.configuration['TDC'].get('EN_INVERT_TRIGGER', 0)

    def enable_tdc_modules(self, modules=(0, 1, 2, 3)):
        for module in modules:
            self['tdc_hitor{0}'.format(module)].ENABLE = 1

    def disable_tdc_modules(self, modules=(0, 1, 2, 3)):
        for module in modules:
            self['tdc_hitor{0}'.format(module)].ENABLE = 0

    def enable_tlu_module(self):
        self['tlu']['TRIGGER_ENABLE'] = True
        self.tlu_module_enabled = True

    def disable_tlu_module(self):
        self['tlu']['TRIGGER_ENABLE'] = False
        self.tlu_module_enabled = False

    def enable_ext_trigger(self):
        self['cmd'].set_ext_trigger(True)

    def disable_ext_trigger(self):
        self['cmd'].set_ext_trigger(False)

    def set_trigger_data_delay(self, trigger_data_delay):
        self['tlu']['TRIGGER_DATA_DELAY'] = trigger_data_delay

    def configure_tlu_module(self, max_triggers=False):
        self.log.info('Configuring TLU module...')
        self['tlu']['RESET'] = 1    # Reset first TLU module
        for key, value in self.configuration['TLU'].items():    # Set specified registers
            self['tlu'][key] = value
        self['tlu']['TRIGGER_COUNTER'] = 0

        if max_triggers:
            self['tlu']['MAX_TRIGGERS'] = int(max_triggers)  # Set maximum number of triggers
        else:
            self['tlu']['MAX_TRIGGERS'] = 0  # unlimited number of triggers

    def get_tlu_erros(self):
        return (self['tlu']['TRIGGER_LOW_TIMEOUT_ERROR_COUNTER'], self['tlu']['TLU_TRIGGER_ACCEPT_ERROR_COUNTER'])

    def configure_trigger_cmd_pulse(self, trigger_length, trigger_delay):
        # configures pulse which is sent to CMD for incoming triggers; factor 4 is needed for conversion from 160 MHz to 40 MHz (BC)
        self['pulser_trig'].set_en(True)
        self['pulser_trig'].set_width(trigger_length * 4)
        self['pulser_trig'].set_delay(trigger_delay * 4)
        self['pulser_trig'].set_repeat(1)

    def configure_tlu_veto_pulse(self, veto_length):
        # configures pulse for veto of new triggers; factor 4 is needed for conversion from 160 MHz to 40 MHz (BC)
        self['pulser_veto'].set_en(True)
        self['pulser_veto'].set_width(4)
        self['pulser_veto'].set_delay(veto_length * 4)
        self['pulser_veto'].set_repeat(1)

    def trigger_on_cmd_loop_start(self):
        # write TLU words every CMD repetition
        self.log.debug('Write TLU words on CMD_LOOP_START signal')
        self['tlu']['TRIGGER_MODE'] = 0
        self['tlu']['TRIGGER_SELECT'] = 256  # Trigger on CMD_LOOP_START signal
        self['tlu']['TRIGGER_COUNTER'] = 0
        self.enable_tlu_module()

    def dm_init(self, invert=False, datatype='monitor'):
        self['DATA_MERGING']['DM_RESET'] = 1
        self['DATA_MERGING'].write()
        self['DATA_MERGING']['DM_RESET'] = 0
        self['DATA_MERGING'].write()
        self.dm_invert(invert)
        self.dm_type(datatype)

    def dm_type(self, datatype=''):
        ''' Parameters:
            datatype: string ['monitor', 'hitdata']
                Aurora data type
        '''
        if datatype in {'monitor', 'hitdata'}:
            self['DATA_MERGING']['DM_TYPE'] = 0 if datatype == 'monitor' else 1
            self['DATA_MERGING'].write()

    def dm_send(self, data=0):
        ''' Parameters:
            data: 64 bit
                Aurora data field
        '''
        self.log.debug('Sending data merging frame {}'.format(hex(data)))
        self['DATA_MERGING']['DM_DATA'] = data
        self['DATA_MERGING'].write()
        self['DATA_MERGING']['DM_START'] = 1
        self['DATA_MERGING'].write()
        self['DATA_MERGING']['DM_START'] = 0
        self['DATA_MERGING'].write()

    def dm_send_register(self, address=0, data=0, header=0x55, id=0, state=0):
        ''' Parameters:
            ---
            address: 10 bit int
                register address
            data: 16 bit
                register data
            header: 8 bit [0xd2, 0x99, 0x55, 0xb4]
                Aurora bit type field (BTF), d2:MM, 99:MA, 55:AM, AA:B4
            id: 2 bit
                Chip ID [0..3]
            state: 2 bit
                status
        '''
        self.log.debug('Sending data merging register data. Header={}\tAddress={}\tData={}'.format(hex(header), hex(address), hex(data)))
        self.dm_send(data=header << 56 | id << 46 | state << 44 | address << 42 | (data & 0xffff) << 26 | address << 16 | (data & 0xffff))

    def dm_send_hitdata(self, data=0):
        ''' Parameters:
            ---
            data: 64 bit
                hit data
        '''
        self.log.debug('Sending data merging hit data. tData={}'.format(hex(data)))
        self.dm_send(data=data)

    def dm_output_enable(self, lane=0):
        ''' Paramters:
            ---
            lane: [0...3, 'all']
                Specifies the active input(s). 1 out of 4 [0..3] or 'all'
        '''
        self.log.info('Enabling data merging output {}'.format(lane))
        if lane == 'all':
            self['DATA_MERGING']['DM_ENABLE'] = 0xf
        else:
            self['DATA_MERGING']['DM_ENABLE'] = 1 << lane
        self['DATA_MERGING'].write()

    def dm_output_disable(self):
        ''' Paramters:
            ---
            lane: [0...3, 'all']
                Specifies the active input(s). 1 out of 4 [0..3] or 'all'
        '''
        self['DATA_MERGING']['DM_ENABLE'] = 0
        self['DATA_MERGING'].write()

    def dm_invert(self, invert=True):
        ''' Parameters:
            ---
            invert: [True, False]
                Inverts all data merging output channels
        '''
        self['DATA_MERGING']['DM_INVERT'] = invert
        self['DATA_MERGING'].write()

    def dm_delay(self, delay):
        ''' Parameters:
            ---
            delay: 5-bit
                Sets the delay between cmd clock and data merging out
        '''
        self['DATA_MERGING']['DM_DELAY'] = delay
        self['DATA_MERGING'].write()

    def dm_increment(self):
        ''' Parameters:
            ---
            delay: 5-bit
                Sets the delay between cmd clock and data merging out
        '''
        self['DATA_MERGING']['DM_INC_SET'] = 1
        self['DATA_MERGING']['DM_INC_ENABLE'] = 1
        self['DATA_MERGING'].write()
        self['DATA_MERGING']['DM_INC_ENABLE'] = 0
        self['DATA_MERGING'].write()

    def dm_decrement(self):
        ''' Parameters:
            ---
            delay: 5-bit
                Sets the delay between cmd clock and data merging out
        '''
        self['DATA_MERGING']['DM_INC_SET'] = 0
        self['DATA_MERGING']['DM_INC_ENABLE'] = 1
        self['DATA_MERGING'].write()
        self['DATA_MERGING']['DM_INC_ENABLE'] = 0
        self['DATA_MERGING'].write()
