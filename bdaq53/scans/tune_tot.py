#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
'''
    This script tunes ToT to a defined target value (with specified delta to target value) using binary search algorithm.
    Tuning ToT for all FEs in parallel is supported.
'''

from tqdm import tqdm
import numpy as np

from bdaq53.system.scan_base import ScanBase
from bdaq53.chips.shift_and_inject import shift_and_inject, get_scan_loop_mask_steps
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting
from bdaq53.analysis import online as oa
import bdaq53.analysis.analysis_utils as au

scan_configuration = {
    'start_column': 128,
    'stop_column': 264,
    'start_row': 0,
    'stop_row': 192,

    'target_tot': {'SYNC': 5.3, 'LIN': 5.3, 'DIFF': 5.3},  # target value for each FE to which ToT is tuned
    'delta_tot': 0.2,  # max. difference between target ToT and tuned ToT value for each FE
    'lower_limit': {'SYNC': 20, 'LIN': 10, 'DIFF': 20},  # lower bound for the feedback register value for each FE
    'upper_limit': {'SYNC': 160, 'LIN': 130, 'DIFF': 120},  # upper bound for the feedback register value for each FE
    'max_iterations': 10,  # max. number of iterations after which tuning is aborted

    'VCAL_MED': 500,
    'VCAL_HIGH': 1100
}


class TotTuning(ScanBase):
    scan_id = 'tot_tuning'

    def _configure(self, start_column=0, stop_column=400, start_row=0, stop_row=192, VCAL_MED=1000, VCAL_HIGH=4000, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.

        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            VCAL_HIGH DAC value.
        '''

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update(force=True)

        # Determine active FEs and feedback registers which need to be adjusted
        self.data.active_FEs = []
        self.data.fb_curr_registers = {}
        if len(set(range(0, 128)).intersection(set(range(start_column, stop_column)))) > 0:
            self.data.active_FEs.append('SYNC')
            self.data.fb_curr_registers.update({'SYNC': 'IBIAS_KRUM_SYNC'})
        if len(set(range(128, 264)).intersection(set(range(start_column, stop_column)))) > 0:
            self.data.active_FEs.append('LIN')
            self.data.fb_curr_registers.update({'LIN': 'KRUM_CURR_LIN'})
        if len(set(range(264, 400)).intersection(set(range(start_column, stop_column)))) > 0:
            self.data.active_FEs.append('DIFF')
            self.data.fb_curr_registers.update({'DIFF': 'VFF_DIFF'})

        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)
        self.data.tot_hist = oa.TotHistogramming(chip_type=self.chip.chip_type.lower(), rx_id=int(self.chip.receiver[-1]))

    def _scan(self, n_injections=100, target_tot={'SYNC': 5.3, 'LIN': 5.3, 'DIFF': 5.3}, delta_tot=0.2, lower_limit={'SYNC': 20, 'LIN': 10, 'DIFF': 40}, upper_limit={'SYNC': 50, 'LIN': 40, 'DIFF': 70}, max_iterations=10, **_):
        '''
        ToT tuning main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.
        target_tot : float
            Target value to which ToT is tuned.
        delta_tot : float
            Maximum difference between target ToT and tuned ToT value.
        lower_limit : int
            Lower limit for the feeback current register.
        upper_limit : int
            Upper limit for the feeback current register.
        max_iterations : int
            Maximum number of iterations after which tuning is aborted.

        '''

        self.data.start_data_taking = True
        self.data.n_injections = n_injections
        n_iterations = 0

        feedback_current, actual_tot_mean, scan_par_values = {}, {}, {}

        self.log.info('Tune ToT for: ' + ', '.join(self.data.active_FEs))
        pbar = tqdm(total=get_scan_loop_mask_steps(scan=self) * max_iterations, unit=' Mask steps')

        succesfull_FEs = []  # List of succesfully tuned FEs
        # Binary search loop until max iteration is reached or all FEs were tuned
        while n_iterations < max_iterations and len(self.data.active_FEs) > 0:
            n_iterations += 1
            for active_FE in self.data.active_FEs:
                actual_fb_curr_register = self.data.fb_curr_registers[active_FE]
                feedback_current.update({active_FE: int(np.floor((lower_limit[active_FE] + upper_limit[active_FE]) / 2.0))})
                # Write feedback current
                self.chip.registers[actual_fb_curr_register].write(value=feedback_current[active_FE])
                scan_par_values.update({actual_fb_curr_register: feedback_current[active_FE]})
                pbar.write('Setting {0} = {1} (iteration {2})'.format(actual_fb_curr_register, feedback_current[active_FE], n_iterations))

            # Store scan parameter values
            self.store_scan_par_values(scan_param_id=n_iterations, **scan_par_values)
            # Take data
            with self.readout(scan_param_id=n_iterations, callback=self.analyze_data_online):
                shift_and_inject(scan=self, n_injections=self.data.n_injections, pbar=pbar, scan_param_id=n_iterations, cache=True)

                # Get mean of ToT from online analysis
                tot = self.data.tot_hist.get()
                for active_FE in self.data.active_FEs:
                    actual_fb_curr_register = self.data.fb_curr_registers[active_FE]
                    hist_tot = tot[min(self.chip.flavor_cols[active_FE]):max(self.chip.flavor_cols[active_FE]), :, :].sum(axis=(0, 1))
                    actual_tot_mean.update({active_FE: au.get_mean_from_histogram(hist_tot, np.arange(hist_tot.shape[0]))})
                    pbar.write('Mean ToT at {0} = {1} is {2:1.2f}'.format(actual_fb_curr_register, feedback_current[active_FE], actual_tot_mean[active_FE]))

                    # Update upper and lower limit based on mean ToT
                    if np.sqrt((actual_tot_mean[active_FE] - target_tot[active_FE])**2) < delta_tot:  # Break if difference to target is smaller then delta
                        pbar.write('ToT tuning was succesful for {0}! Mean ToT is {1:1.2f} (target was {2:1.2f}) at {3} = {4}'.format(active_FE, actual_tot_mean[active_FE], target_tot[active_FE], actual_fb_curr_register, feedback_current[active_FE]))
                        self.chip.configuration['registers'][actual_fb_curr_register] = feedback_current[active_FE]
                        succesfull_FEs.append(active_FE)
                        continue
                    else:  # Update limits
                        if target_tot[active_FE] < actual_tot_mean[active_FE]:
                            lower_limit.update({active_FE: feedback_current[active_FE] + 1})
                        else:
                            upper_limit.update({active_FE: feedback_current[active_FE] - 1})

            # Remove succesfuly tuned FE from list
            self.data.active_FEs = list(set(self.data.active_FEs) - set(succesfull_FEs))

        pbar.close()
        # Max iteration is reached. Check if still have untuned ToT for activated FEs.
        if len(self.data.active_FEs) > 0:
            self.log.warning('Number of maximum iteration is reached. Could not tune ToT for: ' + ', '.join(self.data.active_FEs))

        self.data.start_data_taking = False
        self.data.tot_hist.close()  # stop and wait for analysis process
        self.log.success('Scan finished')

    def analyze_data_online(self, data_tuple, receiver=None):
        ''' Used to overwrite data storing function: self.readout.handle_data '''
        raw_data = data_tuple[0]
        self.data.tot_hist.add(raw_data)
        # Only store ToT histograms when scan started
        if self.data.start_data_taking:
            super(TotTuning, self).handle_data(data_tuple, receiver)

    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


if __name__ == '__main__':
    with TotTuning(scan_config=scan_configuration) as tuning:
        tuning.start()
